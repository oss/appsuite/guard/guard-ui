/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * This sends a new email to recipient
 */

const SELECTOR = require('../constants')

module.exports = function (o, subject, data, attachment, options) {
  const userdata = o.user.userdata
  options = options || {}

  // Open compose
  this.wait(2)
  this.click('.inline-toolbar [data-action="io.ox/mail/actions/compose"]')
  this.waitForVisible('~Security')
  this.waitForVisible(SELECTOR.EDITOR)
  this.waitForElement('.io-ox-mail-compose-window.complete')
  this.wait(0.2) // gentle wait for listeners

  // Mark as secure unless specified as non-encrypted
  if (!options.unencrypted) {
    this.click('~Security')
    this.waitForVisible('.dropdown-menu a[data-name="encrypt"]')
    if (options.smime) {
      this.click('Secure')
    } else {
      this.click('Encrypt')
    }
  }
  if (options.expectDraftPrompt) {
    this.waitForElement('#encryptDraft .btn')
    this.click('OK', '#encryptDraft')
    this.waitForInvisible('#encryptDraft')
  }
  if (options.expectPassword) {
    this.waitForVisible('#ogPassword')
    this.fillField('#ogPassword', userdata.password)
    this.click('.btn[data-action="ok"]')
  }

  if (options.attachKey) {
    this.click('~Security')
    this.waitForVisible('.dropdown-menu a[data-name="PubKey"]')
    this.click('Attach my key')
  }

  if (options.sign) {
    this.click('~Security')
    this.waitForVisible('.dropdown-menu a[data-name="sign"]')
    this.click('Sign')
  }

  if (options.inline) {
    this.wait(1)
    this.click('~Security')
    this.waitForVisible('.dropdown-menu a[data-name="pgpInline"]')
    this.click('.dropdown-menu a[data-name="pgpInline"][data-value="true"]')
    this.waitForVisible('.btn[data-action="okx"]')
    this.click('.btn[data-action="okx"]')
    this.waitForInvisible('.modal-backdrop')
  }

  // Insert test data
  this.fillField(SELECTOR.TO_FIELD, userdata.email1)
  this.fillField(SELECTOR.SUBJECT, subject)
  const I = this

  if (options.inline) { // Inline plaintext only
    I.fillField('.plain-text', data)
  } else {
    within({ frame: SELECTOR.EDITOR_IFRAME }, async () => {
      I.fillField(SELECTOR.EMAIL_BODY, data)
    })
  }

  // Attachments if any
  if (attachment) {
    this.attachFile('.composetoolbar input[type="file"]', attachment)
    this.wait(2)
  }

  // Verify key is next to recipient names
  if (!options.unencrypted) {
    this.waitForVisible(SELECTOR.TRUSTED_KEY)
    // Send
    if (options.smime) {
      this.click('Send Secure', '.save')
    } else {
      this.click('Send Encrypted', '.save')
    }
  } else {
    this.click('Send')
  }

  if (options.sign) {
    this.waitForVisible('#ogPassword')
    this.fillField('#ogPassword', userdata.password)
    this.click('.btn[data-action="confirm"]')
  }
  I.waitForInvisible('~Security')
  this.waitForVisible('.inline-toolbar [data-action="io.ox/mail/actions/compose"]')
  I.waitForInvisible('.mail-send-progress', 45)
}
