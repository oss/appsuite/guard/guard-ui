/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * This resets a user's Guard account, referenced by userIndex
 * Changes the URL.  Will need redirect after calling (such as using the login command)
 */

const puppeteer = require('puppeteer-core')
const codecept = require('codeceptjs')

function getServerURL () {
  const config = codecept.config.get()
  const driver = config.helpers.WebDriver || config.helpers.Puppeteer
  const ox = config.helpers.OpenXchange
  const url = ox.serverURL || driver.url
  const pathArray = url.split('/')
  const protocol = pathArray[0]
  const host = pathArray[2]
  return `${protocol}//${host}`
}

async function launch (url) {
  const config = codecept.config.get()
  const chrome = config.helpers.Puppeteer.chrome
  const browser = await puppeteer.launch(chrome)
  const page = await browser.newPage()
  await page.goto(url)

  // Fetches page's content
  await page.content()
  await browser.close()
}

module.exports = async function (users) {
  let launchURL = getServerURL()
  if (launchURL.search('appsuite\\/?$') >= 0) launchURL = launchURL.substring(0, launchURL.search('appsuite\\/?$'))
  if (!/\/$/.test(launchURL)) launchURL += '/'
  launchURL += 'appsuite/'
  const promises = []
  users.forEach(function (userData) {
    const user = userData.userdata
    promises.push(launch(launchURL + 'api/oxguard/demo?action=reset&email=' + user.email1))
  })
  await Promise.all(promises) // wait for all to complete
  return this
}
