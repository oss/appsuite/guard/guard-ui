/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * This sends a new email to recipient
 */

const SELECTOR = require('../constants')

module.exports = function (from, recips, subject, data, pin, options) {
  const userdata = from.user.userdata
  options = options || {}

  // Open compose
  this.wait(2)
  this.click('New email')
  this.waitForVisible(SELECTOR.WINDOW_BUSY)
  this.waitForInvisible(SELECTOR.WINDOW_BUSY)
  this.waitForVisible(SELECTOR.EDITOR)

  // Mark as secure
  this.click('.toggle-encryption')

  if (options.expectPassword) {
    this.waitForVisible('#ogPassword')
    this.fillField('#ogPassword', userdata.password)
  }

  this.waitForVisible(SELECTOR.GUARD_LOCK)

  // Verify HTML
  this.click('~Mail compose actions')
  this.click('HTML')
  this.waitForVisible('.io-ox-mail-compose .editor .tox-edit-area')

  // Insert test data
  const I = this
  let recipLocation = SELECTOR.TO_FIELD
  if (options.type) {
    if (options.type === 'cc') {
      I.click('CC')
      I.waitForVisible('[placeholder="CC"]')
      recipLocation = SELECTOR.CC
    }
    if (options.type === 'bcc') {
      I.click('BCC')
      I.waitForVisible('[placeholder="BCC"]')
      recipLocation = SELECTOR.BCC
    }
  }

  recips.forEach((r, i) => {
    if (i === 0) {
      this.fillField(recipLocation, r.user.userdata.email1)
    } else {
      I.type('\r', 30)
      I.wait(1)
      I.type(r.user.userdata.email1, 30)
      I.wait(1)
    }
  })

  this.fillField(SELECTOR.SUBJECT, subject)

  within({ frame: SELECTOR.EDITOR_IFRAME }, async () => {
    I.fillField(SELECTOR.EMAIL_BODY, data)
  })

  // Verify icon is next to recipient names
  this.waitForVisible(SELECTOR.GUEST_ICON, 10)
  this.moveCursorTo(SELECTOR.GUEST_ICON, 2, 2)
  // this.waitForVisible('.tooltip');
  // this.see('guest', '.tooltip');

  // Send
  this.click('Send')

  // Add guest message
  this.waitForVisible('#message')
  this.see('Recipient Options')
  this.fillField('.og_guest_message', 'Test introduction text')

  this.click('OK')

  if (!pin) { // Additional handling required for pin
    this.waitForVisible('.io-ox-mail-window .leftside')
    I.wait(1)
    I.waitForInvisible('.mail-send-progress', 45)
  }
}
