/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * This sends a new email to recipient
 */

const SELECTOR = require('../constants')

module.exports = async function (o, subject, data, attachment, options) {
  const userdata = o.user.userdata
  options = options || {}

  // Open compose
  this.wait(2)
  this.click('New email')
  this.waitForVisible(SELECTOR.WINDOW_BUSY)
  this.waitForInvisible(SELECTOR.WINDOW_BUSY)
  this.waitForVisible(SELECTOR.EDITOR)

  if (options.smime) {
    this.click('~Mail compose actions')
    this.waitForText('Use S/MIME', 10, '.dropdown.open .dropdown-menu')
    this.click('Use S/MIME', '.dropdown.open .dropdown-menu')
  }

  // Mark as secure unless specified as non-encrypted
  if (!options.unencrypted) {
    this.seeElement('.io-ox-mail-compose-window .toggle-encryption')
    this.click('.io-ox-mail-compose-window .toggle-encryption')
    if (options.expectDraftPrompt) {
      if (options.expectPassword) {
        this.waitForElement('#encrDraftBox')
        this.click('#encrDraftBox')
      }
      this.waitForElement('#encryptDraft .btn')
      this.click('OK', '#encryptDraft')
      this.waitForInvisible('#encryptDraft')
    }
    if (options.expectPassword) {
      this.waitForVisible('#ogPassword')
      this.fillField('#ogPassword', userdata.password)
      this.click('.btn[data-action="confirm"]')
    }
    this.waitForVisible(SELECTOR.GUARD_LOCK)
    this.wait(1) // delay for possible auth
  }

  if (options.attachKey) {
    this.click('~Mail compose actions')
    this.waitForText('Attach my public key', 10, '.dropdown.open .dropdown-menu')
    this.wait(1)
    this.click('Attach my public key', '.dropdown.open .dropdown-menu')
    this.waitForElement('.btn[data-filename="public.asc"]')
  }

  if (options.sign) {
    this.click('~Mail compose actions')
    this.waitForText('Sign email', 10, '.dropdown.open .dropdown-menu')
    this.click('Sign email', '.dropdown.open .dropdown-menu')
  }

  // Verify HTML
  this.click('~Mail compose actions')
  this.click('HTML')
  this.waitForVisible('.io-ox-mail-compose .editor .tox-edit-area')

  if (options.inline) {
    this.wait(1)
    this.waitForVisible(SELECTOR.SECURITY_OPTIONS)
    this.click(SELECTOR.SECURITY_OPTIONS)
    this.waitForVisible('.dropdown.open a[data-name="pgpInline"]')
    this.click('.dropdown.open a[data-name="pgpInline"][data-value="true"]')

    this.waitForVisible('.btn[data-action="ok"]')
    this.waitForText('Saving...', 17, '.inline-yell')
    this.click('.btn[data-action="ok"]')

    this.waitForInvisible('.modal-backdrop')
    this.wait(1) // Occasional race condition with editor redraw
  }
  const I = this
  // Insert test data
  let recipLocation = SELECTOR.TO_FIELD
  I.waitForVisible(SELECTOR.TO_FIELD)
  if (options.type) {
    if (options.type === 'cc') {
      I.click('CC')
      I.waitForVisible('[placeholder="CC"]')
      recipLocation = SELECTOR.CC
    }
    if (options.type === 'bcc') {
      I.click('BCC')
      I.waitForVisible('[placeholder="BCC"]')
      recipLocation = SELECTOR.BCC
    }
  }
  this.fillField(recipLocation, userdata.email1)
  this.fillField(SELECTOR.SUBJECT, subject)

  if (options.inline) { // Inline plaintext only
    I.fillField('.plain-text', data)
  } else {
    within({ frame: SELECTOR.EDITOR_IFRAME }, async () => {
      I.fillField(SELECTOR.EMAIL_BODY, data)
    })
  }

  // Attachments if any
  if (attachment) {
    this.attachFile('.composetoolbar input[type="file"]', attachment)
    this.wait(2)
  }

  // Verify key is next to recipient names
  if (!options.unencrypted) {
    this.waitForVisible(SELECTOR.TRUSTED_KEY)
  }
  if (options.startOnly) {
    return
  }

  if (!options.dontsend) {
    this.click('Send')
    if (options.sign) {
      this.waitForVisible('#ogPassword')
      this.fillField('#ogPassword', userdata.password)
      this.click('.btn[data-action="confirm"]')
    }
    this.waitForVisible('.io-ox-mail-window .leftside')
    return I.waitForInvisible('.mail-send-progress', 45, attachment ? 60 : 10)
  }
}
