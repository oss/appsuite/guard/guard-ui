/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * This simplifies the input of passwords into input fields.
 * There is an array of users defined in the globals. If you want to fill in a password of such a user
 * you can simply use this function.
 * @param subsequent  boolean if this is a subsequent visit to settings, the security dropdown will already be open
 */
const { I } = inject()

module.exports = {

  tree: locate('.io-ox-settings-main .tree-container').as('".io-ox-settings-main .tree-container"'),
  main: locate('.io-ox-settings-main .settings-detail-pane').as('".io-ox-settings-main .settings-detail-pane"'),

  open (folder) {
    I.click('#io-ox-topbar-settings-dropdown-icon button')
    I.waitForVisible('#topbar-settings-dropdown a[data-name="settings-app"]')
    I.click('#topbar-settings-dropdown a[data-name="settings-app"]')
    I.waitForApp()
    if (folder) this.select(folder)
  },

  close () {
    I.waitForVisible('.close-settings')
    I.click('.close-settings')
    I.waitForDetached('.io-ox-settings-main')
  },

  select (folder) {
    I.waitForText(folder, 5, this.tree)
    I.forceClick(locate('.folder-label').withText(folder).inside(this.tree).as(folder))
    // actually instead of using another parameter for an alternative pageTitle
    // or injecting meta data into the DOM just for the rare case that the folder name
    // differs from the page title, we add the two exceptions here
    I.waitForVisible(locate('h1').withText(getPageTile(folder)).inside(this.main))
    I.waitForApp()
    I.waitForElement('.settings-detail-pane .expandable-section.expanded, .settings-detail-pane .settings-section')
  },

  expandSection (section) {
    const locator = ('.settings-detail-pane .expandable-section[data-section="io.ox/guard/settings/' + section + '"]')
    I.waitForElement(locator)
    I.click(locator)
    // wait for expanded section
    const expanded = locator + '[open="open"]'
    I.waitForElement(expanded)
  },

  startHere ({ cap = '', user, page, section } = {}) {
    I.login(`app=io.ox/mail&cap=${cap}&settings=virtual/settings/io.ox/core`, { user })
    I.waitForVisible('.io-ox-settings-main')
    if (page) this.select(page)
    if (section) this.expandSection(section)
  },

  waitForSettingsSave (timeout = 7) {
    I.waitForResponse(response => response.url().includes('api/jslob') && response.request().method() === 'PUT', timeout)
  },

  goToSettings () {
    this.open('Guard')
    I.wait(1)
  }
}

function getPageTile (page) {
  if (page === 'Security') return 'Security & Privacy'
  if (page === 'Download personal data') return 'Download your personal data'
  return page
}
