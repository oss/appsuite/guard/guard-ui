/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * This wipes a users inbox
 *
 */

const SELECTOR = require('../constants')

module.exports = function () {
  this.waitForVisible('.io-ox-mail-window .window-body .classic-toolbar-container')
  // Mark empty inbox (needed for temporary password prompt with correct email)
  this.selectFolder('Inbox')
  this.waitForVisible(SELECTOR.FOLDER_CONTEXT_MENU)
  this.click(SELECTOR.FOLDER_CONTEXT_MENU)
  this.click('.dropdown.open a[data-action="clearfolder"]')
  this.waitForElement('.btn[data-action="delete"]')
  this.click('.btn[data-action="delete"]')
}
