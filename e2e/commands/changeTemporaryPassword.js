/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * Handles change password prompt for first time user
 *
 */

module.exports = function (o, norecovery) {
  this.waitForVisible('#newogpassword')
  this.wait(1)
  this.insertCryptPassword('#newogpassword', o)
  this.insertCryptPassword('#newogpassword2', o)
  if (norecovery) {
    this.see('password for encryption cannot be restored or recovered')
  } else {
    this.clearField('#recoveryemail')
    this.fillField('#recoveryemail', o.user.userdata.email1)
    this.clearField('#verifyemail')
    this.fillField('#verifyemail', o.user.userdata.email1)
  }
  this.click('.btn[data-action="okpass"]')
}
