/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('Smime: Certificate Management')
let filename

Before(async function ({ users }) {
  filename = Math.floor((Math.random() * 100000))
  await users.create().then(async function (user) {
    await user.hasCapability('smime')
    await user.doesntHaveCapability('guard')
    await user.doesntHaveCapability('guard-mail')
    await user.hasConfig('com.openexchange.guard.pki.ejbca.managesUsersCertificates', false)
  })
})

After(async function ({ I, users }) {
  I.cleanupSmimeFiles(filename)
  await users.removeAll()
})

async function upload (I, settings, filename) {
  await settings.goToSettings()
  I.waitForElement('.io-ox-guard-settings')
  I.waitForElement('.guard-start-button')

  // Click the setup button and verify wizard;
  I.click('.guard-start-button')

  I.waitForElement('.modal-title')
  I.waitForElement('#pgppassword')
  I.see('Upload S/MIME Keys')

  I.fillField('#pgppassword', 'secret')
  I.fillField('#guardpassword', 'secret')
  I.fillField('#guardpassword2', 'secret')

  I.waitForElement('#smimeUserCertFileInput')
  await I.executeScript(async function () {
    const { default: $ } = await import(new URL('jquery.js', location.href))
    const input = $('#smimeUserCertFileInput')
    const { default: smimeUploader } = await import('./io.ox/guard/smime/uploader.js')
    const { default: getModel } = await import('./io.ox/guard/core/guardModel.js')
    input.on('change', function () {
      const files = this.files
      if (files.length > 0) {
        smimeUploader.upload(files, $('#pgppassword').val(), $('#guardpassword').val()).done(function (data) {
          getModel().trigger('change')
        })
      }
    })
  })

  I.attachFile('#smimeUserCertFileInput', filename)

  I.wait(1)
}

Scenario('I can upload an smime key', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  // Log in to settings
  await I.login('app=io.ox/mail', o)

  await upload(I, settings, './testFiles/smime.p12')

  I.click('Cancel') // we don't go through the file input dialog

  await settings.expandSection('certificates')

  I.see('Your key')

  I.waitForElement('.keytable')

  I.see('test@encr.us')

  I.see('test.ox') // certifier
}).tag('smime').tag('settings')

Scenario('Smime expired certificate shows as expired', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }
  const userdata = o.user.userdata

  // Log in to settings
  I.login('app=io.ox/mail', o)
  await I.setupSmimeKey(o.user, { expired: true })

  // Next, log in to settings
  await settings.goToSettings()

  await settings.expandSection('certificates')
  I.see('Your key')

  I.waitForElement('.keytable')

  I.see(userdata.email1)

  I.see('Expired', '.subtle-red')

  I.see('test.ox') // certifier
}).tag('smime').tag('settings').tag('expired')

Scenario('Smime upload expired key', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }
  const userdata = o.user.userdata

  // Log in to settings
  I.login('app=io.ox/mail', o)
  await I.saveSmimeKey(o.user, { expired: true }, filename)

  await upload(I, settings, filename + '.p12')

  I.waitForElement('#certExpired')
  I.see('Certificate Expired')

  I.click('Upload')

  I.wait(1)

  I.click('Cancel') // we don't go through the file input dialog

  await settings.expandSection('certificates')

  I.see('Your key')

  I.waitForElement('.keytable')

  I.see(userdata.email1)

  I.see('Expired')

  I.see('test.ox') // certifier
}).tag('smime').tag('settings').tag('expired')
