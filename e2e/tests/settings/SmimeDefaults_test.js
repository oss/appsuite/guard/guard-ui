/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('Settings - Smime Defaults')

const SELECTOR = require('../../constants.js')

Before(async function ({ users }) {
  await users.create().then(async function (user) {
    await user.hasCapability('smime')
    await user.doesntHaveCapability('guard')
  })
})

After(async function ({ users }) {
  await users.removeAll()
})

Scenario('Smime: Default encrypted works', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o)
  await I.setupSmimeKey(o.user)

  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('view')

  I.waitForVisible('.guardDefaults .checkbox')
  I.click('Default to send encrypted when composing email')
  I.wait(1)
  settings.close()
  I.openApp('Mail')
  I.waitForVisible(SELECTOR.TOOL_BAR)
  I.click('New email')
  I.waitForVisible(SELECTOR.WINDOW_BUSY)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  I.waitForVisible(SELECTOR.GUARD_LOCK)
}).tag('smime').tag('defaults')

Scenario('Smime: Default signature works', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o)
  await I.setupSmimeKey(o.user)

  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('view')

  I.waitForVisible('.guardDefaults .checkbox')
  I.click('Default to sign outgoing emails')
  I.wait(1)
  settings.close()
  I.openApp('Mail')
  I.waitForVisible(SELECTOR.TOOL_BAR)
  I.click('New email')
  I.waitForVisible(SELECTOR.WINDOW_BUSY)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  I.waitForVisible(SELECTOR.GUARD_UNLOCK)
  I.click('~Mail compose actions')
  I.waitForElement('[data-name="sign"][aria-checked="true"]')
}).tag('smime').tag('defaults')

Scenario('Smime: Default sign and encrypt works', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o)
  await I.setupSmimeKey(o.user)

  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('view')

  I.waitForVisible('.guardDefaults .checkbox')
  I.click('Default to send encrypted when composing email')
  I.click('Default to sign outgoing emails')
  I.wait(1)
  settings.close()
  I.openApp('Mail')
  I.waitForVisible(SELECTOR.TOOL_BAR)
  I.click('New email')
  I.waitForVisible(SELECTOR.WINDOW_BUSY)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  I.waitForVisible(SELECTOR.GUARD_LOCK)
  I.click('~Mail compose actions')
  I.waitForElement('[data-name="sign"][aria-checked="true"]')
}).tag('smime').tag('defaults')
