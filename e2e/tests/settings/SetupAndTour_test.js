/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

const SELECTOR = require('../../constants.js')

Feature('Settings')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

Scenario('First Setup and tour', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o)

  // Next, log in to settings
  I.login(['app=io.ox/settings', 'folder=virtual/settings/io.ox/guard'], o)

  I.waitForElement('.io-ox-guard-settings')
  I.waitForElement('.guard-start-button')
  I.wait(1) // Let background loading finish
  // Click the setup button and verify wizard;
  I.click('.guard-start-button')
  I.waitForElement(SELECTOR.WIZARD_NEXT)
  I.see('Start Setup')

  // Click Start
  I.click(SELECTOR.WIZARD_NEXT)
  I.waitForElement('.wizard-content .guard-create-key')
  I.see('Please enter a password')

  // Try mismatched password and verify error
  I.insertCryptPassword('#newogpassword', o)
  I.wait(1)
  I.fillField('#newogpassword2', 'mismatch')
  I.wait(1)
  I.see('Passwords not equal')
  I.seeElement(SELECTOR.WIZARD_NEXT + ':disabled')

  // Correct password and verify error gone
  I.insertCryptPassword('#newogpassword2', o)
  I.wait(1)
  // Verify next button no longer disabled
  I.seeElement(SELECTOR.WIZARD_NEXT + ':not(:disabled)')
  I.fillField('input[name="recoverymail"]', 'test@email.com')
  I.wait(1)
  I.click(SELECTOR.WIZARD_NEXT)

  // Verify complete shown, and start tour
  I.waitForElement('#startTourLink')
  I.see('Guided tour')
  I.click(SELECTOR.DONE_BUTTON)

  // Make sure tour is set for non-advanced
  I.executeScript(async function () {
    const { default: $ } = await import('./jquery.js')
    if ($('[name="advanced"]').prop('checked')) { $('[name="advanced"]').click() }
  })

  I.wait(1)
  settings.close()
  I.waitForVisible(SELECTOR.TOPBAR_HELP)
  I.click(SELECTOR.TOPBAR_HELP)
  I.click('a[data-name="GuardTour"]')
  I.waitForVisible(SELECTOR.WINDOW_BUSY)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  I.waitForElement('.contenteditable-editor')
  I.waitForElement('.hotspot')
  I.waitForElement('.wizard-content')
  I.see('Security and privacy')
  I.click(SELECTOR.WIZARD_NEXT)

  // Verify Seucrity Settings opened, and password management illustrated
  I.waitForElement('.io-ox-guard-settings')
  I.waitForElement('.wizard-content')
  I.see('You already set up')
  I.waitForElement(SELECTOR.SPOTLIGHT) // Highlighted area
  I.click(SELECTOR.WIZARD_NEXT)

  // Begin illustrating Defaults
  I.waitForElement('.hotspot')
  I.waitForElement(SELECTOR.SPOTLIGHT)
  I.see('From here')
  I.click(SELECTOR.WIZARD_NEXT)

  // Default signing
  I.waitForElement('.hotspot')
  I.waitForElement(SELECTOR.SPOTLIGHT)
  I.see('you can also sign')
  I.click(SELECTOR.WIZARD_NEXT)

  // Advanced, recipient keys
  I.waitForText('Public Key List')
  I.see('you can manage')
  I.click(SELECTOR.WIZARD_NEXT)

  // Advanced options
  I.waitForText('Has private key')
  I.waitForText('can manage their private keys')
  I.click(SELECTOR.WIZARD_NEXT)

  // Opens up compose again to show unlock
  I.waitForElement(SELECTOR.EDITOR)
  I.waitForElement('.hotspot')
  I.waitForElement('.wizard-content')
  I.see('Enabling and disabling')
  I.click(SELECTOR.WIZARD_NEXT)

  // Show file encryption
  I.waitForElement('.io-ox-files-main .window-sidepanel')
  I.waitForElement('.wizard-title')
  I.see('Encrypt files')
  I.click(SELECTOR.WIZARD_NEXT)

  // Show option to restart tour
  I.waitForElement('[data-name="GuardTour"]')
  I.waitForElement('.hotspot')
  I.see('Restart Guided Tour')
  I.click(SELECTOR.WIZARD_NEXT)

  // Show done
  I.waitForElement(SELECTOR.DONE_BUTTON)
  I.click(SELECTOR.DONE_BUTTON)

  // Done
}).tag('tour').tag('pgp')
