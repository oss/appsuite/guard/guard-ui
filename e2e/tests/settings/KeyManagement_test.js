/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

const SELECTOR = require('../../constants.js')

Feature('Settings - Key Management')

Before(async function ({ users, contexts }) {
  const ctx = await contexts.create()
  // create users in same context
  await Promise.all([
    users.create(users.getRandom(), ctx),
    users.create(users.getRandom(), ctx)
  ])
})

After(async function ({ users }) {
  await users.removeAll()
})

const assert = require('chai').assert

Scenario('View details of your keys', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()

  await settings.expandSection('pgpkeys')

  // Check yourkeys list has at least two keys
  I.waitForVisible('#userkeytable')
  I.waitForVisible('.fingerprint')
  I.seeNumberOfVisibleElements('[data-section="io.ox/guard/settings/pgpkeys"] tr', 2)

  // Check key details
  I.click('.fingerprint')
  I.waitForVisible('.details dl')
  const keyDetail = await I.grabTextFrom('#privKeyDetail .modal-body')
  assert.include(keyDetail, 'Fingerprint', 'Key Detail includes fingerprint')

  // Check able to check signatures
  I.click('.btn[data-action="signatures"]')
  I.waitForVisible('#pkeyDetail')
  I.see('Public Keys Detail', '#pkeyDetail .modal-title')
  I.seeNumberOfVisibleElements('.signatureTable tr', 2)
  I.see('Positive', '[title="Positive certification of a User ID"]')
}).tag('pgp').tag('settings').tag('keys')

Scenario('Private Key Download and Upload', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('pgpkeys')

  // First, download the current private key
  I.waitForVisible('.fingerprint')
  // Get the keyId from table and download
  const keyId = await I.grabAttributeFrom('#userkeytable tr[data-id]', 'data-id')
  const downloadedKey = await I.downloadPrivateKey(keyId, o)

  // OK, delete the key
  I.waitForVisible(SELECTOR.MANAGE_KEYS_BUTTON)
  I.click(SELECTOR.MANAGE_KEYS_BUTTON)
  I.waitForText('Delete')
  I.click('Delete')
  I.waitForVisible('.modal[data-point="io.ox/guard/settings/deletePrivate"]')
  I.click('.btn[data-action="delete"]')
  I.waitForVisible('#deletepass')
  I.insertCryptPassword('#deletepass', o)
  I.click('.btn[data-action="delete"]')

  // Upload dialog should appear now
  I.waitForVisible('.modal[data-point="io.ox/guard/settings/uploadPrivate"]')
  const userdata = o.user.userdata
  // We have to manualy perform the upload here
  await I.executeAsyncScript(async function (downloadedKey, userdata, done) {
    const formData = new FormData()
    formData.append('key', downloadedKey)
    formData.append('keyPassword', userdata.password)
    formData.append('newPassword', userdata.password)
    const { default: ox } = await import('./ox.js')
    const { default: $ } = await import('./jquery.js')
    const url = ox.apiRoot + '/oxguard/keys?action=upload&respondWithJSON=true&session=' + ox.session
    $.ajax({
      url,
      type: 'POST',
      data: formData,
      processData: false, // tell jQuery not to process the data
      contentType: false, // tell jQuery not to set contentType
      success (data) {
        done(data)
      },
      fail (e) {
        done(e)
      }
    })
  }, downloadedKey, userdata)

  I.waitForVisible('.btn-default[data-action="cancel"]')
  // Cancel dialog since uploaded already
  I.click('.btn-default[data-action="cancel"]')

  await I.executeAsyncScript(async function (done) {
    const { refresh } = await import('./io.ox/guard/pgp/userKeys.js')
    refresh()
    done()
  })
  // Confirm key is now there and same
  I.wait(1)
  I.waitForVisible('.fingerprint')
  const checkKeyId = await I.grabAttributeFrom('#userkeytable tr[data-id]', 'data-id')
  assert.equal(checkKeyId[0], keyId[0], 'Key Ids should match')
}).tag('pgp').tag('settings').tag('keys')

Scenario('Revoke Private Key', async function ({ I, users, settings }) {
  const o = {
    user: users[1]
  }

  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('pgpkeys')

  // Revoke the key
  I.waitForVisible('.fingerprint')
  I.waitForVisible(SELECTOR.MANAGE_KEYS_BUTTON)
  I.click(SELECTOR.MANAGE_KEYS_BUTTON)
  I.waitForText('Delete')
  I.click('Delete')
  I.waitForVisible('.modal[data-point="io.ox/guard/settings/deletePrivate"]')
  I.click('.btn[data-action="revoke"]')
  I.waitForVisible('#revokepass')
  I.insertCryptPassword('#revokepass', o)
  I.selectOption('#settings-revokereason', 'KEY_SUPERSEDED')
  I.click('.btn[data-action="revoke"]')

  // Confirm drawn as revoked
  I.waitForVisible('button.revoked')
  I.see('Revoked')
  // Check detail
  I.click('.fingerprint')
  I.waitForVisible('.info-line.stripes-red')
  I.see('Revoked', '.info-line')
}).tag('pgp').tag('settings').tag('keys')

Scenario('I can modify userId in pgp key', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  // This is needed as chrome pops up an alert that 'secret' was in a data breach, disabling the test
  // Can be removed once playwright or chromium are introduced
  o.user.userdata.password = '3n6fEahwLmzbxE'

  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('pgpkeys')
  // First, download the current private key

  I.waitForVisible(SELECTOR.MANAGE_KEYS_BUTTON)
  I.click(SELECTOR.MANAGE_KEYS_BUTTON)
  I.waitForText('Edit IDs')
  I.click('Edit IDs')
  I.waitForVisible('input[name="nameInput"]')
  I.fillField('input[name="nameInput"]', 'Test Name')

  I.fillField('input[name="emailInput"]', 'testemail@somewhere.com')
  I.fillField('#passwordInput', o.user.userdata.password)

  I.click('Add')

  I.waitForVisible(SELECTOR.ALERT_SUCCESS)
  I.see('User ID added', SELECTOR.ALERT_SUCCESS)
  I.waitForInvisible(SELECTOR.ALERT_SUCCESS)
  I.click('.fingerprint')
  I.waitForVisible('.details dl')
  const keyDetail = await I.grabTextFrom('#privKeyDetail .modal-body')

  assert.include(keyDetail, 'Test Name <testemail@somewhere.com>')
}).tag('pgp').tag('settings').tag('keys')

async function uploadRecipKey (I) {
  I.waitForElement('.publicKeyDiv [name="publicKey"]')

  I.executeAsyncScript(async function (done) {
    const { default: $ } = await import('./jquery.js')
    const { default: uploader } = await import('./io.ox/guard/pgp/uploadkeys.js')
    $('[name="publicKey"]').on('change', function () {
      const files = this.files
      if (files.length > 0) {
        uploader.uploadExternalKey(files)
          .always(function () {

          })
      }
    })
    done()
  })
  I.attachFile('.publicKeyDiv [name="publicKey"]', 'testFiles/email-example-com.asc')
  I.wait(1)
  await I.executeAsyncScript(async function (done) {
    const { default: details } = await import('./io.ox/guard/pgp/keyDetails.js')
    details.listPublic({ id: 'settings' }).triggerRefresh()
    done()
  })
}

Scenario('Upload public keys and verify available', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('recipients')

  uploadRecipKey(I)

  I.wait(1)
  // Confirm uploaded, and check key details
  I.seeNumberOfVisibleElements('#keylisttablesettings tr', 1)
  I.see('Test User ', 'td')
  I.click('.oxguard_pubkeylist a[ids="6be6dc8 b2f16762"]')
  I.waitForVisible('#pkeyDetail .modal-title')
  I.see('Public Keys Detail', '.modal-title')
  I.seeNumberOfVisibleElements('#keyDetail', 2)

  I.click('.btn[data-action="signatures"')
  I.waitForVisible('.signatureTable')
  I.seeNumberOfVisibleElements('.signatureTable tr', 2)
  I.see('Positive', '.signatureTable')
  I.click('.modal[data-point="oxguard/pgp/signatureview"] [data-action="cancel"]')
  I.wait(1)

  I.click('#pkeyDetail [data-action="cancel"]')

  settings.close()

  // Start a mail to see if key properly loaded
  I.openApp('Mail')
  I.waitForVisible('.io-ox-mail-window .classic-toolbar-container .classic-toolbar')
  I.startEncryptedEmail(o)
  I.waitForVisible(SELECTOR.GUARD_LOCK)

  // Add uploaded key email address and confirm key found and properly labeled
  I.fillField(SELECTOR.TO_FIELD, 'email@example.com')
  I.fillField('[name="subject"]', 'test')
  I.waitForVisible(SELECTOR.TRUSTED_KEY)
  I.wait(1)
  I.moveCursorTo(SELECTOR.TRUSTED_KEY, 5, 5)
  I.waitForVisible('.tooltip')
  I.see('Uploaded', '.tooltip')

  // Cleanup
  I.click(SELECTOR.SAVE_AND_CLOSE)
  I.waitForElement('.modal')
  I.click('Delete draft')
  I.waitForInvisible('.modal-backdrop')
  I.logout()
}).tag('pgp').tag('settings').tag('keys')

Scenario('I can share public key', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }
  const o2 = {
    user: users[1]
  }

  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('recipients')

  uploadRecipKey(I)
  I.wait(1)

  I.click('.oxguard_pubkeylist a[ids="6be6dc8 b2f16762"]')
  I.waitForVisible('#pkeyDetail .modal-title')
  I.see('Public Keys Detail', '.modal-title')
  I.seeNumberOfVisibleElements('#keyDetail', 2)
  I.click('Share Keys')
  I.wait(1)
  I.click('Close', '#pkeyDetail .modal-footer')
  settings.close()
  await I.logout()

  // Log in as second user to see if they can find the public key shared
  I.login('app=io.ox/mail', o2)

  await I.verifyUserSetup(o2)

  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('recipients')
  I.waitForVisible('.oxguard_pubkeylist a[ids="6be6dc8 b2f16762"]')
  I.click('.oxguard_pubkeylist a[ids="6be6dc8 b2f16762"]')
  I.waitForVisible('#pkeyDetail .modal-title')
  I.see('Public Keys Detail', '.modal-title')
  I.seeNumberOfVisibleElements('#keyDetail', 2)
}).tag('pgp').tag('settings').tag('keys')

Scenario('Public keys marked as Inline default to inline', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('recipients')

  uploadRecipKey(I)
  I.wait(1)

  I.waitForVisible('.oxguard_pubkeylist a[ids="6be6dc8 b2f16762"]')
  I.click('.oxguard_pubkeylist a[ids="6be6dc8 b2f16762"]')
  I.waitForVisible('#pkeyDetail .modal-title')
  I.see('Public Keys Detail', '.modal-title')
  I.seeNumberOfVisibleElements('#keyDetail', 2)
  I.click('Use PGP Inline')
  I.wait(1)
  I.click('Close', '#pkeyDetail .modal-footer')
  settings.close()

  // Start a mail to see if key properly loaded
  I.openApp('Mail')
  I.waitForVisible('.io-ox-mail-window .classic-toolbar-container .classic-toolbar')
  I.startEncryptedEmail(o)
  I.waitForVisible(SELECTOR.GUARD_LOCK)

  // Add uploaded key email address and confirm key found and properly labeled
  I.fillField(SELECTOR.TO_FIELD, 'email@example.com')
  I.fillField('[name="subject"]', 'test')
  I.waitForVisible(SELECTOR.TRUSTED_KEY)

  I.waitForVisible('#htmlWarning')
  I.see('HTML Formatted Email')
  I.click('OK')
  I.waitForVisible('textarea.plain-text')
}).tag('pgp').tag('settings').tag('keys')
