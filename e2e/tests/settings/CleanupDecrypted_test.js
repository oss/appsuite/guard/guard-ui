/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

const SELECTOR = require('../../constants.js')

Feature('Wipe Tests')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

async function loadEmails (I, user, count) {
  const promises = []
  for (let i = 0; i < count; i++) {
    const data = {
      attachments: [{
        content: 'Test content ' + i,
        content_type: 'text/html',
        disp: 'inline'
      }],
      from: [[user.get('display_name'), user.get('primaryEmail')]],
      sendtype: 0,
      subject: 'Test subject ' + i,
      to: [[user.get('display_name'), user.get('primaryEmail')]],
      security: {
        encrypt: true,
        sign: false
      }
    }
    promises.push(I.haveMail(data))
  }
  await Promise.all(promises)
  I.waitForElement(SELECTOR.REFRESH + ' .animate-spin')
  I.waitForDetached(SELECTOR.REFRESH + ' .animate-spin')
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
}

Scenario('I remove decrypted emails when moving between emails', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await I.setupUser(o, true) // need advanced settings
  await I.haveSetting('oxguard//wipe', 'true', { user: o.user, noCache: true })

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  await loadEmails(I, users[0], 2)

  await I.selectEmailNumber(1)

  I.decryptEmail(o)
  I.wait(1)
  // password prompt gone (decyrpted)
  I.waitForInvisible(SELECTOR.GUARD_READ_BUTTON)

  await I.selectEmailNumber(2)
  // Switch to another email
  I.waitForElement(SELECTOR.GUARD_READ_BUTTON)

  await I.selectEmailNumber(1)
  // Come back and verify password prompt returned
  I.waitForElement(SELECTOR.GUARD_READ_BUTTON)
}).tag('pgp').tag('cleanup')

Scenario('I remove decrypted emails when switching apps', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await I.setupUser(o, true) // need advanced settings
  await I.haveSetting('oxguard//wipe', 'true', { user: o.user, noCache: true })

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  await loadEmails(I, users[0], 2)

  await I.selectEmailNumber(1)

  I.decryptEmail(o)
  I.wait(1)
  // password prompt gone (decyrpted)
  I.waitForInvisible(SELECTOR.GUARD_READ_BUTTON)

  await I.openApp('Drive')
  I.waitForVisible('ul.file-list-view.complete')
  await I.openApp('Mail')
  I.waitForVisible('ul.list-view')
  await I.selectEmailNumber(1)
  // Come back and verify password prompt returned
  I.waitForElement(SELECTOR.GUARD_READ_BUTTON)
}).tag('pgp').tag('cleanup')

Scenario('I remove decrypted emails but not within threads', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await I.setupUser(o, true) // need advanced settings
  await I.haveSetting('oxguard//wipe', 'true', { user: o.user, noCache: true })

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)
  I.click(SELECTOR.GRID_OPTIONS)
  I.click('Conversations')
  await loadEmails(I, users[0], 2)
  const subject = 'Test subject 0'

  // setup one thread and one singleton
  await I.selectEmailNumber(1)
  I.waitForVisible(SELECTOR.REPLY)
  I.decryptEmail(o)
  I.verifyDecryptedMail(subject, 'Test content')
  I.click(SELECTOR.REPLY)
  I.auth(o)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  I.waitForVisible(SELECTOR.EDITOR)
  I.waitForVisible(SELECTOR.EDITOR_IFRAME)
  I.wait(1)
  // Change contents to ReplyTest to verify later
  within({ frame: SELECTOR.EDITOR_IFRAME }, async () => {
    I.see('wrote:')
    I.click(SELECTOR.EMAIL_BODY)
    I.fillField(SELECTOR.EMAIL_BODY, 'ReplyTest')
  })

  I.waitForVisible(SELECTOR.TRUSTED_KEY)
  I.click('Send')
  I.waitForVisible('.io-ox-mail-window .leftside')
  I.wait(1)
  I.click(SELECTOR.REFRESH)
  I.waitForVisible('.thread-size .number')
  // OK, test it out now.
  I.selectEmailNumber(1)

  // Still decrypted with reply
  within({ frame: '.thread-view article:nth-of-type(2) iframe' }, () => {
    I.see('Test content')
  })
  // Switch to the reply
  I.wait(1)
  await I.openThreadEmail(1)
  I.waitForElement(SELECTOR.GUARD_READ_BUTTON)
  I.wait(1)
  I.decryptEmail(o)
  const email1 = await I.getThreadSelector(1)
  const email2 = await I.getThreadSelector(2)
  I.waitForElement(email1 + ' iframe')
  within({ frame: email1 + ' iframe' }, () => {
    I.see('ReplyTest')
  })

  // FIrst is still readable (wasn't removed)
  within({ frame: email2 + ' iframe' }, () => {
    I.see('Test content')
  })

  // OK, let's switch to next email
  I.selectEmailNumber(2)
  I.wait(2)

  // Switch back
  I.selectEmailNumber(1)
  // Verify both have been wiped
  I.wait(1)
  I.see('Secure Email', email1)
  await I.openThreadEmail(2)
  I.wait(1)
  I.waitForVisible(SELECTOR.GUARD_READ_BUTTON, email2)
  I.see('Secure Email', email2)
}).tag('pgp').tag('cleanup')
