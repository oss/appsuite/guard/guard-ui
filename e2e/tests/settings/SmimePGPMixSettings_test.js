/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('Smime & PGP: Settings')

const SELECTOR = require('../../constants.js')

Before(async function ({ users }) {
  await users.create().then(async function (user) {
    await user.hasCapability('smime')
  })
})

After(async function ({ users }) {
  await users.removeAll()
})

async function enableSmime () {
  const { I, settings } = inject()
  await settings.expandSection('certificates')
  I.waitForVisible('input[name="smime"]')
  I.click('Enable S/MIME')
}

Scenario('Can view PGP or Smime Private Keys, existing', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)
  // Create pgp and smime keys
  await I.verifyUserSetup(o)
  await I.setupSmimeKey(o.user)

  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('pgpkeys')

  I.waitForVisible('.keytable')
  I.waitForVisible('#userkeytable')
  I.seeNumberOfVisibleElements('#userkeytable tr', 2) // two keys displayed (main and sub)
  I.waitForVisible('.fingerprint') // PGP table has details
  enableSmime()

  // await settings.expandSection('pgpkeys')
  I.waitForVisible('.keytable')
  I.waitForVisible('#userkeytable')
  I.seeNumberOfVisibleElements('#userkeytable tr', 2) // two keys displayed (main and sub)
  I.waitForElement('.fingerprint') // PGP table has details

  await settings.expandSection('certificates')
  I.waitForVisible('#smimekeytable')
  I.seeNumberOfVisibleElements('#smimekeytable tr', 2)
  I.see('Certifier')
}).tag('smime').tag('settings')

Scenario('Can view PGP or Smime Private Keys, smime missing', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)
  // Create pgp and smime keys
  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('pgpkeys')

  I.waitForVisible('.keytable')
  I.waitForVisible('#userkeytable')
  I.seeNumberOfVisibleElements('#userkeytable tr', 2) // two keys displayed (main and sub)
  I.waitForVisible('.fingerprint') // PGP table has details

  await settings.expandSection('certificates')
  I.waitForVisible('#smimekeytable')
  I.see('No keys found.')
}).tag('smime').tag('settings')

async function doResetTest (I, users, settings, smime) {
  const o = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o)
  await I.setupSmimeKey(o.user)
  // Unique password to pgp
  o.user.userdata.password = 'pgpsecret'
  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()

  await enableSmime()

  if (smime) {
    await settings.expandSection('smimePassword')
  } else {
    await settings.expandSection('pgpPassword')
  }

  // Reset the password
  I.see('Reset password')
  I.click('Reset password', smime ? SELECTOR.SMIME_PASSWORD_SECTION : SELECTOR.PGP_PASSWORD_SECTION)

  I.waitForVisible('.modal-footer [data-action="reset"]')
  I.seeTextEquals('Reset Password', '.resetDialog .modal-title') // Header
  I.click('.btn[data-action="reset"]')
  I.waitForInvisible('.modal-footer [data-action="reset"]')

  settings.close()

  // Goto mail and wait for mail to arrive
  I.openApp('Mail')
  I.waitForVisible('.io-ox-mail-window .leftside')
  I.wait(1)
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  // Pull the new temporary password from the email
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  const newPass = await I.grabTextFrom('.bodyBox')
  I.switchTo()

  // Got back to settings and change the password
  await settings.goToSettings()

  if (smime) { // PGP change password should automatically appear
    await settings.expandSection('smimePassword')

    I.waitForVisible('#oldpasssmime')
    I.fillField('#oldpasssmime', newPass)
    I.insertCryptPassword('#newpass1smime', o)
    I.insertCryptPassword('#newpass2smime', o)
    I.click('Change Password', SELECTOR.SMIME_PASSWORD_SECTION)
  } else {
    // Change password dialog will pop up automatically for pgp
    I.waitForVisible('#oldogpassword')
    I.fillField('#oldogpassword', newPass)
    I.insertCryptPassword('#newogpassword', o)
    I.insertCryptPassword('#newogpassword2', o)
    I.click('.btn[data-action="okpass"]')
  }

  // Confirm good change of the temporary password
  I.waitForVisible('.io-ox-alert-success')
  I.seeTextEquals('Password changed successfully.', '.io-ox-alert-success')
};

Scenario('Reset PGP Password and Change when PGP and SMIME enabled', async function ({ I, users, settings }) {
  await doResetTest(I, users, settings, false)
}).tag('smime').tag('settings').tag('reset').tag('password')

Scenario('Reset Smime Password and Change when PGP and SMIME enabled', async function ({ I, users, settings }) {
  await doResetTest(I, users, settings, true)
}).tag('smime').tag('settings').tag('reset').tag('password')

Scenario('Smime and PGP: Default S/Mime encrypted works', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }
  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)
  await I.setupSmimeKey(o.user)
  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()

  enableSmime()

  await settings.expandSection('view')

  I.waitForVisible('.guardDefaults .checkbox')
  I.click('Default to send encrypted when composing email')
  I.wait(1)
  I.click('Default to using S/MIME for signing and encryption')
  settings.close()
  I.openApp('Mail')
  I.waitForVisible(SELECTOR.TOOL_BAR)
  I.click('New email')
  I.waitForVisible(SELECTOR.WINDOW_BUSY)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  I.waitForVisible(SELECTOR.GUARD_LOCK)
  I.click('~Mail compose actions')
  I.waitForElement('[data-name="smime"][aria-checked="true"]')
}).tag('smime').tag('defaults')
