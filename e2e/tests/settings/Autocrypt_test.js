/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

const assert = require('chai').assert

const SELECTOR = require('../../constants.js')

Feature('AutoCrypt Tests')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

function verify (I, o, advanced) {
  I.startEncryptedEmail(o)
  I.waitForVisible(SELECTOR.GUARD_LOCK)
  I.fillField(SELECTOR.TO_FIELD, 'actest@encr.us')
  I.fillField(SELECTOR.SUBJECT, 'test')
  I.waitForVisible(advanced ? SELECTOR.TRUSTED_KEY : SELECTOR.UNTRUSTED_KEY)
  I.click(SELECTOR.SAVE_AND_CLOSE)
  I.waitForVisible('.modal-dialog')
  I.click('Delete draft')
}

Scenario('Autocrypt header recognized - Advanced user', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  await I.haveMail({ folder: 'default0/INBOX', path: 'testFiles/ac.eml' })

  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  I.waitForVisible('.autoCryptSummary')
  I.see('C878615D')
  I.click('.ac-toggle-details a')
  I.see('IDs: Autocrypt Test')
  I.see('C878 615D')
  I.see('4011 35EE')
  I.click('Add')
  I.waitForVisible(SELECTOR.ALERT_SUCCESS)
  I.see('Key imported', SELECTOR.ALERT_SUCCESS)
  I.click(SELECTOR.ALERT_SUCCESS_CLOSE)

  verify(I, o, true)

  I.logout()
}).tag('autocrypt').tag('pgp')

Scenario('Autocrypt signature - Advanced user', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  await I.haveMail({ folder: 'default0/INBOX', path: 'testFiles/signed.eml' })

  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  I.waitForVisible('.autoCryptSummary')
  I.wait(2) // wait for background signature check to finish.  Avoid race
  I.click('Add')
  I.waitForVisible(SELECTOR.ALERT_SUCCESS)
  I.see('Key imported', SELECTOR.ALERT_SUCCESS)
  I.waitForInvisible('.oxguard_info')
  I.waitForVisible(SELECTOR.SIGNATURE_OK) // Should pull again and verify signature
  I.wait(1)
  I.dontSee('Missing public key')

  I.logout()
}).tag('autocrypt').tag('pgp')

Scenario('Autocrypt signature - Sent with email and recognized', async function ({ I, users }) {
  const user1 = {
    user: users[0]
  }
  const tempuser = await users.create()
  const user2 = {
    user: tempuser
  }

  await I.login('app=io.ox/mail', user2)
  await I.verifyUserSetup(user2)
  await I.sendEmail(user1, 'test subject', 'test data', undefined, { unencrypted: true, sign: true })
  I.wait(1)
  I.logout()
  I.waitForVisible('#io-ox-login-username')

  await I.setupUser(user1, true) // need advanced settings

  I.login('app=io.ox/mail', user1)
  await tempuser.remove() // Destroy the first user so that Guard doesn't find the key

  await I.verifyUserSetup(user1)

  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  I.waitForVisible('.autoCryptSummary')
  I.wait(2) // wait for background signature check to finish.  Avoid race
  I.click('Add')
  I.waitForVisible(SELECTOR.ALERT_SUCCESS)
  I.see('Key imported', SELECTOR.ALERT_SUCCESS)
  I.waitForInvisible('.oxguard_info')
  I.waitForVisible(SELECTOR.SIGNATURE_OK) // Should pull again and verify signature
  I.wait(1)
  I.dontSee('Missing public key')

  I.logout()
}).tag('autocrypt').tag('pgp')

Scenario('Autocrypt header recognized - Basic user', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  await I.haveMail({ folder: 'default0/INBOX', path: 'testFiles/ac.eml' })

  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  I.wait(1) // Wait a moment for silent import

  verify(I, o)

  I.logout()
}).tag('autocrypt').tag('pgp')

Scenario('Autocrypt signature- Basic user', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  await I.haveMail({ folder: 'default0/INBOX', path: 'testFiles/signed.eml' })

  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)
  I.waitForVisible('.oxguard_info')
  I.wait(1) // Wait a moment

  I.see('Missing public key')

  I.logout()
}).tag('autocrypt').tag('pgp')

Scenario('Autocrypt keys collected and avail in settings', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }
  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  await I.haveMail({ folder: 'default0/INBOX', path: 'testFiles/ac.eml' })

  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  I.waitForVisible('.autoCryptSummary')
  I.see('C878615D')
  I.click('.ac-toggle-details a')
  I.see('IDs: Autocrypt Test')
  I.see('C878 615D')
  I.see('4011 35EE')
  I.click('Add')
  I.waitForVisible(SELECTOR.ALERT_SUCCESS)
  I.see('Key imported', SELECTOR.ALERT_SUCCESS)
  I.click(SELECTOR.ALERT_SUCCESS_CLOSE)

  await settings.goToSettings()
  await settings.expandSection('recipients')

  I.waitForVisible('.modal-body .publicKeyDiv')
  I.waitForVisible('.oxguard_pubkeylist.emailKey')
  const keyDetail = await I.grabTextFrom('.oxguard_pubkeylist.emailKey')
  assert.include(keyDetail, 'Autocrypt Test <actest@encr.us>')
}).tag('autocrypt').tag('pgp')

Scenario('Autocrypt transfer of keys', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }
  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)
  await I.verifyUserSetup(o)

  await settings.goToSettings()
  await settings.expandSection('advanced')

  I.waitForVisible('.btn[name="transferKeys"]')
  I.click('.btn[name="transferKeys"]')

  I.waitForVisible('#guardpassword')
  I.fillField('#guardpassword', 'secret')

  I.click('Transfer')

  I.waitForVisible('.autocryptpasscode')

  const keyCode = await I.grabTextFrom('.autocryptpasscode')

  I.click('.btn[data-action="copy"]')

  I.click('Done')
  settings.expandSection('pgpkeys')
  // OK, delete the key
  I.waitForVisible('.keytable')
  I.waitForVisible(SELECTOR.MANAGE_KEYS_BUTTON)
  I.click(SELECTOR.MANAGE_KEYS_BUTTON)
  I.waitForText('Delete')
  I.click('Delete')
  I.waitForVisible('.modal[data-point="io.ox/guard/settings/deletePrivate"]')
  I.click('.btn[data-action="delete"]')
  I.waitForVisible('#deletepass')
  I.insertCryptPassword('#deletepass', o)
  I.click('[data-action="delete"]')
  I.waitForVisible('.btn-default[data-action="cancel"]')
  I.click('Cancel')
  I.waitForVisible('#userkeytable')
  I.see('No Keys Found', '#userkeytable')

  await settings.close()

  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  I.waitForVisible('.importKey')
  I.see('AutoCrypt startup found')

  I.click('.importKey')

  I.waitForVisible('#setupcode')
  I.fillField('#setupcode', keyCode)
  // I.click('#setupcode')
  // I.pressKey(['Control Left', 'v', 'Control Left']) // paste

  I.fillField('#pgppassword', 'secret')
  I.wait(1)
  I.fillField('#guardpassword', 'secret')
  I.wait(1)
  I.fillField('#guardpassword2', 'secret')
  I.wait(1)
  I.click('.btn[data-action="add"]')

  I.waitForVisible(SELECTOR.ALERT_SUCCESS)
  I.see('Keys imported successfully', SELECTOR.ALERT_SUCCESS)

  I.click('.io-ox-alert .btn')

  // Confirm keys have been imported and 2 present
  await settings.goToSettings()
  await settings.expandSection('pgpkeys')
  I.waitForVisible('.keytable')
  I.waitForVisible('#userkeytable')
  I.waitForVisible('.fingerprint')
  I.seeNumberOfVisibleElements('[data-section="io.ox/guard/settings/pgpkeys"] tr', 2)
}).tag('autocrypt').tag('pgp')
