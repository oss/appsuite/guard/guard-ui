/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

const SELECTOR = require('../../constants.js')

Feature('Basic and Advanced Settings Tests')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

Scenario('Basic user only sees basic email wording', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)
  I.waitForVisible('.io-ox-mail-window .leftside')
  I.startEncryptedEmail(o)
  I.waitForVisible('.guard-info-line')
  I.waitForVisible(SELECTOR.SECURITY_OPTIONS)
  I.click(SELECTOR.SECURITY_OPTIONS)
  I.see('Sign email')
  I.dontSee('PGP Mime')
  I.dontSee('PGP Inline')
  // TODO: should action really not be available for basic users?
  // I.dontSee('Attach my public key');
}).tag('advanced').tag('pgp')

Scenario('Advanced user sees advanced email wording', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)
  I.waitForVisible('.io-ox-mail-window .leftside')
  I.startEncryptedEmail(o)

  I.waitForVisible('.guard-info-line')
  I.waitForVisible(SELECTOR.SECURITY_OPTIONS)
  I.click(SELECTOR.SECURITY_OPTIONS)

  I.see('Security')
  I.see('Sign email')
  I.see('Attach my public key')

  I.see('PGP Format')
  I.see('Mime')
  I.see('Inline')
}).tag('advanced').tag('pgp')
