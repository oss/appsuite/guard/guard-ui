/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

const SELECTOR = require('../../constants.js')

Feature('Key Creation')

Before(async function ({ users }) {
  await Promise.all([
    users.create(),
    users.create()
  ])
})

After(async function ({ users }) {
  await users.removeAll()
})

async function doSetup (I, o, mobile) {
  I.waitForVisible(mobile ? '.modal-content' : '.wizard-content')
  if (!mobile) {
    I.see('Welcome to Guard', '.wizard-content')
    I.click('Start Setup')
  }
  I.waitForVisible('.password')
  I.waitForVisible('#newogpassword')
  I.insertCryptPassword('#newogpassword', o)
  I.wait(1)
  I.insertCryptPassword('#newogpassword2', o)
  if (mobile) {
    I.click('OK')
  } else {
    I.click('Next')
  }

  if (!mobile) {
    I.waitForVisible('.wizard-title')
    I.see('Guard set up completed', '.wizard-title')
    I.click('Close', '.wizard-footer')
  }
}

Scenario('Create keys from within compose', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o1)

  I.waitForVisible(SELECTOR.TOOL_BAR)

  I.click('New email')
  I.waitForVisible(SELECTOR.WINDOW_BUSY)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  I.waitForVisible(SELECTOR.EDITOR)

  I.click('.toggle-encryption')

  doSetup(I, o1)
  // I.auth(o1)

  I.waitForVisible(SELECTOR.GUARD_LOCK)
}).tag('pgp').tag('mail').tag('keys')

Scenario('Create keys from within compose, mobile', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }

  await I.emulateDevice('iPhone X')
  I.login('app=io.ox/mail', o1)

  I.waitForVisible('.inline-toolbar [data-action="io.ox/mail/actions/compose"]')

  I.click('.inline-toolbar [data-action="io.ox/mail/actions/compose"]')
  I.waitForVisible('~Security')
  I.waitForVisible(SELECTOR.EDITOR)
  I.waitForElement('.io-ox-mail-compose-window.complete')
  I.wait(0.2) // gentle wait for listeners

  I.click('~Security')
  I.waitForVisible('.dropdown-menu a[data-name="encrypt"]')
  I.click('Secure')

  doSetup(I, o1, true)

  I.waitForVisible('svg.bi-lock')
}).tag('pgp').tag('mail').tag('keys').tag('mobile')

Scenario('Create keys from within drive', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  const infostoreFolderID = await I.grabDefaultFolder('infostore')
  const fileId = await I.haveFile(infostoreFolderID, 'testFiles/simpleText.txt', o)
  const id = fileId.folder_id + '.' + fileId.id

  I.login('app=io.ox/files', o)

  // Start to encrypt the file
  I.waitForVisible('.list-view li[data-cid="' + id + '"]')
  I.click('.list-view li[data-cid="' + id + '"]')
  I.wait(1)
  I.click(SELECTOR.FILE_MORE)
  I.click('Encrypt')

  doSetup(I, o)

  I.waitForVisible(SELECTOR.GUARD_FILE_SELECTOR)
  I.see('.txt.pgp', '.extension')
  I.logout()
}).tag('pgp').tag('drive').tag('keys')

Scenario('Key creation in setup', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  // Log in to settings
  I.login(['app=io.ox/settings', 'folder=virtual/settings/io.ox/guard'], o)

  I.waitForElement('.io-ox-guard-settings')
  I.waitForElement('.guard-start-button')
  I.wait(1) // Let background loading finish
  // Click the setup button and verify wizard;
  I.click('.guard-start-button')

  doSetup(I, o)

  // Verify settings now shown
  I.waitForVisible(SELECTOR.PGP_PASSWORD_SECTION)
}).tag('pgp').tag('settings').tag('keys').tag('this')

Scenario('Key creation in setup, mobile', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  await I.emulateDevice('iPhone X')
  // Log in to settings
  I.login(['app=io.ox/settings', 'folder=virtual/settings/io.ox/guard'], o)
  I.wait(2)
  I.waitForVisible('h2.title')
  I.see('Setup')
  I.click('.bi-20.bi-chevron-right')

  I.waitForElement('.io-ox-guard-settings')
  I.waitForVisible('.guard-start-button')
  I.wait(1) // Let background loading finish
  // Click the setup button and verify wizard;
  I.click('.guard-start-button')

  doSetup(I, o, true)

  // Verify settings now shown
  I.waitForVisible(SELECTOR.PGP_PASSWORD_SECTION)
}).tag('pgp').tag('settings').tag('keys').tag('mobile')
