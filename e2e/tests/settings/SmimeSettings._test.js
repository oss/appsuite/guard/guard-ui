/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('Smime: Settings')
const SELECTOR = require('../../constants.js')

Before(async function ({ users }) {
  await users.create().then(async function (user) {
    await user.hasCapability('smime')
    await user.doesntHaveCapability('guard')
    await user.hasConfig('com.openexchange.guard.pki.ejbca.managesUsersCertificates', false)
  })
})

After(async function ({ users }) {
  await users.removeAll()
})

Scenario('Smime settings correct and key avail', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }
  const userdata = o.user.userdata

  // Log in to settings
  I.login('app=io.ox/mail', o)
  await I.setupSmimeKey(o.user)

  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('view')
  I.see('Default to send encrypted when composing email')
  I.see('Default to sign outgoing emails')

  await settings.expandSection('smimePassword')
  I.see('Change Password', SELECTOR.SMIME_PASSWORD_SECTION)

  await settings.expandSection('certificates')
  I.see('Your key')

  I.waitForElement('.keytable')

  I.see(userdata.email1)

  I.see('test.ox') // certifier
}).tag('smime').tag('settings')

Scenario('Smime keys show multiple subject alt email', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }
  const userdata = o.user.userdata

  // Log in to settings
  I.login('app=io.ox/mail', o)
  await I.setupSmimeKey(o.user, { multiple: true })

  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('certificates')
  I.see('Your key')

  I.waitForElement('.keytable')

  const email = userdata.email1
  const email2 = email.substring(0, email.indexOf('@')) + '_test1' + email.substring(email.indexOf('@'))
  I.see(email)
  I.see(email2)

  I.see('test.ox') // certifier
}).tag('smime').tag('settings')

Scenario('Smime change password', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }
  const userdata = o.user.userdata

  // Log in to settings
  I.login('app=io.ox/mail', o)
  await I.setupSmimeKey(o.user)

  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('smimePassword')

  I.see('Change Password')

  I.waitForVisible('#oldpasssmime')

  I.fillField('#oldpasssmime', userdata.password)

  I.fillField('#newpass1smime', userdata.password + '2')

  I.fillField('#newpass2smime', userdata.password)

  I.click('Change Password')

  I.see('Passwords not equal')

  I.fillField('#newpass2smime', userdata.password + '2')

  I.click('Change Password')

  I.waitForVisible('.io-ox-alert')

  I.see('Password changed', '.io-ox-alert')
  settings.expandSection('certificates')
  I.waitForVisible('.keytable')
  // Confirm only 1 key
  I.seeNumberOfVisibleElements('.keytable tr', 2)
}).tag('smime').tag('settings').tag('password')

Scenario('Smime Reset Password and Change', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o)

  await I.setupSmimeKey(o.user)

  await I.logout() // we have to login again so that we test smime login data shows reset avail

  I.login('app=io.ox/mail', o)

  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('smimePassword')
  // Reset the password
  I.see('Reset password')
  I.click('Reset password')
  I.waitForVisible('.modal-footer [data-action="reset"]')
  I.seeTextEquals('Reset Password', '.resetDialog .modal-title') // Header
  I.click('.btn[data-action="reset"]')
  I.waitForInvisible('.modal-footer [data-action="reset"]')

  settings.close()

  // Goto mail and wait for mail to arrive
  I.openApp('Mail')
  I.waitForVisible('.io-ox-mail-window .leftside')
  I.wait(1)
  I.click('#io-ox-refresh-icon')
  I.waitForVisible('.io-ox-mail-window .leftside ul li.unread')
  I.click('.io-ox-mail-window .leftside ul li.unread')

  // Pull the new temporary password from the email
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  const newPass = await I.grabTextFrom('.bodyBox')
  I.switchTo()

  // Got back to settings and change the password
  await settings.goToSettings()
  await settings.expandSection('smimePassword')
  I.waitForVisible('#oldpasssmime')
  I.fillField('#oldpasssmime', newPass)
  I.insertCryptPassword('#newpass1smime', o)
  I.insertCryptPassword('#newpass2smime', o)
  I.click('Change Password')

  // Confirm good change of the temporary password
  I.waitForVisible('.io-ox-alert-success')
  I.seeTextEquals('Password changed successfully.', '.io-ox-alert-success')
}).tag('smime').tag('settings').tag('reset').tag('password').tag('testme')

Scenario('No Smime key recognized in settings', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  // Log in to settings
  await I.login('app=io.ox/mail', o)
  await settings.goToSettings()
  I.waitForElement('.io-ox-guard-settings')
  I.waitForElement('.guard-start-button')

  // Click the setup button and verify wizard;
  I.click('.guard-start-button')

  I.waitForElement('.modal-title')
  I.waitForVisible('.password_prompt')
  I.see('Upload S/MIME Keys')
}).tag('smime').tag('settings')

Scenario('Create keys error from within compose', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o1)
  I.waitForVisible(SELECTOR.TOOL_BAR)

  I.click('New email')
  I.waitForVisible('.window-blocker.io-ox-busy')
  I.waitForInvisible('.window-blocker.io-ox-busy')
  I.waitForVisible('.io-ox-mail-compose textarea.plain-text,.io-ox-mail-compose .contenteditable-editor')

  I.click('.toggle-encryption')

  I.waitForVisible('.io-ox-alert')

  I.see('No S/MIME keys available')

  I.dontSee(SELECTOR.GUARD_LOCK) // unlocked
  I.seeElement(SELECTOR.GUARD_UNLOCK)
}).tag('smime').tag('mail')

function confirm (checked, location) {
  const { I } = inject()
  within(locate('.keytable tr').at(location + 1), () => {
    if (checked) {
      I.seeElement('.subtle-green')
    } else {
      I.dontSee('.subtle_green')
    }
  })
}

Scenario('Select current smime key', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o)
  await I.setupSmimeKey(o.user) // two keys
  await I.setupSmimeKey(o.user)

  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('certificates')

  I.waitForVisible('.keytable')
  // Confirm 2 rows
  I.seeNumberOfVisibleElements('.keytable tr', 4)
  // Second row has check, first row input
  confirm(false, 1)
  confirm(true, 3)
  // Make current
  I.click('[data-toggle="dropdown"]', locate('.keytable tr').at(2))
  I.waitForText('Mark current', '.dropdown.open')
  I.click('.open a[data-name="current"]')
  I.wait(1)
  // Confirm flip of input/check
  confirm(true, 1)
  confirm(false, 3)
}).tag('smime').tag('settings')
