/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('Bad Passwords')

const SELECTOR = require('../../constants')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

function createBadUser () {
  return {
    user: {
      userdata: {
        password: 'failme'
      }
    }
  }
}

async function haveMail (I, user, smime) {
  await I.haveMail({
    attachments: [{
      content: 'Test content ',
      content_type: 'text/html',
      disp: 'inline'
    }],
    from: [[user.get('display_name'), user.get('primaryEmail')]],
    sendtype: 0,
    subject: 'Test subject ',
    to: [[user.get('display_name'), user.get('primaryEmail')]],
    security: {
      encrypt: true,
      sign: false,
      type: smime ? 'smime' : 'pgp'
    }
  })
}

async function doBadTest (I, user, smime) {
  await haveMail(I, user, smime)

  I.waitForElement(SELECTOR.REFRESH_SPIN)
  I.waitForDetached(SELECTOR.REFRESH_SPIN)
  I.click(SELECTOR.REFRESH)

  I.waitForVisible('.list-item.unread')

  I.click('.list-item.unread') // click email

  I.decryptEmail(createBadUser())

  I.waitForVisible('.form-group.has-error')
  I.see('Bad password', '.has-error')
}

async function doLockout (I, user, smime) {
  haveMail(I, user, smime)

  I.waitForVisible(SELECTOR.TOOL_BAR)
  I.waitForElement(SELECTOR.REFRESH_SPIN)
  I.waitForDetached(SELECTOR.REFRESH_SPIN)
  I.click(SELECTOR.REFRESH)

  I.waitForVisible('.list-item.unread')

  I.click('.list-item.unread') // click email

  const baduser = createBadUser()
  I.decryptEmail(baduser)

  I.waitForVisible('.form-group.has-error')
  I.see('Bad password', '.has-error')
  for (let i = 0; i < 6; i++) {
    I.click('.btn[data-action="confirm"]')
    I.wait(1)
    I.decryptEmail(baduser)
    I.waitForVisible('.btn[data-action="confirm"]')
  }

  I.waitForText('locked', '.io-ox-alert-error')
}

Scenario('Bad password tests decrypting email', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const user = o.user

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  await doBadTest(I, user, false)
}).tag('badpasswords').tag('pgp')

Scenario('SMIME Bad password tests decrypting email', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const user = o.user

  I.setupForSmime(o.user)

  I.login('app=io.ox/mail', o)

  await I.setupSmimeKey(o.user)

  await doBadTest(I, user, true)
}).tag('badpasswords').tag('smime')

Scenario('Lockout test', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const user = o.user

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  doLockout(I, user)
}).tag('badpasswords').tag('pgp')

Scenario('SMIME Lockout test', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const user = o.user

  await I.setupForSmime(user)

  I.login('app=io.ox/mail', o)

  await I.setupSmimeKey(user)

  doLockout(I, user, true)
}).tag('badpasswords').tag('smime')

Scenario('Bad password test file', async function ({ I, users }) {
  const o = {
    user: users[0],
    cryptoAction: 'encrypt'
  }

  I.login('app=io.ox/files', o)

  await I.verifyUserSetup(o)

  const infostoreFolderID = await I.grabDefaultFolder('infostore')
  const fileId = await I.haveFile(infostoreFolderID, 'testFiles/simpleText.txt', o)
  const id = fileId.folder_id + '.' + fileId.id
  I.waitForElement(SELECTOR.REFRESH_SPIN)
  I.waitForDetached(SELECTOR.REFRESH_SPIN)
  I.click(SELECTOR.REFRESH)
  // Encrypt the file
  I.waitForVisible('.list-view li[data-cid="' + id + '"]')
  I.doubleClick('.list-view li[data-cid="' + id + '"]')

  I.auth(createBadUser())
  I.waitForVisible('.form-group.has-error')
  I.see('Bad password', '.has-error')
}).tag('badpasswords').tag('drive').tag('pgp')
