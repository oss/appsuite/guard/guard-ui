/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('Settings - Password Management')

const SELECTOR = require('../../constants')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

Scenario('PGP Reset Password and Change', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()

  await settings.expandSection('pgpPassword')

  // Reset the password
  I.see('Reset password')
  I.click('Reset password')
  I.waitForVisible('.modal-footer [data-action="reset"]')
  I.seeTextEquals('Reset Password', '.resetDialog .modal-title') // Header
  I.click('.btn[data-action="reset"]')
  I.waitForInvisible('.modal-footer [data-action="reset"]')

  settings.close()

  // Goto mail and wait for mail to arrive
  I.openApp('Mail')
  I.waitForVisible('.io-ox-mail-window .leftside')
  I.wait(1)
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  // Pull the new temporary password from the email
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  const newPass = await I.grabTextFrom('.bodyBox')
  I.switchTo()

  // Got back to settings and change the password
  await settings.goToSettings()
  I.waitForVisible('#oldogpassword')
  I.fillField('#oldogpassword', newPass)
  I.insertCryptPassword('#newogpassword', o)
  I.insertCryptPassword('#newogpassword2', o)
  I.click('OK')

  // Confirm good change of the temporary password
  I.waitForVisible(SELECTOR.ALERT_SUCCESS)
  I.seeTextEquals('Password changed successfully.', SELECTOR.ALERT_SUCCESS)
}).tag('pgp').tag('reset').tag('settings')
