/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('Remembering Passwords')

const SELECTOR = require('../../constants')
const count = 4 // number of emails

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

async function doTest (I, o, smime) {
  const user = o.user
  for (let i = 0; i < count; i++) {
    await I.haveMail({
      attachments: [{
        content: 'Test content ' + i,
        content_type: 'text/html',
        disp: 'inline'
      }],
      from: [[user.get('display_name'), user.get('primaryEmail')]],
      sendtype: 0,
      subject: 'Test subject ' + i,
      to: [[user.get('display_name'), user.get('primaryEmail')]],
      security: {
        encrypt: true,
        sign: false,
        type: smime ? 'smime' : 'pgp'
      }
    })
  }
  I.waitForElement(SELECTOR.REFRESH_SPIN)
  I.waitForDetached(SELECTOR.REFRESH_SPIN)
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)

  let subj = await I.grabTextFrom('.unread .subject .drag-title')
  I.click('.list-item.unread') // click one email

  I.waitForVisible(SELECTOR.GUARD_READ_BUTTON)
  I.click(SELECTOR.GUARD_READ_BUTTON)
  I.waitForElement(SELECTOR.PASSWORD_PROMPT)
  I.insertCryptPassword(SELECTOR.PASSWORD_PROMPT, o)
  I.selectOption('#duration', 'After 2 hours')
  I.click(SELECTOR.GUARD_PASS_BUTTON)

  I.verifyDecryptedMail(subj[0], subj[0].replace('subject', 'content'), false)

  for (let j = 1; j < count; j++) {
    subj = await I.grabTextFrom('.unread .subject .drag-title')
    I.click('.list-item.unread')
    const subject = Array.isArray(subj) ? subj[0] : subj
    I.verifyDecryptedMail(subject, subject.replace('subject', 'content'), false)
  }
}

Scenario('Decrypt multiple emails with one password', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  await doTest(I, o, false)
}).tag('passwords').tag('pgp')

Scenario('SMIME Decrypt multiple smime emails with one password', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const user = o.user

  await I.setupForSmime(user)

  await I.login('app=io.ox/mail', o)

  await I.setupSmimeKey(user)

  await doTest(I, o, true)
}).tag('passwords').tag('smime')
