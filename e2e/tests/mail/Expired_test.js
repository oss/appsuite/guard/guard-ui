/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('Expired')

const SELECTOR = require('../../constants')

Before(async function ({ users }) {
  await Promise.all([
    users.create(),
    users.create()
  ])
})

After(async function ({ users }) {
  await users.removeAll()
})

async function startEmail (I) {
  I.wait(2)
  I.click('New email')
  I.waitForVisible(SELECTOR.WINDOW_BUSY)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  I.waitForVisible(SELECTOR.EDITOR)

  I.seeElement('.io-ox-mail-compose-window .toggle-encryption')
  I.click('.io-ox-mail-compose-window .toggle-encryption')
}

Scenario('PGP expiring key shows dialog at start.  Can be dismissed', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }

  await o1.user.hasConfig('com.openexchange.guard.keyValidDays', 10)
  await I.setupUser(o1, true)

  I.login('app=io.ox/mail', o1)

  I.waitForVisible(SELECTOR.TOOL_BAR)

  await I.verifyUserSetup(o1) // Verify user has Guard setup

  I.refreshPage()

  I.waitForVisible('.modal-title')
  I.see('PGP Expiration Warning')
  I.click('Dismiss', '.modal-footer')

  I.waitForInvisible('.modal-title')
  I.waitForResponse(response => response.url().includes('api/jslob?action=set') && response.request().method() === 'PUT', 10)
  I.wait(1)
  I.refreshPage()

  I.waitForVisible(SELECTOR.TOOL_BAR)
  I.wait(1)
  I.dontSee('PGP Expiration Warning')
}).tag('pgp').tag('expire')

Scenario('S/Mime expiring key shows dialog at start.  Can be dismissed', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }

  await I.setupForSmime(o1.user)

  await I.login('app=io.ox/mail', o1)

  I.waitForVisible(SELECTOR.TOOL_BAR)

  await await I.setupSmimeKey(o1.user, { expiring: true })
  I.refreshPage()

  I.waitForVisible('.modal-title')
  I.see('S/Mime Expiration Warning')
  I.click('Dismiss', '.modal-footer')

  I.waitForInvisible('.modal-title')
  I.waitForResponse(response => response.url().includes('api/jslob?action=set') && response.request().method() === 'PUT', 10)
  I.wait(1)
  I.refreshPage()

  I.waitForVisible(SELECTOR.TOOL_BAR)
  I.wait(1)
  I.dontSee('Expiration Warning')
}).tag('smime').tag('expire')

// *******************  EMAIL

Scenario('Send PGP mail with expired key causes dialog', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }
  const o2 = {
    user: users[1]
  }

  await o1.user.hasConfig('com.openexchange.guard.keyValidDays', -1)
  await I.setupUser(o1, true)
  await I.setupUser(o2)

  I.login('app=io.ox/mail', o1)

  I.waitForVisible(SELECTOR.TOOL_BAR)

  await I.verifyUserSetup(o1) // Verify user has Guard setup

  I.wait(1) // Key is valid for one second, so wait that time to make sure it expires

  await o1.user.hasConfig('com.openexchange.guard.keyValidDays', 300) // Change so that key created in dialog remains valid
  I.refreshPage() // reload page with expired key

  I.waitForVisible('.modal-title')
  I.see('PGP Expiration Warning')
  I.click('Dismiss', '.modal-footer')

  I.waitForInvisible('.modal-title')

  // Test data
  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  await startEmail(I)

  I.waitForVisible('.wizard-title')
  I.see('Your keys are expired')
  I.click('Start Setup')

  I.waitForVisible('#newogpassword')
  I.wait(1)
  I.insertCryptPassword('#newogpassword', o1)
  I.insertCryptPassword('#newogpassword2', o1)

  I.click('Next')

  I.waitForVisible('#startTourLink')

  I.click('Close', '.wizard-container')

  // Finish email

  I.fillField(SELECTOR.TO_FIELD, o2.user.email1)
  I.fillField(SELECTOR.SUBJECT, subject)

  within({ frame: SELECTOR.EDITOR_IFRAME }, async () => {
    I.fillField(SELECTOR.EMAIL_BODY, data)
  })
  I.waitForVisible(SELECTOR.TRUSTED_KEY)
  I.click('Send')
  // Confirm in sent folder, encryted with new key
  I.selectFolder('Sent')
  I.waitForVisible('.io-ox-mail-window .leftside ul li.list-item', 20)
  I.click('.io-ox-mail-window .leftside ul li.list-item')

  I.waitForVisible(SELECTOR.DETAIL_SUBJECT)
  I.waitForElement(SELECTOR.GUARD_READ_BUTTON)
  I.decryptEmail(o1)
  I.verifyDecryptedMail(subject, data)
}).tag('pgp').tag('expire')

Scenario('Send S/Mime mail with expired key causes dialog', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }
  await o1.user.hasConfig('com.openexchange.capability.guard', false)
  await o1.user.hasConfig('com.openexchange.capability.smime', true)
  await I.setupUser(o1, true)

  I.login('app=io.ox/mail', o1)

  I.waitForVisible(SELECTOR.TOOL_BAR)

  await await I.setupSmimeKey(o1.user, { expired: true })

  I.wait(3) // Key is valid for one second, so wait that time to make sure it expires

  I.refreshPage() // reload page with expired key

  I.waitForVisible('.modal-title')
  I.see('S/Mime Expiration Warning')
  I.click('Dismiss', '.modal-footer')

  I.waitForInvisible('.modal-title')
  // Open compose
  await startEmail(I)

  I.waitForVisible('.io-ox-alert')
  I.see('S/Mime certificate is expired.', '.io-ox-alert')

  I.waitForVisible('svg.bi-unlock') // confirm unlocked
}).tag('smime').tag('expire')

// *********************  SETTINGS

Scenario('PGP Keys show expiring and expired as appropriate', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true) // need advanced settings

  await I.login('app=io.ox/mail', o)

  await o.user.hasConfig('com.openexchange.guard.keyValidDays', -1)
  await I.verifyUserSetup(o)
  I.logout() // Unfortunately, we have to get new session to update config reliably
  await o.user.hasConfig('com.openexchange.guard.keyValidDays', 10)
  await I.login('app=io.ox/mail', o)
  await I.verifyUserSetup(o, true)
  I.logout()
  await o.user.hasConfig('com.openexchange.guard.keyValidDays', 100)
  await I.login('app=io.ox/mail', o)
  await I.verifyUserSetup(o, true)

  // We'll have the warning dialog
  I.waitForVisible('.modal-title')
  I.see('PGP Expiration Warning')
  I.click('Dismiss', '.modal-footer')

  // Next, log in to settings
  await settings.goToSettings()

  await settings.expandSection('pgpkeys')

  // Check yourkeys list has at least two keys
  I.waitForVisible('#userkeytable')
  I.waitForVisible('.fingerprint')
  I.seeNumberOfVisibleElements('[data-section="io.ox/guard/settings/pgpkeys"] tr', 8)

  // Top is marked current with no expiring/expired flags
  I.see('Current', locate('[data-section="io.ox/guard/settings/pgpkeys"] tr').at(2))
  I.dontSee('Expiring', locate('[data-section="io.ox/guard/settings/pgpkeys"] tr').at(2))
  I.dontSee('Expired', locate('[data-section="io.ox/guard/settings/pgpkeys"] tr').at(2))
  // Other keys marked appropriately
  I.see('Expiring', locate('[data-section="io.ox/guard/settings/pgpkeys"] tr').at(4))
  I.see('Expired', locate('[data-section="io.ox/guard/settings/pgpkeys"] tr').at(7))
}).tag('smime').tag('expire').tag('settings')

Scenario('S/Mime Keys show expiring and expired as appropriate', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  await o.user.hasConfig('com.openexchange.capability.guard', false)
  await o.user.hasConfig('com.openexchange.capability.smime', true)
  I.login('app=io.ox/mail', o)
  await I.setupSmimeKey(o.user, { expired: true })
  await I.setupSmimeKey(o.user, { expiring: true })
  await I.setupSmimeKey(o.user)
  I.wait(2) // Let the expired key fully expire
  I.refreshPage()
  // Next, log in to settings
  await settings.goToSettings()
  await settings.expandSection('certificates')

  I.waitForVisible('.keytable')
  // Confirm 2 rows
  I.seeNumberOfVisibleElements('.keytable tr', 6)
  // Make current
  I.see('Expired', locate('.keytable tr').at(2))
  I.see('Expiring', locate('.keytable tr').at(4))
  // Third is marked current, and no other flags seen
  I.see('Current', locate('.keytable tr').at(6))
  I.dontSee('Expired', locate('.keytable tr').at(6))
  I.dontSee('Expiring', locate('.keytable tr').at(6))
}).tag('smime').tag('expire').tag('settings')
