/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

const SELECTOR = require('../../constants')
Feature('Send Encrypted To OX User without Guard-Mail capability')

Before(async function ({ users }) {
  await Promise.all([
    users.create(),
    users.create()
  ])
})

After(async function ({ users, I }) {
  await I.wipeUser(users).catch(e => console.log(e)) // cleanup guest accounts
  await users.removeAll()
})

Scenario('Compose and email to ox user without guard-mail', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }
  const o2 = {
    user: users[1]
  }

  await I.setupUser(o1, true)
  await I.setupUser(o2)

  await o2.user.doesntHaveCapability('guard-mail') // Testing for user without Guard-mail capability
  await o2.user.doesntHaveCapability('smime') // We must make sure not smime either

  // Log in as User 0
  I.login('app=io.ox/mail', o1)

  I.waitForVisible(SELECTOR.TOOL_BAR)

  I.verifyUserSetup(o1) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Compose email to Guest
  I.sendEmail(o2, subject, data)

  I.wait(2)

  I.logout()

  // Login to retrieve guest invite email
  I.login('app=io.ox/mail', o2)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  // Change temporary password
  I.changeTemporaryPassword(o2)

  // Verify decrypted
  I.verifyDecryptedMail(subject, data)

  I.waitForElement('.guardUpsell') // Upsell should be present

  // test Guest reply
  I.click(SELECTOR.REPLY)

  I.waitForVisible(SELECTOR.PASSWORD_PROMPT)
  I.insertCryptPassword(SELECTOR.PASSWORD_PROMPT, o2)
  I.click('.btn[data-action="confirm"]')

  I.waitForVisible(SELECTOR.EDITOR)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  // Check unable to add recipients
  I.waitForInvisible('.recipient-actions')
}).tag('pgp').tag('mail')
