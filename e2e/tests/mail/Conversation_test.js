/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('Conversation between users')

const SELECTOR = require('../../constants')

Before(async function ({ users }) {
  await Promise.all([
    users.create(),
    users.create()
  ])
})

After(async function ({ users }) {
  await users.removeAll()
})

Scenario('Encrypted conversation between two users', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }
  const o2 = {
    user: users[1]
  }

  await I.setupUser(o1, true)
  await I.setupUser(o2, true)

  I.login('app=io.ox/mail', o1)

  I.waitForVisible(SELECTOR.TOOL_BAR)

  I.verifyUserSetup(o1) // Verify user has Guard setup

  // Test data
  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  I.sendEmail(o2, subject, data)

  I.wait(1)

  // Logout and log back in as second user
  I.logout()
  I.login('app=io.ox/mail', o2)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  // Change temporary password
  I.changeTemporaryPassword(o2)

  I.waitForVisible(SELECTOR.DONE) // Wizard shows done
  I.click(SELECTOR.DONE)

  // Verify decrypted
  I.verifyDecryptedMail(subject, data)

  // Reply
  I.click(SELECTOR.REPLY)

  I.auth(o2)

  I.wait(2)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  // Change contents to ReplyTest to verify later
  within({ frame: SELECTOR.EDITOR_IFRAME }, async () => {
    I.see(data, SELECTOR.EMAIL_BODY)
    I.see('wrote:')
    I.click(SELECTOR.EMAIL_BODY)
    I.fillField(SELECTOR.EMAIL_BODY, 'ReplyTest')
  })

  I.waitForVisible(SELECTOR.TRUSTED_KEY)
  // Send
  I.click('Send')
  I.waitForInvisible('.mail-send-progress', 45, 10)
  I.waitForVisible('.io-ox-mail-window .leftside')
  I.wait(2)
  I.logout()

  // Switch back to first user
  I.login('app=io.ox/mail', o1)
  I.waitForVisible(SELECTOR.TOOL_BAR)
  I.waitForVisible(SELECTOR.UNREAD)
  // Conversation view
  I.click(SELECTOR.GRID_OPTIONS)
  I.click('Conversations')
  I.wait(1)

  // Read first
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)
  I.decryptEmail(o1)

  I.verifyDecryptedMail(subject, 'ReplyTest')

  // Check the original is still there in the conversation and decryptable
  I.click('.thread-view article:nth-of-type(2) .toggle-mail-body')
  I.decryptEmail(o1)
  I.wait(1)
  I.switchTo('.thread-view article:nth-of-type(2) iframe')
  I.see(data)
  I.switchTo()
  I.wait(1)

  I.logout()
}).tag('pgp').tag('conversation').tag('mail')
