/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('Send Encrypted To guest user')

const codecept = require('codeceptjs')
const SELECTOR = require('../../constants')
const ox = codecept.config.get().helpers.OpenXchange

Before(async function ({ users, contexts }) {
  await contexts.create().then(async function (context) {
    context.hasCapability('guard')
    context.doesntHaveCapability('guard-pin')
    context.hasCapability('guard-mail')
    await users.create(undefined, context)
    await users.create(undefined, context) // second recipient with guard-mail
    await context.hasConfig('com.openexchange.share.guestHostname', ox.shareDomain)
    await context.hasConfig('com.openexchange.share.staticGuestCapabilities', 'guard, guard-docs, text')
  })

  await contexts.create().then(async function (context) {
    const guestUser = await users.create(undefined, context) // Guest users
    await guestUser.hasAccessCombination('groupware_standard')
    await guestUser.doesntHaveCapability('guard')
    const guestUser2 = await users.create(undefined, context) // Second
    await guestUser2.hasAccessCombination('groupware_standard')
    await guestUser2.doesntHaveCapability('guard')
  })
})

After(async function ({ users, contexts, I }) {
  await I.wipeUser(users).catch(e => console.log(e)) // cleanup guest accounts
  await users.removeAll()
  await contexts.removeAll()
})

Scenario('Compose and email to guest user and reply', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }
  const o2 = {
    user: users[2]
  }

  await I.setupUser(o1, true)
  await I.setupUser(o2)

  await o2.user.doesntHaveCapability('guard') // Non Guard user.  Guest only

  // Log in as User 0
  I.login('app=io.ox/mail', o1)
  I.waitForVisible(SELECTOR.TOOL_BAR)

  I.verifyUserSetup(o1) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Compose email to Guest
  I.sendEmailGuest(o1, [o2], subject, data)

  I.logout()

  // Login to retrieve guest invite email
  I.login('app=io.ox/mail', o2)
  I.waitForVisible('.io-ox-mail-window .leftside')
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)
  I.wait(1)
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  I.see('Test introduction text')
  I.click('a.bodyButton')

  I.wait(2)
  // Opening Guest user tab
  await I.switchToNextTab()

  I.waitForVisible('.io-ox-mail-window', 30)

  I.changeTemporaryPassword(o2)

  // I.waitForVisible('.wizard-footer [data-action="done"]');
  // I.click('.wizard-footer [data-action="done"]');

  I.wait(1)

  // Confirm email decrypted
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  I.see(data)
  I.switchTo()

  // Reply
  I.click('.mail-header-actions .more-dropdown .dropdown-toggle')
  I.click('Reply')
  I.auth(o2)

  I.waitForVisible(SELECTOR.WINDOW_BUSY)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  // Check unable to add recipients

  I.waitForInvisible('.recipient-actions')

  // Change contents to ReplyTest to verify later
  await within({ frame: SELECTOR.EDITOR_IFRAME }, async () => {
    I.see(data, SELECTOR.EMAIL_BODY)
    I.see('wrote:')
    I.click(SELECTOR.EMAIL_BODY)
    I.fillField(SELECTOR.EMAIL_BODY, 'ReplyTest')
  })

  I.waitForVisible(SELECTOR.TRUSTED_KEY)
  // Send
  I.click('Send')

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.wait(3)
  I.waitForInvisible('.mail-send-progress', 45)

  // Cleanup
  // Guest link logout will bring this tab back to the recipient tab logged in
  // Close the other tab and just wipe the cookies.  That will prevent auto-login
  await I.closeOtherTabs()
  I.clearCookie()
  I.refreshPage()
  I.wait(1)

  // Log in as User 0
  I.login('app=io.ox/mail', o1)

  // Verify sender can read the reply
  I.waitForVisible(SELECTOR.TOOL_BAR)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)
  I.decryptEmail(o1)

  I.verifyDecryptedMail(subject, 'ReplyTest')
}).tag('guest').tag('pgp')

Scenario('Compose and email to guest user and reply-all', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }
  const o2 = {
    user: users[1]
  }
  const guest = {
    user: users[2]
  }
  const guest2 = {
    user: users[3]
  }

  await I.setupUser(o1, true)

  await guest.user.doesntHaveCapability('guard') // Non Guard user.  Guest only
  await guest2.user.doesntHaveCapability('guard')

  // Log in as User 0
  I.login('app=io.ox/mail', o1)
  I.waitForVisible(SELECTOR.TOOL_BAR)

  I.verifyUserSetup(o1) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Compose email to Guest
  I.sendEmailGuest(o1, [o2, guest, guest2], subject, data)

  I.logout()

  // Login to retrieve guest invite email
  I.login('app=io.ox/mail', guest)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)
  I.wait(1)
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  I.see('Test introduction text')
  I.click('a.bodyButton')

  I.wait(2)
  // Opening Guest user tab
  await I.switchToNextTab()

  I.waitForVisible('.io-ox-mail-window', 30)

  I.changeTemporaryPassword(guest)

  // I.waitForVisible('.wizard-footer [data-action="done"]');
  // I.click('.wizard-footer [data-action="done"]');

  I.wait(1)

  // Confirm email decrypted
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  I.see(data)
  I.switchTo()

  // Reply-all
  I.click('.mail-header-actions .btn[data-action="io.ox/mail/actions/reply-all"]')
  I.auth(guest)

  I.waitForVisible(SELECTOR.WINDOW_BUSY)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  // Check unable to add recipients

  I.waitForInvisible('.recipient-actions')

  // Change contents to ReplyTest to verify later
  await within({ frame: SELECTOR.EDITOR_IFRAME }, async () => {
    I.see(data, SELECTOR.EMAIL_BODY)
    I.see('wrote:')
    I.click(SELECTOR.EMAIL_BODY)
    I.fillField(SELECTOR.EMAIL_BODY, 'ReplyTest')
  })

  I.waitForVisible(SELECTOR.TRUSTED_KEY)
  // Send
  I.click('Send')

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.wait(3)
  I.waitForInvisible('.mail-send-progress', 45)

  // Cleanup
  // Guest link logout will bring this tab back to the recipient tab logged in
  // Close the other tab and just wipe the cookies.  That will prevent auto-login
  await I.closeOtherTabs()
  I.clearCookie()
  I.refreshPage()
  I.wait(1)

  // Log in as User 0
  I.login('app=io.ox/mail', o1)

  // Verify sender can read the reply
  I.waitForVisible(SELECTOR.TOOL_BAR)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)
  I.decryptEmail(o1)

  I.verifyDecryptedMail(subject, 'ReplyTest')
  I.see(o1.user.userdata.sur_name, '.recipients') // OX users will have name displayed
  I.see(o2.user.userdata.sur_name, '.recipients')
  I.see(guest2.user.userdata.email1, '.recipients') // Guest users will only have email addr
}).tag('guest').tag('pgp')

Scenario('Compose and email to guest user using bcc', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }
  const guest = {
    user: users[2]
  }

  await I.setupUser(o1, true)
  await I.setupUser(guest)

  await guest.user.doesntHaveCapability('guard') // Non Guard user.  Guest only

  // Log in as User 0
  I.login('app=io.ox/mail', o1)

  I.waitForVisible(SELECTOR.TOOL_BAR)

  I.verifyUserSetup(o1) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Compose email to Guest
  I.sendEmailGuest(o1, [guest], subject, data, undefined, { type: 'bcc' })

  I.logout()

  // Login to retrieve guest invite email
  I.login('app=io.ox/mail', guest)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)
  I.wait(1)
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  I.see('Test introduction text')
  I.click('a.bodyButton')

  I.wait(2)
  // Opening Guest user tab
  await I.switchToNextTab()

  I.waitForVisible('.io-ox-mail-window', 30)

  I.changeTemporaryPassword(guest)

  I.wait(1)
  I.dontSee('Blind') // make sure Blind copy not seen
  // Confirm email decrypted
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  I.see(data)
  I.switchTo()
}).tag('guest').tag('pgp')

Scenario('Compose and email to guest user with password email', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }
  const guest = {
    user: users[2]
  }

  await o1.user.hasConfig('com.openexchange.guard.newGuestsRequirePassword', true) // Setup for password email
  await I.setupUser(o1, true)
  await I.setupUser(guest)

  await guest.user.doesntHaveCapability('guard') // Non Guard user.  Guest only

  // Log in as User 0
  I.login('app=io.ox/mail', o1)

  I.waitForVisible(SELECTOR.TOOL_BAR)

  I.verifyUserSetup(o1) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Compose email to Guest
  I.sendEmailGuest(o1, [guest], subject, data, undefined, { type: 'to' })

  I.logout()

  // Login to retrieve guest invite email
  I.login('app=io.ox/mail', guest)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.waitForElement('.list-item.selectable.unread [title="Welcome to Guard"]')
  I.retry(5).click('.list-item.selectable.unread [title="Welcome to Guard"]')
  I.wait(1)
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  I.see('Test introduction text')
  const password = await I.grabTextFrom('td.bodyBox')
  I.switchTo()
  I.waitForElement('.list-item.selectable.unread [title="' + subject + '"]')
  I.retry(5).click('.list-item.selectable.unread [title="' + subject + '"]')
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  I.see('Test introduction text')
  I.click('a.bodyButton')
  I.wait(2)
  // Opening Guest user tab
  await I.switchToNextTab()

  I.waitForVisible('.io-ox-mail-window', 30)
  I.waitForVisible('#oldogpassword')
  I.wait(1)
  I.fillField('#oldogpassword', password)
  I.changeTemporaryPassword(guest)
  I.wait(1)

  // Confirm email decrypted
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  I.see(data)
}).tag('guest').tag('pgp')
