/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('PIN')

const codecept = require('codeceptjs')
const SELECTOR = require('../../constants')
const ox = codecept.config.get().helpers.OpenXchange

Before(async function ({ users, contexts }) {
  await contexts.create().then(async function (context) {
    context.hasCapability('guard')
    context.hasCapability('guard-pin')
    context.hasCapability('guard-mail')
    context.hasConfig('com.openexchange.share.staticGuestCapabilities', 'guard, guard-docs, text')
    await context.hasConfig('com.openexchange.share.guestHostname', ox.shareDomain)
    await context.hasConfig('com.openexchange.guard.pinEnabled', true)
    await users.create(undefined, context)
  })

  await contexts.create().then(async function (context) {
    context.doesntHaveCapability('guard')
    context.hasCapability('guard-pin')
    await users.create(undefined, context) // Two users
  })
})

After(async function ({ users, contexts, I }) {
  await I.wipeUser(users).catch(e => console.log(e)) // cleanup guest accounts
  await users.removeAll().catch(e => console.log(e))
  await contexts.removeAll().catch(e => console.log(e))
})

Scenario('Compose and email with PIN to guest user', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }
  const o2 = {
    user: users[1]
  }

  await I.setupUser(o1, true)
  await I.setupUser(o2)

  await o2.user.doesntHaveCapability('guard') // Non Guard user.  Guest only

  // Log in as User 0
  I.login('app=io.ox/mail', o1)

  I.waitForVisible(SELECTOR.TOOL_BAR)

  I.verifyUserSetup(o1) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Compose email to Guest
  I.sendEmailGuest(o1, [o2], subject, data, true)

  I.waitForVisible('.pin')
  const pin = await I.grabTextFrom('.pin')

  I.click('Use PIN')
  I.waitForVisible('.io-ox-mail-window .leftside')
  I.wait(1)
  I.waitForInvisible('.mail-send-progress', 45)

  I.selectFolder('Sent')
  I.waitForVisible('.io-ox-mail-window .leftside ul li.list-item', 20)
  I.click('.io-ox-mail-window .leftside ul li.list-item')

  I.waitForVisible(SELECTOR.DETAIL_SUBJECT)
  I.waitForVisible(SELECTOR.MAIL_MORE_MENU)
  I.click(SELECTOR.MAIL_MORE_MENU)
  I.dontSee('PIN')
  I.click(SELECTOR.MAIL_MORE_MENU)
  I.waitForElement(SELECTOR.GUARD_READ_BUTTON)
  I.decryptEmail(o1)
  I.verifyDecryptedMail(subject, data)

  I.click(SELECTOR.MAIL_MORE_MENU)
  I.see('Check assigned PIN')
  I.click('Check assigned PIN')

  I.see(pin, '.modal-body')
  I.click('OK')

  I.logout()

  // Login to retrieve guest invite email
  I.login('app=io.ox/mail', o2)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  I.see('Test introduction text')
  I.wait(1)
  I.click('a.bodyButton')

  I.wait(1)
  // Opening Guest user tab
  await I.switchToNextTab()

  I.waitForVisible('.io-ox-mail-window .leftside', 30)

  I.changeTemporaryPassword(o2)
  I.waitForVisible('.modal-body')
  I.waitForText('The pin is missing', 5, '.has-error')

  I.fillField('#pinInput', pin)
  I.click('.btn[data-action="okpass"]')
  I.wait(3)

  // Confirm email decrypted
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  I.see(data)
  I.switchTo()
}).tag('pin').tag('pgp')
