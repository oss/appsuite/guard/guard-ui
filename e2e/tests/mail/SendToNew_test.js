/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('Send Encrypted To New Guard User')

const SELECTOR = require('../../constants')

Before(async function ({ users }) {
  await Promise.all([
    users.create(),
    users.create()
  ])
})

After(async function ({ users }) {
  await users.removeAll()
})

async function ComposeRecieve (I, users, norecovery) {
  const o1 = {
    user: users[0]
  }
  const o2 = {
    user: users[1]
  }

  await I.setupUser(o1, true)
  await I.setupUser(o2)

  I.login('app=io.ox/mail', o1)

  I.waitForVisible(SELECTOR.TOOL_BAR)

  I.verifyUserSetup(o1) // Verify user has Guard setup

  // Test data
  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  I.sendEmail(o2, subject, data)

  I.wait(2)

  // Logout and log back in as second user
  I.logout()
  I.login('app=io.ox/mail', o2)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  // Change temporary password
  I.changeTemporaryPassword(o2, norecovery)

  I.waitForVisible(SELECTOR.DONE) // Wizard shows done
  I.click(SELECTOR.DONE)

  // Verify decrypted
  I.verifyDecryptedMail(subject, data)
}

Scenario('Desktop: Compose and receive encrypted email, database composition space', async function ({ I, users }) {
  await users[0].hasConfig('com.openexchange.mail.compose.mailstorage.enabled', false)
  await ComposeRecieve(I, users)
}).tag('pgp').tag('mail')

Scenario('Desktop: Compose and receive encrypted email, default composition space', async function ({ I, users }) {
  await ComposeRecieve(I, users)
}).tag('pgp').tag('mail')

Scenario('Desktop: Compose and receive encrypted email, no recovery', async function ({ I, users, settings }) {
  await users[0].hasCapability('guard-norecovery')
  await users[1].hasCapability('guard-norecovery')
  await ComposeRecieve(I, users, true)
  await settings.goToSettings()
  await settings.expandSection('pgpPassword')
  I.dontSee('Reset password')
}).tag('pgp').tag('mail')

// Testing sending in mobile views
async function sendMobile (I, users, device, landscape) {
  const o1 = {
    user: users[0]
  }
  const o2 = {
    user: users[1]
  }

  await I.setupUser(o1, true)
  await I.setupUser(o2)

  await I.emulateDevice(device, landscape)

  I.login('app=io.ox/mail', o1)

  I.waitForVisible('.inline-toolbar [data-action="io.ox/mail/actions/compose"]')

  I.verifyUserSetup(o1) // Verify user has Guard setup

  // Test data
  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  I.sendMobileEmail(o2, subject, data)

  I.wait(2)

  // Logout and log back in as second user
  I.logout()
  I.login('app=io.ox/mail', o2)

  I.waitForVisible('.list-item.unread')
  I.tap('.unread .list-item-content')

  // Change temporary password
  I.changeTemporaryPassword(o2)

  I.waitForVisible('.io-ox-alert')
  I.wait(1)
  // Verify decrypted
  I.verifyDecryptedMobileMail(subject, data)
}

// Test with Android in landscape
Scenario('Mobile: Android (Landscape) - Compose and receive encrypted email', async function ({ I, users }) {
  await sendMobile(I, users, 'Pixel 2', true)
}).tag('pgp').tag('mail').tag('mobile')

// Test with large iphone size
Scenario('Mobile: Iphone X - Compose and receive encrypted email', async function ({ I, users }) {
  await sendMobile(I, users, 'iPhone X')
}).tag('pgp').tag('mail').tag('mobile')
