/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('Smime: Send Emails')

Before(async function ({ users }) {
  await users.create().then(async function (user) {
    await user.hasCapability('smime')
    await user.doesntHaveCapability('guard')
  })
})

After(async function ({ users }) {
  await users.removeAll()
})

async function sendMail (I, user, sign, unencrypted, expectPassword, expectDraftPrompt) {
  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  I.sendEmail(user, subject, data, undefined, { smime_only: true, sign, unencrypted, expectPassword, expectDraftPrompt })

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.wait(1)
  I.click('#io-ox-refresh-icon')
  I.waitForVisible('.io-ox-mail-window .leftside ul li.unread')
  I.click('.io-ox-mail-window .leftside ul li.unread')
  I.waitForVisible('.io-ox-mail-window .mail-detail-pane .subject')
  I.see(subject)

  // Decrypt
  if (!unencrypted) {
    I.decryptEmail(user)

    I.verifyDecryptedMail(subject, data, sign)
  }
}

Scenario('Certificate with multiple emails in SAN recognized', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  I.login('app=io.ox/mail', o)
  await I.setupSmimeKey(o.user, { multiple: true })
  I.waitForVisible('.io-ox-mail-window .window-body')
  // Generate one of the sub keys
  const email = o.user.userdata.email1
  const email2 = email.substring(0, email.indexOf('@')) + '_test1' + email.substring(email.indexOf('@'))

  I.sendEmail({ user: { userdata: { email1: email2 } } }, 'Test multiple san', 'test data', undefined, { smime_only: true, startOnly: true })
}).tag('smime')

Scenario('Compose and receive Smime encrypted email', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  // Log in as User 0
  I.login('app=io.ox/mail', o)
  I.waitForVisible('.io-ox-mail-window .window-body')

  await I.setupSmimeKey(o.user)
  await sendMail(I, o)

  // OK, done
  I.logout()
}).tag('smime')

Scenario('Compose and receive Smime encrypted email, draft prompt', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await I.haveSetting('oxguard//encryptDraft', undefined, { user: o.user, noCache: true })

  // Log in as User 0
  I.login('app=io.ox/mail', o)
  I.waitForVisible('.io-ox-mail-window .window-body')

  await I.setupSmimeKey(o.user)
  await sendMail(I, o, false, false, false, true)

  // OK, done
  I.logout()
}).tag('smime')

Scenario('Compose and receive Smime encrypted email, Encrypted draft', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await I.haveSetting('oxguard//encryptDraft', true, { user: o.user, noCache: true })
  // Log in as User 0
  I.login('app=io.ox/mail', o)
  I.waitForVisible('.io-ox-mail-window .window-body')

  await I.setupSmimeKey(o.user)
  await sendMail(I, o, false, false, true)

  // OK, done
  I.logout()
}).tag('smime')

Scenario('Compose and receive Smime encrypted email, Encrypted draft with draft prompt', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await I.haveSetting('oxguard//encryptDraft', undefined, { user: o.user, noCache: true })
  // Log in as User 0
  I.login('app=io.ox/mail', o)
  I.waitForVisible('.io-ox-mail-window .window-body')

  await I.setupSmimeKey(o.user)
  await sendMail(I, o, false, false, true, true)

  // OK, done
  I.logout()
}).tag('smime')

Scenario('Compose and receive Smime signed only email', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  // Log in as User 0
  I.login('app=io.ox/mail', o)
  I.waitForVisible('.io-ox-mail-window .window-body')

  await I.setupSmimeKey(o.user)

  await sendMail(I, o, true, true)

  I.waitForVisible('.guard_signed')

  // OK, done
  I.logout()
}).tag('smime').tag('sign')

// Should be same behaviour as not encrypted draft
Scenario('Compose and receive Smime signed only email, Encrypted draft', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await I.haveSetting('oxguard//encryptDraft', true, { user: o.user, noCache: true })
  // Log in as User 0
  I.login('app=io.ox/mail', o)
  I.waitForVisible('.io-ox-mail-window .window-body')

  await I.setupSmimeKey(o.user)

  await sendMail(I, o, true, true)

  I.waitForVisible('.guard_signed')

  // OK, done
  I.logout()
}).tag('smime').tag('sign')

Scenario('Compose and receive Smime encrypted and signed email', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  // Log in as User 0
  I.login('app=io.ox/mail', o)
  I.waitForVisible('.io-ox-mail-window .window-body')

  await I.setupSmimeKey(o.user)

  await sendMail(I, o, true)

  I.waitForVisible('.guard_signed')
  // OK, done
}).tag('smime').tag('sign')

Scenario('Compose and receive Smime encrypted and signed email, Encrypted draft', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await I.haveSetting('oxguard//encryptDraft', true, { user: o.user, noCache: true })
  // Log in as User 0
  I.login('app=io.ox/mail', o)
  I.waitForVisible('.io-ox-mail-window .window-body')

  await I.setupSmimeKey(o.user)

  await sendMail(I, o, true, false, true)

  I.waitForVisible('.guard_signed')
  // OK, done
}).tag('smime').tag('sign')

// Testing sending in mobile views
async function sendMobile (I, users, device, landscape) {
  const o1 = {
    user: users[0]
  }

  await I.emulateDevice(device, landscape)

  I.login('app=io.ox/mail', o1)
  I.waitForVisible('.inline-toolbar [data-action="io.ox/mail/actions/compose"]')

  await I.setupSmimeKey(o1.user)

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  await I.sendMobileEmail(o1, subject, data, undefined, { smime: true })

  I.wait(2)

  I.waitForVisible('.list-item.unread')
  I.tap('.unread .list-item-content')

  // Decrypt
  I.decryptEmail(o1)

  // Verify decrypted
  I.verifyDecryptedMobileMail(subject, data)
}

// Test with Android in landscape
Scenario('Smime: Mobile: Android (Landscape) - Compose and receive encrypted email', async function ({ I, users }) {
  await sendMobile(I, users, 'Pixel 2', true)
}).tag('smime').tag('mobile').tag('testme')

// Test with large iphone size
Scenario('Smime: Mobile: Iphone X - Compose and receive encrypted email', async function ({ I, users }) {
  await sendMobile(I, users, 'iPhone X')
}).tag('smime').tag('mobile')
