/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('Smime with PGP')

const SELECTOR = require('../../constants.js')

Before(async function ({ users }) {
  await users.create().then(async function (user) {
    await user.hasCapability('smime')
  })
})

After(async function ({ users }) {
  await users.removeAll()
})

async function createTestEmail (I, user, smime) {
  await I.haveMail({
    attachments: [{
      content: 'Test content ',
      content_type: 'text/html',
      disp: 'inline'
    }],
    from: [[user.get('display_name'), user.get('primaryEmail')]],
    sendtype: 0,
    subject: 'Test subject ',
    to: [[user.get('display_name'), user.get('primaryEmail')]],
    security: {
      encrypt: true,
      sign: false,
      type: smime ? 'smime' : 'pgp'
    }
  })
}

async function doReplyTest (I, users, type, decryptFirst, smime) {
  const o = {
    user: users[0]
  }

  await I.haveSetting('oxguard//smime', true, { user: o.user, noCache: true })

  // Log in as User 0
  I.login('app=io.ox/mail', o)
  I.waitForVisible('.io-ox-mail-window .window-body')

  await I.verifyUserSetup(o) // Verify user has Guard setup
  await I.setupSmimeKey(o.user)
  await createTestEmail(I, o.user, smime)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  if (decryptFirst) {
    I.decryptEmail(o)
  }

  if (type === 'reply') {
    I.waitForVisible(SELECTOR.REPLY)
    I.wait(1)
    I.click(SELECTOR.REPLY)
  }
  if (type === 'forward') {
    I.waitForVisible(SELECTOR.FORWARD)
    I.wait(1)
    I.click(SELECTOR.FORWARD)
  }

  I.auth(o)

  I.wait(1)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  I.waitForVisible('.guard-info-line.encrypted')
  if (smime) {
    I.waitForText('will be encrypted with S/MIME')
  } else {
    I.waitForText('will be encrypted with PGP')
  }
}

Scenario('Reply is of proper type, smime', async function ({ I, users }) {
  await doReplyTest(I, users, 'reply', true, true)
}).tag('smime').tag('both')

Scenario('Reply is of proper type, pgp', async function ({ I, users }) {
  await doReplyTest(I, users, 'reply', true, false)
}).tag('pgp').tag('both')

Scenario('Reply is of proper type, smime, without decryption', async function ({ I, users }) {
  await doReplyTest(I, users, 'reply', false, true)
}).tag('smime').tag('both')

Scenario('Reply is of proper type, pgp, without decryption', async function ({ I, users }) {
  await doReplyTest(I, users, 'reply', false, false)
}).tag('pgp').tag('both')

Scenario('Forward is of proper type, smime', async function ({ I, users }) {
  await doReplyTest(I, users, 'forward', true, true)
}).tag('smime').tag('both')

Scenario.skip('Forward is of proper type, pgp', async function ({ I, users }) {
  await doReplyTest(I, users, 'forward', true, false)
}).tag('pgp').tag('both')

Scenario('Able to select between smime and pgp', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await I.haveSetting('oxguard//smime', true, { user: o.user, noCache: true })

  // Log in as User 0
  I.login('app=io.ox/mail', o)
  I.waitForVisible('.io-ox-mail-window .window-body')

  await I.verifyUserSetup(o) // Verify user has Guard setup
  await I.setupSmimeKey(o.user)

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  I.sendEmail(o, subject, data, undefined, { smime: true, dontsend: true })
  I.waitForVisible(SELECTOR.TRUSTED_KEY)
  I.moveCursorTo(SELECTOR.TRUSTED_KEY, 5, 5)
  I.waitForVisible('.tooltip')
  I.see('S/MIME', '.tooltip')

  I.click('~Mail compose actions')
  I.waitForText('Use PGP', 10, '.dropdown.open .dropdown-menu')
  I.click('Use PGP')

  I.waitForVisible(SELECTOR.TRUSTED_KEY)
  I.moveCursorTo(SELECTOR.TRUSTED_KEY, 5, 5)
  I.waitForVisible('.tooltip')
  I.see('System key', '.tooltip')
}).tag('smime').tag('pgp').tag('both')
