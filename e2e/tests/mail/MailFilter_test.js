/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

const SELECTOR = require('../../constants')

Feature('Mail Filter')
///  As of now, MailFilter not supported in 3.0

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

async function createFilterRule (I, name, condition, comparison, action) {
  I.login('app=io.ox/settings&folder=virtual/settings/io.ox/mailfilter')

  I.waitForVisible('.settings-detail-pane .io-ox-mailfilter-settings h1')
  I.see('Mail Filter Rules')

  I.see('There is no rule defined')

  // create a test rule and check the inintial display
  I.click('Add new rule')
  I.see('Create new rule')
  I.see('This rule applies to all messages. Please add a condition to restrict this rule to specific messages.')

  I.fillField('rulename', name)

  // add condition
  if (condition) {
    I.click('Add condition')
    I.click(condition)
  }

  // add action
  I.click('Add action')
  I.waitForText(action)
  I.click(action)

  I.click('Save')
  I.waitForInvisible('.modal-dialog')
  I.waitForVisible('.io-ox-mailfilter-settings .list-group')
}

Scenario.skip('MailFilter: Signature check', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }

  await I.setupUser(o1, true)

  await createFilterRule(I, 'Signature Check', 'PGP signature', '', 'Set color flag')

  I.dontSee('no rule defined')
  I.see('Signature Check')

  I.openApp('Mail')

  I.verifyUserSetup(o1) // Verify user has Guard setup

  // Test data
  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  I.sendEmail(o1, subject, data, undefined, { unencrypted: true, sign: true })

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  I.waitForVisible('.guard_signed')
  I.waitForVisible('.flag-picker .btn.flag_1')

  I.logout()
}).tag('mailfilter')

Scenario.skip('MailFilter: Encrypt action', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }

  await I.setupUser(o1, true)

  await createFilterRule(I, 'Encrypt Action', undefined, '', 'Encrypt the email')

  I.dontSee('no rule defined')
  I.see('Encrypt Action')

  I.openApp('Mail')

  I.verifyUserSetup(o1) // Verify user has Guard setup

  // Test data
  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  I.sendEmail(o1, subject, data, undefined, { unencrypted: true, sign: true })

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  // Decrypt
  I.decryptEmail(o1)

  I.verifyDecryptedMail(subject, data, true)

  I.logout()
}).tag('mailfilter')
