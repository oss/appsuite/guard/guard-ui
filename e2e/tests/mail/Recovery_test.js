/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('Recovery test')

const SELECTOR = require('../../constants')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

Scenario('Recovery draft with sign and PGP Inline set', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const testSubject = 'Recovery test'
  const testData = 'Test data'

  await I.setupUser(o, true) // Advanced settings

  await I.haveSetting({ 'io.ox/mail': { 'didYouKnow/saveOnCloseDontShowAgain': true } })
  await I.haveSetting('io.ox/mail//autoSaveAfter', 7000, true, { user: o.user, noCache: true })

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  I.waitForVisible('.io-ox-mail-window .leftside')
  await I.sendEmail(o, testSubject, testData, undefined, { sign: true, inline: true, startOnly: true })
  I.logout()

  I.login('app=io.ox/mail')

  I.waitForVisible('.io-ox-mail-window .leftside')

  I.waitForElement('.folder-node[title*="Drafts"]')
  I.waitForElement(locate('.folder-node[title*="Drafts"] .folder-counter').withText('1'))
  I.click(locate('.folder-node[title*="Drafts"] .folder-counter').withText('1'))
  I.waitForText(testSubject, 5, '~Messages')
  I.click(testSubject, '~Messages')
  I.waitForElement('.mail-detail')
  I.wait(3)
  I.waitForVisible(SELECTOR.EDIT_DRAFT_LINK)
  I.click(SELECTOR.EDIT_DRAFT_LINK)
  I.wait(1)
  // I.auth(o)

  I.waitForVisible('.plain-text')
  I.wait(1)
  I.seeInField('.plain-text', testData)

  I.waitForVisible(SELECTOR.SECURITY_OPTIONS)
  I.click(SELECTOR.SECURITY_OPTIONS)

  I.waitForVisible('.security-options a[data-name="sign"][aria-checked="true"]')
  I.waitForVisible('.security-options a[data-name="pgpInline"][data-value="true"][aria-checked="true"]')
}).tag('recovery').tag('pgp')

Scenario('Recovery draft with sign only', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const testSubject = 'Recovery test'
  const testData = 'Test data'

  await I.setupUser(o, true) // Advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)
  I.waitForVisible('.io-ox-mail-window .leftside')
  await I.sendEmail(o, testSubject, testData, undefined, { sign: true, unencrypted: true, startOnly: true })

  I.logout()

  I.login('app=io.ox/mail')

  I.waitForVisible('.io-ox-mail-window .leftside')

  I.waitForElement('.folder-node[title*="Drafts"]')
  I.waitForElement(locate('.folder-node[title*="Drafts"] .folder-counter').withText('1'))
  I.click(locate('.folder-node[title*="Drafts"] .folder-counter').withText('1'))
  I.waitForText(testSubject, 5, '~Messages')
  I.click(testSubject, '~Messages')
  I.waitForElement('.mail-detail')
  I.waitForVisible(SELECTOR.EDIT_DRAFT_LINK)
  I.wait(1) // race with event registration
  I.click(SELECTOR.EDIT_DRAFT_LINK)

  I.wait(1)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  I.waitForVisible(SELECTOR.EDITOR)
  I.waitForVisible(SELECTOR.MAIL_OPTIONS)
  I.click(SELECTOR.MAIL_OPTIONS)
  I.waitForVisible('.open a[data-name="sign"][aria-checked="true"]')
  I.switchTo('iframe')
  I.see(testData)
  I.switchTo()
}).tag('recovery').tag('pgp')
