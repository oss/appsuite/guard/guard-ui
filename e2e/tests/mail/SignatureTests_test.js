/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('Test signatures')

const SELECTOR = require('../../constants')

Before(async function ({ users }) {
  await Promise.all([
    users.create(),
    users.create()
  ])
})

After(async function ({ users }) {
  await users.removeAll()
})

Scenario('Send signed non-encrypted email', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }
  const o2 = {
    user: users[1]
  }

  await I.setupUser(o1, true)

  I.login('app=io.ox/mail', o1)

  I.waitForVisible(SELECTOR.TOOL_BAR)

  I.verifyUserSetup(o1) // Verify user has Guard setup

  // Test data
  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  I.sendEmail(o2, subject, data, undefined, { unencrypted: true, sign: true })

  I.wait(2)

  // Logout and log back in as second user
  I.logout()
  I.login('app=io.ox/mail', o2)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  I.waitForVisible('.guard_signed') // Verify PGP icon
  I.dontSeeElement('.oxguard_icon_fa_error')
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  I.see(data) // Check data in body
}).tag('pgp').tag('mail').tag('sign')

Scenario('Send signed and encrypted email', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }
  const o2 = {
    user: users[1]
  }

  await I.setupUser(o1, true)

  I.login('app=io.ox/mail', o1)

  I.waitForVisible(SELECTOR.TOOL_BAR)

  I.verifyUserSetup(o1) // Verify user has Guard setup

  // Test data
  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  I.sendEmail(o2, subject, data, undefined, { sign: true })

  I.wait(2)

  // Logout and log back in as second user
  I.logout()
  I.login('app=io.ox/mail', o2)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  // Change temporary password
  I.changeTemporaryPassword(o2)

  I.waitForVisible(SELECTOR.DONE) // Wizard shows done
  I.click(SELECTOR.DONE)

  // Verify decrypted
  I.verifyDecryptedMail(subject, data, true)

  // OK, done
  I.logout()
}).tag('pgp').tag('mail').tag('sign')

Scenario('Send signed and encrypted inline email', async function ({ I, users }) {
  const o1 = {
    user: users[0]
  }
  const o2 = {
    user: users[1]
  }

  await I.setupUser(o1, true)

  I.login('app=io.ox/mail', o1)

  I.waitForVisible(SELECTOR.TOOL_BAR)

  I.verifyUserSetup(o1) // Verify user has Guard setup

  // Test data
  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  I.sendEmail(o2, subject, data, undefined, { sign: true, inline: true })

  I.wait(2)

  // Logout and log back in as second user
  I.logout()
  I.login('app=io.ox/mail', o2)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  // Change temporary password
  I.changeTemporaryPassword(o2)

  I.waitForVisible(SELECTOR.DONE) // Wizard shows done
  I.click(SELECTOR.DONE)

  // Verify decrypted
  I.verifyDecryptedMail(subject, data, true)

  // OK, done
  I.logout()
}).tag('pgp').tag('mail').tag('sign')
