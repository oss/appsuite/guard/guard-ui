/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('Draft email tests')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

const SELECTOR = require('../../constants')

// Create a simple test email
async function createTestEmail (I, o, opt, attachment) {
  if (!opt) opt = {}
  opt.dontsend = true
  await I.sendEmail(o, 'Test subject', 'Test data', attachment, opt)
}

// Check that everything restored properly
async function checkRestored (I, userdata, decryptFirst, opt) {
  I.waitForVisible('.io-ox-mail-window .window-body')
  I.wait(1)
  I.waitForText('Drafts')
  const selector = locate('li.folder.selectable').withText('Drafts').last()
  I.click(selector)
  I.wait(1)
  I.click('.list-item.selectable[data-index="0"]')
  if (decryptFirst && !(opt.smime || opt.smime_only)) {
    await I.decryptEmail(userdata)
    I.waitForVisible('.oxguard_icon_fa')
  }

  I.wait(1) // Necessary due to race populating view
  I.click(SELECTOR.EDIT_DRAFT_LINK)
  if (opt.expectDecryptPassword) {
    I.auth(userdata)
  }

  I.wait(1)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  I.see('will be encrypted')
  if (opt.both) {
    if (opt.smime) {
      I.see('will be encrypted')
    } else {
      I.see('will be encrypted with PGP')
    }
  }

  if (opt.sign) {
    I.click('~Mail compose actions')
    I.waitForText('Sign email', 10, '.dropdown.open .dropdown-menu')
    I.waitForElement('[data-name="sign"][aria-checked="true"]')
    I.click('~Mail compose actions')
  }
  if (opt.inline) {
    I.click(SELECTOR.SECURITY_OPTIONS)
    I.waitForVisible('.dropdown.open a[data-name="pgpInline"]')
    I.waitForElement('[data-name="pgpInline"][aria-checked="true"]')
  }
}

// Setup user
async function setupUser (I, o, type, encryptDraft) {
  // Log in as User 0
  await I.haveSetting('io.ox/mail//autoSaveAfter', 7000, true, { user: o.user, noCache: true })
  if (type === 'both') {
    await I.haveSetting('oxguard//smime', true, { user: o.user, noCache: true })
  }
  if (type === 'both' || type === 'smime') {
    await o.user.hasCapability('smime')
  }
  if (encryptDraft) {
    await I.haveSetting('oxguard//encryptDraft', true, { user: o.user, noCache: true })
  }
  I.login('app=io.ox/mail', o)
  I.waitForVisible('.io-ox-mail-window .window-body')
  if (type !== 'smime') {
    I.verifyUserSetup(o) // Verify user has Guard setup for pgp
  }

  if (type === 'smime') {
    await o.user.doesntHaveCapability('guard')
  }
  if (type === 'both' || type === 'smime') {
    await I.setupSmimeKey(o.user)
  }
}

async function testRefresh (I, o, opt) {
  await createTestEmail(I, o, opt)
  I.waitForText('Saving...', 17, '.inline-yell')
  I.refreshPage()
  await checkRestored(I, o, false, opt)
}

async function testSaved (I, o, opt) {
  await createTestEmail(I, o, opt)
  I.click(SELECTOR.CLOSE_MAIL)
  I.waitForVisible('.btn[data-action="savedraft"]')
  I.click('Save draft')
  I.wait(1)
  await checkRestored(I, o, false, opt)
}

Scenario('Draft without saved password - refresh', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await setupUser(I, o, 'pgp')
  const opt = {
    sign: true
  }
  await testRefresh(I, o, opt)
}).tag('pgp')

Scenario('Encrypted Draft without saved password - refresh', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await setupUser(I, o, 'pgp', true)
  const opt = {
    sign: true,
    expectPassword: true,
    expectDecryptPassword: false
  }
  await testRefresh(I, o, opt)
}).tag('pgp')

Scenario('Draft without saved password, smime only, - refresh', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await setupUser(I, o, 'smime')
  const opt = {
    sign: true,
    smime_only: true
  }
  await testRefresh(I, o, opt)
}).tag('smime')

Scenario('Encrypted Draft without saved password, smime only, - refresh', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await setupUser(I, o, 'smime', true)
  const opt = {
    sign: true,
    smime_only: true,
    expectPassword: true,
    expectDecryptPassword: false
  }
  await testRefresh(I, o, opt)
}).tag('smime')

Scenario('Draft without saved password, smime with both enabled, - refresh', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await setupUser(I, o, 'both')
  const opt = {
    sign: true,
    smime: true
  }
  await testRefresh(I, o, opt)
}).tag('smime').tag('both')

Scenario('Encrypted Draft without saved password, smime with both enabled, - refresh', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await setupUser(I, o, 'both', true)
  const opt = {
    sign: true,
    smime: true,
    expectPassword: true,
    expectDecryptPassword: false
  }
  await testRefresh(I, o, opt)
}).tag('smime').tag('both')

Scenario('Draft without saved password, pgp with both enabled, - refresh', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await setupUser(I, o, 'both')
  const opt = {
    sign: true
  }
  await testRefresh(I, o, opt)
}).tag('pgp')

Scenario('Draft without saved password, smime with both enabled, close then edit', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await setupUser(I, o, 'both')
  const opt = {
    sign: true,
    smime: true
  }
  await testSaved(I, o, opt)
}).tag('smime').tag('both')

Scenario('Encrypted Draft without saved password, smime with both enabled, close then edit', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await setupUser(I, o, 'both', true)
  const opt = {
    sign: true,
    smime: true,
    expectPassword: true,
    expectDecryptPassword: true
  }
  await testSaved(I, o, opt)
}).tag('smime').tag('both')

Scenario('Draft without saved password, pgp with both enabled, close then edit', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await I.setupUser(o, true) // setup advanced
  await setupUser(I, o, 'both')
  const opt = {
    sign: true,
    inline: true
  }
  await testSaved(I, o, opt)
}).tag('pgp').tag('both')

Scenario('Encrypted Draft with saved password, smime with both enabled, close then edit', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await setupUser(I, o, 'both', true)
  const opt = {
    sign: true,
    smime: true,
    expectPassword: false,
    expectDecryptPassword: false
  }
  await I.savePassword(o, 'smime')
  await testSaved(I, o, opt)
}).tag('smime').tag('both')
