/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />

Feature('Send Encrypted To Self')

const SELECTOR = require('../../constants')

Before(async function ({ users }) {
  await users.create()

  /*
  await new Promise(function (resolve, reject) {
    let fileData = ''
    for (let i = 0; i < 2500000; i++) {
      fileData += 'test '
    }
    fs.writeFile(largeFilename, fileData, () => {
      resolve()
    })
  })
  */
})

After(async function ({ users }) {
  await users.removeAll()
  // fs.unlinkSync(largeFilename)
})

async function sendMail (I, subject, data, o, attachment, options) {
  // Open compose
  I.sendEmail(o, subject, data, attachment, options)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)
  I.waitForVisible(SELECTOR.DETAIL_SUBJECT)
  I.see(subject)

  // Decrypt
  I.decryptEmail(o)

  I.verifyDecryptedMail(subject, data)
}

Scenario('Compose and receive encrypted email using to', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true)

  // Log in as User 0
  await I.login('app=io.ox/mail', o)

  I.waitForVisible('.io-ox-mail-window .window-body')

  I.verifyUserSetup(o) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  await sendMail(I, subject, data, o)

  // OK, done
  I.logout()
}).tag('pgp').tag('mail')

Scenario('Compose and receive encrypted email with encrypted drafts', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await I.haveSetting('oxguard//encryptDraft', true, { user: o.user, noCache: true })
  await I.setupUser(o, true)

  // Log in as User 0
  await I.login('app=io.ox/mail', o)

  I.waitForVisible('.io-ox-mail-window .window-body')

  I.verifyUserSetup(o) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  await sendMail(I, subject, data, o, undefined, { expectPassword: true })

  // OK, done
  I.logout()
}).tag('pgp').tag('mail')

Scenario('Compose and receive encrypted email using bcc', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true)

  // Log in as User 0
  I.login('app=io.ox/mail', o)

  I.waitForVisible('.io-ox-mail-window .window-body')

  I.verifyUserSetup(o) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  await sendMail(I, subject, data, o, undefined, { type: 'bcc' })

  I.dontSee('Blind') // make sure Blind copy not seen
  // OK, done
  I.logout()
}).tag('pgp').tag('mail')

Scenario('Compose and receive encrypted email using cc', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true)

  // Log in as User 0
  I.login('app=io.ox/mail', o)

  I.waitForVisible('.io-ox-mail-window .window-body')

  I.verifyUserSetup(o) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  await sendMail(I, subject, data, o, undefined, { type: 'cc' })

  // OK, done
  I.logout()
}).tag('pgp').tag('mail')

Scenario.skip('Test umlauts, compose and receive encrypted email', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true)

  // Log in as User 0
  I.login('app=io.ox/mail', o)

  I.waitForVisible('.io-ox-mail-window .window-body')

  I.verifyUserSetup(o) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Üben Subject ' + random
  const data = 'die Prüfung ' + random

  await sendMail(I, subject, data, o)

  // OK, done
  I.logout()
}).tag('pgp').tag('mail')

Scenario('Compose and receive encrypted pgp-inline email', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true)

  // Log in as User 0
  I.login('app=io.ox/mail', o)

  I.waitForVisible('.io-ox-mail-window .window-body')

  I.verifyUserSetup(o) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  I.sendEmail(o, subject, data, undefined, { inline: true })

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)
  I.waitForVisible(SELECTOR.DETAIL_SUBJECT)
  I.see(subject)

  // Decrypt
  I.decryptEmail(o)

  I.verifyDecryptedMail(subject, data)

  // OK, done
  I.logout()
}).tag('pgp').tag('mail')
