/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/// <reference path="../../steps.d.ts" />
const SELECTOR = require('../../constants')

Feature('Office: Create encrypted documents')

// const SELECTOR = require('../../constants')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

Scenario('Office: Create an encrypted text document', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true)
  await I.haveSetting('io.ox/office//isCheckSpellingPermanently', false, o)
  await o.user.hasCapability('guard-docs')

  I.login('app=io.ox/files', o)

  await I.verifyUserSetup(o)
  I.wait(1)
  I.waitForVisible(SELECTOR.NEW_BUTTON)
  I.click(SELECTOR.NEW_BUTTON)
  I.waitForVisible('.smart-dropdown-container [data-name="text-newblank"]')
  I.click('.smart-dropdown-container [data-name="text-newblank"]')

  I.wait(3)

  // Switch to office tab and add test text
  I.switchToNextTab()
  I.waitForVisible('.button[data-value="format"]', 60)
  I.waitForVisible('.page.formatted-content.user-select-text.edit-mode')
  I.wait(1)
  I.type('test', 10)
  // I.addToTextDocument('test')
  I.wait(1)
  // Save as encrypted file
  I.click('.button[data-value="file"]')
  I.waitForVisible('.group[data-key="view/saveas/menu"]')
  I.wait(1)
  I.click('.group[data-key="view/saveas/menu"]')
  I.click('Save as (encrypted)')
  I.waitForVisible('#save-as-filename')
  I.wait(1)
  I.fillField('#save-as-filename', 'testTextEncr')
  I.click('Save')

  // Enter Guard password
  I.auth(o)

  // Office closes and re-opens.  Wait
  I.wait(1)
  I.waitForVisible('.page.formatted-content.user-select-text', 20)
  I.waitForText('test', '.page.formatted-content.user-select-text')

  // Close the office tab, find file in drive and click
  I.closeCurrentTab()
  I.waitForVisible('.visible-selection')
  I.waitForVisible('.apptitle[aria-label="Refresh"]')
  I.click('.apptitle[aria-label="Refresh"]')
  I.wait(2)
  I.see('.docx.pgp', '.extension')
  I.doubleClick(SELECTOR.GUARD_FILE_SELECTOR_DOCX)

  // Lets check we can read it
  I.auth(o)

  I.wait(2)
  I.switchToNextTab()

  I.waitForVisible('.page.formatted-content.user-select-text', 20)
  I.waitForVisible('.button[data-value="format"]', 60)
  I.waitForVisible('.page.formatted-content.user-select-text')
  I.wait(1)
  I.waitForText('test', 10, '.page.formatted-content.user-select-text')

  // Closeup
  I.closeCurrentTab()
  I.wait(2)
  I.logout()
}).tag('office').tag('pgp')
