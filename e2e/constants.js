/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

const SELECTOR = {
  // Core
  TOPBAR_HELP: '#io-ox-topbar-help-dropdown-icon > .btn.dropdown-toggle',
  ALERT_SUCCESS: '.io-ox-alert-success',
  ALERT_SUCCESS_CLOSE: '.io-ox-alert-success [data-action="close"]',
  DIALOG_CANCEL: '.btn[data-action="cancel"]',

  WINDOW_BUSY: '.window-blocker.io-ox-busy',

  UNREAD: '.io-ox-mail-window .leftside ul li.unread',
  REFRESH: '#io-ox-refresh-icon',
  REFRESH_SPIN: '#io-ox-refresh-icon .animate-spin',

  FOLDER_CONTEXT_MENU: '.selected .contextmenu-control',
  FOLDER_CONTEXT_MENU_MARK_READ: '.dropdown.open a[data-action="markfolderread"]',

  GRID_OPTIONS: '.grid-options button[data-toggle="dropdown"]',

  FILE_MORE: '.dropdown-toggle[data-action="more"]',

  DONE_BUTTON: '.btn-primary[data-action="done"]',
  DONE: '.btn[data-action="done"]',

  APP_LAUNCHER: '.btn.dropdown-toggle.launcher-btn',
  LAUNCHER_DROPDOWN: '.launcher-dropdown',

  // Mail
  TOOL_BAR: '.io-ox-mail-window .window-body .rightside .classic-toolbar',
  DETAIL_SUBJECT: '.io-ox-mail-window .mail-detail-pane .subject',
  DETAIL_VIEW: '.inline-toolbar [data-action="io.ox/mail/attachment/actions/view"]',
  DETAIL_SAVE: '.inline-toolbar [data-action="io.ox/mail/attachment/actions/save"]',
  REPLY: '.classic-toolbar button[data-action="io.ox/mail/actions/reply"]',
  FORWARD: '.classic-toolbar button[data-action="io.ox/mail/actions/forward"]',
  MAIL_MORE_MENU: '.thread-view .mail-header-actions [data-action="more"]',
  EDIT_DRAFT_LINK: '.btn[data-action="io.ox/mail/actions/edit"]',
  GUARD_READ_BUTTON: '.btn.oxguard_passbutton',

  CLOSE_MAIL: '.btn[data-action="close"]',

  // Drive
  VIEWER_CLOSE: '.classic-toolbar [data-action="io.ox/core/viewer/actions/toolbar/close"]',
  VIEWER_IMAGE: '.swiper-slide-active .viewer-displayer-item.viewer-displayer-image',
  VIEWER_WHITE_BACKGROUND: '.white-page.plain-text',
  VIEWER_FILENAME: '.filename',
  VERSION_TOGGLE: '.viewer-fileversions.sidebar-panel .panel-toggle-btn',
  TEXT_EDIT: 'button[data-action="io.ox/files/actions/editor"]',
  TEXT_EDITOR_CONTENT: 'textarea.content.form-control',
  DRIVE_INVITE: 'button[data-action="io.ox/files/actions/share"]',
  MOBILE_VIEW: 'button[data-action="io.ox/files/actions/viewer"]',
  GUARD_ENCRYPT: '.dropdown-menu [data-action="oxguard/encrypt"]',
  GUARD_DECRYPT: '.dropdown-menu [data-action="oxguard/remencrypt"]',
  NEW_BUTTON: '.window-sidepanel .btn[data-toggle="dropdown"]',
  DOWNLOAD_BUTTON: 'button[data-action="io.ox/files/actions/download"]',

  GUARD_FILE_SELECTOR: locate('li').withDescendant(locate('.extension').withText('.txt.pgp')),
  GUARD_FILE_SELECTOR_DOCX: locate('li').withDescendant(locate('.extension').withText('.docx.pgp')),

  // WIZARD
  WIZARD_NEXT: '.btn.btn-primary[data-action="next"]',
  SPOTLIGHT: '.wizard-overlay.wizard-spotlight.abs',

  // Mail compose
  GUARD_LOCK: 'a.toggle-encryption .bi-lock',
  GUARD_UNLOCK: 'a.toggle-encryption .bi-unlock',
  TRUSTED_KEY: '.oxguard_token.trusted_key .bi-key',
  UNTRUSTED_KEY: '.oxguard_token.untrusted_key .bi-key',
  GUEST_ICON: '.bi-person.key',
  SECURITY_OPTIONS: '.security-options [data-toggle="dropdown"]',
  MAIL_OPTIONS: '~Mail compose actions',

  EDITOR: '.io-ox-mail-compose textarea.plain-text,.io-ox-mail-compose .contenteditable-editor',
  EDITOR_IFRAME: '.io-ox-mail-compose-window .editor iframe',
  EMAIL_BODY: '.mce-content-body',

  SAVE_AND_CLOSE: '.io-ox-mail-compose-window button[aria-label="Save and close"]',
  TO_FIELD: '.io-ox-mail-compose div[data-extension-id="to"] input.tt-input',
  SUBJECT: '.io-ox-mail-compose [name="subject"]',
  CC: '.io-ox-mail-compose div[data-extension-id="cc"] input.tt-input',
  BCC: '.io-ox-mail-compose div[data-extension-id="bcc"] input.tt-input',

  // GUARD ICONS
  SIGNATURE_OK: '.bi-pencil-square.guard_signed',

  // GUARD
  PASSWORD_PROMPT: 'input.form-control.password_prompt',
  GUARD_PASS_BUTTON: '.btn[data-action="confirm"]',

  // Guard Settings
  MANAGE_KEYS_BUTTON: '.dropdown [aria-label="Manage keys"]',
  PGP_PASSWORD_SECTION: '[data-section="io.ox/guard/settings/pgpPassword"]',
  SMIME_PASSWORD_SECTION: '[data-section="io.ox/guard/settings/smimePassword"]'
}

module.exports = SELECTOR
