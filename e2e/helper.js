/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

const Helper = require('@open-xchange/codecept-helper').helper
const output = require('codeceptjs').output
const codecept = require('codeceptjs')
const config = codecept.config.get()
const assert = require('chai').assert
const fs = require('node:fs')

class GuardHelper extends Helper {
  amDisabled (filter) {
    if (!Array.isArray(filter)) {
      filter = [filter]
    }
    let disabled = false
    const ox = config.helpers.OpenXchange
    if (ox.skipTests) {
      const list = ox.skipTests.toLowerCase()
      filter.forEach(d => {
        if (list.indexOf(d.toLowerCase()) > -1) {
          disabled = true
          output.log('Disabling test. Skip items: ' + d)
        }
      })
    }
    return disabled
  }

  async setupForSmime (user, keepGuard) {
    assert(user) // Check user exists
    await user.hasCapability('smime')
    if (!keepGuard) await user.doesntHaveCapability('guard')
  }

  async setupSmimeKey (user, opt) {
    const { page } = this.helpers.Puppeteer

    assert(user) // Check user exists

    const result = await page.evaluate(async function (user, opt, done) {
      const { default: $ } = await import(new URL('jquery.js', location.href))
      const { default: ox } = await import(new URL('ox.js', location.href))
      const def = $.Deferred()
      Promise.all([import('./io.ox/guard/core/og_http.js'), import('./io.ox/guard/util.js')]).then(function ([{ default: og_http }, { default: api }]) {
        const json = {
        }
        try {
          let params = ''
          if (opt) {
            if (opt.multiple) params += '&multiple=true'
            if (opt.expired) params += '&expiration=' + (new Date().getTime() - 100000)
            if (opt.expiring) params += '&expiration=' + (new Date().getTime() + 87400000)
          }
          og_http.simplePost(ox.apiRoot + '/oxguard/smime?action=test' + params, '', json)
            .done(function (data) {
              if (data.error) done(data.error_desc)
              if (data.data) {
                api.hasSetupDone(true) // Load the certificates
                def.resolve(true)
              } else {
                def.resolve('Failed to setup smime')
              }
            })
            .fail(function (e) {
              def.resolve('fail post')
            })
        } catch (e) {
          def.resolve(e.message)
        }
      })
      return def
    }, user, opt)

    assert(result === true, result)
  }

  async saveSmimeKey (user, opt, filename) {
    const { page } = this.helpers.Puppeteer

    assert(user) // Check user exists

    const result = await page.evaluate(async function (user, opt, done) {
      const { default: $ } = await import(new URL('jquery.js', location.href))
      const { default: ox } = await import(new URL('ox.js', location.href))
      const def = $.Deferred()
      Promise.all([import('./io.ox/guard/core/og_http.js'), import('./io.ox/guard/util.js')]).then(function ([{ default: og_http }, { default: api }]) {
        const json = {
        }
        try {
          let params = ''
          if (opt && opt.multiple) params += '&multiple=true'
          if (opt && opt.expired) params += '&expiration=' + (new Date().getTime() - 100000)
          params += '&createOnly=true' // Not storing key, just creating and returning data
          og_http.simplePost(ox.apiRoot + '/oxguard/smime?action=test' + params, '', json)
            .done(function (data) {
              if (data.error) done(data.error_desc)
              if (data.data) {
                api.hasSetupDone() // Load the certificates
                def.resolve(data)
              } else {
                def.resolve('Failed to setup smime')
              }
            })
            .fail(function (e) {
              def.resolve('fail post')
            })
        } catch (e) {
          def.resolve(e.message)
        }
      })
      return def
    }, user, opt)

    assert(result.data && result.private, result)
    fs.writeFileSync(filename + '.p12', result.private)
    fs.writeFileSync(filename + '.p7', result.data)
  }

  async cleanupSmimeFiles (filename) {
    if (fs.existsSync(filename + '.p7')) {
      fs.unlinkSync(filename + '.p7')
    }
    if (fs.existsSync(filename + '.p12')) {
      fs.unlinkSync(filename + '.p12')
    }
  }

  async savePassword (o, type) {
    const user = o.user.userdata
    const { page } = this.helpers.Puppeteer

    assert(user && user.password, 'user or user password not configured') // Check user exists

    const auth = await page.evaluate(async function (user, type) {
      return new Promise(function (resolve, reject) {
        import('./io.ox/guard/oxguard_core.js').then(({ default: core }) => {
          core.savePassword(user.password, 99999, type)
            .done(function (data) {
              resolve(data.auth)
            })
            .fail(reject)
        })
      })
    }, user, type)
    assert(auth.length > 20, auth)
  }

  async verifyUserSetup (o, additional) {
    const user = o.user.userdata
    const { page } = this.helpers.Puppeteer

    assert(user && user.password, 'user or user password not configured') // Check user exists

    const result = await page.evaluate(async function (user, additional) {
      return new Promise(function (resolve, reject) {
        import('./io.ox/guard/core/guardModel.js').then(({ default: guardModel }) => {
          if (guardModel().needsKey() || additional) { // Needs setup
            return import('./io.ox/guard/api/keys.js').then(({ default: api }) => {
              return api.create({ name: user.name, email: user.email1, password: user.password }).then((data) => {
                if (data.error) reject(data.error_desc)
                if (data.publicRing) {
                  guardModel().setAuth(null)
                  guardModel().set('recoveryAvail', !guardModel().getSettings().noRecovery)
                  resolve(true)
                }
              })
            })
          } else {
            console.log('Guard reports existing key or problem with login')
            reject(new Error('fail')) // temporary password or lockout
          }
        })
      })
    }, user, additional)

    assert(result === true, result)
  }

  insertPassword (selector, o) {
    const user = o.user.userdata
    if (!user.password) this.assert.fail('user without password', 'Expected a user with password when calling insertPassword.')
    this.clearValue(selector)
    this.setValue(selector, user.password)

    return this
  }

  async downloadPrivateKey (keyId, o) {
    const { page } = this.helpers.Puppeteer
    const result = await page.evaluate(function (userdata, id) {
      return new Promise(function (resolve, reject) {
        Promise.all([import('./jquery.js'), import('./underscore.js'), import('./ox.js'), import('./io.ox/guard/core/og_http.js')])
          .then(function ([{ default: $ }, { default: _ }, { default: ox }, { default: og_http }]) {
            const params = '&keyid=' + (_.isArray(id) ? id[0] : id) + '&keyType=private'
            const data = { password: userdata.password }
            const link = ox.apiRoot + '/oxguard/keys?action=downloadKey'
            og_http.simplePost(link, params, data).then(resolve, reject)
          })
      })
    }, o.user.userdata, keyId)
    return result
  }

  async addToTextDocument (textToAdd) {
    const { page } = this.helpers.Puppeteer
    page.keyboard.type(textToAdd)
  }

  async selectEmailNumber (num) {
    const { page } = this.helpers.Puppeteer
    const selector = 'ul.list-view li:nth-child(' + (num + 1) + ')'
    await page.$eval(selector, el => { el.click() })
  }

  getThreadSelector (num) {
    return '.thread-view article:nth-of-type(' + num + ')'
  }

  async openThreadEmail (num) {
    const { page } = this.helpers.Puppeteer
    const selector = this.getThreadSelector(num)
    return new Promise(function (resolve, reject) {
      page.evaluate(async function (num, selector) {
        const { default: $ } = await import('./jquery.js')
        const def = $.Deferred()
        function isOpen () {
          return $(selector + ' .content').is(':visible') || $(selector + ' .body').is(':visible') || $(selector + ' .guard_found').is(':visible')
        }
        function tryOpen (attempts) {
          console.log('attempting' + attempts)
          console.log(selector)
          console.log(selector + ' .toggle-mail-body')
          $(selector + ' .toggle-mail-body').click()
          setTimeout(function () {
            if (isOpen()) {
              def.resolve()
              return
            }
            if (!isOpen() && attempts++ < 2) {
              tryOpen(attempts)
            }
            if (attempts > 2) {
              def.reject()
            }
          }, 500)
        }
        if (isOpen()) {
          def.resolve()
        } else {
          tryOpen(0)
        }
        return def
      }, num, selector).then(resolve, reject)
    })
  }

  async waitForApp () {
    const { Puppeteer } = this.helpers

    await Puppeteer.waitForInvisible('#background-loader.busy', 30)
    await Puppeteer.waitForVisible({ css: 'html.complete' }, 10)
  }
}
module.exports = GuardHelper
