## Motivation

A brief summary of the motivation for that feature. Here are some bullet points that could help with formulation.

- _Problem_: Clearly state the issue the feature aims to solve.
- _User Benefit_: Describe how the feature will improve user experience or efficiency.
- _Business Value_: Highlight the potential impact on productivity, cost, or customer satisfaction.
- _Market Demand_: Mention any evidence of market need or customer feedback.

## Mockup

If applicable, add potential mockup designs here.

## Tech

If any technical details are available already, please include them here.

## Additional Information

- **Target version**: [e.g. 8.27]
- **Environment**: [e.g oxcloud easy]
- **Budget**: [if there is one]

## Misc

Any other information that might be helpful

/label ~"Request"
