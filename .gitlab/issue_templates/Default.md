## Important

> Please first select a template (Bug, Question, Feature Request) from the dropdown menu in the description. The more information we have in structured form, the faster we can process your request.
