# OX Guard UI
All notable changes to this project will be documented in this file.

## [Unreleased]

## [8.33.0] - 2024-12-20

- no changes

## [8.32.0] - 2024-11-20

### Added

- [`GUARD-453`](https://jira.open-xchange.com/browse/Guard-453): Warn of expiring keys



## [8.29.0] - 2024-08-29

### Changed

- Change from yarn to pnpm for build [`d1bcf1d`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/d1bcf1dd409392ee4f0da1aea40ecb987a091163)

### Fixed

- Fix Tour.  Improve spotlight placement.  Handle no smime certs.  Fix default encrypt link. [`e2711ec`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/e2711ec5445bf15ba3ccf58ecff3bc11f53c08e7)

## [8.28.0] - 2024-07-04

### Changed

- E2E test improvements

## [8.27.0] - 2024-07-04

### Added

- [`Guard-491`](https://jira.open-xchange.com/browse/GUARD-491): Allow upload of certificate that has expired [`257c4b15`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/257c4b1565d5adb0bb8ebffc98f28f3829b5da08)

### Fixed
- [`Guard-493`](https://jira.open-xchange.com/browse/GUARD-493): Fix password input box too large [`6870a775`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/6870a775ee70e5ad177f859698122e49589e8523)


## [8.26.0] - 2024-06-05

### Added

- Add e2e test for multiple emails in subject alt name extension [`ddb5e5c`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/ddb5e5c214ea0d8ab1150e269a18c27c225f184e)

### Changed

- Changelog header change [`fae3384`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/fae3384d8f0bdebcdffff4d7373a6a2ec69e7cf4)

### Fixed

- Fix banner regarding signature status. Was showing cursor pointer when no link available. [`d1449e5`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/d1449e5a0894fe47f491c286517ebd4c659bb489)
- Improve layout of public key list.  Left align email addresses [`7fdc6ca`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/7fdc6ca45660697b1ca20ecf398af4d68978cecf)

## [8.25.0] - 2024-05-09

### Added

- Add Guard settings to the setting search. [`b64ad67`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/b64ad670e085d2e81e88f9e50f37616cb54cd42a)
- Add prominent infoline to private key dialog when key expired [`080b580`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/080b58075a02de9ed68303cbc8c08ee53c8a764b)

### Changed

- Changed mobile mail detail header to fit new order of elements [`4c99299`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/4c992997076b9f351f97a8f517ebc224721d2772)

### Removed

- Remove deprecated compose info-line reference, migrate required style code to guard-ui. [`6978a29`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/6978a296bf01c7cf31333491a541d8e161de0695)

### Fixed

- Defaults for remember password missing [`7d17a25`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/7d17a250564447abf1a6b087cdfbc78cd212e5f9)
- Fix preview environment image reference [`8d39ae8`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/8d39ae8872668b97291d65688aae44e9b8f14d3d)

## [8.24.0] - 2024-04-04

### Fixed

- Fix DeleteRecovery not offering selection of s/mime or pgp, rather just defaulting to pgp [`42dc0ef`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/42dc0efe42347f796ac22c96ac74550bd5c243bb)

## [8.23.0] - 2024-03-10

### Removed

- Remove debug code [`51f8a25`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/51f8a25c9e5e2d94c27318f50e6847a9f76ee7e3)

### Fixed

- [`OXUI-1280`](https://jira.open-xchange.com/browse/OXUI-1280): Guard UI Issues [`f3f4d50`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/f3f4d507de0e3dbe4d1fbde81f34439ea767d081)
- Fix temporaryPassword dialog for users with no password recovery set.  Add e2e tests [`6db3cf0`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/6db3cf07f32f13fc77880c1fceca01d87ec91e22)

## [8.22.0] - 2024-02-09

### Added

- User can choose if Guard draft emails are encrypted in Settings.  Draft S/Mime emails may also now be encrypted.
- Add initial choice for encrypting drafts dialog first time user composes a Guard email

### Changed

- Refactor/icons. Customers can now change individual icons with a config setting (https://gitlab.open-xchange.com/frontend/ui/-/issues/129) [`ff1b319`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/ff1b31940e9b9a3199b7dd49b492030c48d93fd2)

### Fixed

- [`OXUI-1401`](https://jira.open-xchange.com/browse/OXUI-1401): Mails sent with Guard have attachment icon without any attachments [`99c89a7`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/99c89a780a0f8ce090ab9e8da00755070eb27a06)
- [`OXUIB-2723`](https://jira.open-xchange.com/browse/OXUIB-2723): Wrong display of attachments in Guard encrypted mails [`21dd47f`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/21dd47f13533deea5c337f48475db7a07d687d41)
- Encrypt draft prompt was missing extension point definition [`7965e13`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/7965e136b7a1dccefcfff46eb9d70c6558708174)

## [8.21.0] - 2024-01-12

### Fixed

- Compose-actions not opening [`3dbca20`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/3dbca20adf8f27fac4133858b9f5241432b49034)
- Guard-467 Handle bad password when attaching encrypted file.  Remove failed file from attachment list. [`24e29b1`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/24e29b168aba7e5a2ef46e7c66a4ef627a3a13b5)
- Unable to retrieve details of some uploaded keys.  Key cache was being overwritten. [`7ba299b`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/7ba299bf35c6100bd5f0443ae76bf15223efab8b)

## [8.20.0] - 2023-12-13

## [4.2.2] - 2023-12-13

### Fixed

- Changed German translation of Mark current [`31a8207`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/31a82070e6de78b8f6ae9097a4cc84ceaea9c3cc)
- Guard-464 helm chart defines imagePullSecrets twice [`24609f3`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/24609f3d2f14dd313ddc7eb07d2ae7c77b3754ad)


## [4.2.1] - 2023-11-30

### Fixed

- Fix icon layout in mobile compose


## [4.2.0] - 2023-11-30

### Added

- Ability to upload recipient certificate [`5697130`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/5697130a4dd0a1fe150f1595ff6ab295807f5f90)
- Add Certificate lookup [`2657dd4`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/2657dd40af4ee651967a2247c3b9df544364db3b)
- List certificates in contacts [`c22fe65`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/c22fe65b3545bed2b179240e98e7265c24dd0d10)

### Changed

- [`OXUI-1295`](https://jira.open-xchange.com/browse/OXUI-1295): Redesign of Mobile Mail App Experience [`4fdc0be`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/4fdc0be0d12658af2bccb0cd880c99e9ac2e0438)
- Redo settings pane, new layout [`072972e`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/072972efc2ddd5525bcbe97d1d039e283907b020)
- Update tours for new settings layout [`6515114`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/65151146ead1921f17315305a289568b30d4857e)

### Removed

### Fixed

- Fix autocrypt test for headless browser [`e930884`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/e930884467c0ea16b48982e5163c582953421be1)
- Fix minor spelling error [`13ebf21`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/13ebf216802a3ba2663e9999df465f7ef8e662f2)
- Fix smime buttons disabled incorrectly [`245d2db`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/245d2db2493d17f7103c075d169bae1181f8457f)
- Fix tour back buttons, highlights [`65034bc`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/65034bc77b55f45b919689c8d6ce50c979fa0536)

## [4.0.7] - 2023-10-23

### Changed

- Adjust extension points 'lock-mobile' and 'security' for [`OXUI-1300`](https://jira.open-xchange.com/browse/OXUI-1300) (PWA compose redesign) [`a4c4f27`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/a4c4f278c2fc67068d0448ec4ed4284f20aa2fe7)

### Fixed

- Fix forward disabled for smime [`84b5343`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/84b534331481e1c63e0cae0e2dfec68c1766f673)
- Fix uploadkeys not checking for password mismatch [`3209ba2`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/3209ba258a5621d9e056cc4dc13b6dc581bcede1)

## [4.0.6] - 2023-08-04

### Changed

- Guard settings location and icon changed for the new settings layout [`0c14e1b`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/0c14e1bc54a9f63da54a65bfd4e1fca1a76e1f98)

### Fixed

- Fix Guard-440, possible script injection with Guard product name.  Use DomPurify instead of regex [`d66a6bf`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/d66a6bf2810fc3450dc832fd880972dd9ff5d31b)
- Fix Guard-443, improve label alignment and stabilize error message moving other labels [`cd9e8b4`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/cd9e8b40963d9e33fb1938eb05be8e7a9e10beb4)
- Fix Guard-444.  Modify the main drive download button to recognize Guard files and prompt for password.  Add "Download Encrypted" links to drive links and context menu. [`2c59ba8`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/2c59ba81cb50ec6dc7a1f42e37b39d817bc54762)
- Fix Guard-446: Don't display error message for simple cancel when sharing file [`a00e992`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/a00e9925357a48ce0a47d0d999274c6637f80cee)
- Guard settings spacing a bit off.  Corrected [`a0ac321`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/a0ac321c50b1620819fff735a4c17243b51a57c4)
- Guard-441, mobile change password view broken.  Function was not being properly called. [`3904592`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/39045920add5b4890789f4e8e968d4ce43cc68b6)
- Guard-449, double entry for view this version link. [`8341d57`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/8341d5795b0e1a833a8014fab037c1965e4ce802)
- Only check autocrypt headers if user has guard-mail [`a31ef63`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/a31ef63e2f78fae31067491a52242bc04d9ccb47)
- Upsell not being shown for unsupported crypto type. [`2e34e7a`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/2e34e7a893c90a1698ed3c36a8e8c2894ec7f52d)

## [4.0.5] - 2023-05-30

### Changed

- Update appsuite dependency version

## [4.0.4] - 2023-05-29

### Fixed

- Guard-381, tour fails or doesn't work properly if "Default encrypted" option selected [`4d890e5`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/4d890e5763da63de7dc3d97f5ca9f10a2fa76bc6)
- Guard-424.  Import public key doesn't work after print.  Fix check to determine if decryption required [`999d9db`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/999d9db1619a3744c0b2f3f6359f4977c590fb74)
- Guard-437, wiping emails between app changes failing due to model wipe. [`73e4334`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/73e4334dea630afefbca417034c84fd4a5fde60b)
- Add some missing translations
- Guard-438.  Update class for pgp keys in contact details for proper alignment [`536e9f5`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/536e9f5ea8b2277af3ff6038a301dd510498722c)

## [4.0.3] - 2023-04-11

### Changed

- Stack chart references needed to publish the actual release


## [4.0.2] - 2023-04-07

### Fixed

- Fix [`GUARD-417`](https://jira.open-xchange.com/browse/GUARD-417), Settings not being drawn properly with capabilities smime,guard (no guard-mail).  Advanced settings missing. [`040cc57`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/040cc5749402a6f71640e9da96c8221c79f31cd8)
- [`OXUIB-2291`](https://jira.open-xchange.com/browse/OXUIB-2291):, console error when loading compose in mobile [`97e4e5b`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/97e4e5b506e40265cccbf8e43dbb7f4a046d5f8b)
- Fix build e2e pipeline [`99d6a9b`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/99d6a9b18f1cc36c02357c4e706061edfc30d5e7)
- Guard-415, Guard signing defaults applied even if guard-mail capabilites removed, locking the user out of replies [`f60f58c`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/f60f58c095065abcffbfcf9a4e1d0178333d683c)
- Guard-416, fix reply may be broken if mail cache reloaded [`d69bce2`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/d69bce26de636d4e416cc8662841ec246fd2579e)
- Guard-423, spinner persists when cancelling temporary password dialog.  Removed redundant spinner [`f9ef800`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/f9ef800a53bf641d658deb12b8c753c7aeada27d)

## [4.0.1] - 2023-03-14

### Added

- Addit e2e tests [`8ed504f`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/8ed504f85e643a1e7eea632b80689321c225cfb4)
- Addit smime merge fixes [`4c8c0e0`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/4c8c0e0eb61edadc11294c3427bd6ff43c08dbc6)
- Add autocrypt setting to disable adding header [`ea1a9d1`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/ea1a9d13f47ed2015eab6dddd0ff3a2a1b3fedeb)

### Changed

- Redesign encrypted email password prompt flow. [`92bf458`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/92bf4581167d94b94ee100c3403fad0f93c16d9e)
- Replace deprecated notifications api with yell [`9973058`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/997305893cbb432b8de735918378883cbdf7b1ab)
- Changelog update [`db88bbb`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/db88bbbbd5d601cb7b3dbec9c3c9c5abc7d2a486)

### Removed

- Remove project_version_part variable from gitlab-ci [`6b83915`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/6b839154ff38cc0786af5403584f6a7534429400)

### Fixed

- Guard-398, fix type of encryption not preserved during reply/forward [`0cd4a59`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/0cd4a59e5a91e955c632e3a9a1db863c4eabf557)
- Guard-411, fix missing crypto type.  Improve auth token cleanup and checking [`da2872e`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/da2872eb0bdbaf144c0375595f30e02e14544135)
- Guard-404.  S/MIME default not applied when both PGP and S/MIME available.  E2e test added. [`0d18b1b`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/0d18b1b5645c0fed23258998135486d099b04c43)
- Icon for Secure email found now changing with theme [`9a164fd`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/9a164fd6723b9e6aa97b78c3d2544ed185ba8039)
- Fix Password duration not properly passed with new password UI [`32b239d`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/32b239da213d0aa343fd6a9602d70d5e4a91042e)
- S/Mime uploader dialog not closing with error [`ead9f46`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/ead9f46f2a7d82a2b86be96040b00d52841ecc67)
- Fix reset token not passing type [`6d036fa`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/6d036fad78425c9ce954f9c85c70c03695acddf7)
- Guard-401, fixes visual gap between sender and subject when lock icon present [`735a07d`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/735a07d18619b497ddd08ee60ba9482762b7a548)
- Fix pgp inline not properly restored from drafts [`fc29ba3`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/fc29ba31ef2f1cb4140d1224efd6302987b3ca69)
- Guard-399, restore type of encryption when editing draft [`09cb497`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/09cb49720409cebd3301f95962f0b6ef299976a2)
- Guard emails not prompting for password for printing. [`017655d`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/017655d955542cec2cbdd50bb13e07772a87bc97)


## [4.0.0] - 2023-02-22

### Added

- S/Mime support added to Guard

## [3.8.1] - 2023-02-10

### Changed

- Replace deprecated notifications api with yell [`9973058`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/997305893cbb432b8de735918378883cbdf7b1ab)

### Removed

- Remove project_version_part variable from gitlab-ci [`6b83915`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/6b839154ff38cc0786af5403584f6a7534429400)

### Fixed

- Guard emails not prompting for password for printing. [`017655d`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/017655d955542cec2cbdd50bb13e07772a87bc97)


## [3.8.0] - 2022-12-13

### Added

- Initial changelog entry [`671c97b`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/671c97b41a79905bcf5081a17900b57680760211)

### Fixed

- [`OXUIB-2090`](https://jira.open-xchange.com/browse/OXUIB-2090): Injection of css imports missing in bundles [`1cc2944`](https://gitlab.open-xchange.com/appsuite/guard-ui/commit/1cc294404f89c2b46adfb9b6cbf6f782c0662413)


## [3.7.0] - 2022-11-20

### Added

- Initial Changelog


[unreleased]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/8.33.0...main
[3.8.1]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/3.8.0...3.8.1
[3.8.0]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/3.7.0...3.8.0

[4.2.2]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/4.2.1...4.2.2
[4.2.1]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/4.2.0...4.2.1
[4.2.0]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/4.0.7...4.2.0
[4.0.7]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/4.0.6...4.0.7
[4.0.6]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/4.0.5...4.0.6
[4.0.5]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/4.0.4...4.0.5
[4.0.4]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/4.0.3...4.0.4
[4.0.3]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/4.0.2...4.0.3
[4.0.2]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/4.0.1...4.0.2
[4.0.1]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/3.8.1...4.0.1

[8.27.0]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/8.26.0...8.27.0
[8.26.0]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/8.25.0...8.26.0
[8.25.0]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/8.24.0...8.25.0
[8.24.0]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/8.23.0...8.24.0
[8.23.0]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/8.22.0...8.23.0
[8.22.0]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/8.21.0...8.22.0
[8.21.0]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/8.20.0...8.21.0

[8.33.0]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/8.32.0...8.33.0
[8.32.0]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/8.29.0...8.32.0
[8.29.0]: https://gitlab.open-xchange.com/appsuite/guard-ui/compare/8.28.0...8.29.0
