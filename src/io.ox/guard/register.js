/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import _ from '$/underscore'
import ox from '$/ox'
import ext from '$/io.ox/core/extensions'
import capabilities from '$/io.ox/core/capabilities'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import guardModel from '@/io.ox/guard/core/guardModel'
import { gt } from 'gettext'
import yell from '$/io.ox/core/yell'
import oxguard from '@/io.ox/guard/oxguard_core'
import authAPI from '@/io.ox/guard/api/auth'

settings.ensureData().then(() => {
  // Default settings if needed
  if (settings.get('advanced') === undefined) {
    settings.set('advanced', !!settings.get('defaultAdvanced'))
    settings.save()
  }
  // Login section
  sendauth('') // initial login, no password yet
})

// Load data generated at compile (build version, etc)

// Sends first login to get settings info, key data
function sendauth (pass) {
  const typeLogin = capabilities.has('guard') ? 'pgp' : 'smime'
  oxguard.auth(ox.user_id, pass, typeLogin)
    .done(function (data) {
      try {
        if (data.auth.length > 20 && !(/^Error:/i.test(data.auth))) { // If good code comes back, then store userid
          guardModel().setAuth(data.auth, typeLogin)
        } else { // If bad code, then log and mark oxguard.user_id as -1
          if (/^((Error)|(Bad)|(Lockout))/i.test(data.auth)) {
            guardModel().clearAuth()
          } else {
            guardModel().setAuth(data.auth, typeLogin)
          }
          if (data.error) yell('error', guardModel().getName() + '\r\n' + data.error)
        }
        guardModel().loadData(data)
        settings.ensureData().then(() => {
          if (settings.get('defaultEncrypted') === undefined) {
            import('@/io.ox/guard/settings/pane').then(({ default: init }) => {
              init(data.settings)
            })
          }
        })

        if (ox.context_id === 0) ox.context_id = data.cid // If the cid wasn't loaded, update from backend

        if (sessionStorage !== null) {
          // If we were passed login info from login screen
          try {
            if (sessionStorage.oxguard && (sessionStorage.oxguard !== 'null')) {
              const p = sessionStorage.oxguard
              sessionStorage.oxguard = null
              sendauth(p)
            }
          } catch (e) {
            console.log('Private mode')
          }
        }
      } catch (e) {
        console.log(e)
        console.debug('Unable to connect to the encryption server')
      }
    })
    .fail(function () {
      console.debug('Unable to connect to the encryption server')
    })
}

function doLogout (notify) {
  const def = $.Deferred()
  guardModel().clearAuth()
  // Destroy Guard mapping and auth-token.  Setting type to none destroys all on server
  authAPI.resetToken('none').done(function () {
    if (notify) yell('success', gt('Logged out of %s', guardModel().getName()))
  })

  return def
}

ext.point('io.ox/core/appcontrol/right/help').extend({
  id: 'guard-guided-tour',
  index: 290,
  extend () {
    if (_.device('small') || capabilities.has('guest')) return
    if (capabilities.has('guard-mail') || capabilities.has('guard-drive') || capabilities.has('smime')) {
      this.append(
        this.link('GuardTour', gt('Guided tour for %s', guardModel().getName()), function (e) {
          e.preventDefault()
          import('@/io.ox/guard/tour/main').then(() => {
            import('$/io.ox/core/tk/wizard').then(({ default: Tour }) => {
              Tour.registry.run('default/oxguard')
            })
          })
        })
      )
    }
  }
})

ext.point('io.ox/core/appcontrol/right/account/signouts').extend({
  id: 'logoutOG',
  index: 50,
  extend () {
    this.link('logoutOG', gt('Sign out %s', guardModel().getName()), function (e) {
      e.preventDefault()
      guardModel().clearAuth()
      doLogout(true)
    })
    this.$el.on('shown.bs.dropdown', function () {
      if (guardModel().hasAnyAuth()) {
        $('[data-name="logoutOG"]').show()
      } else {
        $('[data-name="logoutOG"]').hide()
      }
    })
  }
})

ext.point('io.ox/core/stages').extend({
  id: 'first',
  index: 107,
  run () {
    if (_.url.hash('i') === 'guestReset') {
      _.url.hash({ i: null, m: null, guardReset: 'true', app: 'io.ox/mail', settings: 'virtual/settings/io.ox/guard' })
    }
  }
})
