/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import JS from 'jsencrypt'
import guardModel from '@/io.ox/guard/core/guardModel'

function getEncrypted (pubKey, data) {
  try {
    if (pubKey.key === null) return (null)
    const encrypt = new JS()
    encrypt.setPublicKey(pubKey.key)
    if (encrypt.getPublicKey().indexOf('PUBLIC') < 0) {
      console.log('pubkey import fail')
      return (null)
    }
    const encrypted = encrypt.encrypt(data)
    return (encrypted)
  } catch (e) {
    console.log(e)
    return (null)
  }
}

function cryptPass (password) {
  if (guardModel().get('pubKey') === undefined) return (null)
  return (getEncrypted(guardModel().get('pubKey'), password))
}

export { getEncrypted, cryptPass }
