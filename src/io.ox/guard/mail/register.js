/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

'use strict'

import $ from '$/jquery'
import _ from '$/underscore'
import ext from '$/io.ox/core/extensions'
import { Action } from '$/io.ox/backbone/views/actions/util'
import http from '@/io.ox/guard/core/og_http'
import * as checker from '@/io.ox/guard/mail/checker'
import account from '$/io.ox/core/api/account'
import capabilities from '$/io.ox/core/capabilities'
import * as util from '@/io.ox/guard/mail_lock/util'
import auth_core from '@/io.ox/guard/auth'
import guardModel from '@/io.ox/guard/core/guardModel'
import yell from '$/io.ox/core/yell'
import mailMain from '$/io.ox/mail/main'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import mailApi from '$/io.ox/mail/api'
import ox from '$/ox'
import gt from 'gettext'
import keyCreator from '@/io.ox/guard/core/createKeys'
import ModalDialog from '$/io.ox/backbone/views/modal'
import '@/io.ox/guard/pgp/style.scss'
import publicKeys from '@/io.ox/guard/pgp/keyDetails'
import autocrypt from '@/io.ox/guard/pgp/autocrypt/autoCrypt'
import attachmentSave from '$/io.ox/mail/actions/attachmentSave'
import { createIcon } from '$/io.ox/core/components'

function drawIcons (baton, options = {}) {
  if (!baton.model.get('security')) return
  const security = baton.model.get('security')
  const div = $('<div class="guardIcons">')
  if (security.decrypted) {
    if (options.asText) {
      if (security.type === 'SMIME') {
        this.append($('<span class="guard">').text(gt('This Email was encrypted with S/MIME.')))
      } else {
        this.append($('<span class="guard">').text(gt('This Email was encrypted with PGP.')))
      }
    } else {
      if (security.type === 'SMIME') {
        const smime = $('<span class="oxguard_icon_fa">').attr('title', gt('This Email was encrypted with S/MIME.'))
        smime.append(createIcon('bi/lock-fill.svg').addClass('icon-lock'))
        div.append(smime)
      } else {
        const pgp = $('<span class="oxguard_icon_fa">').attr('title', gt('This Email was encrypted with PGP.'))
        pgp.append('<span style="font-size:0.5em;">Pgp</span>')
        const lockicon = createIcon('bi/lock-fill.svg').addClass('pgp_superscript icon-lock')
        pgp.append(lockicon)
        if (_.device('small')) {
          pgp.css('margin-right', '5px')
        }
        div.append(pgp)
      }
    }
  }
  if (security.signatures) {
    let verified = true
    let error
    let missing = false
    security.signatures.forEach(function (sig) {
      verified = verified && sig.verified
      if (sig.missing) missing = true
      if (sig.error) error = sig.error
    })
    const signedicon = $('<span class="oxguard_icon_fa">')
    if (options.asText) { // Only add signed text if valid, otherwise will appear in warning section
      if (verified) this.append($('<span class="guard">').text(gt('This Email was signed and verified.')))
    } else {
      if (verified) {
        // TODO  Change icon if smime signed
        // if (security.type === 'SMIME') {
        signedicon.append(createIcon('bi/pencil-square.svg').addClass('oxguard_icon_fa guard_signed')).attr('title', gt('This Email was signed and verified.'))
      } else if (!missing && !error) {
        signedicon.append(createIcon('bi/x-circle.svg').addClass('oxguard_icon_fa_error guard_signed')).attr('title', gt('This Email was signed and failed verification.'))
      }
      if (missing) {
        signedicon.append(createIcon('bi/pencil-square.svg').addClass('guard_signed').css('color', 'lightgrey')).attr('title', gt('This Email was signed but unable to verify.'))
      }
      if (error) {
        signedicon.append(createIcon('bi/x-circle.svg').addClass('oxguard_icon_fa_error guard_signed')).attr('title', gt('Error verifying signature: %s', error))
      }
      if (_.device('small')) {
        signedicon.css('margin-right', '10px')
      }
      div.append(signedicon)
    }
  }
  if (!options.asText) this.append(div)
  checkIntegrity(baton.data)
}

ext.point('io.ox/mail/mobile/detail/header/flags').replace('security', function () {
  return {
    draw: drawIcons
  }
})

ext.point('io.ox/mail/mobile/detail/header/info-container').replace('security', function () {
  return {
    draw (baton) {
      drawIcons.call(this, baton, { asText: true })
    }
  }
})

ext.point('io.ox/mail/detail/header/row1').replace('security', function () {
  return {
    draw: drawIcons
  }
})

function checkIntegrity (data) {
  if (data && data.headers && data.headers['X-Guard-Failed-Integrity']) {
    yell('warning', gt('This email is missing one or more security checks.  HTML markup was removed for your security.'))
  }
}

// If so configured, cleanup any opened Guard emails
async function cleanGuardCache (baton) {
  // If not configured to do so, exit
  if (settings.get('wipe') !== 'true') return
  // Don't wipe if still authenticated
  if (guardModel().getAuth()) return

  if (!mailApi.pool) return
  // Don't wipe when just opening compose
  if (baton.options && baton.options.name === 'io.ox/mail/compose') return

  const collection = mailApi.pool.get('detail')
  if (collection && collection.models) {
    collection.models.forEach(async function (model) {
      const security = model.get('security')
      if (security && security.decrypted) {
        let found = false
        if (baton.model) { // Still within mail application
          if (model.get('cid') === baton.model.cid) { // Don't wipe the active mail
            found = true
          }
          const mailApp = mailMain.getApp()
          if (mailApp.isThreaded()) {
            mailApp.threadView.collection.models.forEach(function (thrModel) {
              if (thrModel.get('cid') === model.cid) {
                found = true
              }
            })
          }
        }
        if (!found && model.get('origMail')) {
          const orig = model.get('origMail')
          await model.set(orig)
          model.unset('security')
          const view = model.get('view')
          if (view.$el) { // when changing apps, need to redraw
            view.redraw()
          }
        }
      }
    })
  }
}

if (capabilities.has('webmail')) { // Register mail cleanup only if has mail
  ox.on('app:start', cleanGuardCache)
  ox.on('app:resume', cleanGuardCache)
}

// Create a warning div if signatures unable to be evaluated
function getWarning (baton, node) {
  const security = baton.model.get('security')
  if (security) {
    let warn
    const div = $('<div class="oxguard_info">')
    if (security && security.signatures) {
      const sig = security.signatures[0]
      if (sig && sig.missing && !settings.get('suppressInfo')) {
        div.append($('<span>').append(gt('Unable to verify signature.  Missing public key.')))
        warn = true
      }
      if (sig && !sig.verified && !sig.missing && !sig.error) {
        div.append($('<span class="signError">').append(gt('This Email was signed and failed verification.')))
        warn = true
      }
    }
    if (warn) {
      node.append(div)
    }
  }
}

// Add warnings to mail detail of failed signatures or missing keys
ext.point('io.ox/mail/detail/header').extend({
  id: 'guardNotices',
  index: 'last',
  draw: function (baton) {
    getWarning(baton, this)
  }
})

// Add warnings to mobile detail of failed signatures or missing keys
ext.point('io.ox/mail/mobile/detail').extend({
  id: 'warnings',
  index: 'last',
  draw (baton) {
    getWarning(baton, this.find('section.notifications'))
  }
})

ext.point('io.ox/mail/detail/attachments').extend({
  id: 'ogCheck',
  index: 1,
  draw (baton) {
    if (baton.data.security_info && baton.data.security_info.encrypted) baton.stopPropagation()
  }
})

ext.point('io.ox/mail/detail/body').extend({
  id: 'ogPGPCheck',
  index: 1,
  draw (baton) {
    if (this[0] !== undefined) {
      let location
      if (this[0].host !== undefined) { // Chrome
        location = $(this[0].host)
      } else { // Others
        location = this[0]
      }
      checker.checkPGP(baton, location)
    }
    if (baton.data.security_info && baton.data.security_info.encrypted) {
      baton.stopPropagation()
      checkAutocryptHeaders(baton) // Check headers for the encrypted emails
      cleanGuardCache(baton) // Clean previously decrypted emails
    }
  }
})

// Check for autocrypt header
ext.point('io.ox/mail/detail/body').extend({
  requires: 'guard-mail',
  index: 1000,
  id: 'autocrypt',
  draw (baton) {
    checkAutocryptHeaders(baton)
  }
})

ext.point('io.ox/mail/mobile/detail/body').extend({
  index: 230,
  id: 'ogPGPCheckMobile',
  draw (baton) {
    const location = this[0]
    checker.checkPGP.call(this, baton, location)
    baton.view.listenTo(baton.view.model, 'change:attachments', function () {
      checker.checkPGP(baton, location)
      if (baton.view.$el === null) return
      if (!_.device('ios')) {
        window.setTimeout(function () {
          baton.view.$el.find('#oxgrpass').focus()
        }, 50)
      }
    })
    if (baton.data.security_info && baton.data.security_info.encrypted) {
      baton.stopPropagation()
    }
  }
})

ext.point('io.ox/mail/detail/body').extend({
  index: 'last',
  id: 'cleanup',
  draw (baton) {
    cleanGuardCache(baton)
  }
})

//  Wipe previews for encrypted emails
function wipePreview (baton) {
  if (baton.data.text_preview) {
    if (baton.data.content_type && baton.data.content_type.indexOf('encrypted') > 0) {
      baton.data.text_preview = ''
    }
    if (baton.data.text_preview.indexOf('----BEGIN PGP') > -1) {
      baton.data.text_preview = ''
    }
  }
}

ext.point('io.ox/mail/listview/item').extend({
  id: 'guard-preview',
  index: 10,
  draw: wipePreview
})

ext.point('io.ox/mail/view-options').extend({
  id: 'guardThread',
  index: 1,
  draw (baton) {
    if (capabilities.has('guest')) {
      baton.app.settings.set('threadSupport', false)
    }
  }
})

// Methods for actions
// Check if attachment filename ends with .asc
function isASC (e) {
  try {
    if (e.context instanceof Array) {
      for (let i = 0; i < e.context.length; i++) {
        if (e.context[i].filename.indexOf('.asc') > 0) {
          // Do not return positive for signatures.  Handled differently
          if (e.context[i].content_type.indexOf('sig') > 0) return false
          return true
        }
      }
    }
    const filename = e.context.filename
    if (filename === undefined) return false
    if (e.context.content_type.indexOf('sig') > 0) return false
    return (filename.indexOf('.asc') > 0)
  } catch (d) {
    console.log(d)
    return false
  }
}

function checkAutocryptHeaders (baton) {
  if (baton.model && baton.model.get('headers')) {
    const headers = baton.model.get('headers')
    if (headers.Autocrypt || headers['Autocrypt-Gossip']) {
      if (headers.Autocrypt) {
        autocrypt.check(baton.model)
          .done(function () {
            if (!settings.get('advanced')) return // Only users with advanced settings would have verified key
            if (baton.data.security_info && baton.data.security_info.signed) { // If signed, pull again after import
              checker.pullAgain(false, baton)
            }
          })
      }
      if (headers['Autocrypt-Gossip']) {
        autocrypt.checkGossip(baton.model)
      }
    }
  }
}

// test a single file if it is a key
function testKey (file) {
  const test = {
    collection: null,
    context: file
  }
  return (isKey(test))
}

function isKey (e) {
  if (isASC(e)) {
    if (e.collection === null || e.collection.has('one')) { // only return true for individual files
      let type
      let name
      if (e.context instanceof Array) {
        type = e.context[0].content_type
        name = e.context[0].filename
      } else {
        type = e.context.content_type
        name = e.context.filename
      }
      if (type !== undefined) {
        if (type.indexOf('application/pgp-keys') > -1) return true
      }
      if (name === 'public.asc' || name === 'publickey.asc') return true
      return (/[0x]*[0-9A-F]{8}\.asc/i).test(name) // Regex for 8 hex.asc
    }
  }
  return false
}

function testAutoCrypt (file) {
  if (file && file.content_type === 'application/autocrypt-setup') return true
  return false
}

ext.point('io.ox/mail/detail/attachments').extend({
  id: 'Import',
  draw (e) {
    if (capabilities.has('guest')) return
    let keyfound = false
    let autoCryptStart = false
    e.attachments.forEach(function (a) {
      if (testKey(a)) {
        keyfound = true
      }
      if (testAutoCrypt(a)) {
        autoCryptStart = true
      }
    })
    if (keyfound) {
      const keydiv = $('<div class="importKey">')
      const notice = $('<span>' + gt('PGP Public Key Found.  Click to Import') + '</span>')
      notice.on('click', function () {
        doImportKey(e.attachments)
      })
      this.append(keydiv.append(notice))
    }
    if (autoCryptStart) {
      const aCkeydiv = $('<div class="importKey">')
      const aCnotice = $('<span>').text(gt('AutoCrypt startup found.  Click to import key'))
      aCnotice.on('click', function () {
        autocrypt.doStart(e.attachments)
      })
      this.append(aCkeydiv.append(aCnotice))
    }
  }
})

new Action('io.ox/mail/actions/save-encrypted-attachment', {
  capabilities: 'infostore guard-drive',
  matches (baton) {
    return util.isDecrypted(baton.first())
  },
  collection: 'some',
  action (baton) {
    const options = {
      optPrompt: gt('Please re-enter your %s password.', guardModel().getName()),
      minSingleUse: true
    }
    auth_core.authorize(baton.data, options)
      .done(function () {
        const list = _.isArray(baton.data) ? baton.data : [baton.data]
        for (let i = 0; i < list.length; i++) {
          list[i].reEncrypt = true
        }
        attachmentSave.multiple(list)
      })
  }
})

ext.point('io.ox/mail/attachment/links').extend({
  id: 'saveEncrypted',
  index: 550,
  mobile: 'high',
  // #. %1$s is usually "Drive" (product name; might be
  // customized)
  title: gt('Save encrypted to %1$s', gt.pgettext('app',
    'Drive')),
  ref: 'io.ox/mail/actions/save-encrypted-attachment'
})

function doImportKey (list) {
  if (guardModel().needsKey()) {
    keyCreator.createKeysWizard()
      .done(function () {
        loadPublicKey(list)
      })
  } else {
    loadPublicKey(list)
  }
}

// Sent folder extensions

ext.point('io.ox/mail/links/inline').extend({
  index: 101,
  prio: 'lo',
  id: 'pinlink',
  title: gt('Check assigned PIN'),
  ref: 'io.ox/mail/actions/pin',
  mobile: 'lo'
})

const unified_sent = ''

new Action('io.ox/mail/actions/pin', {
  id: 'statusaction',
  matches (baton) {
    const e = baton.first()
    if (!(_.contains(account.getFoldersByType('sent'), e.folder_id)) && (e.folder_id !== unified_sent)) return (false)
    try {
      if (e.headers === undefined) return (false)
      return (e.headers['X-OxGuard-PIN'] !== undefined)
    } catch (ex) {
      console.log(ex)
      return (false)
    }
  },
  action (baton) {
    pin(baton)
  }
})

function pin (baton) {
  new ModalDialog({
    async: true,
    point: 'io.ox/guard/files/showPin',
    title: gt('PIN'),
    id: 'showPin',
    width: 300
  })
    .extend({
      content () {
        const pin = baton.first().headers['X-OxGuard-PIN']
        this.$body.append($('<h2>').append(pin))
      }
    })
    .addButton({ label: gt('OK'), action: 'ok' })
    .on('ok', function () {
      this.close()
    })
    .open()
}

// We need to update the extensions that the email has security json
ext.point('io.ox/mail/detail/attachments').extend({
  index: 1,
  id: 'guardDecrypted',
  draw (baton) {
    dupSecurity(baton)
  }
})

ext.point('io.ox/mail/mobile/detail/attachments').extend({
  index: 1,
  id: 'guardDecrypted',
  draw (baton) {
    dupSecurity(baton)
  }
})

ext.point('io.ox/mail/externalImages').extend({
  index: 1,
  id: 'checkAuth',
  perform (baton) {
    const def = $.Deferred()
    if (baton.data.security && baton.data.security.decrypted) {
      const options = {
        optPrompt: gt('Please re-enter your %s password.', guardModel().getName()),
        minSingleUse: true
      }
      auth_core.authorize(baton, options)
        .done(function () {
          def.resolve()
        })
        .fail(def.reject)
    } else {
      def.resolve()
    }
    return def
  }
})

function dupSecurity (baton) {
  if (baton.data.security && baton.data.attachments.length > 0) {
    for (let i = 0; i < baton.data.attachments.length; i++) {
      baton.data.attachments[i].security = baton.data.security
    }
  }
}

// Function to do authentication validation before calling an extension.
// Checks if valid.  If not, prompts for password, calls extension again once auth
function doAuthCheck (original, baton, storeAuth, minSingleUse) {
  baton.stopPropagation() // Stop this extension and check authorization

  const callback = function (authData) {
    if (baton.data) {
      if (_.isArray(baton.data)) {
        baton.data.forEach(function (d) {
          d.auth = authData
        })
      } else {
        baton.data.auth = authData
      }
    }
    baton.resumePropagation()
    if (storeAuth) {
      if (_.isArray(baton.data)) {
        baton.data.forEach(function (d) {
          d.security = _.extend({ authToken: authData, authentication: authData }, d.security)
        })
      } else {
        baton.first().security = _.extend({ authToken: authData, authentication: authData }, baton.first().security)
      }
      // Guard-199, direct select of attachment adds list array
      if (baton.list && _.isArray(baton.list)) {
        baton.list.forEach(function (d) {
          d.auth = authData
          d.security = _.extend({ authToken: authData, authentication: authData }, d.security)
        })
      }
    }
    original.action(baton)
  }
  const options = {
    optPrompt: gt('Please re-enter your %s password.', guardModel().getName()),
    minSingleUse: minSingleUse !== false,
    callback
  }
  const d = _.isArray(baton.data) ? baton.data[0] : baton.data
  // Still encrypted type
  if (d.security_info && d.security_info.type) {
    options.type = d.security_info.type
  }
  // decrypted type
  if (d.security && d.security.type) {
    options.type = d.security.type
  }
  auth_core.authorize(baton, options)
    .fail(function () {
      baton.stopPropagation()
      window.setTimeout(function () {
        baton.resumePropagation()
      }, 500)
    })
}

registerPasswordCheck('io.ox/mail/actions/reply', true)
registerPasswordCheck('io.ox/mail/actions/reply-all', true)
registerPasswordCheck('io.ox/mail/actions/forward', true)
// registerPasswordCheck('io.ox/mail/actions/edit', true)
registerPasswordCheck('io.ox/mail/actions/edit-copy', true)
registerPasswordCheck('io.ox/mail/actions/print', true)
registerPasswordCheck('io.ox/mail/attachment/actions/view', true, 'default', false)
registerPasswordCheck('io.ox/mail/attachment/actions/open')
registerPasswordCheck('io.ox/mail/attachment/actions/download')
registerPasswordCheck('io.ox/mail/attachment/actions/save')
registerPasswordCheck('io.ox/mail/attachment/actions/vcard')
registerPasswordCheck('io.ox/mail/attachment/actions/ical')

// Function to check actions for Guard emails, and if found, proper authentication
function registerPasswordCheck (extName, storeAuth, id, minSingleUse) {
  ext.point(extName).replace((id || 'default'), function (original) {
    return {
      action (baton) {
        if (storeAuth) {
          if (util.isEncryptedMail(baton.first())) {
            baton.first().security = {
              type: util.getCryptoType(baton.first()),
              decrypted: true
            }
            doAuthCheck(original, baton, storeAuth, minSingleUse)
            return
          }
        }
        if (!util.isDecrypted(baton.first())) {
          original.action(baton)
          return
        }
        doAuthCheck(original, baton, storeAuth, minSingleUse)
      }
    }
  })
}

/**
 * Extension to check if allowed to forward decrypted/encrypted email
 * Must have Guard-mail capability
 */
ext.point('io.ox/mail/actions/forward').extend({
  id: 'guardLimitedReply',
  index: 10,
  matches (baton) {
    if (baton && baton.first()) {
      if (util.isDecrypted(baton.first()) || util.isEncryptedMail(baton.first())) {
        if (!capabilities.has('guard-mail') && !util.isSmime(baton.first())) {
          baton.stopPropagation()
        }
      }
    }
    return false
  }
})

// Check that mail data fully loaded.  If not, pull from api
function getData (baton) {
  const data = baton.first()
  if (data.attachments) {
    return $.when(data)
  }
  return mailApi.get(data.cid)
}

// Guard Bug-347, double password for composition space
// Comp space emails already have password attached, and Guard will try that first
// Don't prompt for password for spaces.
ext.point('io.ox/mail/actions/edit').replace(('default'), function (original) {
  return {
    action (baton) {
      getData(baton).then(function (data) {
        if (util.isEncryptedMail(data)) {
          baton.first().security = {
            decrypted: true
          }
          const mailref = _.cid({ id: data.id, folder: data.folder_id })
          const space = (ox?.ui?.spaces || {})[mailref]
          if (space) {
            original.action(baton)
          } else {
            doAuthCheck(original, baton, true, true)
          }
          return
        }
        original.action(baton)
      })
    }
  }
})

// End extensions
// Functions

// Save public key from attachment
function loadPublicKey (list) {
  _(list).each(function (data) {
    if (testKey(data)) { // make sure key
      let params =
        '&emailid=' + data.parent.id +
        '&attach=' + data.id +
        '&userid=' + ox.user_id +
        '&cid=' + ox.context_id +
        '&folder=' + data.parent.folder_id
      const mail = mailApi.pool.get('detail').get(_.cid(data.mail))
      const security = mail.get('security')
      if (security && security.decrypted) { // If was encrypted
        auth_core.authorize(list).then(function (auth) {
          params = params + '&auth=' + encodeURIComponent(auth) +
            '&inline=' + (security.pgpInline ? 'true' : 'false') +
            '&filename=' + encodeURIComponent(data.filename)
          doLoadPublicKey(params)
        })
      } else {
        doLoadPublicKey(params)
      }
    }
  })
}

function doLoadPublicKey (params) {
  const link = ox.apiRoot + '/oxguard/pgpmail/?action=savepublicattach' + params
  http.get(link, '')
    .done(function (data) {
      const keys = data.data.externalPublicKeyRings
      let added = gt('Added keys: \r\n')
      for (let i = 0; i < keys.length; i++) {
        added = added + gt('Key ID: ') + keys[i].ids + '\r\n'
        if (keys[i].publicRing && keys[i].publicRing.keys) {
          const details = publicKeys.keyDetail(keys[i].publicRing.keys)
          added = added + gt('User IDs: ') + details.keyids
        }
      }
      yell('success', added.replace(/&lt;/g, '<').replace(/&gt;/g, '>'))
    })
    .fail(function (e) {
      console.log(e)
      if (e.status === 503) {
        yell('error', gt('Service Unavailable'))
      } else { yell('error', e.responseText) }
    })
}

export default registerPasswordCheck
