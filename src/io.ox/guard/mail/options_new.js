/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import _ from '$/underscore'
import core from '@/io.ox/guard/mail/oxguard_mail_compose_recip'
import coreUtil from '@/io.ox/guard/util'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import guardModel from '@/io.ox/guard/core/guardModel'
import gt from 'gettext'
import yell from '$/io.ox/core/yell'
import '@/io.ox/guard/mail/style.scss'
import composeAPI from '$/io.ox/mail/compose/api'
import keysAPI from '@/io.ox/guard/api/keys'
import ModalDialog from '$/io.ox/backbone/views/modal'

function createlock (baton) {
  baton.view.listenTo(baton.view.securityModel, 'change:encrypt', function () {
    setLock(baton)
  })
  setLock(baton)
}

function setLock (baton) {
  if (baton.view.securityModel.get('encrypt')) {
    core.lock(baton)
  } else {
    core.unlock(baton)
  }
}

function warnHTML (baton) {
  if (baton.view.securityModel.get('smime')) return // Doesn't apply to smime
  const inline = baton.view.securityModel.get('pgpInline')
  const encrypt = baton.view.securityModel.get('encrypt')
  if (!inline || !encrypt) return // Don't display warning if not Inline or not encrypted
  if (baton.config.get('editorMode') === 'text') return
  if (baton.view.securityModel.get('warned')) return // Only warn once
  baton.view.securityModel.set('warned', true)
  new ModalDialog({
    async: true,
    point: 'io.ox/guard/files/htmlWarning',
    title: gt('HTML Formatted Email'),
    id: 'htmlWarning',
    width: 450
  })
    .extend({
      explanation () {
        this.$body.append($('<p>').text(gt('HTML formatted emails may cause issues with PGP inline.  Click OK to change to Plain Text formatted Emails.')))
      }
    })
    .addButton({ label: gt('OK'), action: 'ok' })
    .addCancelButton()
    .on('ok', function () {
      this.close()
      baton.config.set('editorMode', 'text')
      baton.config.on('change:editorMode', function () {
        if ((baton.config.get('editorMode') === 'html') && (baton.view.securityModel.get('pgpInline'))) {
          new ModalDialog({
            async: true,
            point: 'io.ox/guard/files/htmlSwitchWarning',
            title: gt('HTML Formatted Email'),
            id: 'htmlSwitchWarning',
            width: 450
          })
            .extend({
              explanation () {
                this.$body.append($('<p>').append(gt('HTML format is not recommended when using PGP Inline')))
              }
            })
            .addButton({ label: gt('OK'), action: 'ok' })
            .on('ok', function () {
              this.close()
            })
            .open()
        }
      })
    })
    .open()
}

function createOptions (baton, count) {
  if (!guardModel().isLoaded()) {
    coreUtil.addOnLoaded(function () {
      createOptions(baton, count)
    })
    return
  }

  const securityModel = baton.view.securityModel

  const headers = baton.model.get('headers')
  if (headers && (headers['X-OxGuard-Sign'] === 'true')) {
    securityModel.set('sign', true)
  }

  createlock(baton)

  securityModel.on('change:pgpInline change:encrypt', () => warnHTML(baton))

  // Guard bug GUARD-200, deleted user still has defaults applied.  Verify configured before applying
  // New email doesn't have recips yet.  Check to see if there are defaults
  if (baton.config.get('newmail') === true && coreUtil.hasGuardMailCapability() && coreUtil.isGuardConfigured()) {
    if (settings.get('defaultSmime') && settings.get('smime') && coreUtil.hasSmime()) {
      securityModel.set('smime', true)
    }
    if (settings.get('defaultInline')) {
      securityModel.setInline()
    } else securityModel.setMime()

    baton.config.unset('newmail')

    if (settings.get('defaultSign')) {
      securityModel.set('sign', true)
    }

    // If default is to use guard, go ahead and lock
    if (settings.get('defaultEncrypted')) {
      securityModel.set('encrypt', true)
      core.lock(baton)
    }
  }

  if (baton.config.get('type') !== 'new' && baton.config.get('type') !== 'edit') { // set for replies, forward
    if (coreUtil.hasSmime() || coreUtil.hasGuardMailCapability()) {
      const original = _.isArray(baton.model.original) ? baton.model.original[0] : baton.model.original
      const origSmime = original && original.security && (original.security.type === 'SMIME')
      if ((settings.get('defaultSmime') || origSmime) && settings.get('smime') && coreUtil.hasSmime()) {
        securityModel.set('smime', true)
      }
      if (settings.get('defaultSign')) {
        securityModel.set('sign', true)
      }
    }
  }

  securityModel.on('change:pgpInline', function () {
    securityModel.set('encrypt', true)
  })

  const attachmentsCollection = baton.model.get('attachments')
  securityModel.set('PubKey', attachmentsCollection.some(testModelForKey))

  securityModel.on('change:PubKey', function () {
    if (securityModel.get('PubKey')) {
      attachPubKey(baton)
      return
    }
    removePubKeys(baton)
  })
}
// Attach the users public key to the email
function attachPubKey (baton) {
  keysAPI.download({ keyType: 'public' })
    .done(function (data) {
      try {
        const file = new Blob([data.key], { type: 'application/pgp-keys' })
        file.name = 'public.asc'
        composeAPI.space.attachments.add(baton.model.get('id'), { file })
          .then(data => baton.model.attachFiles(data))
      } catch (e) {
        console.error(e)
        yell('error', gt('Unable to attach your public key'))
      }
    })
    .fail(function () {
      yell('error', gt('Unable to attach your public key'))
    })
}

function removePubKeys (baton) {
  try {
    const attachments = baton.model.get('attachments')
    const key = attachments.models.find(model => testModelForKey(model))
    if (key) attachments.remove(key)
  } catch (e) {
    console.error(e)
    yell('error', gt('Unable to remove your public key'))
  }
}

function testModelForKey (model) {
  if (model.get('mimeType') !== 'application/pgp-keys') return false
  if (!/.*\.asc/.test(model.get('name'))) return false
  return true
}

export default {
  createOptions,
  warnHTML
}
