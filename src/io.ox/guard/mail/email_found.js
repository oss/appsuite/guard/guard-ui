/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import gt from 'gettext'
import guardModel from '@/io.ox/guard/core/guardModel'
import auth from '@/io.ox/guard/auth'
import core from '@/io.ox/guard/oxguard_core'
import { createIllustration } from '$/io.ox/core/components'

function getType (baton) {
  if (baton.data.security_info && baton.data.security_info.type) { return baton.data.security_info.type }
  return 'unkown'
}

function getPrompt (baton) {
  // Generic
  if ((core.hasGuardMail() && !core.hasSmime()) || // Has guard-mail, but not smime
    (core.hasSmime() && !core.hasGuardMail()) || // Has smime, but not guard-mail
    (getType(baton) === 'PGP' && !core.hasSmimeEnabled())) { // Has both, but type is PGP and doesn't have smime enabled in settings
    return gt('Secure Email, enter your %s security password.', guardModel().getName())
  }
  if (getType(baton) === 'PGP') {
    return gt('PGP encrypted email, enter the password for the PGP key.')
  }
  if (getType(baton) === 'SMIME') {
    return gt('S/MIME encrypted email, enter the password for the S/MIME key.')
  }
  return gt('Uncertain encryption type')
}

const drawDiv = async function (baton, goFunction) {
  const div = $('<div class="guard_found">')
  const iconDiv = $('<div class="secure_email_icon">').append(
    createIllustration('./io.ox/guard/core/icons/img_guard_locked.svg', { width: 160, height: 160 }).css('opacity', '100%')
  )
  const title = $('<div class="title">').append(gt('Secure Email'))
  const prompt = $('<div class="prompt">').append(gt('This mail was encrypted by the sender.  Your %s password is required to read it.', guardModel().getName()))
  const button = $('<button class="btn btn-primary oxguard_passbutton" type="button">').append(gt('Read mail'))
  button.on('click', function () {
    auth.authorize(baton, { optPrompt: getPrompt(baton), minSingleUse: true, type: getType(baton) }).then(function () {
      goFunction()
    })
  })
  const buttonDiv = $('<div class="prompt_button">').append(button)
  div.append(iconDiv, title, prompt, buttonDiv)
  return div
}

export default drawDiv
