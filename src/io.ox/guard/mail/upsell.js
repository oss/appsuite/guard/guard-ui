/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import http from '@/io.ox/guard/core/og_http'
import ox from '$/ox'
import $ from '$/jquery'

let adding = false

// Add upsell message to header of email
function upsell (location) {
  const params = '&cid=' + ox.context_id + '&id=' + ox.user_id + '&lang=' + ox.language
  if (adding) return
  adding = true
  http.get(ox.apiRoot + '/oxguard/mail?action=upsell', params)
    .done(function (data) {
      const upsellDiv = $('<div class="guardUpsell"></div>').append(data)
      location.find('.detail-view-header').after(upsellDiv)
    })
    .always(function () {
      adding = false
    })
}

export default upsell
