/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import _ from '$/underscore'
import { PasswordView } from '@/io.ox/guard/core/passwordView'
import guardModel from '@/io.ox/guard/core/guardModel'
import { renderTimeOptions } from '@/io.ox/guard/core/passwordPromptDialog'
import core from '@/io.ox/guard/oxguard_core'
import openSettings from '$/io.ox/settings/util'
import gt from 'gettext'
import yell from '$/io.ox/core/yell'
import '@/io.ox/guard/mail/style.scss'

// RECEIVE / READ

// Unified inbox combines folder and email ID, returns email id
function getId (baton) {
  let id = decodeURIComponent(baton.data.id)
  try {
    id = id.substring(id.lastIndexOf('/') + 1) + (baton.view.cid ? baton.view.cid : '')
  } catch (e) {
    console.error(e)
  }
  return (id)
}

function getType (baton) {
  if (baton.data.security_info && baton.data.security_info.type) { return baton.data.security_info.type }
  return 'unkown'
}

function getPrompt (baton) {
  // Generic
  if ((core.hasGuardMail() && !core.hasSmime()) || // Has guard-mail, but not smime
      (core.hasSmime() && !core.hasGuardMail()) || // Has smime, but not guard-mail
      (getType(baton) === 'PGP' && !core.hasSmimeEnabled())) { // Has both, but type is PGP and doesn't have smime enabled in settings
    return gt('Secure Email, enter your %s security password.', guardModel().getName())
  }
  if (getType(baton) === 'PGP') {
    return gt('PGP encrypted email, enter the password for the PGP key.')
  }
  if (getType(baton) === 'SMIME') {
    return gt('S/MIME encrypted email, enter the password for the S/MIME key.')
  }
  return gt('Uncertain encryption type')
}

function mailPasswordPrompt (baton, badpass, location, goFunction, localOnly) {
  const id = getId(baton)
  const passwordBoxView = new PasswordView({ id: 'oxgrpass' + id, class: 'password_prompt', validate: false })
  const passwordbox = passwordBoxView.render().$el.css('display', 'inline')
  const grdPasswordPrompt = $('<div class="alert og_password"/>')
  // #. %s product Name
  const mainpassprompt = $('<label style="font-weight:bold;" for="oxgrpass' + id + '">' + getPrompt(baton) + '</span><br/><div style="height:10px;"/>')
  if (_.device('small')) {
    passwordbox.css('width', '150px')
  }
  const noSaveWorkAround = $('<input style="display:none" type="text" name="dontremember"/><input style="display:none" type="password" name="dontrememberpass"/>')
  const passButton = $('<button class="btn btn-primary oxguard_passbutton" type="button">').text(gt('OK'))
  const placeholder = $('<span></span><br/>')
  // #. 'Keep me logged into guard' is followed by dropdown box specifying number of minutes or indefinite, %s product name

  const rememberpass = $('<div>').append(renderTimeOptions(baton.model, 'duration', gt('Duration')))

  const forgotDiv = $('<div class="og_forgot" style="display:none">')

  if (guardModel().get('recoveryAvail')) { // Display password help if available
    const forgotLink = $('<a href="#">').append(gt('Forgot Password'))
    forgotLink.on('click', function (e) {
      e.preventDefault()
      openSettings('virtual/settings/io.ox/guard')
    })
    forgotDiv.append(forgotLink)
  }

  const cont = $('<br/><div class="oxguard_error" id = "error' + id + '" style="display:none;"></div>')

  passButton.on('click', function () {
    go()
  })

  grdPasswordPrompt.append(noSaveWorkAround)
    .append(mainpassprompt.append(passwordbox).append(passButton))
    .append(placeholder)
    .append(rememberpass)
    .append(forgotDiv)
    .append(cont)

  grdPasswordPrompt.keydown(function (e) {
    if (e.which === 13) {
      go()
    }
  })

  window.setTimeout(function () {
    passwordbox.removeAttr('readonly')
  }, 500)

  const contdiv = $('<div class="content" id = "content' + id + '"/>').append(grdPasswordPrompt)

  function go () {
    const password = passwordBoxView.getValue()
    if (password === undefined) return
    let duration = -1
    const model = baton.model
    if (model.get('rememberpass')) {
      duration = model.get('duration')
    }
    if (localOnly) {
      goFunction(password, duration)
      return
    }
    passwordbox.parent().busy()
    passButton.prop('disabled', true)
    core.savePassword(password, duration, getType(baton))
      .done(function () {
        goFunction(password)
      })
      .fail(function (data) {
        $('#busygif').hide()
        passwordbox.parent().idle()
        passButton.prop('disabled', false)
        if (data.error && data.code === 'GRD-MW-0005') {
          yell('error', gt('Temporary Lockout'))
        } else {
          yell('error', gt('Bad password'))
          if (guardModel().get('recoveryAvail')) $('.og_forgot').show()
        }
      })
  }
  return (contdiv)
}

export default {
  passwordPrompt: mailPasswordPrompt,
  getId
}
