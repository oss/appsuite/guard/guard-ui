/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ox from '$/ox'
import ModalDialog from '$/io.ox/backbone/views/modal'
import gt from 'gettext'
import guardModel from '@/io.ox/guard/core/guardModel'
import capabilities from '$/io.ox/core/capabilities'
import pin from '@/io.ox/guard/mail/pin'
import '@/io.ox/guard/mail/style.scss'

// Display options for guest account header (introduction)
function guestOptions (baton) {
  const def = $.Deferred()
  new ModalDialog({
    async: true,
    point: 'io.ox/guard/mail/guestPrompt',
    id: 'guestPrompt',
    title: gt('Recipient Options'),
    width: 490,
    data: {
      baton,
      def
    }
  })
    .extend({
      body () {
        const data = this.options.data
        const help = $('<p>' + gt('At least one of your recipients uses a different webmail system.  They will receive an email with a link for them to read this message.  If you like, you can add a personalized message that will appear at the top of this message.  This will help identify the message as coming from you.') + '</p>')
        const messageLabel = $('<label class="oxguard_message_label">' + gt('Message:') + '</label>')
        data.message = $('<textarea type="text" id="message" class="og_guest_message"></textarea>')
        const languageLabel = $('<br/><label style="width:100%; padding-top:10px;">' + gt('Language for recipient Email') + '</label>')
        data.langselect = $('<select class="og_language"></select>')
        const warning = $('<p class="oxguard_warn" style="margin-top:10px;">').append(gt('Note: Emails sent to mailing lists or groups will not work.'))
        // Populate language selector with languages avail
        const languages = guardModel().get('lang')
        $.each(languages, function (c, n) {
          data.langselect.append('<option value="' + c + '">' + n + '</option>')
        })
        this.$body.append(help).append(messageLabel).append(data.message).append(languageLabel.append(data.langselect)).append(warning)
      },
      setup () {

      }
    })
    .addButton({ label: gt('OK'), action: 'ok' })
    .addCancelButton()
    .on('ok', function () {
      const message = this.options.data.message
      const data = {
        message: message.val(),
        language: this.options.data.langselect.val()
      }
      const def = this.options.data.def
      this.close()
      // Check if requires PIN to be displayed
      if (capabilities.has('guard-pin')) {
        pin.showpin(baton)
          .done(function (e) {
            data.pin = e
            def.resolve(data)
          })
          .fail(function () {
            def.resolve(data)
          })
      } else def.resolve(data)
    })
    .on('cancel', function () {
      this.options.data.def.reject()
      baton.view.app.getWindow().idle()
    })
    .on('open', function () {
      const urlLang = ox.language
      if ($('.og_language option[value="' + urlLang + '"]').length > 0) {
        this.options.data.langselect.val(urlLang)
      } else {
        this.options.data.langselect.val(urlLang.substring(0, 2))
      }
    })
    .open()

  return def
}

export default {
  guestOptions
}
