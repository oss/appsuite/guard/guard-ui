/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import _ from '$/underscore'
import ext from '$/io.ox/core/extensions'
import gt from 'gettext'
import Dropdown from '$/io.ox/backbone/mini-views/dropdown'
import recip from '@/io.ox/guard/mail/oxguard_mail_compose_recip'
import core from '@/io.ox/guard/oxguard_core'
import util from '@/io.ox/guard/util'
import auth from '@/io.ox/guard/auth'
import options from '@/io.ox/guard/mail/options_new'
import ToggleEncryption from '@/io.ox/guard/mail_lock/toggle-encryption'
import HelpView from '$/io.ox/backbone/mini-views/helplink'
import keyman from '@/io.ox/guard/mail/keymanager'
import SecurityModel from '@/io.ox/guard/mail/security_model'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import composeAPI from '$/io.ox/mail/compose/api'
import guardModel from '@/io.ox/guard/core/guardModel'
import yell from '$/io.ox/core/yell'
import capabilities from '$/io.ox/core/capabilities'
import { createIcon } from '$/io.ox/core/components'
import '@/io.ox/guard/mail/mailReplyActions'
import '@/io.ox/guard/mail/style.scss'

ext.point('io.ox/mail/compose/composetoolbar-mobile').replace({
  id: 'security',
  index: 100,
  draw (baton) {
    const node = this
    if (_.device('!smartphone')) return
    if (!util.hasCryptoCapability()) return
    // Create security model
    const securityModel = new SecurityModel(baton.model.get('security'), baton.model)
    baton.view.securityModel = securityModel
    const view = new ToggleEncryption.View({
      mailModel: baton.model,
      model: securityModel
    })
    // Different menus for s/mime.  When changed, redraw
    baton.view.securityModel.on('change:smime', function () {
      baton.config.set('gkeys', undefined)
      keyman.checkAll(baton)
      drawDropDownMobile(baton, node)
    })
    view.noLinkMail(baton.view)
    baton.view.toggleEncryption = view
    node.find('.sender').append(view.render().$el)
    // Draw items
    setupCompose(baton)
    node.append(node)
    drawDropDownMobile(baton, node)
  }
})

// Desktop only
ext.point('io.ox/mail/compose/fields').extend({
  id: 'info-and-options',
  index: 400,
  draw (baton) {
    if (_.device('smartphone')) return
    if (!util.hasCryptoCapability()) return

    // row
    const that = this
    const node = $('<div class="guard-info-line">').append(
      createIcon('bi/lock.svg'),
      $('<span class="text">').text(gt('This email will be encrypted.')).attr('title', gt('This email will be encrypted.'))
    ).addClass('stripes-green encrypted')
    that.append(node)
    // toggle
    update()
    baton.view.securityModel.on('change:encrypt', update)
    baton.view.securityModel.on('change:smime', update)
    baton.model.on('change:gkeys', update)
    function update () {
      if (core.hasPGPandSmime() && baton.view.securityModel.get('encrypt')) {
        const smime = baton.view.securityModel.get('smime')
        node.find('.text').html(smime ? gt('This email will be encrypted with S/MIME') : gt('This email will be encrypted with PGP'))
      } else {
        node.find('.text').html(gt('This email will be encrypted.'))
      }

      const keys = baton.config.get('gkeys')
      if (keys) {
        let fail = false
        for (const email in keys) {
          if (keys[email].result === 'fail') fail = true
        }
        node.toggleClass('stripes-green', !fail).toggleClass('stripes-red', fail)
        if (fail) {
          node.find('.text').html(gt('Unable to encrypt until all recipient keys available.'))
        }
      }
      node.toggleClass('hidden', !baton.view.securityModel.get('encrypt'))
    }

    // dropdown
    if (!guardModel().hasGuardMail() && !guardModel().hasSmime()) return
    const dropdown = drawDropdown(baton)
    node.append(dropdown)
    baton.view.securityModel.on('change:smime', function () {
      baton.config.set('gkeys', undefined)
      baton.model.save()
      keyman.checkAll(baton)
      $('.pgpDropdownOptions').remove()
      node.append(drawDropdown(baton))
    })
  }
})

function drawDropdown (baton) {
  const dropdown = new Dropdown({
    model: baton.view.securityModel,
    label: gt('Options'),
    caret: true
  })
    .header(gt('Security'))
    .option('sign', true, gt('Sign email'))
  if (!baton.view.securityModel.get('smime')) {
    dropdown.option('PubKey', true, gt('Attach my public key'))
  }
  if (settings.get('advanced')) {
    if (!baton.view.securityModel.get('smime')) {
      dropdown
        // #. Format of the email, HTML or Plaintext
        .header(gt('PGP Format'))
        .option('pgpInline', false, gt('Mime'))
        .option('pgpInline', true, gt('Inline'))
    }
  }
  //  Help icon
  dropdown.render().$el
    .addClass('security-options')
    .find('ul').prepend(
      new HelpView({
        base: 'help',
        href: 'ox.appsuite.user.sect.guard.email.send.html',
        tabindex: '-1'
      }).render().$el.addClass('guard-help-icon')
    )
  dropdown.$el.find('[data-name="pgpInline"]').attr('data-toggle', false)
  return dropdown.$el.addClass('pgpDropdownOptions')
}

ext.point('io.ox/mail/compose/composetoolbar').extend({
  id: 'lock',
  index: 900,
  draw (baton) {
    if (_.device('smartphone')) return
    // TODO check for encr reply
    if (!util.hasCryptoCapability()) return
    const container = $('<li role="presentation">')
    const securityModel = new SecurityModel(baton.model.get('security'), baton.model)
    const view = new ToggleEncryption.View({
      tagName: 'a',
      model: securityModel,
      mailModel: baton.model
    })

    baton.view.securityModel = securityModel
    baton.view.toggleEncryption = view
    view.noLinkMail(baton.view)
    if (view.isEnabled()) {
      this.append(
        container.append(view.render().$el)
      )
    }
    setupCompose(baton)
  }
})

ext.point('io.ox/mail/compose/menuoptions').extend({
  id: 'guard-options',
  index: 400,
  draw (baton) {
    if (_.device('smartphone')) return
    if (!util.hasCryptoCapability()) return

    const intermediateModel = this.data('view').model
    const securityModel = baton.view.securityModel;

    // sync updates of intermediateModel (dropdown) and securityModel
    ['sign', 'PubKey', 'smime'].forEach(function (key) {
      intermediateModel.set(key, securityModel.get(key))
      intermediateModel.on('change:' + key, function name () {
        securityModel.set(key, intermediateModel.get(key))
      })
      securityModel.on('change:' + key, function name () {
        intermediateModel.set(key, securityModel.get(key))
      })
    })

    const node = this.data('view')
      .divider()
      .header(gt('Security'))
    if (core.hasPGPandSmime()) {
      node.option('smime', true, gt('Use S/MIME'), { radio: true })
      node.option('smime', false, gt('Use PGP'), { radio: true })
    }
    node.option('sign', true, gt('Sign email'))
    if (core.hasGuardMail()) {
      node.option('PubKey', true, gt('Attach my public key'))
      securityModel.on('change:smime', function (m) {
        node.$el.find('[data-name="PubKey"]').css('display', m.get('smime') ? 'none' : 'block')
      })
    }
    node.$ul.find('[data-name="smime"]').attr('data-toggle', 'false')
    node.$ul.find('[data-name="pgpInline"]').attr('data-toggle', 'false')
  }
})

function setupCompose (baton) {
  // Check the options for the page. Lock icon settings, etc
  options.createOptions(baton)
}

// Check if attachment is PGP file, and set email to encrypt if found
function checkEncrAttachmentAdded (val, baton) {
  if ((val.get('file_mimetype') === 'application/pgp-enrypted') ||
    (val.get('filename') && val.get('filename').indexOf('.pgp') > 0)) {
    if (capabilities.has('guard-mail')) {
      const o = {
        type: 'pgp', // only support pgp attachments
        failAuthCallback: function () {
          baton.model.get('attachments').remove(val)
        }
      }
      auth.authorize(undefined, o).then(function (authcode) {
        baton.view.securityModel.set('authToken', authcode)
        baton.view.securityModel.set('encrypt', true)
      })
    }
  }
}

// Add to attachment extensions monitor to see if attaching PGP file
ext.point('io.ox/mail/compose/attachments').extend({
  id: 'checkPGPAttachment',
  index: 200,
  draw (baton) {
    baton.model.get('attachments').on('add', function (val) {
      checkEncrAttachmentAdded(val, baton)
    })
  }
})

// Token and key handling

ext.point('io.ox/mail/compose/createtoken').extend({
  id: 'guardToken',
  action (baton) {
    if (baton.view.securityModel.get('encrypt') === true) {
      const target = $(baton.event.relatedTarget)
      const email = baton.event.attrs.model.get('token').value
      // OK, check the keys
      keyman.checkRecips(email, baton)
        .done(function (keyResults) {
          recip.drawKeyIcons(keyResults[email], target)
        })
      // Handle tooltip with keyboard nav
      target.on('focus', function () {
        if (target.hasClass('grabbed')) {
          // Don't show if grabbed
          target.find('.oxguard_token').tooltip('hide')
        } else {
          target.find('.oxguard_token').tooltip('show')
        }
      })
      target.on('blur', function () {
        target.find('.oxguard_token').tooltip('hide')
      })
      // If moving token around, hide tooltip
      target.on('keyup', function (e) {
        if (e.which === 32) {
          target.find('.oxguard_token').tooltip('hide')
        }
      })
    }
  }
})

// If token removed, then remove it from our list of gkeys
ext.point('io.ox/mail/compose/removetoken').extend({
  id: 'guardDelToken',
  action (baton) {
    if (baton.config.get('gkeys')) {
      const email = baton.event.attrs.model.get('token').value
      keyman.deleteRecip(email, baton)
    }
  }
})

// Send extension points

ext.point('io.ox/mail/compose/actions/send').extend(
  {
    // Check if all keys have been checked
    id: 'keysDoneCheck',
    index: 100,
    before: 'busy:start',
    perform (baton) {
      if (!baton.view.securityModel.get('encrypt')) return true
      const def = $.Deferred()
      if (isReady(baton)) {
        def.resolve()
      } else {
        waitUntilReady(baton, def)
      }
      return (def)
    }
  },
  {
    // Check if password needed for signing
    id: 'signCheck',
    index: 200,
    before: 'busy:start',
    perform (baton) {
      const def = $.Deferred()
      const security = baton.model.get('security')
      if (security.sign) {
        let prompt = gt('Please enter your %s password to sign this email.', guardModel().getName())
        if (util.hasGuardMailCapability() && util.hasSmime()) { // If has both types, get specific reference
          if (security.type.toLowerCase() === 'pgp') {
            prompt = gt('Please enter your PGP password to sign this email.')
          }
          if (security.type.toLowerCase() === 'smime') {
            prompt = gt('Please enter your S/MIME password to sign this email.')
          }
        }
        const options = {
          optPrompt: prompt,
          minSingleUse: true,
          type: security.type
        }
        auth.authorize(baton, options)
          .done(function (auth) {
            const security = baton.model.get('security')
            security.authToken = auth
            baton.model.set('security', security)
            if (baton.model.get('security').encrypt) {
              checkKeys(baton, def)
            } else {
              def.resolve()
            }
          })
          .fail(function () {
            baton.stopPropagation()
            def.reject()
          })
      } else if (security.encrypt) {
        checkKeys(baton, def)
      } else {
        def.resolve()
      }
      return (def)
    }
  },
  {
    // Check if encrypted files attached that need password for decryption
    id: 'encryptedFiles',
    index: 300,
    before: 'busy:start',
    perform (baton) {
      if (!baton.view.securityModel.get('encrypt')) return true
      const security = baton.model.get('security')
      const def = $.Deferred()
      const attachments = baton.model.get('attachments')
      let found = false
      if (security.authToken) { // we already have authentication
        def.resolve()
        return
      }
      attachments.models.forEach(function (attach) {
        if (found) return // Only prompt once
        const mimeType = attach.get('file_mimetype')
        const fileName = attach.get('filename')
        if ((mimeType && (mimeType.indexOf('pgp-encrypted') > -1)) || (fileName && (fileName.indexOf('.pgp') > -1))) {
          found = true
          const options = {
            optPrompt: gt('There appears to be an encrypted attachment.  Please enter your %s password to decode the attachment for sending.', guardModel().getName()),
            minSingleUse: true
          }
          auth.authorize(baton, options)
            .done(function (auth) {
              security.authToken = auth
              baton.model.set('security', security)
              def.resolve()
            })
            .fail(function () {
              def.reject()
            })
        }
      })
      if (!found) def.resolve()
      return (def)
    }
  },
  {
    // Check for authToken errors
    id: 'checkAuthErrors',
    index: 2900,
    after: 'send',
    perform (baton) {
      if (baton.error && containsError(baton.errorCode, baton.error)) {
        handleFail(baton)
        handleReauth(gt('Please re-enter your %s password before sending.', guardModel().getName()), baton)
          .done(function () {
            updateAuth(baton.model).then(function () {
              baton.view.send()
            })
          })
        return new $.Deferred().reject()
      }
    }
  }
)

function updateAuth (model) {
  const security = model.get('security')
  return composeAPI.space.update(model.get('id'), { security })
}

// Save extension points

ext.point('io.ox/mail/compose/actions/save').extend({
  id: 'checkAuthErrors',
  index: 1050,
  perform (baton) {
    if (baton.error && containsError(baton.errorCode, baton.error)) {
      handleFail(baton)
      handleReauth(gt('Please re-enter your %s password before saving.', guardModel().getName()), baton)
        .done(function () {
          updateAuth(baton.model)
        })
      baton.stopPropagation()
      return new $.Deferred().reject()
    }
  }
})

// GET response event listener
ext.point('io.ox/mail/compose/actions/get/error').extend({
  index: 100,
  id: 'guardAuthError',
  handler (baton) {
    if (baton.result && containsError(baton.result.code, baton.result.error)) {
      baton.handled = $.Deferred()
      return handleReauth(gt('Please enter your %s password to edit this encrypted draft.', guardModel().getName()), baton)
        .then(function () {
          // TODO: this needs proper converters and serializers/deserializers for compose model <=> guard model <=> JSON
          const security = baton.model.get('security')
          return composeAPI.space.update(baton.model.get('id'), { security })
        }).then(function () {
          composeAPI.space.get(baton.model.get('id')).then(function (data) {
            const security = baton.model.get('security')
            data.security = security
            baton.handled.resolve(data)
          })
        }).catch(function (error) {
          baton.handled.reject(error)
        })
    }
  }
})

let reauthorizing = false

ext.point('io.ox/mail/compose/boot').extend({
  index: 'last',
  id: 'guardAuthSaveError',
  perform (baton) {
    baton.model.on('fail:save', function (e) {
      if (e.error && e.code) {
        if (containsError(e.code, e.error)) {
          const model = this
          handleReauth(gt('Please re-enter your %s password before saving.', guardModel().getName()), baton)
            .done(function () {
              updateAuth(model).then(function () {
                model.save()
              })
            })
        }
      }
    })
  }
})

ext.point('io.ox/mail/compose/autosave/error').extend({
  index: 1,
  id: 'guardAuthError',
  handler (baton) {
    if (reauthorizing) return
    if (containsError(baton.code, baton.error)) {
      baton.preventDefault()
      if (reauthorizing) return
      reauthorizing = true
      const security = baton.model.get('security')
      const options = {
        optPrompt: gt('Please re-enter your %s password.', guardModel().getName()),
        minSingleUse: true,
        forceRelogin: true
      }
      auth.authorize(baton, options)
        .done(function (auth) {
          if (security) { // Update security authorization
            security.authToken = auth
            baton.view.model.set('security', security)
          }
          baton.view.autoSaveDraft() // Try again
        })
        .always(function () {
          reauthorizing = false
          baton.returnValue.reject() // Reject the action as needed re-auth
        })
    }
  }
})

// Functions

// Drawing lock icon and options for mobile
function drawDropDownMobile (baton, node) {
  if (guardModel().hasGuardMail() || guardModel().hasSmime()) {
    const lockIcon = function () {
      const encrypted = baton.view.securityModel.get('encrypt')
      return createIcon(encrypted ? 'bi/lock.svg' : 'bi/unlock.svg').addClass('bi-22')
    }
    let $icon
    const $toggle = $('<button type="button" class="btn btn-link dropdown-toggle mr-6" data-toggle="dropdown">')
      .attr('aria-label', gt('Security'))
      .append(
        $icon = $('<div aria-hidden="true">').attr({ title: gt('Security') }).append(
          lockIcon()
        )
      )
    baton.view.securityModel.on('change:encrypt', function () {
      $icon.html(lockIcon())
    })
    const dropdown = new Dropdown({ model: baton.view.securityModel, label: gt('Security'), $toggle })
    dropdown
      .header(gt('Security'))
    if (core.hasPGPandSmime()) {
      dropdown.option('smime', true, gt('Use S/MIME'))
      dropdown.option('smime', false, gt('Use PGP'))
    }
    dropdown
      .option('encrypt', true, (settings.get('advanced') ? gt('Encrypt') : gt('Secure')))
      .option('sign', true, gt('Sign'))
    if (core.hasGuardMail() && settings.get('advanced') && !baton.view.securityModel.get('smime')) {
      dropdown
        // #. Format of the email, HTML or Plaintext
        .header(gt('Format'))
        .option('pgpInline', false, gt('PGP Mime'))
        .option('pgpInline', true, gt('PGP Inline'))
      dropdown
        .header(gt('Keys'))
        .option('PubKey', true, gt('Attach my key'))
    }
    node.append(dropdown.render().$el.addClass('security-mobile'))
    node.find('[data-name="smime"]').attr('data-toggle', 'false')
    node.find('[data-name="pgpInline"]').attr('data-toggle', 'false')
  }
}

// Check if any of the recipients are Guest and prompt for greeting/password
function checkKeys (baton, def) {
  const keys = baton.config.get('gkeys')
  let isGuest = false
  let fail = false
  for (const email in keys) {
    if (keys[email].result === 'guest') isGuest = true
    if (keys[email].result === 'fail') fail = true
  }
  if (fail) {
    yell('error', gt('Unable to find keys for all recipients.'))
    baton.stopPropagation()
    def.reject()
    return
  }
  if (!isGuest) {
    def.resolve()
    return
  }
  import('@/io.ox/guard/mail/guestPrompt').then(({ default: prompt }) => {
    prompt.guestOptions(baton)
      .done(function (data) {
        const security = _.extend(baton.model.get('security'), data)
        baton.model.set('security', security)
        def.resolve()
      })
      .fail(function () {
        baton.stopPropagation()
        def.reject()
      })
  })
}

// Check all keys ready
function isReady (baton) {
  const keys = baton.config.get('gkeys')
  if (keys === undefined) return true // No keys
  let pending = false
  for (const email in keys) {
    pending = pending || keys[email].pending
  }
  return !pending
}

function containsError (code, string) {
  if (code === 'GRD-0002' || code === 'GRD-AUTH-0002' || code === 'GRD-MW-0001' || code === 'SMIME-0005') return true // Midleware Auth error
  return (string && string !== null &&
    ((string.indexOf('Missing authentication') > -1) || (string.indexOf('GRD-AUTH-0002') > -1 || string.indexOf('GRD-PGP-0005') > -1) || (string.indexOf('ENCRYPT-0006') > -1)))
}

function handleFail (baton) {
  if (baton.error && !baton.warning) {
    baton.stopPropagation()
    const win = baton.app.getWindow()
    if (win) {
      // reenable the close button in toolbar
      if (baton.closelink) {
        baton.closelink.show()
      }
      win.idle().show()
    }
    baton.app.launch()
  }
}

function handleReauth (prompt, baton) {
  guardModel().clearAuth() // wipe stored auth token
  const def = $.Deferred()
  const security = baton.model.get('security')
  const options = {
    optPrompt: prompt,
    minSingleUse: true,
    forceRelogin: true,
    type: security.type
  }
  auth.authorize(baton, options) // authorize and force relogin
    .done(function (auth) {
      security.authToken = auth
      baton.model.set('security', security)
      baton.stopPropagation()
      def.resolve()
    })
    .fail(function (data, e) {
      // return a ox error object (fallback build one)
      const error = _.extend({ code: 'unknown', error: gt('An error occurred. Please try again.') }, e || {})

      // adjust error message to be less technical
      if (data === 'cancel') _.extend(error, { code: 'GRD-UI-0001', error: gt('Authorization failed as password prompt was closed by user') })
      if (error.code === 'GRD-MW-0003') error.message = gt('Bad password')
      if (error.code === 'GRD-MW-0005') error.message = gt('Account Locked out')

      // flag error objects that (temporary) breaks access to composition spaces
      if (/^(GRD-UI-0001|GRD-MW-0003|GRD-MW-0005)$/.test(error.code)) error.critical = true

      baton.stopPropagation()
      def.reject(error)
    })
  return def
}

// If not all keys returned, wait until completed.
// Timeout if no backend response
function waitUntilReady (baton, def) {
  baton.waiting = true
  const win = baton.view.app.getWindow()
  win.busy()
  baton.model.on('change:gkeys', function () {
    if (isReady(baton)) {
      win.idle()
      def.resolve()
      baton.waiting = false
    }
  })
  window.setTimeout(function () {
    if (baton !== undefined) {
      if (baton.waiting === true) {
        win.idle()
        baton.waiting = false
        def.resolve()
      }
    }
  }, 15000)
}
