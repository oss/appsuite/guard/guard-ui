/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from '$/underscore'
import $ from '$/jquery'
import gt from 'gettext'
import ext from '$/io.ox/core/extensions'
import guardModel from '@/io.ox/guard/core/guardModel'
import yell from '$/io.ox/core/yell'
import util from '@/io.ox/guard/util'
import SecurityModel from '@/io.ox/guard/mail/security_model'

ext.point('io.ox/mail/compose/boot').extend({
  id: 'detectNewMail',
  index: 'first',
  perform (baton) {
    // returns here if any other type than new (e.g. replies etc)
    if (baton.data && baton.data.type && baton.data.type.toLowerCase() !== 'new') return
    // returns here, if restorepoint or copy of draft is opened
    if (baton.data && baton.data.id) return
    baton.config = _.extend({}, baton.config, { newmail: true })
  }
})

function checkRestricted (baton) {
  if (baton.model.get('security') && baton.view.securityModel) {
    if (baton.model.get('security').encrypt && !baton.view.securityModel.get('smime')) { // if pgp reply, check if forced encryption
      if (!util.hasGuardMailCapability() || util.isGuest()) {
        baton.view.toggleEncryption.forceEncryption()
        disableRecipients(baton.view)
      }
    }
  }
}

ext.point('io.ox/mail/compose/boot').extend({
  id: 'checkSecuritySettings',
  index: 2001,
  perform (baton) {
    if (baton.data.type === 'edit') {
      checkEdit(baton)
      checkRestricted(baton)
      return
    }
    if (baton.data.original) {
      let original = baton.data.original
      if (_.isArray(baton.data.original) && baton.data.original.length > 0) {
        original = baton.data.original[0]
      }
      if (original.security && original.security.decrypted) {
        if (!baton.view?.securityModel) baton.view.securityModel = new SecurityModel(baton.model.get('security'), baton.model)
        baton.view.securityModel.set('smime', original.security.type === 'SMIME')
        baton.view.securityModel.set('authToken', original.security.authentication, true)
        baton.view.securityModel.set('encrypt', true)
      }
    }

    if (baton.data.type === 'reply' || baton.data.type === 'replyall' || baton.data.type === 'forward') {
      checkReply(baton)
      checkRestricted(baton)
      return
    }
    checkEncrFile(baton)
    if (baton.config && baton.config.newmail) return // New mail, no previous settings

    if (baton.model.get('security')) {
      if (!baton.view?.securityModel) baton.view.securityModel = new SecurityModel(baton.model.get('security'), baton.model)
      baton.view.securityModel.set('smime', baton.model.get('security').type === 'smime')
      baton.view.securityModel.set('sign', baton.model.get('security').sign, { silent: true })
      baton.view.securityModel.set('pgpInline', baton.model.get('security').pgpInline)
      checkRestricted(baton)
    }
  }
})

function toggleScheduledSend (config, security) {
  const isGuardEnabled = !!(security.get('sign') || security.get('encrypt'))
  config.set('scheduled_mail', isGuardEnabled ? false : config.get('scheduled_mail'))
}

ext.point('io.ox/mail/compose/boot').extend({
  id: 'scheduled-send',
  index: 2010,
  perform () {
    const { config, view } = this
    const security = view.securityModel

    // At this time, Guard does not support scheduled send. Disable if encrypt or sign
    view.listenTo(config, 'change:scheduled_mail', () => toggleScheduledSend(config, security))
    view.listenTo(security, 'change', () => toggleScheduledSend(config, security))
  }
})

function getOriginal (baton) {
  if (_.isArray(baton.data.original)) {
    return baton.data.original[0]
  }
  return baton.data.original
}

function checkEncrFile (baton) {
  if (!baton.data) return
  const atts = baton.data.attachments
  if (atts) {
    atts.forEach((att) => {
      if (att.encrypted) {
        baton.view.securityModel.set('encrypt', true)
      }
    })
  }
}

// If loading a draft email, need to set security settings same as before
function checkEdit (baton) {
  const obj = getOriginal(baton)
  if (obj && obj.headers && obj.headers['X-Security']) {
    const reply = obj.headers['In-Reply-To'] !== undefined // If this is editing a reply
    const securitySettings = _.isArray(obj.headers['X-Security']) ? obj.headers['X-Security'][0] : obj.headers['X-Security']
    const settings = securitySettings.split(';')
    settings.forEach(function (setting) {
      const s = setting.split('=')
      const value = (s[1] ? (s[1].indexOf('true') > -1) : false)
      switch (s[0].trim()) {
        case 'sign':
          baton.view.securityModel.set('sign', value)
          break
        case 'encrypt':
          baton.view.securityModel.set('encrypt', value)
          if (value === true && reply) { // Check if we should enforce the secure reply here
            if (guardModel().getSettings().secureReply) {
              baton.view.toggleEncryption.forceEncryption()
            }
          }
          break
        case 'pgpInline':
          if (baton.view.securityModel.get('encrypt')) baton.view.securityModel.set('pgpInline', value)
          break
        default:
          console.error('Unknown security parameter ' + s[0])
      }
    })
    if (obj.security.authToken) {
      baton.view.securityModel.set('authToken', obj.security.authToken)
    }
    baton.model.unset('security_info')
  }
}

// When doing reply/forward, check essential security data moved to new model
function checkReply (baton) {
  const obj = getOriginal(baton)
  const securityModel = baton.view.securityModel
  if (obj && obj.security && obj.security.decrypted) {
    securityModel.set('encrypt', true)
    const security = baton.model.get('security')
    security.encrypt = true
    if (obj.security.authToken) {
      security.authToken = obj.security.authToken
    }
    const headers = baton.model.get('headers') || {}
    baton.model.set('headers', _.extend(headers, { 'X-Guard-msg-ref': obj.id }))
    security.msgRef = obj.id
    if (guardModel().getSettings().secureReply) {
      baton.view.toggleEncryption.forceEncryption()
    }
    baton.model.set('security', _.extend(baton.model.get('security'), { authToken: obj.security.authToken }))
  }
  baton.model.unset('security_info')
}

function disableRecipients (view) {
  view.$el.find('.recipient-actions').remove()
  view.$el.find('.tokenfield').css('background-color', '#e0e0e0')
  view.$el.find('.token-input').on('click', e => {
    $(e.currentTarget).blur()
    e.preventDefault()
    yell('error', gt('Recipients cannot be added to this encrypted reply'))
  })
  view.$el.find('.twitter-typeahead').hide()
}
