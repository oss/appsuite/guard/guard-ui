/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import extensions from '$/io.ox/mail/common-extensions'
import mailApi from '$/io.ox/mail/api'
import ext from '$/io.ox/core/extensions'
import '@/io.ox/guard/mail/style.scss'
import { createIcon } from '$/io.ox/core/components'

// Override default icon extension for encrypted icons
extensions.pgp.encrypted = function (baton) {
  if (
    !/^multipart\/encrypted/.test(baton.data.content_type) &&
    !/^application\/pkcs7-mime/.test(baton.data.content_type) &&
    !(baton.model.get('security_info') && baton.model.get('security_info').encrypted) &&
    !(baton.model.get('security') && baton.model.get('security').decrypted)
  ) return
  baton.data.text_preview = '' // wipe the preview
  this.append(
    // createIcon('bi/lock-fill.svg').addClass('guard_maillist encrypted')
    createIcon('./io.ox/guard/core/icons/lock.svg').addClass('guard_maillist encrypted')
  )
}

extensions.pgp.signed = function (baton) {
  // simple check for signed mail
  if (
    !/^multipart\/signed/.test(baton.data.content_type) &&
    !(baton.data.security && baton.data.security.signatures)
  ) return

  this.append(
    createIcon('bi/pencil-square.svg').addClass('guard_maillist signed')
  )
}

// Disable default paper clip, as all oxguard emails have attachments.  Will call manually.
ext.point('io.ox/mail/listview/item/default/row2').disable('paper-clip')
ext.point('io.ox/mail/listview/item/small').disable('col2')

ext.point('io.ox/mail/listview/item').extend({
  id: 'checkReload',
  index: 10,
  draw (baton) {
    const pooledMail = mailApi.pool.get('detail').get(baton.data.cid)
    if (pooledMail) {
      if (pooledMail.get('guardIcons')) { // PGP emails may have been already decrypted, etc
        baton.data.security = pooledMail.get('guardIcons').security
        baton.data.attachment = pooledMail.get('guardIcons').attachment
      }
    }
  }
})

ext.point('io.ox/mail/listview/item/default/row2').extend({
  id: 'OGpaper-clip',
  index: 300,
  draw (baton) {
    addLock.call(this, baton)
  }
})

ext.point('io.ox/mail/listview/item/small/col5').disable('paper-clip')
ext.point('io.ox/mail/listview/item/small/col5').extend({
  id: 'OGpaper-clipSm',
  index: 300,
  draw (baton) {
    addLock.call(this, baton)
  }
})

ext.point('io.ox/mail/listview/item/small').extend({
  id: 'OGpaper-col2',
  index: 200,
  draw (baton) {
    addLock.call(this, baton)
  }
})

// Check if OxGuard email, and if so, add lock icon.  Add paperclip if applicable
function addLock (baton) {
  try {
    if (baton.data.attachment === undefined) {
      return
    }
    if (baton.model === undefined) {
      extensions.paperClip.call(this, baton)
      return
    }
    // Encrypted, we don't know if attachments
    if (baton.model.get('security_info') && baton.model.get('security_info').encrypted) {
      if (!(baton.model.get('security') && baton.model.get('security').decrypted)) {
        return
      }
    }
    // PGP Components
    // Check if decoded
    if (baton.model.get('security') && baton.model.get('security').decrypted) {
      // encrypted mails have at least 1 attachment by default
      if (baton.model.get('attachment') === true && baton.model.get('attachments').length > 1) {
        extensions.paperClip.call(this, baton)
      }
      return
    }
    if (/multipart\/encrypted/.test(baton.model.get('content_type')) || /application\/pkcs7-mime/.test(baton.model.get('content_type'))) {
      return
    }
    extensions.paperClip.call(this, baton) // Otherwise, call paperclip
  } catch (ex) {
    console.log('Problem checking list for OG email')
    console.log(ex)
  }
}
