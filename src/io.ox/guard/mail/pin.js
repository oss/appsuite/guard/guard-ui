/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ModalDialog from '$/io.ox/backbone/views/modal'
import keyman from '@/io.ox/guard/mail/keymanager'
import gt from 'gettext'
import '@/io.ox/guard/mail/style.scss'

function showpin (baton) {
  const def = $.Deferred()
  new ModalDialog({
    async: true,
    point: 'io.ox/guard/mail/pinGen',
    title: gt('Advanced Security - PIN'),
    id: 'pinGen',
    width: 450,
    enter: 'ok',
    def
  })
    .extend({
      showPin () {
        const pin = getpin()
        this.pin = pin
        const message = $('<p>')
        const tableMessage = $('<p style="float:left;">' + gt('Recipients:') + '</p>')
        const table = $('<textarea cols="40" rows="3" style="float:right; padding:5px; margin-right:5%; max-width:90%;"/>')
        let names = '';
        (keyman.getGuests(baton)).forEach(function (v) {
          names = names + v + '\r\n'
        })
        table.text(names)
        message.append(gt('The new recipients listed below will be sent instructions how to read this email.  For additional security, you can assign a PIN that will be used for additional verification.'))
        const pinlabel = $('<label class="pin">' + pin + '</label>')
        const message2 = $('<p>').append(gt('You may deliver this pin any way you see fit, but we recommend doing it in a second method other than email (Phone, SMS, Skype, etc).'))
        const message3 = $('<p>').append(gt('If you forget this pin, you can find it in the sent email (requires decrypting).'))
        this.$body.append(message).append(pinlabel).append(message2).append(message3).append('<hr/>').append(tableMessage).append(table).css('max-height', '450px')
      }
    })
    .addButton({ label: gt('Use PIN'), action: 'ok' })
    .addButton({ label: gt('Skip'), action: 'cancel' })
    .on('ok', function () {
      this.options.def.resolve(this.pin)
      this.close()
    })
    .on('cancel', function () {
      this.options.def.reject()
      this.close()
    })
    .open()

  return def
}

function getpin () {
  const length = 4
  const num = Math.floor((Math.random() * (Math.pow(10, length))) + 1)
  return (pad(num, length))
}

function pad (n, width) {
  n = n.toString()
  return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n
}

export default {
  showpin
}
