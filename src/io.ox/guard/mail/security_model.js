/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import Backbone from '$/backbone'
import _ from '$/underscore'
import guardModel from '@/io.ox/guard/core/guardModel'
import yell from '$/io.ox/core/yell'
import gt from 'gettext'
import certs from '@/io.ox/guard/api/certs'
import core from '@/io.ox/guard/oxguard_core'
import auth from '@/io.ox/guard/auth'
import { settings } from '@/io.ox/guard/settings/guardSettings'

const SecurityModel = Backbone.Model.extend({
  initialize (init, mailModel) {
    init = _.extend({ encrypt: false, sign: false }, init)
    this.mailModel = mailModel
    this.set('sign', init.sign)
    this.set('encrypt', init.encrypt)
    this.set('pgpInline', init.pgpInline)
    this.set('authToken', init.authToken)
    this.set('smime', init.smime === true || init.type === 'smime')
    this.listenTo(this, 'change', this.onChange)
    if (!core.hasGuardMail() && core.hasSmime()) this.set('smime', true) // No pgp mail avail
    mailModel.set('security', this.toJson())
  },
  setMailModel (model) {
    this.mailModel = model
  },
  events: {
    change: 'onChange',
    set: 'onChange'
  },
  defaults: {
    encrypt: false,
    sign: false
  },
  toJson () {
    return _.extend(
      {}, (this.mailModel ? this.mailModel.get('security') : {}), {
        encrypt: this.get('encrypt'),
        sign: this.get('sign'),
        pgpInline: this.get('pgpInline'),
        authToken: this.get('authToken'),
        type: this.get('smime') ? 'smime' : 'pgp',
        encryptDraft: settings.get('encryptDraft'),
        addAutocrypt: settings.get('autocryptHeader')
      })
  },
  onChange (model) {
    if (this.mailModel) {
      // Encrypting
      if (this.get('encrypt')) {
        this.updateMailModel(model)
        if (guardModel().forceEncryptedDrafts() || settings.get('encryptDraft')) {
          if (!this.hasAuthToken()) {
            this.set('encrypt', false)
            this.checkAuth(model)
          } else {
            this.updateMailModel(model)
          }
        }
        if (this.get('smime')) {
          checkSmimeKeys(model)
        }
      } else {
        this.updateMailModel(model)
      }
      // Signing
      if (this.get('sign') && this.get('smime')) {
        checkSmimeKeys(model)
      }
    }
  },
  hasAuthToken: function () {
    if (this.get('smime')) {
      return this.get('authToken') && this.get('authToken').indexOf('SMIME') === 0
    } else {
      return this.get('authToken') && this.get('authToken').indexOf('PGP') === 0
    }
  },
  updateMailModel: function (model) {
    const mailModel = this.mailModel
    const savit = _.debounce(function () {
      mailModel.set('security', model.toJson())
      mailModel.save()
    }, 500)
    if (mailModel.requestSave) {
      savit()
    } else {
      window.setTimeout(function () {
        savit()
      }, 1000)
    }
  },
  checkAuth (model) {
    if (this.get('tour')) {
      return
    }
    const that = this
    const o = {
      type: model.get('smime') ? 'smime' : 'pgp'
    }
    auth.authorize(undefined, o).then(function (authcode) {
      model.set('authToken', authcode)
      model.set('encrypt', true)
      that.updateMailModel(model)
    }, function () {
      model.set('encrypt', false)
      that.updateMailModel(model)
    })
  },
  setInline () {
    this.set('pgpInline', true)
  },
  setMime () {
    this.set('pgpInline', false)
  }
})

// Check that we have the required smime keys for signature
function checkSmimeKeys (model) {
  const from = model.mailModel.get('from')
  const fromEmail = from[1]
  function check () {
    if (!guardModel().hasSmimeKeys()) {
      yell('error', gt('No S/MIME keys available.  Please add keys through the settings page.'))
      model.set('sign', false)
      model.set('encrypt', false)
      return
    }
    let match = false
    guardModel().getCerts().forEach(function (k) {
      if (k.email.toLowerCase().indexOf(fromEmail.toLowerCase() > -1)) {
        match = true
      }
    })
    if (!match) {
      yell(
        'error',
        gt('No Smime keys for this email address available for signing.  Please add the key through the settings page.'))

      model.set('sign', false)
    }
  }
  if (guardModel().hasSmimeKeys() === undefined) {
    certs.getCertificates().then(function () {
      check()
    })
  } else check()
}

export default SecurityModel
