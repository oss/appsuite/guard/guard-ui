/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ox from '$/ox'
import _ from '$/underscore'
import core from '@/io.ox/guard/oxguard_core'
import capabilities from '$/io.ox/core/capabilities'
import auth from '@/io.ox/guard/api/auth'
import guardModel from '@/io.ox/guard/core/guardModel'
import drawDiv from '@/io.ox/guard/mail/email_found'
import gt from 'gettext'
import upseller from '@/io.ox/guard/mail/upsell'
import mailAPI from '$/io.ox/mail/api'
import { createOxGuardPasswordPrompt } from '@/io.ox/guard/core/tempPassword'

let firstload = true

function upsell (baton) {
  const loc = baton.view.$el
  if (loc.find('.guardUpsell').length < 1) { // Make sure not already drawn
    upseller(loc)
  }
}

// Do the check if the email is PGP email.  Check for inline, MIME, or signature
function checkPGP (baton, location) {
  if (baton.data.security && baton.data.security.decrypted) {
    if (!guardModel().hasGuardMail() && !capabilities.has('guest') && baton.data.security.type === 'PGP') { // If not guard user, display upsell if any
      upsell(baton)
    }
    return // If already decoded
  }
  if (baton.view.model === null) return
  const headers = baton.view.model.get('headers')
  // not yet loaded
  if (headers === undefined) return
  // Init
  if (baton.data.headers === undefined) { // If email wasn't fully loaded into baton, do so now.
    baton.data = baton.view.model.toJSON()
  }
  const mail = baton.data
  if (mail.results !== undefined) return

  if (headers['X-OXGUARD-GUEST'] !== undefined) {
    baton.view.model.set('pgp_found', true)
    pgpAttach(baton)
    pgpFound.call(this, baton)
  }
  // Main check if Guard email
  if (baton.view.model.get('security') && baton.view.model.get('security').decrypted) return // Bug 53622, loop in mobile view
  if (mail.security_info && checkCapabilities(mail.security_info)) {
    if (mail.security_info.encrypted) {
      baton.view.model.set('pgp_found', true)
      pgpAttach(baton)
      pgpFound.call(this, baton, location)
    } else if (mail.security_info.signed) {
      if (!mail.security && !baton.signatureCheck) pullAgain(false, baton) // If we don't already have result, reload
      baton.signatureCheck = true // avoid loops
    }
  } else if (mail.security_info && mail.security_info.encrypted) {
    baton.data.attachments.forEach(function (a) {
      if (a.disp === 'inline') {
        a.content = gt('Encrypted email')
      }
    })
    baton.data.unsupported = true
    upsell(baton)
  }
}

function checkCapabilities (security) {
  if (security.type === 'SMIME') {
    if (capabilities.has('smime')) return true
    return false
  }
  return capabilities.has('guard')
}

function showError (data, baton) {
  const type = (baton.data.security_info && baton.data.security_info.type) ? baton.data.security_info.type : 'pgp'
  auth.resetToken(type)
  core.showError(data)
  if (!baton.signatureCheck) baton.view.redraw() // avoid loop
}

function pullAgain (decrypt, baton) {
  const options = {
    cache: false
  }
  const obj = {
    id: baton.data.id,
    folder_id: baton.data.folder_id
  }
  if (decrypt) {
    obj.decrypt = 'true'
  } else {
    obj.verify = 'true'
  }
  mailAPI.get(obj, options)
    .then(function (data) {
      if (!data.security) {
        console.error('Problem getting security response from guard')
        return
      }
      data = removeSignatureFile(data) // remove verified signatures
      if (baton.sessionKey) { // If local decrypt, save the sessionKey for attachments, etc
        data.security.sessionKey = baton.sessionKey
      }
      const origData = _.extend(baton.data, {})
      baton.model.set(data)
      origData.view = baton.view
      baton.model.set('origMail', origData)
      // Update the pool mail with the attachment and security data of the email.  Otherwise will
      // be overwritten with changes in listViews, sort order, etc
      const stored = mailAPI.pool.get('detail').get(_.cid(data))
      if (stored) {
        stored.set('guardIcons', _.pick(data, 'attachment', 'security'))
      }
      delete (baton.view.attachmentView) // Need to redo the attachment list
      baton.view.redraw()
    }, function (data) {
      showError(data, baton)
    })
}

// Remove signature.asc files from attachments if signatures verified
function removeSignatureFile (data) {
  if (data.security && data.security.signatures) {
    let verified = true
    data.security.signatures.forEach(function (d) {
      verified = verified && d.verified
    })
    if (verified) {
      const newAttachments = []
      data.attachments.forEach(function (att) {
        if (att.content_type !== 'application/pgp-signature') {
          newAttachments.push(att)
        }
      })
      data.attachments = newAttachments
    }
  }
  return data
}

// PGP found, do send for decode
function pgpFound (baton, location) {
  const goFunction = function () {
    pullAgain(true, baton)
  }
  checkLoaded().then(function () {
    const smime = (baton.data.security_info && baton.data.security_info.type === 'SMIME')
    checkSession(smime).then(function (auth) {
      import('@/io.ox/guard/mail/oxguard_mail_password').then(({ default: password }) => {
        if (auth && auth.length > 10) { // If auth code in session, load decrypted
          pullAgain(true, baton)
        } else if (!smime && guardModel().notReady()) { // PGP will often have auto created keys, check if additional password action required
          checkPGPauth(goFunction, baton)
        } else {
          // Otherwise, draw password prompt
          drawDiv(baton, goFunction).then(function (div) {
            $(location).replaceWith(div)
          })
          baton.view.$el.find('.attachments').hide()
          baton.view.$el.show()
        }
      })
    })
  })
}

function checkSession (smime) {
  const def = $.Deferred()
  const type = smime ? 'SMIME' : 'PGP'
  if (!firstload) {
    def.resolve(guardModel().getAuth(type))
    return def
  }
  // First time checking.  See if we have any stored in MW
  core.checkAuth({ type: 'both' })
    .done(function (data) {
      firstload = false
      data.forEach(function (d) {
        guardModel().setAuth(d.auth, d.type)
      })
      def.resolve(guardModel().getAuth(type))
    })
    .fail(function () {
      firstload = false
      def.resolve()
    })
  return def
}

// Timeout loop waiting for the Guard backend data to be loaded
function waitForLoad (def) {
  console.debug('Waiting for Guard load')
  if (guardModel().isLoaded()) {
    def.resolve()
    return
  }
  setTimeout(() => { waitForLoad(def) }, 500)
}

// Check if Guard backend data loaded.  If not, start wait loop
function checkLoaded () {
  const def = $.Deferred()
  if (guardModel().isLoaded()) {
    def.resolve()
  } else {
    waitForLoad(def)
  }
  return def
}

// Check PGP needs setup
function checkPGPauth (go, baton) {
  if (guardModel().needsPassword()) { // If no password for user has been defined, open dialog
    createOxGuardPasswordPrompt(baton, go, '', undefined, undefined, location)
    return
  }
  if (guardModel().needsKey()) { // If received a first encrypted email while already logged in, will still have 'no key' in passcode
    core.auth(ox.user_id, '') // Try to reload authorization to see if keys now exist
      .done(function (data) {
        guardModel().loadData(data)
        if (data.auth === 'No Key') {
          baton.model.unset('security_info')
          baton.view.redraw()
          return // Still no key, non guard generated email?
        }
        createOxGuardPasswordPrompt(baton, go, '', undefined, undefined, location) // Create new password
      })
  }
  // Nothing found to do
}

function pgpAttach (baton) {
  baton.data.PGP = true
}

export { checkPGP, pullAgain, showError, pgpFound }
