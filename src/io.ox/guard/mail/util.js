/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

const util = {}

util.hasEncryptedAttachment = function hasEncryptedAttachment (baton) {
  const attachments = baton.model.get('attachments').models
  for (let i = 0; i < attachments.length; i++) {
    if (attachments[i].get('epass') === undefined) { // Do not need to worry if epass present
      if (attachments[i].get('meta').encrypted === true) return (3)
      if (attachments[i].get('filename').indexOf('.pgp') > 0) return (3)
    }
  }
  return (0)
}

export default util
