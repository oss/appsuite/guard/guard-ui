/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import _ from '$/underscore'
import guardModel from '@/io.ox/guard/core/guardModel'
import util from '@/io.ox/guard/util'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import '@/io.ox/guard/oxguard_core'
import gt from 'gettext'
import keyCreator from '@/io.ox/guard/core/createKeys'
import '@/io.ox/guard/mail/style.scss'
import keyman from '@/io.ox/guard/mail/keymanager'
import publicKeys from '@/io.ox/guard/pgp/keyDetails'
import { createIcon } from '$/io.ox/core/components'

let prompting = false

function getKeyIcon () {
  return $('<div class="oxguard_token">').append(createIcon('bi/key.svg').addClass('key'))
}

function getFailIcon () {
  return $('<div class="oxguard_token failure">').append(createIcon('bi/slash-circle.svg').addClass('key'))
}

function getGuestIcon () {
  return $('<div class="oxguard_token">').append(createIcon('bi/person.svg').addClass('key'))
}

function drawKeyIcons (info, location) {
  const input = $(location).parent().find('.token-input')
  const inputwidth = input.width() - 30
  const key = getKeyIcon()
  if (info && info.result) {
    switch (info.result) {
      case 'pgp':
        if (info.key.keySource && info.key.keySource.trusted) {
          key.addClass('trusted_key')
        } else {
          key.addClass('untrusted_key')
        }
        createTooltip(info.key, key, location)
        $(location).find('.close').before(key)
        break
      case 'new':
        key.addClass('trusted_key')
        $(location).find('.close').before(key)
        createTooltip('new', key, location)
        break
      case 'guest': {
        const gKey = getGuestIcon().addClass('guardGuest')
        $(location).find('.close').before(gKey)
        createTooltip('guest', gKey, location)
        break
      }
      case 'smime':
        key.addClass('trusted_key')
        $(location).find('.close').before(key)
        createTooltip(info, key, location)
        break
      case 'fail':
        $(location).find('.close').before(getFailIcon())
        break
      default:
        console.error('Unknown key type ' + info.result)
    }
  }
  input.width(inputwidth)
}

function createTooltip (data, loc, t) {
  let div = $('<div>')
  const name = $(t).attr('aria-label')
  div.append($('<p>').append(name))
  if (data.result === 'smime') {
    div.append(gt('S/MIME public certificate'), '<br>')
    div.append(gt('Expires: %s', new Date(data.key.expires).toLocaleDateString()), '<br>')
    div.append(gt('Serial: %s', data.key.serial))
  } else if (data === 'new') { // New recipient
    div.append(gt('Key will be created upon sending.'))
  } else if (data === 'guest') {
    div.append(gt('Will create new guest account for %s', guardModel().getName()))
  } else {
    const detail = publicKeys.keyDetail([data], true)
    div = $(detail.div).find('#keyDetail')
    div.append(gt('Source: ') + getSourceTranslation(data.keySource.name) + '<br/>')
    div.append(data.keySource.trusted ? gt('Trusted') : gt('Untrusted Source'))
    div.addClass(data.keySource.trusted ? 'trusted_key' : 'untrusted_key')
  }
  $(loc).tooltip({
    title: div.html(),
    trigger: 'hover',
    placement: 'bottom',
    html: true,
    selector: loc,
    delay: 200,
    animation: true,
    viewport: { selector: '.io-ox-mail-compose', padding: 10 }
  })
  const id = 'div' + (data.id ? data.id : Math.random())
  $(loc).append($('<div id="' + id + '" class="sr-only srdetails">').html(div.html()))
  t.attr('aria-describedby', id + ' ' + t.attr('aria-describedby'))
}

function getSourceTranslation (source) {
  switch (source) {
    case 'HKP_PUBLIC_SERVER':
      return gt('Public server')
    case 'HKP_SRV_DNSSEC_SERVER':
      return gt('Secure DNS referred')
    case 'HKP_TRUSTED_SERVER':
      return gt('Trusted server')
    case 'HKP_SRV_SERVER':
      return gt('DNS referred')
    case 'HKP_CACHE':
      return gt('DNS referred')
    case 'GUARD_KEY_PAIR':
      return gt('System key')
    case 'GUARD_USER_UPLOADED':
      return gt('Uploaded key')
    case 'GUARD_USER_SHARED':
      return gt('Shared key')
    case 'WKS_SERVER':
      return gt('WebKey Directory Service')
    case 'WKS_SRV_DNSSEC_SERVER':
      return gt('WebKey DNSEC Service')
    case 'AUTOCRYPT_KEY':
      return gt('Autocrypt Keys')
    case 'AUTOCRYPT_KEY_USER_VERIFIED':
      return gt('Autocrypt Keys')
    default:
      return source
  }
}

// Return a has of string
function hash (str) {
  let hashed = 0
  if (str.length === 0) return hashed
  for (let i = 0; i < str.length; i++) {
    hashed = (31 * hashed + str.charCodeAt(i)) << 0
  }
  return hashed
}

function checkRecipient (baton) {
  if (!util.isGuardConfigured()) {
    if (!prompting) {
      prompting = true
      keyCreator.createKeysWizard()
        .done(function () {
          checkRecipient(baton)
        })
        .fail(function () {
          baton.view.securityModel.set('encrypt', false)
        })
        .always(function () {
          prompting = false
        })
    }
    // Do not check recipients until the primary key is created (in case mailing to self)
    return
  }
  keyman.checkAll(baton)
}

function unlock (baton) {
  if (_.device('smartphone')) {
    $('.io-ox-mail-compose-window .btn-primary:visible[data-action="send"]').text(gt('Send'))
  } else if (baton.view.app.get('window')) {
    const headerview = baton.view.app.get('window').nodes.header
    const footerview = baton.view.app.get('window').nodes.footer
    headerview.find('.btn-primary:visible[data-action="send"]').text(gt('Send'))
    if (footerview) {
      footerview.find('.btn-primary:visible[data-action="send"]').text(gt('Send'))
    }
  }
  baton.view.$el.find('.oxguard_key').hide()
  baton.view.$el.find('.oxguard_token').hide()
  baton.view.$el.find('.oxguard_token .srdetails').remove()
}

function lock (baton) {
  if (_.device('smartphone')) {
    $('.io-ox-mail-compose-window .btn-primary:visible[data-action="send"]').text((settings.get('advanced') ? gt('Send Encrypted') : gt('Send Secure')))
  }
  checkRecipient(baton)
  baton.view.$el.find('.oxguard_key').show()
  baton.view.$el.find('.oxguard_token').show()
}

export default {
  lock,
  hash,
  unlock,
  drawKeyIcons
}
