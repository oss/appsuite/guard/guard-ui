/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ox from '$/ox'
import keysAPI from '@/io.ox/guard/api/keys'
import certsAPI from '@/io.ox/guard/api/certs'
import core from '@/io.ox/guard/oxguard_core'
import gt from 'gettext'
import yell from '$/io.ox/core/yell'
import { settings as calSettings } from '$/io.ox/calendar/settings'

// We only want to pull the blocklist once
let blockList

// Check the list of previously fetched key results to see if the email is there.  If so, return, otherwise query backend
function checkRecips (email, baton) {
  const def = $.Deferred()
  let gkeys = baton.config.get('gkeys')

  if (gkeys === undefined) {
    // If undefined, create gkeys now
    gkeys = {}
    baton.config.set('gkeys', gkeys)
    getKeyResult(email, baton)
      .done(function (keyResults) {
        def.resolve(keyResults)
      })
    return def
  }
  if (gkeys[email] && !gkeys[email].pending) {
    def.resolve(gkeys)
    return def
  }
  if (gkeys[email] && gkeys[email].pending) {
    return gkeys[email].def
  }
  getKeyResult(email, baton)
    .done(function (keyResults) {
      def.resolve(keyResults)
    })
  return def
}

// Query guard server for key result
function getKeyResult (email, baton) {
  const def = $.Deferred()
  const result = {
    email,
    result: null,
    pending: true,
    def
  }
  const gkeys = baton.config.get('gkeys')

  gkeys[email] = result
  baton.config.set('gkeys', gkeys)
  const smime = baton.view.securityModel.get('smime')
  if (isBlockListed(email)) {
    yell('error', gt('Mailing list / blocked email found.  Encrypted emails to this email will fail.  %s', email))
    baton.view.securityModel.set('encrypt', false)
    def.resolve(updateKeys(email, baton, 'fail', undefined, smime))
  }

  return (smime ? certsAPI : keysAPI).getRecipientsPublicKey(email).then(function (data) {
    try {
      if (!data) { // No key data
        return { result: 'fail', keyData: undefined }
      }
      if (core.checkJsonOK(data)) {
        if (smime) {
          return { result: 'smime', keyData: data }
        }
        if (data.preferredFormat === 'PGP/INLINE') {
          // If needs inline, then set
          baton.view.securityModel.setInline()
        }
        const keys = (data.publicRing && data.publicRing.keys) ? data.publicRing.keys : undefined
        let encryptionKey
        if (keys) {
          keys.forEach(function (key) {
            if (key.encryptionKey && !key.expired && !key.revoked) {
              encryptionKey = key
              encryptionKey.keySource = data.keySource
              const lang = ox.language.substring(0, 2)
              encryptionKey.Created = (new Date(key.creationTime)).toLocaleDateString(lang, { year: '2-digit', month: '2-digit', day: '2-digit' })
            }
          })
        }
        if (encryptionKey || data.newKey) {
          if (data.guest && data.newKey) {
            return { result: 'guest', keyData: encryptionKey }
          }
          return { result: data.newKey ? 'new' : 'pgp', keyData: encryptionKey }
        }
      }
    } catch (e) {
      console.log(e)
    }
    return { result: 'fail', keyData: undefined }
  }, function (error) {
    return { result: 'fail', keyData: error }
  }).then(function (data) {
    return def.resolve(updateKeys(email, baton, data.result, data.keyData, smime))
  })
}

// Check if email is in blocklist
function isBlockListed (email) {
  if (!blockList) { // Initialize blocklist
    blockList = calSettings.get('participantBlacklist')
    if (blockList) {
      blockList = blockList.toLowerCase()
    } else {
      blockList = ''
    }
  }
  if (blockList && blockList !== '') {
    if (blockList.indexOf(email.toLowerCase()) > -1) {
      return true
    }
  }

  return false
}

// Update the list of gkeys with the result
function updateKeys (email, baton, result, keyData, smime) {
  const gkeys = baton.config.get('gkeys')
  if ((smime === true) === baton.view.securityModel.get('smime')) {
    gkeys[email] = {
      result,
      pending: false,
      key: keyData
    }
    baton.config.set('gkeys', gkeys)
    baton.model.trigger('change:gkeys')
  }
  return gkeys
}

function updateTokens (baton) {
  baton.model.trigger('change:to', baton.model, baton.model.get('to'))
  baton.model.trigger('change:cc', baton.model, baton.model.get('cc'))
  baton.model.trigger('change:bcc', baton.model, baton.model.get('bcc'))
}

function checkAll (baton) {
  updateTokens(baton)
}

// Delete specific email from gkeys array
function deleteRecip (email, baton) {
  const gkeys = baton.config.get('gkeys')
  delete gkeys[email]
  baton.config.set('gkeys', gkeys)
}

// Check if an email is located in gkeys
function isPresent (email, recips) {
  for (let i = 0; i < recips.length; i++) {
    if (recips[i][1] === email) return true
  }
  return false
}

// Clean the list of gkeys, remove tokens that were deleted
function clean (baton) {
  const to = baton.model.get('to') || []
  const cc = baton.model.get('cc') || []
  const bcc = baton.model.get('bcc') || []
  const total = to.concat(cc).concat(bcc)
  const gkeys = baton.config.get('gkeys') || []
  const newkeys = []
  for (const email in gkeys) {
    if (isPresent(email, total)) {
      newkeys[email] = gkeys[email]
    }
  }
  baton.config.set('gkeys', newkeys)
  baton.model.trigger('change:gkeys')
}

// Check if Guest present in list
function isGuest (baton) {
  try {
    clean(baton)
    let keys = baton.config.get('gkeys')
    if (keys === undefined) keys = []
    for (const email in keys) {
      if (keys[email].result === 'guest') return true
    }
  } catch (e) {
    console.log(e)
  }

  return false
}

function getGuests (baton) {
  const guests = []
  try {
    let keys = baton.config.get('gkeys')
    if (keys === undefined) keys = []
    for (const email in keys) {
      if (keys[email].result === 'guest') guests.push(email)
    }
  } catch (e) {
    console.log(e)
  }
  return (guests)
}

export default {
  checkRecips,
  checkAll,
  clean,
  isGuest,
  getGuests,
  deleteRecip
}
