/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import capabilities from '$/io.ox/core/capabilities'
import ox from '$/ox'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import Backbone from '$/backbone'
import { showExpiring, getExpiringType } from '@/io.ox/guard/core/expiringView'
import _ from '$/underscore'
import DOMPurify from 'dompurify'
import CertsModel from '@/io.ox/guard/core/certsModel'

let guardModel // instance of Guard Model

// Parse out available languages, comparing Guard templates and UI available
function checkLanguages (guardLanguages) {
  const available = {}
  if (!guardLanguages) return available
  for (const lang in guardLanguages) {
    if (ox.serverConfig.languages[lang]) {
      available[lang] = guardLanguages[lang]
    }
  }
  return available
}

function normalize (type) {
  if (!type) return 'pgp'
  return type.toLowerCase()
}

// Guard model contains Guard settings as well as PGP key data
// S/Mime certificates held withing certsModel within this model
const GuardModel = Backbone.Model.extend({

  defaults: {
    needsKey: false,
    needsPassword: false,
    authcode: {},
    settings: {},
    loaded: false
  },

  initialize () {
    this.on('change:loaded', this.loadEvents)
    this.on('change:expiring', this.expiring)
    this.set('certsModel', new CertsModel())
  },
  /** Initialize with the Guard login data */
  loadData (data) {
    const model = this
    Object.entries(data).forEach(function ([key, value]) {
      switch (key) {
        case 'lang':
          model.set('lang', checkLanguages(value))
          break
        case 'auth':
          break
        default:
          model.set(key, value)
      }
    })
    this.set('loaded', true)
  },

  loadEvents () { // Load any functions pending Guard loaded
    const startupList = this.get('onload')
    if (startupList) {
      try {
        startupList.forEach(function (load) {
          load.call()
        })
        this.set('onload', null)
      } catch (e) {
        console.error('Problem executing onload functions for Guard ' + e)
      }
    }
  },

  expiring () {
    const expiring = this.get('expiring')
    if (getExpiringType(expiring)) {
      showExpiring(expiring)
    }
  },

  allExpired (smime) {
    if (smime) {
      return this.get('certsModel').expired()
    } else {
      return this.get('expired')
    }
  },

  getCerts () {
    return this.get('certsModel').getCerts()
  },

  setCerts (certs) {
    this.get('certsModel').setCerts(certs)
  },

  updatePGPKeyData (data) {
    this.set('expired', data.expired)
    this.set('needsKey', data.passwordModificationTimestamp === undefined)
  },

  getName: _.memoize(function () {
    const name = DOMPurify.sanitize(ox.serverConfig['guard.productName'] || settings.get('productName'))
    return !name || name === 'undefined' ? 'Guard' : name
  }),

  // Authentication
  setAuth: function (auth, type) {
    if (!type) type = 'pgp'
    if (type.toLowerCase() === 'pgp') {
      switch (auth) {
        case 'Bad Password':
          return
        case 'Password Needed':
          this.set('needsPassword', true)
          return
        case 'No Key':
          this.set('needsKey', true)
          return
        default:
          this.resetFlags()
          this.updateAuth(auth, type)
      }
    } else {
      this.updateAuth(auth, type)
    }
  },

  resetFlags () {
    this.set('needsPassword', false)
    this.set('needsKey', false)
  },

  clearAuth (type) {
    this.resetFlags()
    this.updateAuth(null, type)
  },

  hasAuth (type) {
    return this.getAuth(type)
  },

  getAuth (type) {
    const authCodes = this.get('authcode')
    return authCodes[normalize(type)]
  },

  hasAnyAuth () {
    const authCodes = this.get('authcode')
    return Object.entries(authCodes).length > 0
  },

  /** Guard key status  */
  needsPassword () {
    return this.get('needsPassword') === true
  },

  needsKey () {
    return this.get('needsKey') === true
  },

  notReady () {
    return this.needsPassword() || this.needsKey()
  },

  updateAuth (auth, type) {
    if (!type) {
      this.set('authcode', {})
      return
    }
    const currentAuth = this.get('authcode')
    currentAuth[normalize(type)] = auth
    this.set('authcode', currentAuth)
    this.trigger('change:authcode')
  },

  // Guard status
  isLoaded () {
    return this.get('loaded')
  },

  hasGuardMail () {
    return this.getSettings().oxguard && capabilities.has('guard-mail')
  },

  hasSmime () {
    return capabilities.has('smime')
  },

  hasSmimeKeys () {
    return this.get('certsModel').hasCerts()
  },

  forceEncryptedDrafts () {
    return this.getSettings().encryptDrafts
  },

  getSettings () {
    return this.get('settings') || {}
  },

  canCreateCertificates: function () {
    return this.get('pki') && this.get('pki').canCreate
  }
})

export default function getModel () {
  if (!guardModel) {
    return initialize()
  }
  return guardModel
}

export function initialize (data) {
  data = data || {
    passcode: null,
    settings: {}
  }
  guardModel = new GuardModel(data)
  return guardModel
}
