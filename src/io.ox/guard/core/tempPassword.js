/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ox from '$/ox'
import gt from 'gettext'

import ModalDialog from '$/io.ox/backbone/views/modal'
import capabilities from '$/io.ox/core/capabilities'
import yell from '$/io.ox/core/yell'
import { settings as mailSettings } from '$/io.ox/mail/settings'

import authAPI from '@/io.ox/guard/api/auth'
import loginAPI from '@/io.ox/guard/api/login'
import keyCreator from '@/io.ox/guard/core/createKeys'
import guardModel from '@/io.ox/guard/core/guardModel'
import { PasswordView } from '@/io.ox/guard/core/passwordView'
import { MailView } from '@/io.ox/guard/core/mailView'
import { PinView } from '@/io.ox/guard/core/pinView'
import settingsPage from '@/io.ox/guard/settings/pane'
import '@/io.ox/guard/core/style.scss'

let firstprompt = false

export function createOxGuardPasswordPrompt (baton, go, errors, oldpass, index, priorData) {
  if (firstprompt) return
  firstprompt = true

  const dialog = new ModalDialog({
    async: true,
    point: 'io.ox/guard/core/tempPassword',
    id: 'tempPassword',
    width: 560,
    title: gt('First %s Security Use', guardModel().getName()),
    data: {
      isnew: guardModel().get('new'),
      hasPin: guardModel().get('pin'),
      baton,
      go,
      errors,
      oldpass,
      index,
      priorData
    }
  })
    .extend({
      header () {
        if (this.options.data.isnew) {
          return this.$body.append(
            // #. %s product name
            $('<p>').text(gt('You have been sent an email using %s to secure the contents', guardModel().getName())),
            $('<p>').text(gt('Please select a new password for use with %s.  This password will be used to open encrypted emails and files that may be sent to you.', guardModel().getName()))
          )
        }

        // #. %s product name
        this.$body.append($('<p>').text(gt('Please change the initial temporary %s security password that was sent to you in a separate email.', guardModel().getName())))
      },
      passDiv () {
        this.passwordView = new PasswordView({
          id: 'newogpassword',
          label: gt('New Password'),
          class: 'password_prompt popup_password_prompt',
          validate: true
        })
        this.passwordConfirmView = new PasswordView({
          id: 'newogpassword2',
          label: gt('Confirm new password'),
          class: 'password_prompt popup_password_prompt',
          validate: false
        })
        if (!this.options.data.isnew) {
          this.$body.append(
            this.initPasswordView = new PasswordView({
              id: 'oldogpassword',
              label: gt('Initial Password'),
              class: 'password_prompt popup_password_prompt',
              validate: false
            }).render().$el
          )
        }
        this.$body.append(
          this.passwordView.render().$el,
          this.passwordConfirmView.render().$el
        )
      },
      pin () {
        if (!this.options.data.hasPin) return

        this.pinView = new PinView({
          id: 'pinInput',
          label: gt('PIN'),
          class: 'password_prompt popup_password_prompt',
          validate: true
        })
        this.$body.append('<hr/>').append(
          $('<p>').append(gt('The sender assigned an additional PIN to the account.  This PIN must be entered before you can use this account.  Please enter it now.  Contact the sender directly if you don\'t yet have a pin.')),
          this.pinView.render().$el
        )
      },
      recovery () {
        if (guardModel().getSettings().noRecovery === true) {
          return this.$body.append(
            $('<hr/>'),
            $('<p class="recoveryWarning">').append(
              $('<b>').text(gt('Warning: This password for encryption cannot be restored or recovered in any way.  If forgotten, all encrypted data will be lost.'))
            )
          )
        }

        this.emailView = new MailView({
          id: 'recoveryemail',
          label: gt('Email'),
          class: 'password_prompt popup_password_prompt',
          validate: true
        })

        this.$body.append(
          $('<hr/>'),
          $('<p>').text(gt('Please enter a secondary email address in case you need to reset your password.')),
          this.emailView.render().$el,
          $('<label for="verifyemail">').text(gt('Verify')),
          $('<input id="verifyemail" name="verifyemail" class="form-control password_prompt"/>'))
      }
    })
    .addButton({ label: gt('OK'), action: 'okpass' })
    .addCancelButton()
    .on('cancel', function () {
      firstprompt = false
    })
    .on('okpass', function () {
      ox.busy()
      // triggering valid clears the errors, so they can be updated with isValid
      const data = this.options.data
      dialog.passwordView.model.trigger('valid')
      if (dialog.emailView) {
        dialog.emailView.model.trigger('valid')
      }
      data.hasPin && dialog.pinView.model.trigger('valid')

      if (!dialog.passwordView.model.isValid() || (dialog.emailView && !dialog.emailView.model.isValid())) {
        ox.idle()
        return dialog.idle()
      }

      doChange(data.hasPin, data.baton, data.go, data.index).then(function (success) {
        ox.idle()
        if (success) {
          dialog.close()
        }
      }).catch((error) => {
        console.error(error)
        ox.idle()
        dialog.idle()
      })
    })
    .open()

  // invalidate
  ox.on('guard:change:mail', message => {
    dialog.idle()
    dialog.emailView.$el.trigger('invalid', message)
  })
  ox.on('guard:change:password', message => {
    dialog.idle()
    dialog.passwordView.$el.trigger('invalid', message)
  })
  ox.on('guard:change:initPassword', message => {
    dialog.idle()
    dialog.initPasswordView.$el.trigger('invalid', message)
  })
  ox.on('guard:change:pin', message => {
    dialog.idle()
    dialog.pinView.$el.trigger('invalid', message)
  })

  ox.on('guard:change:error', function (errors) {
    dialog.idle()
    dialog.$el.prepend(
      $('<div class="alert-danger error-line">').append(
        $('<span role="alert">')
          .append(errors)
      )
    )
  })
}

async function doChange (hasPin, baton, go, index) {
  firstprompt = false
  const oldpass = $('#oldogpassword').val() === undefined ? '' : $('#oldogpassword').val()
  const pass1 = $('#newogpassword').val()
  const pass2 = $('#newogpassword2').val()
  const pin = hasPin ? $('#pinInput').val() : ''
  const emailaddr = $('#recoveryemail').val() === undefined ? '' : $('#recoveryemail').val()
  const verify = $('#verifyemail').val() === undefined ? '' : $('#verifyemail').val()

  if (emailaddr !== verify) {
    ox.trigger('guard:change:mail', gt('Emails not equal'))
    return false
  }
  if (pass1 !== pass2) {
    ox.trigger('guard:change:password', gt('Passwords not equal'))
    return false
  }

  const userdata = {
    newpass: pass1,
    oldpass,
    email: emailaddr,
    userEmail: mailSettings.get('defaultSendAddress', '').trim() || ox.user,
    user_id: ox.user_id,
    sessionID: ox.session,
    cid: ox.context_id,
    pin
  }

  return loginAPI
    .changePassword(userdata)
    .done(function (resp) {
      let data = resp.data ? resp.data : resp
      if (typeof data === 'string') data = $.parseJSON(data)
      if (data.auth === undefined) {
        if (data.error) {
          if (/.*pin.*missing/.test(data.error)) {
            ox.trigger('guard:change:pin', data.error)
          } else {
            ox.trigger('guard:change:error', data.error)
          }
        } else {
          import('@/io.ox/guard/core/errorHandler').then(({ default: err }) => {
            err.showError(data)
          })
        }
        return
      }
      if (data.auth.length > 20) {
        $('#grdsettingerror').text(gt('Success'))
        $('input[name="newogpassword"]').val('')
        $('input[name="newogpassword2"]').val('')
        guardModel().clearAuth()
        guardModel().set('recoveryAvail', !guardModel().getSettings().noRecovery)
        authAPI.setToken(data.auth).then(function () {
          if (go) go(baton, pass1, index)
        })
        yell('success', gt('Password changed successfully'))
        guardModel().set('pin', false)
        if ($('#currentsecondary').length > 0) settingsPage.updateEmail()
        guardModel().set('new', false)
        // Complete, now offer tour

        if (capabilities.has('guard-mail') && !capabilities.has('guest')) { // only run tour if has guard-mail
          keyCreator.createKeysWizard()
        }
        return true
      } else {
        if (data.auth === 'Bad new password') {
          ox.trigger('guard:change:password', gt('New password must be at least %s characters long', data.minlength))
          return
        }
        if (data.auth === 'Bad password') {
          // #. Bad, incorrect password
          ox.trigger('guard:change:initPassword', gt('Bad password'))
          return
        }
        if (data.auth === 'Key not found') {
          yell('error', gt('Encryption key not found'))
          return
        }
        yell('error', gt('Failed to change password'))
        return false
      }
    })
    .fail(function (data) {
      if (data.error) {
        if (/.*pin.*missing/.test(data.error)) {
          ox.trigger('guard:change:pin', data.error)
        } else {
          ox.trigger('guard:change:error', data.error)
        }
      } else {
        yell('error', gt('Failed to change password'))
      }
      return false
    })
}

export default {
  createOxGuardPasswordPrompt
}
