/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import Backbone from '$/backbone'
import _ from '$/underscore'
import http from '$/io.ox/core/http'

function log (e, o) {
  const collection = http.log()
  collection.add(new Backbone.Model(e).set({
    params: o.params,
    data: o.data,
    index: collection.length,
    timestamp: _.now(),
    url: o.url
  }))
}

function fail (r, url, params, data) {
  let errorText = r.statusText
  if (r.status === 406 || r.status === 500) {
    errorText = r.status + ': ' + errorText + ' - ' + r.responseText
    r.status = undefined
  }
  const e = {
    error: errorText,
    status: r.status
  }
  const o = {
    params,
    url,
    data
  }
  log(e, o)
}

function slow (took, url, params, data) {
  const e = { error: 'Took: ' + (Math.round(took / 100) / 10) + 's', status: 200, took }
  const o = {
    params,
    url,
    data
  }
  log(e, o)
}

export default {
  fail,
  slow
}
