/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import gt from 'gettext'
import ext from '$/io.ox/core/extensions'
import BasicModel from '$/io.ox/backbone/basicModel'
import AbstractView from '$/io.ox/backbone/mini-views/abstract'
import mini from '$/io.ox/backbone/mini-views/common'
import guardModel from '@/io.ox/guard/core/guardModel'
import Backbone from 'backbone'

ext.point('io.ox/guard/core/passwordView/model/validation').extend([{
  id: 'password-length',
  validate ({ id, name }, err, model) {
    const min = guardModel().getSettings().min_password_length || 6
    let len = guardModel().getSettings().password_length || 10
    if (len <= min) len = min + 1
    const password = model.get(id || name)
    if (!!password && password?.length < min) {
      this.add(model.get('id'), gt('Password not long enough.  Needs to be at least %i characters long', min))
    }
  }

}])

const PasswordModel = BasicModel.extend({
  ref: 'io.ox/guard/core/passwordView/model/',
  initialize (options = {}) {
    BasicModel.prototype.initialize.call(this, options)
  }
})

export const PasswordView = mini.PasswordView.extend({
  events: {
    change: 'onChange'
  },

  initialize (options) {
    this.options = {
      autocomplete: false,
      validate: false,
      ...options
    }

    const { id, name, validate } = this.options
    this.model = new PasswordModel({ id, validate })

    this.id = id
    this.name = name || id
    this.isValid = true

    this.listenTo(this.model, 'invalid', this.invalid)
    this.listenTo(this.model, 'valid', this.valid)
  },

  onChange () {
    this.$el.trigger('valid')
    this.model.set(this.name, this.$el.find('input').val())
    if (!this.model.get('validate')) return
    this.model.validate()
  },

  getForCompare () {
    return this.$el.find('input')
  },

  getValue () {
    this.$el.trigger('change')
    if (this.isValid) return this.model.get(this.id)
    return undefined
  },

  render () {
    const input = mini.PasswordView.prototype.render.apply(this).$el
    input.addClass(this.options.class || '')

    this.$el = $('<div class="pwView">')

    const label = this.options.label ? $('<label>').attr('for', this.id).text(this.options.label) : ''

    const error = new mini.ErrorView({
      name: this.id,
      model: this.model,
      selector: this.$el
    }).render().$el

    this.$el.append(label, input, error)
    return this
  }
})

// refactored version if PasswordModel and PasswordView
const PlainPasswordModel = Backbone.Model.extend({
  validate (attr) {
    const value = attr[this.name]
    const minLength = guardModel().getSettings().min_password_length || 6
    const maxLength = Math.max(guardModel().getSettings().password_length || 10, minLength + 1)
    if (!value || value.length < minLength || value.length > maxLength) {
      return gt('Password length must be between %1$d and %2$d characters.', minLength, maxLength)
    }
    this.trigger('valid')
  }
})

export const PlainPasswordView = mini.PasswordView.extend({

  initialize (options = { }) {
    this.model = new PlainPasswordModel()
    const opt = {
      name: 'password',
      className: '',
      mandatory: true,
      autocomplete: false,
      validate: false,
      ...options
    }
    AbstractView.prototype.initialize.call(this, opt)
    const { className, id, name } = this.options
    this.$el.attr('id', id).addClass(className)
    // used to identify right attribute key to verify
    this.model.name = name

    if (opt.validate) {
      this.listenTo(this.model, 'valid', () => this.valid())
      this.listenTo(this.model, 'invalid', (message) => this.invalid(message))
    } else {
      // in case custom event set view to invalid (example: guard:auth:fail:password)
      this.listenTo(this.model, `change:${this.name}`, () => this.valid())
      this.listenTo(this.model, 'invalid', (message) => this.invalid(message))
    }
  }
})
