/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import gt from 'gettext'

function validate (email) {
  return (email.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[^\s.]*)?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i))
}

function setValidate (e1, hint) {
  $(e1).on('input', function () {
    if (validate($(e1).val())) {
      $(e1).removeClass('oxguard_badpass')
      if (hint) $(hint).html('')
    } else {
      $(e1).addClass('oxguard_badpass')
      if (hint) {
        $(hint).html(gt('Not valid email'))
      }
    }
  })
}

function autoCompare (e1, e2, hint) {
  $(e2).on('input', function () {
    if ($(e1).val() === $(e2).val()) {
      $(e2).removeClass('oxguard_badpass')
      if (hint) $(hint).html('')
    } else {
      $(e2).addClass('oxguard_badpass')
      if (hint) {
        $(hint).html(gt('Emails not equal'))
      }
    }
  })
}

export default {
  validate,
  autoCompare,
  setValidate
}
