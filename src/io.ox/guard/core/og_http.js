/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import _ from '$/underscore'
import guardModel from '@/io.ox/guard/core/guardModel'
import log from '@/io.ox/guard/core/log'
import ox from '$/ox'
import { cryptPass } from '@/io.ox/guard/crypto/encr'

function doPost (url, params, data, json) {
  const def = $.Deferred()
  params = params + '&session=' + ox.session
  if (data === null || data === undefined) data = {}
  const t0 = _.now()
  $.ajax({
    url: url + params,
    type: 'POST',
    dataType: json ? 'json' : 'text',
    data: (json ? JSON.stringify(data) : data),
    contentType: (json ? 'application/json; charset=utf-8;' : 'application/x-www-form-urlencoded'),
    success (rdata) {
      if (!json && rdata && rdata.indexOf('{') === 0) { // If json response instead of text, parse the json
        rdata = JSON.parse(rdata)
      }
      def.resolve(rdata)
      if ((_.now() - t0) > 1000) {
        log.slow((_.now() - t0), url, params, data)
      }
    }
  })
    .fail(function (r) {
      log.fail(r, url, params, data)
      def.reject(r)
    })
  return (def)
}

function doPut (url, params, data) {
  const def = $.Deferred()
  params = params + '&session=' + ox.session
  if (data === null || data === undefined) data = {}
  const t0 = _.now()
  $.ajax({
    url: url + params,
    type: 'PUT',
    data: JSON.stringify(data),
    dataType: 'json',
    contentType: 'application/json; charset=utf-8',
    success (rdata) {
      def.resolve(rdata)
      if ((_.now() - t0) > 1000) {
        log.slow((_.now() - t0), url, params, data)
      }
    }
  })
    .fail(function (r) {
      log.fail(r, url, params, data)
      def.reject(r)
    })
  return (def)
}

// Post JSON data
function post (url, params, data) {
  return postAction(url, params, data, true)
}

// Post using x-www-form-urlencoded
function simplePost (url, params, data) {
  return postAction(url, params, data, false)
}

// Look for passwords in outgoing data.  If able, encrypt
function postAction (url, params, data, json) {
  const def = $.Deferred()
  if (guardModel().get('pubKey') !== undefined && data !== undefined) {
    if (data.password !== undefined) {
      if (data.password !== '') {
        const epass = cryptPass(data.password)
        if (epass !== null) {
          data.e_password = epass
          delete data.password
        }
      }
    }
    if (data.encr_password !== undefined) {
      if (data.encr_password !== '') {
        const encrpass = cryptPass(data.encr_password)
        if (encrpass !== null) {
          data.e_encr_password = encrpass
          delete data.encr_password
        }
      }
    }
    if (data.extrapass !== undefined) {
      if (data.extrapass !== '') {
        const expass = cryptPass(data.extrapass)
        if (expass !== null) {
          data.e_extrapass = expass
          delete data.extrapass
        }
      }
    }
    data.keyIndex = guardModel().get('pubKey').index
  }
  doPost(url, params, data, json)
    .done(e => { def.resolve(e) })
    .fail(f => { def.reject(f) })
  return (def)
}

function get (url, params) {
  const def = $.Deferred()
  const t0 = _.now()
  params = params + '&session=' + ox.session + '&ticks=' + t0
  $.get(url + params, function (rdata) {
    def.resolve(rdata)
    if ((_.now() - t0) > 1000) {
      log.slow((_.now() - t0), url, params, {})
    }
  })
    .fail(function (r) {
      if (r.status === 200) {
        try {
          const data = JSON.parse(r.responseText)
          def.resolve(data)
        } catch (e) {
          console.log(e)
          def.reject(r)
        }
      }
      log.fail(r, url, params, {})
      def.reject(r)
    })

  return (def)
}

export default {
  post,
  simplePost,
  get,
  doPut
}
