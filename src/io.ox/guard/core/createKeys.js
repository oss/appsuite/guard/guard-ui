/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import Backbone from '$/backbone'
import $ from '$/jquery'
import _ from '$/underscore'
import guardModel from '@/io.ox/guard/core/guardModel'
import ModalDialog from '$/io.ox/backbone/views/modal'
import { PasswordView } from '@/io.ox/guard/core/passwordView'
import keysAPI from '@/io.ox/guard/api/keys'
import gt from 'gettext'
import yell from '$/io.ox/core/yell'
import userAPI from '$/io.ox/core/api/user'
import Tour from '$/io.ox/core/tk/wizard'
import '@/io.ox/guard/tour/main'
import { createIcon } from '$/io.ox/core/components'

/**
 * @returns deferred object resolves to one of:
 *  * "OK" - everything is fine
 *  * "cancel" - user cancelled the dialog
 *  * object - failed with error message stored in responseText attribute
 */
// Prompt for creating primary keys for this user if not yet created.
export function createKeys () {
  const def = $.Deferred()

  new ModalDialog({
    async: true,
    point: 'oxguard/core/createKeys',
    title: gt('Create %s Security Keys', guardModel().getName()),
    width: 640,
    enter: 'ok',
    focus: '#newogpassword'
  })
    .extend({
      view () {
        this.view = new View({
          model: new Model()
        })
        this.$body.append(
          this.view.render().$el
        )
      },
      'button-state' () {
        const button = this.$('button[data-action=ok]')
        // initial value
        button.attr({
          // disable automatic state management of modal dialog
          'data-state': 'manual',
          disabled: true
        })
        this.view.listenTo(this.view.model, 'change', function (model) {
          button.attr('disabled', !model.isValid())
        })
      },
      deferred () {
        this.view.listenTo(this.view.model, 'send', function (error) {
          this.close()
          return error
            ? def.reject({ responseText: error })
            : def.resolve('OK')
        }.bind(this))
      }
    })
    .addButton({ label: gt('OK'), action: 'ok' })
    .addCancelButton()
    .on('cancel', function () {
      def.reject('cancel')
    })
    .on('ok', function () {
      this.view.model.send()
    })
    .on('dispose', function () {
      this.view.remove()
      this.view = null
    })
    .open()

  return def
}

function validateEmail (email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}

const View = Backbone.View.extend({

  className: 'guard-create-key form-horizontal',

  initialize () {
    this.listenTo(this.model, 'change', this.handleChange)
    this.listenTo(this.model, 'before:send', this.busy)
    this.listenTo(this.model, 'send', this.idle)
    this.listenTo(this.model, 'send:error', this.handleError)

    this.$wait = waitDiv().hide()
  },

  events: {
    'keyup #newogpassword': 'onPasswordChange',
    'input #newogpassword': 'onPasswordChange',
    'keyup #newogpassword2': 'onPasswordValidationChange',
    'input #newogpassword2': 'onPasswordValidationChange',
    'change input[name=recoverymail]': 'onRecoverymailChange'
  },
  onPasswordChange (e) {
    this.model.set('password', e.target.value)
  },
  onPasswordValidationChange (e) {
    this.model.set('passwordValidation', e.target.value)
  },
  onRecoverymailChange (e) {
    this.model.set('email', e.target.value)
  },

  handleChange (model) {
    // reset error fields
    this.$el.find('.has-error')
      .removeClass('has-error')
      .find('.error-msg').empty()

    if (!model.isValid()) {
      model.validationError.forEach(error => {
        this.$el.find(error.field)
          .addClass('has-error')
          .find('.error-msg').text(error.message)
      })
    }
    model.checkStrength()
    const strength = model.get('passwordStrength')
    this.$el.find('.password')
      .toggleClass('has-success', strength === 'good')
      .toggleClass('has-warning', strength === 'weak')
  },
  handleError (error) {
    yell('error', error)
  },
  busy () {
    this.$wait.show()
  },
  idle () {
    this.$wait.hide()
  },
  render () {
    this.$el.empty().append(this.$wait)
    return this
      .renderDescription(this.$el)
      .renderPasswordPrompt(this.$el)
      .renderRecoverMailPrompt(this.$el)
  },
  renderDescription (el) {
    let prompt = ''
    if (this.model.get('initialSetup')) {
      prompt = $('<p>').text(gt('Please choose the password you will use for %s. You will need to type this password whenever you want to encrypt or decrypt items. Remember it should be different from your login password, and will not change if you change your login password.', guardModel().getName()))
    }
    if (this.model.get('prompt')) {
      prompt = $('<p>').append(this.model.get('prompt'))
    }
    el.append(
      $('<div style="margin-bottom:30px;">').append(prompt, gt('Please enter a password to protect your new encrypted items.'))
    )
    return this
  },
  renderPasswordPrompt (el) {
    el.append(
      $('<div class="form-group password">').append(
        $('<label for="newogpassword" class="col-sm-12 col-md-4">').text(gt('Password')),
        $('<div class="col-sm-12 col-md-8">').append(
          new PasswordView({ id: 'newogpassword', validate: true }).render().$el
        ),
        $('<div class="col-sm-12 error-msg">')
      ),
      $('<div class="form-group password-validation">').append(
        $('<label for="newogpassword2" class="col-sm-12 col-md-4">').text(gt('Confirm')),
        $('<div class="col-sm-12 col-md-8">').append(
          new PasswordView({ id: 'newogpassword2' }).render().$el
        ),
        $('<div class="col-sm-12 error-msg">')
      )
    )
    window.setTimeout(function () {
      if (_.device('desktop')) {
        // $('#newogpassword').focus();
      } else {
        $('[type="password"]').removeAttr('readonly') // Remove the read only but do not focus.  User needs to click for keyboard
      }
    }, 1000)
    return this
  },
  renderRecoverMailPrompt (el) {
    if (guardModel().getSettings().noRecovery === true) {
      el.append($('<hr style="padding:10px;"/>'),
        $('<p class="oxguard_warning">').text(gt('Warning: This password for encryption cannot be restored or recovered in any way.  If forgotten, all encrypted data will be lost')))
      return this // If no recovery set, don't add prompt for second password
    }
    if (this.model.get('initialSetup')) {
      el.append(
        $('<hr>'),
        $('<p>').text(gt('Please enter a secondary email address in case you need to reset your password.')),
        $('<div class="form-group email">').append(
          $('<label for="recoverymail" class="col-sm-12 col-md-4">').text(gt('Email:')),
          $('<div class="col-sm-12 col-md-8">').append(
            $('<input name="recoverymail" id="recoverymail" class="form-control">')
          ),
          $('<div class="col-sm-12 error-msg">')
        )
      )
    }
    return this
  }

})

const Model = Backbone.Model.extend({
  initialize () {
    if (guardModel().get('username')) return
    // FIXME: should be handled differently (potential race conditions)
    userAPI.getName().then(name => {
      guardModel().set('username', name)
      this.set('name', name, { silent: true })
    })
  },
  defaults: {
    name: guardModel().get('username') || '',
    password: '',
    email: '',
    // config
    initialSetup: false,
    sent: false
  },
  send () {
    const self = this
    this.trigger('before:send')
    if (!this.isValid()) {
      this.trigger('send send:error', this.validationError)
      return
    }
    return keysAPI.create(this.pick('name', 'password', 'email')).then(function (data) {
      // TODO: maybe move to keysAPI
      guardModel().clearAuth()
      guardModel().set('recoveryAvail', !guardModel().getSettings().noRecovery)
      this.set('sent', true)
      self.trigger('send send:ok')
      return data
    }.bind(this), function (data) {
      // TODO: indicator for error that key already exists
      // gt('Problems creating keys. Keys already exist for this email address in another account.')
      self.trigger('send send:error', gt('Problems creating keys, please try again later.'))
      return data
    })
  },
  validate (attrs) {
    const errors = []
    const minlen = guardModel().getSettings().min_password_length
    if (minlen !== undefined && attrs.password !== undefined && attrs.password.length < minlen) {
      errors.push({
        field: '.password',
        message: gt('Passwords must be at least %s characters in length', minlen)
      })
    }
    if (attrs.password !== attrs.passwordValidation) {
      errors.push({
        field: '.password-validation',
        message: gt('Passwords not equal')
      })
    }
    if ((attrs.email.length > 1) && (!validateEmail(attrs.email))) {
      errors.push({
        field: '.email',
        message: gt('Enter new secondary Email address')
      })
    }
    return errors.length === 0 ? undefined : errors
  },
  // handle password strength warnings (those not catched by validate) - adopted from oxquard/core/passwords.passwordCheck
  checkStrength () {
    let min = guardModel().getSettings().min_password_length
    let len = guardModel().getSettings().password_length
    if (min === undefined) min = 6
    if (len === undefined) len = 10

    if (len <= min) len = min + 1
    if (!this.get('password')) return

    if (this.get('password').length < min) {
      this.set('passwordStrength', 'bad')
      return
    }

    const regex = /(?=^.{8,}$)(?=.*\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/
    if (this.get('password').match(regex)) {
      this.set('passwordStrength', 'good')
      return
    }

    this.set('passwordStrength', 'weak')
  }
})

export function createKeysWizard () {
  const def = $.Deferred()
  Tour.registry.get('default/oxguard/createKeys').get('run')().then(def.resolve, def.reject)
  return def
}

function waitDiv () {
  return $('<div class="og_wait" id="keygen">').append(
    createIcon('bi/arrow-clockwise.svg').addClass('animate-spin').css('margin-right', '10px'),
    $('<span>').text(gt('Generating key, Please wait'))
  )
}

export default {
  createKeys,
  createKeysWizard,
  CreateKeysModel: Model,
  CreateKeysView: View,
  waitDiv
}
