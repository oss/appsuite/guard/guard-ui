/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import Backbone from '$/backbone'
import $ from '$/jquery'
import keysAPI from '@/io.ox/guard/api/keys'
import ModalDialog from '$/io.ox/backbone/views/modal'
import gt from 'gettext'

// callers
// - settings -> guard -> Autocrypt keys -> delete

function open (text, data) {
  return new ModalDialog({
    async: true,
    point: 'oxguard/core/confirmView',
    title: gt('Confirmation Required'),
    width: 400,
    model: new Backbone.Model(data)
  })
    .build(function () {
      this.$body.append(
        $('<span>').append(text)
      )
    })
    .addButton({ label: gt('Delete'), action: 'delete' })
    .addCancelButton()
    .on('delete', function () {
      const data = this.model.toJSON()
      return keysAPI.deleteExternalPublicKey(data.id, data.keyType).always(() => {
        this.idle()
        this.close()
      })
    })
    .open()
}

export default {
  open
}
