/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import ModalDialog from '$/io.ox/backbone/views/modal'
import guardModel from '@/io.ox/guard/core/guardModel'
import Backbone from '$/backbone'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import openSettings from '$/io.ox/settings/util'
import $ from '$/jquery'
import gt from 'gettext'

const GUARD_IGNORE_SETTINGS = 'noWarnExp'

function inSettings (id) {
  const noWarnExpiring = settings.get(GUARD_IGNORE_SETTINGS)
  if (!noWarnExpiring) return false
  const match = noWarnExpiring.filter(i => i === id)
  return match && match.length > 0
}

export function getExpiringType (expiring) {
  if (expiring.guardExpiringSoon && !inSettings(expiring.guardExpiringSoon.id)) {
    return {
      type: 'gs',
      data: expiring.guardExpiringSoon
    }
  }
  if (expiring.smimeExpiringSoon && !inSettings(expiring.smimeExpiringSoon.id)) {
    return {
      type: 'ss',
      data: expiring.smimeExpiringSoon
    }
  }
  if (expiring.smimeExpired && !inSettings(expiring.smimeExpired.id)) {
    return {
      type: 'se',
      data: expiring.smimeExpired
    }
  }
  if (expiring.guardExpired && !inSettings(expiring.guardExpired.id)) {
    return {
      type: 'ge',
      data: expiring.guardExpired
    }
  }
  return undefined
}

function getDays (exp) {
  const timeLeft = exp - new Date().getTime()
  if (timeLeft < 0) return 0
  return Math.round(timeLeft / 86400000)
}

export function showExpiring (expiring) {
  const dialog = new ModalDialog({
    async: true,
    point: 'io.ox/guard/core/expiring',
    id: 'expiringView',
    width: 560,
    model: new Backbone.Model({ key: getExpiringType(expiring) })
  }).extend({
    expl () {
      const type = this.model.get('key').type
      const prompt = type === 'gs' || type === 'ge' ? gt('%s PGP Expiration Warning', guardModel().getName()) : gt('%s S/Mime Expiration Warning', guardModel().getName())
      // #. %s product name
      this.$header.find('.modal-title').append(prompt)
    },
    body () {
      const explain = $('<div>')
      if (!this.model.get('key')) return
      const type = this.model.get('key').type
      let prompt = ''
      switch (type) {
        case 'gs':
          prompt = gt.ngettext('Your %2$s PGP keys will expire in one day.  To continue signing and receiving encrypted emails, you need to go to Settings and create new keys.',
            'Your %2$s PGP keys will expire in %1$d days.  To continue signing and receiving encrypted emails, you need to go to Settings and create new keys.',
            getDays(this.model.get('key').data.expiration), getDays(this.model.get('key').data.expiration), guardModel().getName())
          break
        case 'ge':
          prompt = gt('Your %s PGP keys have expired.  You are no longer able to use these keys to sign emails or encrypt items.  Please go to settings to create new keys.', guardModel().getName())
          break
        case 'ss':
          prompt = gt.ngettext('Your S/Mime certificate will expire in one day.  To continue signing and receiving encrypted emails, you need to go to Settings to add a new certificate.',
            'Your S/Mime certificate will expire in %1$d days.  To continue signing and receiving encrypted emails, you need to go to Settings to add a new certificate.',
            getDays(this.model.get('key').data.expiration), getDays(this.model.get('key').data.expiration))
          break
        case 'se':
          prompt = gt('Your S/Mime certificate has expired.  You are no longer be able to use this certificate to sign emails or receive encrypted emails. Please add a new certificate.')
          break
      }
      explain.append($('<p>').append(prompt))
      explain.append(gt('Expires %s', new Date(this.model.get('key').data.expiration).toLocaleDateString()))
      this.$body.append(explain)
    }
  })
    .on('ok', () => {
      dialog.close()
    })
    .on('ignore', () => {
      const ignore = settings.get(GUARD_IGNORE_SETTINGS) || []
      ignore.push(dialog.model.get('key').data.id)
      settings.set(GUARD_IGNORE_SETTINGS, ignore).save()
      dialog.close()
    })
    .on('settings', () => {
      const section = (dialog.model.get('key').type === 'gs' || dialog.model.get('key').type === 'ge') ? 'pgpkeys' : 'certificates'
      openSettings('virtual/settings/io.ox/guard', 'io.ox/guard/settings/' + section)
      dialog.close()
    })
    .addAlternativeButton({ label: gt('Dismiss'), action: 'ignore' })
    .addAlternativeButton({ label: gt('Show later'), action: 'ok', placement: 'right' })
    .addButton({ label: gt('Go to Settings'), action: 'settings' })

    .open()
}
