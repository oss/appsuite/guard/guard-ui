/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'

import BasicModel from '$/io.ox/backbone/basicModel'
import mini from '$/io.ox/backbone/mini-views/common'

const PinModel = BasicModel.extend({
  ref: 'io.ox/guard/core/pinView/model/',

  initialize (options = {}) {
    BasicModel.prototype.initialize.call(this, options)
  }
})

export const PinView = mini.InputView.extend({
  el: '<input type="email" class="form-control">',

  events: {
    change: 'onChange'
  },

  initialize (options) {
    this.options = {
      autocomplete: false,
      validate: false,
      ...options
    }

    const { id, name, validate } = this.options
    this.model = new PinModel({ id, validate })
    this.name = name || id
    this.id = id
    this.isValid = true
  },

  onChange () {
    this.$el.trigger('valid')
  },

  render () {
    const input = mini.PasswordView.prototype.render.apply(this).$el
    input.addClass(this.options.class || '')

    this.$el = $('<div class="form-group">')

    const label = this.options.label ? $('<label>').attr('for', this.id).text(this.options.label) : ''

    const error = new mini.ErrorView({
      name: this.id,
      model: this.model,
      selector: this.$el
    }).render().$el

    this.$el.append(label, input, error)
    return this
  }
})
