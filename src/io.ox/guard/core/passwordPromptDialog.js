/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import _ from '$/underscore'
import Backbone from '$/backbone'
import yell from '$/io.ox/core/yell'
import ox from '$/ox'
import gt from 'gettext'

import ModalDialog from '$/io.ox/backbone/views/modal'
import mini from '$/io.ox/backbone/mini-views/common'
import openSettings from '$/io.ox/settings/util'

import { PlainPasswordView } from '@/io.ox/guard/core/passwordView'
import guardModel from '@/io.ox/guard/core/guardModel'
import { settings } from '@/io.ox/guard/settings/guardSettings'

// DEPRECATED: Please see `duration` for a different implementation
export function renderTimeOptions (model, id, label) {
  const options = [
    { value: 0, label: gt('Always') },
    { value: 10, label: gt('After 10 minutes') },
    { value: 20, label: gt('After 20 minutes') },
    { value: 30, label: gt('After 30 minutes') },
    { value: 60, label: gt('After 1 hour') },
    { value: 120, label: gt('After 2 hours') },
    { value: 99999, label: gt('After I log out') }
  ]

  model.set('duration', settings.get('defRemember', 0))

  return $('<div>').append(
    $('<label for="duration">').text(label),
    new mini.SelectView({ id: 'duration', name: 'duration', model, list: options }).render().$el
  )
}

export function passwordPromptDialog (message, timeOption, o = {}, deferred) {
  const def = deferred || $.Deferred()

  // maximum of one open password prompt box
  if ($('.popup_password_prompt').length > 0) return def.reject('duplicate')

  new ModalDialog({
    async: true,
    point: 'oxguard_core/auth',
    title: gt('Password needed'),
    width: 350,
    focus: '#ogPassword',
    enter: 'confirm',
    model: new Backbone.Model({ type: o.type || 'pgp', message, timeOption, def })
  }).extend({
    password () {
      this.passwordView = new PlainPasswordView({
        id: 'ogPassword',
        className: 'password_prompt popup_password_prompt',
        validate: false
      })

      const message = this.model.get('message')
      const label = message && message.length > 1
        ? message
        : gt('Please enter your %s password', guardModel().getName())

      this.$body.append(
        $('<div class="form-group password">').append(
          $('<label class="simple" for="ogPassword">').text(label),
          this.passwordView.render().$el,
          new mini.ErrorView({
            model: this.passwordView.model
          }).render().$el
        )
      )
    },
    duration () {
      if (!this.model.get('timeOption')) return

      this.durationView = new mini.SelectView({
        id: 'duration',
        name: 'duration',
        model: this.model.set('duration', settings.get('defRemember', 0)),
        list: [
          { value: 0, label: gt('Always') },
          { value: 10, label: gt('After 10 minutes') },
          { value: 20, label: gt('After 20 minutes') },
          { value: 30, label: gt('After 30 minutes') },
          { value: 60, label: gt('After 1 hour') },
          { value: 120, label: gt('After 2 hours') },
          { value: 99999, label: gt('After I log out') }
        ]
      })

      this.$body.append(
        $('<div class="form-group duration">').append(
          $('<label class="simple" for="duration">').text(gt('Ask for password again')),
          this.durationView.render().$el
        )
      )
    },
    recovery () {
      // Display password help if available
      if (!guardModel().get('recoveryAvail')) return
      this.$body.append(
        $('<div class="og_forgot mt-8">').append(
          $('<a href="#">')
            .text(gt('Forgot Password'))
            .on('click', e => {
              e.preventDefault()
              this.close()
              def.reject()
              openSettings('virtual/settings/io.ox/guard')
            })
        )
      )
    },
    events () {
      this.listenTo(ox, 'guard:auth:success', function () {
        this.close()
      })
      this.listenTo(ox, 'guard:auth:fail:password', function (message) {
        const { model } = this.passwordView
        this.idle()
        this.model.set('message', message)
        model.trigger('invalid', message)
      })
      this.listenTo(ox, 'guard:auth:yellandclose', function () {
        yell('error', this.model.get('message'))
        this.close()
      })
    }
  })
    .addCancelButton()
    .addButton({ label: gt('Confirm'), action: 'confirm' })
    .on('cancel', function () {
      this.model.get('def').reject('cancel')
    })
    .on('confirm', function () {
      this.passwordView.onChange()
      this.passwordView.valid()
      if (o && _.isFunction(o.preAuthCallback)) o.preAuthCallback()
      const data = {
        password: this.passwordView.model.get('password'),
        duration: this.model.get('duration')
      }
      ox.trigger('guard:auth:login', data)
      this.model.get('def').resolve(data)
    }).open()

  return def
}

export default {
  open: passwordPromptDialog,
  renderTimeOptions
}
