/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import _ from '$/underscore'
import gt from 'gettext'
import ext from '$/io.ox/core/extensions'
import util from '@/io.ox/guard/util'
import capabilities from '$/io.ox/core/capabilities'
import guardModel from '@/io.ox/guard/core/guardModel'
import { setConfigurable, bulkAdd, st } from '$/io.ox/settings/index'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import { gs } from '@/io.ox/guard/settings/pane'

if (util.isGuardConfigured() || capabilities.has('guard-mail') || capabilities.has('guard-drive') || util.hasSmime()) {
  // Add settings for encryption to the Settings Page

  const pgp = capabilities.has('guard-mail') && !capabilities.has('smime')
  const smime = capabilities.has('smime') && !capabilities.has('guard-mail')
  const either = capabilities.has('guard-mail') || capabilities.has('smime')
  const both = capabilities.has('guard-mail') && capabilities.has('smime')
  const autocrypt = util.autoCryptEnabled()

  setConfigurable({
    DEFAULT_ENCRYPT: either,
    ENCRYPT_DRAFT: either,
    DEFAULT_SIGN: either,
    DEFAULT_SMIME: both,
    DEFAULT_REMEMBER: either,
    PGP_RESET_PASSWORD: pgp || both,
    PGP_CHANGE_PASSWORD: pgp || both,
    PGP_SET_EMAIL: pgp || both,
    SMIME_RESET_PASSWORD: smime || both,
    SMIME_CHANGE_PASSWORD: smime || both,
    SMIME_SET_EMAIL: smime || both,
    PGP_DOWNLOAD_PUBLIC: pgp || both,
    PGP_CREATE_NEW: pgp || both,
    PGP_UPLOAD_PRIVATE: pgp || both,
    PGP_UPLOAD_PUBLIC: pgp || both,
    PGP_UPLOAD_RECIP: pgp || both,
    PGP_RECIP_KEYS: pgp || both,
    PGP_AUTOCRYPT_COLLECTED: autocrypt && (pgp || both),
    UPLOAD_CERTIFICATE: smime || both,
    UPLOAD_RECIP_CERTIFICATE: smime || both,
    CERT_CHECK: smime || both,
    ENABLE_SMIME: both,
    SHOW_ADV: pgp || both,
    DEF_INLINE: pgp || both,
    MAIL_CACHE: either,
    DELETE_RECOVERY: either,
    AUTOCRYPT_ADD_OUTGOING: autocrypt,
    AUTOCRYPT_IMPORT_AUTOMATIC: autocrypt,
    AUTOCRYPT_TRANSFER: autocrypt,
    // Pages
    GUARD: either,
    PGPKEYS: pgp || both,
    PGPPASSWORD: pgp || both,
    RECIPKEYS: pgp || both,
    SMIMEPASSWORD: smime || both,
    SMIME_CERTIFICATES: smime || both,
    SMIME_RECIPIENTS: smime || both,
    GUARD_ADVANCED: either,
    AUTOCRYPT_SETTING: autocrypt
  })

  // Pages
  bulkAdd(st.GUARD_SETTINGS, '', {
    GUARD: [guardModel().getName(), 'io.ox/guard [data-section="io.ox/guard/settings/view"]', 2],
    PGPKEYS: [gs.GUARD_YOUR_KEYS_PGP, 'io.ox/guard [data-section="io.ox/guard/settings/pgpKeys"]', 2],
    PGPPASSWORD: [both ? gs.GAURD_PASSWORD_PGP : gs.GAURD_PASSWORD, 'io.ox/guard [data-section="io.ox/guard/settings/pgpPassword"]', 2],
    RECIPKEYS: [gs.GUARD_RECIPIENT_KEYS_PGP, 'io.ox/guard [data-section="io.ox/guard/settings/recipients"]', 2],
    SMIMEPASSWORD: [both ? gs.GAURD_PASSWORD_SMIME : gs.GAURD_PASSWORD, 'io.ox/guard [data-section="io.ox/guard/settings/smimePassword"]', 2],
    SMIME_CERTIFICATES: [gs.GUARD_YOUR_CERTIFICATES, 'io.ox/guard [data-section="io.ox/guard/settings/certificates"]', 2],
    SMIME_RECIPIENTS: [gs.GUARD_SMIME_RECIPIENTS, 'io.ox/guard [data-section="io.ox/guard/settings/recipCertificates"]', 2],
    GUARD_ADVANCED: [gs.GUARD_ADVANCED_EXPLANATION, 'io.ox/guard [data-section="io.ox/guard/settings/advanced"]', 2],
    AUTOCRYPT_SETTINGS: [gs.AUTOCRYPT_SETTING, 'io.ox/guard [data-section="io.ox/guard/settings/advanced"]', 2]
  })

  // Default Settings
  bulkAdd(st.GUARD_SETTINGS, st.GUARD, {
    DEFAULT_ENCRYPT: [gs.GUARD_DEFAULT_ENCRYPT, 'io.ox/guard [data-section="io.ox/guard/settings/view"]', 2],
    ENCRYPT_DRAFT: [gs.ENCRYPT_DRAFT, 'io.ox/guard [data-section="io.ox/guard/settings/view"]', 2],
    DEFAULT_SIGN: [gs.GUARD_DEFAULT_SIGN, 'io.ox/guard [data-section="io.ox/guard/settings/view"]', 2],
    DEFAULT_SMIME: [gs.GUARD_DEFAULT_SMIME, 'io.ox/guard [data-section="io.ox/guard/settings/view"]', 2],
    DEFAULT_REMEMBER: [gs.GUARD_REMEMBER_PASSWORD, 'io.ox/guard [data-section="io.ox/guard/settings/view"]', 2]
  })

  // PGP Password Management.  This is the default management if has both pgp and smime
  bulkAdd(st.GUARD_SETTINGS, st.PGPPASSWORD, {
    PGP_RESET_PASSWORD: [gs.RESET_PASSWORD, 'io.ox/guard [name="pgpResetButton"]', 2],
    PGP_CHANGE_PASSWORD: [gt('Change password'), 'io.ox/guard [data-section="io.ox/guard/settings/pgpPassword"]', 2],
    PGP_SET_EMAIL: [gs.GUARD_RESET_EMAIL, 'io.ox/guard [name="pgpSecondaryButton"]', 3]
  })

  // Smime Password Management, only shown if smime only
  bulkAdd(st.GUARD_SETTINGS, st.SMIMEPASSWORD, {
    SMIME_RESET_PASSWORD: [gs.RESET_PASSWORD, 'io.ox/guard [name="smimeResetButton"]', 2],
    SMIME_CHANGE_PASSWORD: [gt('Change password'), 'io.ox/guard [data-section="io.ox/guard/settings/smimePassword"]', 2],
    SMIME_SET_EMAIL: [gs.GUARD_RESET_EMAIL, 'io.ox/guard [name="smimeSecondaryButton"]', 3]
  })

  // PGP Keys
  bulkAdd(st.GUARD_SETTINGS, st.PGPKEYS, {
    PGP_YOUR_KEYS: [gs.GUARD_YOUR_KEYS_EXPLANATION, 'io.ox/guard [data-section="io.ox/guard/settings/pgpKeys"]', 2],
    PGP_DOWNLOAD_PUBLIC: [gs.DOWNLOAD_PUBLIC, 'io.ox/guard [name="downloadPublic"]', 2],
    PGP_CREATE_NEW: [gt('Create new keys'), 'io.ox/guard [name="createNew"]', 2],
    PGP_UPLOAD_PRIVATE: [gs.UPLOAD_PRIVATE, 'io.ox/guard [name="uploadPrivate"]', 2],
    PGP_UPLOAD_PUBLIC: [gs.UPLOAD_PUBLIC, 'io.ox/guard [name="uploadPublic"]', 2]
  })

  // PGP Recipients
  bulkAdd(st.GUARD_SETTINGS, st.RECIPKEYS, {
    PGP_UPLOAD_RECIP: [gs.UPLOAD_RECIP, 'io.ox/guard [name="uploadRecip"]', 2],
    PGP_RECIP_KEYS: [gs.GUARD_RECIPIENT_KEYS, 'io.ox/guard [data-section="io.ox/guard/settings/recipients"]', 2],
    PGP_AUTOCRYPT_COLLECTED: [gs.AUTOCRYPT_COLLECTED, 'io.ox/guard [name="autocryptCollected"]', 2]
  })

  // SMIME Certificates
  bulkAdd(st.GUARD_SETTINGS, st.SMIME_CERTIFICATES, {
    ENABLE_SMIME: [gs.ENABLE_SMIME, 'io.ox/guard #oxguard-smime', 2],
    UPLOAD_CERTIFICATE: [gs.UPLOAD_CERTIFICATE, 'io.ox/guard #uploadPrivateButton', 2]
  })

  // Recipient certificates
  bulkAdd(st.GUARD_SETTINGS, st.SMIME_RECIPIENTS, {
    UPLOAD_RECIP_CERTIFICATE: [gs.UPLOAD_RECIP_CERTIFICATE, 'io.ox/guard #uploadRecipButton', 2],
    CERT_CHECK: [gs.CERTIFICATE_CHECK, 'io.ox/guard #certLookup', 2]
  })

  // Advanced
  bulkAdd(st.GUARD_SETTINGS, st.GUARD_ADVANCED, {
    SHOW_ADV: [gs.SHOW_ADVANCED, 'io.ox/guard #oxguard-advanced', 2],
    DEF_INLINE: [gs.DEFAULT_INLINE, 'io.ox/guard #oxguard-defaultinline', 2],
    MAIL_CACHE: [gs.MAIL_CACHE, 'io.ox/guard #mailcache'],
    DELETE_RECOVERY: [gs.DELETE_RECOVERY, 'io.ox/guard #deleteRecovery']
  })

  // Autocrypt
  bulkAdd(st.GUARD_SETTINGS, st.AUTOCRYPT_SETTINGS, {
    AUTOCRYPT_ADD_OUTGOING: [gs.AUTOCRYPT_ADD_OUTGOING, 'io.ox/guard #oxguard-autocryptheader'],
    AUTOCRYPT_IMPORT: [gs.AUTOCRYPT_IMPORT_AUTOMATIC, 'io.ox/guard #oxguard-autoaddautocrypt'],
    AUTOCRYPT_TRANSFER: [gs.AUTOCRYPT_TRANSFER, 'io.ox/guard [name="transferKeys"]']
  })

  ext.point('io.ox/settings/pane/main').extend({
    id: 'io.ox/guard',
    title: guardModel().getName(),
    index: 5000,
    loadSettingPane: true,
    icon: 'bi/lock.svg',

    load: () => Promise.all([
      import('@/io.ox/guard/settings/pane'),
      settings.ensureData()
    ])
  })

  ext.point('io.ox/settings/help/mapping').extend({
    id: 'guardHelp',
    index: 200,
    list () {
      _.extend(this, {
        'virtual/settings/oxguard': {
          base: 'help',
          target: 'ox.appsuite.user.sect.guard.settings.html'
        }
      })
    }
  })
}

if (!util.isGuardLoaded()) {
  const removeIfNotNeeded = function () {
    if (!util.isGuardConfigured() && !(capabilities.has('guard-mail') || capabilities.has('guard-drive'))) {
      $('[data-model="virtual/settings/oxguard"]').remove()
    }
  }
  util.addOnLoaded(removeIfNotNeeded)
}
