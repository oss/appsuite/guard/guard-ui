/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ext from '$/io.ox/core/extensions'
import pubkeys from '$/io.ox/guard/pgp/keyDetails'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import gt from 'gettext'
import core from '@/io.ox/guard/oxguard_core'
import { viewCertDialog } from '@/io.ox/guard/smime/certView'
import certs from '@/io.ox/guard/api/certs'
import '@/io.ox/guard/style.scss'
import '@/io.ox/guard/contacts/style.scss'

ext.point('io.ox/contacts/detail/content').extend({
  id: 'pgppublic',
  index: 'last',
  draw (baton) {
    // FIXME: this completely disables the point for apps other than settings and contacts (like mail called from sidepanel)
    if (!(baton.app && baton.app.cid)) return
    // advanced view only
    if (!settings.get('advanced')) return
    // not listing for distribution lists
    if (baton.data && baton.data.distribution_list) return

    const cid = baton.app.cid; const data = baton.data
    if (core.hasGuardMail()) {
      this.append(
        $('<section class="block" data-block="guard">').attr('id', 'contactKeys' + cid).append(
          $('<dl class="definition-list">').append(
            $('<dt>').text(gt('PGP Keys')),
            $('<dd>').append(
              // only keys ids are listed - all other elements are hidden (css)
              pubkeys.listPublic({
                header: false,
                id: cid,
                folder_id: data.folder_id,
                contact_id: data.id
              }).container
            )
          )
        )
      )
    }
    if (core.hasSmimeEnabled()) { this.append(getCertList(cid, data)) }
  }
})

async function getCert (email) {
  if (!email) return $.when()
  return certs.getRecipientsPublicKey(email).then((resp) => {
    return resp
  })
}

function getLine (email, data) {
  const link = $('<a>').append(email)
  link.on('click', () => {
    viewCertDialog(email, data)
  })
  return link
}

function getCertList (cid, contact) {
  const holder = $('<div>')
  $.when(getCert(contact.email1), getCert(contact.email2), getCert(contact.email3)).done((a, b, c) => {
    if (a || b || c) {
      const div = $('<div>')
      if (a) { div.append(getLine(contact.email1, a)) }
      if (b) { div.append(getLine(contact.email2, b)) }
      if (c) { div.append(getLine(contact.email3, c)) }
      holder.append($('<section class="block" data-block="guard">').attr('id', 'contactCerts' + cid).append(
        $('<dl class="definition-list">').append(
          $('<dt>').text(gt('Certificates')),
          $('<dd>').append(div)

        )
      ))
    }
  })
  return holder
}
