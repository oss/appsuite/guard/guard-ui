/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import ox from '$/ox'
import core from '@/io.ox/guard/oxguard_core'
import util from '@/io.ox/guard/util'
import guardModel from '@/io.ox/guard/core/guardModel'
import gt from 'gettext'
import $ from '$/jquery'
import _ from '$/underscore'
import yell from '$/io.ox/core/yell'
import keyCreator from '@/io.ox/guard/core/createKeys'
import certs from '@/io.ox/guard/api/certs'
import { createOxGuardPasswordPrompt } from '@/io.ox/guard/core/tempPassword'

const auth_core = {}

// Perform auth against Guard server
// Return auth code if any
// options available include
// optPrompt: Optional prompt to display to the user before password box
// minSingleUse:  Save the authentication token in the session for at least single use
// type:  Type of crypto if not PGP
// forceRelogin: If true prompts the password prompt even if auth saved
// preAuthCallback: Function to call before auth
// callback: Function to call after auth done
// failAuthCallback: Function to call on failure
auth_core.authorize = function authorize (baton, o) {
  const options = _.extend({}, o)
  const def = $.Deferred()
  checkAuth(options, def)
  return def
}

function doAuthCheck (options, def) {
  if (options.forceRelogin) { // If we must relogin, show password prompt
    promptPassword(options, def)
  } else { // Else check if we have authentication already in session
    core.checkAuth(options)
      .done(function (data) {
        let found = false
        data.forEach(function (d) {
          if (!options.type || d.type.toLowerCase() === options.type.toLowerCase()) {
            def.resolve(d.auth)
            found = true
          }
        })
        if (!found) promptPassword(options, def)
      })
      .fail(function () {
        promptPassword(options, def)
      })
  }
}
function checkAuth (options, def) {
  // If we have known auth code, then proceed without deferred action (needed for tabs)
  if (util.hasStoredPassword()) {
    doAuthCheck(options, def)
    return
  }
  ensureSetup().then(function () {
    doAuthCheck(options, def)
  }, def.reject)
}

function getPrompt (o) {
  if (o.optPrompt) return o.optPrompt
  if (o && o.type) {
    if (core.hasPGPandSmime()) {
      if (o.type.toLowerCase() === 'smime') {
        return gt('Enter %s S/MIME security password', guardModel().getName())
      }
      return gt('Enter %s PGP security password', guardModel().getName())
    }
  }
}

// Display password prompt to authorize
function promptPassword (options, def) {
  const prompt = getPrompt(options)
  core.getPassword(prompt, true, options) // Get password
  ox.off('guard:auth:login') // we only want one event registered
  ox.on('guard:auth:login', function (passdata) {
    if (passdata.duration > 0 || options.minSingleUse) {
      core.savePassword(passdata.password, passdata.duration, options.type)
        .done(function (data) {
          def.resolve(data.auth)
          if (_.isFunction(options.callback)) {
            options.callback(data.auth)
          }
          ox.trigger('guard:auth:success', data.auth)
        })
        .fail(function (data) {
          if (_.isFunction(options.failAuthCallback)) {
            options.failAuthCallback()
          }
          handleFail(data.auth || data.code, data.error_desc)
        })
      return
    }
    core.auth(ox.user_id, passdata.password, options.type)
      .done(function (data) {
        if (data.auth.length > 20 && !(/Error:/i.test(data.auth))) {
          if (_.isFunction(options.callback)) {
            options.callback(data.auth)
          }
          ox.trigger('guard:auth:success', data.auth)
          def.resolve(data.auth)
          return
        }
        if (_.isFunction(options.failAuthCallback)) {
          options.failAuthCallback()
        }
        handleFail(data.auth || data.code, data.error_desc)
      })
  })
}

function ensureLoaded (smime) {
  if (guardModel().isLoaded()) {
    if (!smime || guardModel().hasSmimeKeys()) return $.when()
    return certs.getCertificates()
  }
  const def = $.Deferred()
  let counter = 0
  const timer = setInterval(function () {
    if (guardModel().isLoaded()) {
      if (smime) {
        certs.getCertificates().then(() => {
          def.resolve()
        })
      } else {
        def.resolve()
      }
      clearInterval(timer)
    }
    if (counter++ > 100) {
      def.reject()
      clearInterval(timer) // not loading
      console.error('Response timeout from Guard server')
    }
  }, 100)
  return def
}

function ensureSetup (encrypting, smime) {
  const def = $.Deferred()
  ensureLoaded(smime).then(function () {
    if (!util.isGuardConfigured() || (encrypting && guardModel().allExpired(smime))) {
      if (!smime) {
        keyCreator.createKeysWizard()
          .then(def.resolve, def.reject)
      } else {
        yell('error', gt('S/Mime certificate is expired. Unable to encrypt.'))
        def.reject()
      }
      return
    }
    // If keys were set up by receiving email, possible user hasn't had prompt to create password.
    if (guardModel().needsPassword()) {
      const go = function () {
        def.resolve()
      }
      createOxGuardPasswordPrompt(null, go)
    } else {
      def.resolve()
    }
  }, def.reject)

  return def
}

function handleFail (auth, message) {
  switch (auth.toLowerCase()) {
    case 'bad password':
      ox.trigger('guard:auth:fail:password', gt('Bad password'))
      return
    case 'lockout':
      ox.trigger('guard:auth:fail:password', gt('Account Locked out'))
      return
    case 'grd-mw-0003':
      ox.trigger('guard:auth:fail:password', gt('Bad password'))
      return
    case 'grd-mw-0005':
      yell('error', gt('Account locked out due to bad attempts.  Please try again later.'))
      break
    default:
      yell('error', gt('Unspecified error') + '\n' + auth + '\n' + (message || ''))
  }
}

auth_core.handleFail = handleFail

auth_core.ensureSetup = ensureSetup

export default auth_core
