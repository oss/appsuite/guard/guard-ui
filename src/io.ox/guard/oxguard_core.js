/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import gt from 'gettext'
import _ from '$/underscore'
import ox from '$/ox'
import authAPI from '@/io.ox/guard/api/auth'
import loginAPI from '@/io.ox/guard/api/login'
import util from '@/io.ox/guard/util'
import guardModel from '@/io.ox/guard/core/guardModel'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import capabilities from '$/io.ox/core/capabilities'
import yell from '$/io.ox/core/yell'
import { passwordPromptDialog } from '@/io.ox/guard/core/passwordPromptDialog'
import auth_core from '@/io.ox/guard/auth'

let badCount = 0

// Escape html data to prevent cross scripting attack
function htmlSafe (data) {
  const xss = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
  }
  return String(data).replace(/[&<>"'`=/]/g, s => xss[s])
}

// Failure Handling

function handleJsonError (error, errorResponse, baton) {
  errorResponse.errorMessage = gt('Error')
  errorResponse.retry = false
  if (error.code !== undefined) {
    switch (error.code) {
      case 'GRD-PGP-0005':
        // If bad password based on auth, then clear auth and redo
        if (guardModel().hasAuth()) {
          guardModel().clearAuth()
          if (baton) baton.view.redraw()
          return
        }
        errorResponse.errorMessage = gt('Unable to decrypt Email, incorrect password.')
        yell('error', gt('Bad password'))
        badCount++
        if (badCount > 2 && guardModel().get('recoveryAvail')) {
          errorResponse.errorMessage += ('<br/>' + gt('To reset or recover your password, click %s', '<a href="#" id="guardsettings">' + gt('Settings') + '</a>'))
        }
        errorResponse.retry = true
        break
      default:
        if (error.error !== undefined) {
          errorResponse.errorMessage = error.error
        }
    }
  }
}

function showError (errorJson) {
  const errorResp = {}
  try {
    handleJsonError(errorJson, errorResp)
  } catch (e) {
    errorResp.errorMessage = gt('Unspecified error')
  }
  yell('error', errorResp.errorMessage)
}

function checkJsonOK (json) {
  if (json && json.error) {
    showError(json)
    return false
  }
  return true
}

// Authentication Section

// Password prompt
function getPassword (message, timeOption, options, _def) {
  const def = _def || $.Deferred()
  passwordPromptDialog(message, timeOption, options, def)
    .done(data => { def.resolve(data) })
    .fail(e => { def.reject(e) })
  return (def)
}

// Save a password in both auth code and token
function savePassword (password, minutesValid, type) {
  // TODO: ensure calling methods use right value
  // 0 for session
  if (_.isString(minutesValid)) {
    try {
      minutesValid = parseInt(minutesValid)
    } catch (e) {
      console.error('Error parsing save duration valid ' + minutesValid)
    }
  }
  if (minutesValid === 0) minutesValid = -1
  if (minutesValid > 9999) minutesValid = 0

  return authAPI.authAndSave({
    password,
    minutesValid,
    type: type || ''
  }).then(function (authData) {
    let data = authData
    // This is a temporary fix for working with smime 2.10.7
    if (_.isArray(authData)) data = authData[0]
    storeAuth(data.auth, data.minutesValid, type)
    return data
  })
    .fail(function (e) {
      switch (e.code) {
        case 'GRD-MW-0003':
          ox.trigger('guard:auth:fail:password', gt('Bad password'))
          break
        case 'GRD-MW-0005':
          yell('error', gt('Account locked out due to bad attempts.  Please try again later.'))
          break
        default:
          yell('error', gt('Error checking password') + '\r\n' + e.error)
          break
      }
      console.error(e)
    })
}

// Store the auth locally for a time
function storeAuth (auth, duration, type) {
  if (duration >= 0) {
    guardModel().setAuth(auth, type)
    const time = duration * 1000 * 60
    if (time > 0) { // 0 is indefinite
      window.setTimeout(function () {
        guardModel().clearAuth(type)
      }, time)
    }
  }
}

// Check if authentication token in session and valid
const checkAuth = (function () {
  // TODO: maybe it's possible to use api-event-handlers instead ('before:check', 'success:check')
  function preAuthCallback (o) {
    if (o && _.isFunction(o.preAuthCallback)) {
      o.preAuthCallback()
      return true
    }
    return false
  }
  // TODO: maybe it's possible to use api-event-handlers instead ('fail:check')
  /**  TODO FIX THIS
    function failAuthCallback(o) {
        if (o && _define('oxguard/oxguard_core', [
            'oxguard/api/auth',
            'oxguard/api/login',
            'oxguard/util',
            'gettext!oxguard'
        ], function (authAPI, loginAPI, util, gt) {
        .isFunction(o.failAuthCallback)) {
            o.failAuthCallback();
        }
    }
    */
  function callback (o, auth) {
    if (o && _.isFunction(o.callback)) {
      o.callback(auth)
    }
  }

  return function (o) {
    let preAuthDone
    // stored auth code
    const type = (o && o.type) ? o.type : 'pgp'
    // stored auth code
    if (guardModel().hasAuth(type)) {
      preAuthDone = preAuthCallback(o)
    }
    return authAPI.check(type).then(function (data) {
      data.forEach(function (d) {
        storeAuth(d.auth, d.minutesValid, d.type)
        // OK only if exists and still time left.
        if (d.type && d.type.toLowerCase() === type.toLowerCase()) {
          if (!preAuthDone) preAuthCallback(o)
          callback(o, d.auth)
        }
      })
      return data
    }, function (e) {
      if (ox.debug) console.log(e)
      // if (preAuthDone) failAuthCallback(o)
      return $.Deferred().reject('fail')
    })
  }
})()

// Authenticate against the Guard server, checking for specific key ID, return settings info
// TODO: remove unused params userid/extra (check other potentially calling modules like office first)
function auth (userid, password, type, keyid) {
  return util.getUsersPrimaryEmail().then(function (email) {
    if (!email) return $.Deferred().reject()
    return loginAPI.login({
      encr_password: password,
      lang: ox.language,
      email,
      type
    }, keyid, type)
  })
}

// Simple call to verify password.  Keyid optional
function verifyPassword (password, keyid, type) {
  return auth(undefined, password, type, keyid).then(function (data) {
    // Good auth
    if (data.auth && data.auth.length > 20) return
    auth_core.handleFail(data.auth)
    return $.Deferred().reject(data.auth)
  })
}

function hasSmimeEnabled () {
  return hasSmime() && (!capabilities.has('guard-mail') || settings.get('smime'))
}

function hasSmime () {
  return capabilities.has('smime')
}

function hasGuardMail () {
  return capabilities.has('guard') && capabilities.has('guard-mail')
}

function hasGuard () {
  return capabilities.has('guard')
}
function hasPGPandSmime () {
  return hasSmimeEnabled() && hasGuardMail()
}

function isGuest () {
  return capabilities.has('guest')
}

export default {
  auth,
  verifyPassword,
  checkAuth,
  checkJsonOK,
  getPassword,
  htmlSafe,
  savePassword,
  showError,
  storeAuth,
  hasSmime,
  hasSmimeEnabled,
  hasGuardMail,
  hasGuard,
  hasPGPandSmime,
  isGuest
}
