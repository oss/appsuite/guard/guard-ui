/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import _ from '$/underscore'
import Backbone from '$/backbone'
import ext from '$/io.ox/core/extensions'
import ModalDialog from '$/io.ox/backbone/views/modal'
import mini from '$/io.ox/backbone/mini-views/common'
import keysAPI from '@/io.ox/guard/api/keys'
import gt from 'gettext'
import '@/io.ox/guard/pgp/style.scss'
import signatureView from '@/io.ox/guard/pgp/signatureView'

// called by
// - contact detail -> PGP Keys -> click
// - settings -> guard --> Your keys -> local keys section -> inspect icon

function openModalDialog (table, keyData) {
  return new ModalDialog({
    async: true,
    point: 'oxguard/pgp/detailview',
    title: gt('Public Keys Detail'),
    id: 'pkeyDetail',
    width: 640,
    model: new Backbone.Model({ keyData, detail: table })
  })
    .addCloseButton()
    .open()
}

ext.point('oxguard/pgp/detailview').extend(
  // Draw Div with key details
  {
    id: 'detail',
    index: 100,
    render (baton) {
      this.$body.append(
        baton.model.get('detail').div
      )
      // remove unwanted inline styles
      this.$('#keyDetail').removeAttr('style')
      // add (initially hidden) container
      this.$body.append($('<div class="keyoptions">'))
    }
  },
  // Inline
  {
    id: 'inline',
    index: 200,
    render (baton) {
      const data = baton.model.get('keyData')
      // we don't display share options for Guard created keys
      if (!data || data.guardKey || (data.shared && !data.owned) || data.localKeys || data.autoCrypt) return
      const dialog = this
      const container = this.$('.keyoptions')
      const guid = _.uniqueId('pkeyDetail-inline')

      // set initial values
      this.model.set('inline', data.inline)

      container.append(
        $('<div class="checkbox">').append(
          $('<label>').attr('for', guid).append(
            new mini.CheckboxView({ id: guid, name: 'inline', model: this.model }).render().$el,
            $.txt(' '),
            $.txt(gt('Use PGP Inline'))
          )
        )
      )

      this.model.on('change:inline', function onChangeInline (model, value, opt) {
        // ignore when value was set as rollback after an error
        if (opt && opt._rollback) return
        dialog.busy()
        return keysAPI.setInline(data.ids, value).fail(function () {
          // rollback on error
          model.set('inline', !value, { _rollback: true })
        }).always(function () {
          dialog.idle()
        })
      })
    }
  },
  // Share
  {
    id: 'share',
    index: 300,
    render (baton) {
      const data = baton.model.get('keyData')
      // we don't display share options for Guard created keys
      if (!data || data.guardKey || (data.shared && !data.owned) || data.localKeys || data.autoCrypt) return
      const dialog = this
      const container = this.$('.keyoptions')
      const guid = _.uniqueId('pkeyDetail-share')

      // set initial values
      this.model.set('share', data.shareLevel > 0)

      container.append(
        $('<div class="checkbox">').append(
          $('<label>').attr('for', guid).append(
            new mini.CheckboxView({ id: guid, name: 'share', model: this.model }).render().$el,
            $.txt(' '),
            $.txt(gt('Share Keys'))
          )
        )
      )

      this.model.on('change:share', function onChangeShare (model, value, opt) {
        // ignore when value was set as rollback after an error
        if (opt && opt._rollback) return
        dialog.busy()
        return keysAPI.setShare(data.ids, value).fail(function () {
          // rollback on error
          model.set('share', !value, { _rollback: true })
        }).always(function () {
          dialog.idle()
        })
      })
    }
  },
  {
    id: 'signatures',
    index: 400,
    render () {
      const keyData = this.model.get('keyData')
      const detail = this.model.get('detail')
      if (keyData.localKeys || keyData.autoCrypt) return

      this.addAlternativeButton({ label: gt('Signatures'), action: 'signatures' })
        .on('signatures', () => signatureView.open(detail.keylong))
    }
  },
  {
    id: 'download',
    index: 500,
    render () {
      const keyData = this.model.get('keyData')
      const detail = this.model.get('detail')
      if (keyData.localKeys || keyData.autoCrypt) return
      this.addAlternativeButton({ label: gt('Download'), action: 'download' })
        .on('download', function () {
          const element = document.createElement('a')
          element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(keyData.publicRing.ring))
          element.setAttribute('download', getFilename(detail.keyids))
          element.style.display = 'none'
          document.body.appendChild(element)
          element.click()
          this.idle()
        })
    }
  }
)

function getFilename (email) {
  let filename = ''
  if (email.indexOf('&lt') > 0) { // If has name (i.e. text before the email address, use the name
    filename = email.substring(0, email.indexOf('&lt')).trim() + '.asc'
  }
  // Return if name could be proper filename.  Otherwise just return public.asc
  // #.  public used for filename for a public key.  I.E. the filename will be public.asc
  return /^[a-z0-9_ .@(),-]+$/im.test(filename) ? filename : gt('public') + '.asc'
}

export default {
  open: openModalDialog
}
