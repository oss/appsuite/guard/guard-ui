/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import Backbone from '$/backbone'
import $ from '$/jquery'
import ox from '$/ox'
import ModalDialog from '$/io.ox/backbone/views/modal'
import http from '@/io.ox/guard/core/og_http'
import yell from '$/io.ox/core/yell'
import guardModel from '@/io.ox/guard/core/guardModel'
import { PasswordView } from '@/io.ox/guard/core/passwordView'
import gt from 'gettext'
import '@/io.ox/guard/pgp/autocrypt/style.scss'

function load (attachments) {
  const attachment = attachments[0]
  const def = $.Deferred()
  const params = `&attachment=${attachment.id}&id=${attachment.mail.id}&folder=${attachment.mail.folder_id}`
  http.get(ox.apiRoot + '/oxguard/keys?action=startAutoCryptImport', params)
    .done(data => { def.resolve(data) })
    .fail(function (err) {
      def.reject()
      console.error(err)
    })
  return def
}

export function autocryptStartupDialog (attachment) {
  load(attachment).then(
    function (details) {
      openModalDialog(attachment, details)
    }, function (e) {
      yell('error', e)
    })
}

function openModalDialog (attachment, details) {
  const dialog = new ModalDialog({
    async: true,
    point: 'oxguard/autoCrypt/view',
    title: gt('Autocrypt Start'),
    id: 'autoCryptStart',
    enter: 'add',
    model: new Backbone.Model({
      attachment,
      details
    })
  })
    .build(function () {
      const newogpassword = new PasswordView({ id: 'guardpassword', class: 'password_prompt', validate: true }).render().$el
      const newogpassword2 = new PasswordView({ id: 'guardpassword2', class: 'password_prompt', validate: true }).render().$el
      const pgpPassword = new PasswordView({ id: 'pgppassword', class: 'password_prompt' }).render().$el
      const hint = $('<td>')

      this.$body.append(
        $('<div class="form-group row">').append(
          $('<div class="col-xs-12">').append(
            $('<label for="setupcode">').text(gt('Please enter the setup code to import this key')),
            $('<input type="text" id="setupcode" class="form-control">')
          )
        ),
        $('<div class="form-group row">').append(
          $('<div class="col-xs-12">').append(
            $('<label for="pgppassword">').append(gt('Key password')),
            pgpPassword
          )
        ),
        $('<div class="form-group row">').append(
          $('<div class="col-xs-12">').append(
            $('<label for="guardpassword">').append(gt('New password')),
            newogpassword
          )
        ),
        $('<div class="form-group row">').append(
          $('<div class="col-xs-12">').append(
            $('<label for="guardpassword2">').append(gt('Verify password')),
            newogpassword2,
            hint
          )
        )
      )
    })
    .addButton({ label: gt('Add'), action: 'add' })
    .addCancelButton()
    .on('add', function () {
      if (submit(attachment[0])) this.close()
      else this.idle()
    })
    .open()
  return dialog
}

function submit (attachment) {
  if ($('#guardpassword').val() !== $('#guardpassword2').val()) {
    return false
  }
  const userdata = {
    password: $('#pgppassword').val(),
    newpassword: $('#guardpassword').val(),
    startkey: $('#setupcode').val()
  }
  const params = `&attachment=${attachment.id}&id=${attachment.mail.id}&folder=${attachment.mail.folder_id}`
  http.post(ox.apiRoot + '/oxguard/keys?action=importAutoCryptKeys', params, userdata)
    .done(function (data) {
      if (data && data.data === 'Success') {
        guardModel().clearAuth()
        yell('success', gt('Keys imported successfully'))
      } else {
        yell('error', data.error ? data.error : gt('Error importing key'))
      }
    })
  return true
}
