/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ModalDialog from '$/io.ox/backbone/views/modal'
import yell from '$/io.ox/core/yell'
import gt from 'gettext'
import { PasswordView } from '@/io.ox/guard/core/passwordView'
import guardModel from '@/io.ox/guard/core/guardModel'
import autocrypt from '@/io.ox/guard/pgp/autocrypt/autoCrypt'
import '@/io.ox/guard/pgp/autocrypt/style.scss'

export function autocryptTransferDialog () {
  const dialog = new ModalDialog({
    async: true,
    title: gt('Autocrypt Transfer Keys'),
    id: 'autoCryptStartTransfer',
    enter: 'transfer'
  })
    .build(function () {
      this.$body.append(
        $('<p>').text(gt('This will send an email to you with your current private keys.  The keys will be securely encrypted.  You can then open this email in another AutoCrypt supported email client and import the keys to be used there.')),
        $('<label for="guardpassword">').text(gt('Please enter your %s password to authorize the transfer.', guardModel().getName())),
        new PasswordView({ id: 'guardpassword', class: 'password_prompt form-control', validate: false }).render().$el
      )
    })
    .addButton({ label: gt('Transfer'), action: 'transfer' })
    .addCancelButton()
    .on('transfer', function () {
      autocrypt.transferKeys($('#guardpassword').val()).then(resp => {
        if (resp.data && resp.data.passcode) autocryptTransferCompleteDialog(resp.data.passcode)
        if (resp.error) {
          yell('error', resp.error)
          dialog.idle()
          return
        }
        dialog.close()
      })
    })
  dialog.open()
  return dialog
}

function autocryptTransferCompleteDialog (passcode) {
  const dialog = new ModalDialog({
    async: true,
    title: gt('Autocrypt Transfer Keys'),
    id: 'autoCryptStartTransfer',
    enter: 'done'
  })
    .build(function () {
      this.$body.append(
        $('<p>').text(gt('An Email with the secure information required to transfer your keys has been sent.')),
        $('<p>').text(gt('To protect your keys, the email was encrypted with the following passcode.  You will need this code in order to import the keys into another client (including the dashes).')),
        $('<div class="flex-center selectable-text font-mono font-bold mt-32 mb-16 autocryptpasscode">').text(passcode)
      )
    })
    .addButton({ label: gt('Done'), action: 'done' })
    .addAlternativeButton({ label: gt('Copy to clipboard'), action: 'copy' })
    .on('done', () => dialog.close())
    .on('copy', () => {
      navigator.clipboard.writeText(passcode)
      dialog.idle()
    })
    .open()
  return dialog
}
