/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import Backbone from '$/backbone'
import $ from '$/jquery'
import ModalDialog from '$/io.ox/backbone/views/modal'
import publicKeys from '@/io.ox/guard/pgp/keyDetails'
import gt from 'gettext'
import '@/io.ox/guard/pgp/autocrypt/style.scss'
import autocrypt from '@/io.ox/guard/pgp/autocrypt/autoCrypt'
import { createIcon } from '$/io.ox/core/components'

export function autocryptPromptDialog (model, keyData, header, def, multiple) {
  const dialog = new ModalDialog({
    async: true,
    point: 'oxguard/autoCrypt/promptView',
    title: gt('New key found'),
    id: 'autoCryptImport',
    width: 640,
    enter: 'add',
    model: new Backbone.Model({
      keyData,
      request: model,
      details: publicKeys.keyDetail(keyData.keys)
    })
  }).build(function () {
    const div = $('<div class="autoheader">')
    div.append(gt('A new public key was sent for this sender.  Would you like to import it?'))
    div.append(getSummary(this.model))
    const acDetails = $('<div class="ac-toggle-details">').on('click', showDetails)
    acDetails.append(
      $('<a>').text(gt('Details')),
      createIcon('bi/arrow-down-up.svg').css('margin-left', '7px')
    )
    this.$body.append(
      div.append(acDetails),
      $('<div class="autocryptDiv">').append(this.model.get('details').div)
    )
  })
    .addButton({ label: gt('Add'), action: 'add' })
    .on('add', function () {
      autocrypt.perform(header, model, true).then(() => { def.resolve() })
      this.close()
    })
    .on('cancel', () => { def.reject() })
    .addCancelButton()
  if (multiple) {
    dialog.addAlternativeButton({ label: gt('Cancel All'), action: 'cancelAll' })
      .on('cancelAll', function () {
        dialog.close()
        def.reject('all')
      })
  }
  dialog.open()
  return dialog
}

function showDetails (e) {
  e.preventDefault()
  $('.ac-toggle-details').toggleClass('open')
  $('.autocryptDiv').toggle($('.ac-toggle-details').hasClass('open'))
}

function getSummary (model) {
  const details = model.get('details')
  const fingerprint = (details.fingerprints && details.fingerprints.length > 0)
    ? $('<span class="fingerprint">').text(details.fingerprints[0].substring(details.fingerprints[0].length - 8))
    : ''
  return $('<div class="autoCryptSummary">').append($('<span class="keyIds">').append(details.keyids), fingerprint)
}
