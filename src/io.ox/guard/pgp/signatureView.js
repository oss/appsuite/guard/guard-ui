/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import Backbone from '$/backbone'
import ModalDialog from '$/io.ox/backbone/views/modal'
import keysAPI from '@/io.ox/guard/api/keys'
import core from '@/io.ox/guard/oxguard_core'
import gt from 'gettext'
import '@/io.ox/guard/pgp/style.scss'

function openModalDialog (keyId) {
  return keysAPI.getSignatures(keyId).then(function (data) {
    return new ModalDialog({
      async: false,
      point: 'oxguard/pgp/signatureview',
      title: gt('Public Keys Detail'),
      id: 'pkeyDetail',
      width: 500,
      model: new Backbone.Model({ data })
    })
      .build(function () {
        const data = this.model.get('data')

        if (data.length === 0) return this.$body.append($('<b>').text(gt('No signatures')))

        // #. Table headers for the ID "signed", the level of the "certification", and who it was "signed by"
        const table = $('<table class="signatureTable">').append(
          $('<th>').text(gt('ID Signed')),
          $('<th>').text(gt('Certification')),
          $('<th>').text(gt('Signed by'))
        )
        for (let i = 0; i < data.length; i++) {
          const tr = $('<tr>')
          const td1 = $('<td>')

          if (data[i].image) {
            if (data[i].image !== undefined) {
              td1.append($('<img alt="Embedded Image" style="max-height:50px;max-width:50px;">').attr('src', data[i].image.imageData))
            } else {
            // #. Type of object signed was an image
              data[i].ID = gt('Image') // Translate image response
              td1.append(data[i].ID)
            }
          } else if (data[i].userId) {
            td1.append($('<span>').append(core.htmlSafe(data[i].userId)))
          }
          tr.append(td1)
          const td2 = $('<td>')
          const signatureType = $('<span>').attr('title', data[i].description).append(translate(data[i].signatureType))
          td2.append(signatureType)
          tr.append(td2)
          const td3 = $('<td>')
          if (data[i].keyMissing) {
            td3.append(gt('Missing public'))
          }
          if (data[i].issuer) {
            td3.append(core.htmlSafe(data[i].issuer))
          }
          tr.append(td3)
          table.append(tr)
        }

        this.$body.append(
          table
        )
      })
      .addCloseButton()
      .open()
  })
}

function translate (result) {
  switch (result) {
    case 19:
      // #. In table, the key was "Positively" identified by the signer (verification level)
      return (gt('Positive'))
    case -1:
      // #. In table, the signature failed to be verified
      return (gt('Failed check'))
    case 0:
      // #. In table, there was no public key available to verify the signature
      return (gt('Missing public'))
    case 18:
      // #. In table, the key was "Casually" identified by the signer (verification level)
      return (gt('Casual'))
    case 32:
      // #. In table, the key was revoked with this signature
      return (gt('Revocation'))
    case 31:
      // #. In table, the key was "Directly" certified by the signer (verification level)
      return (gt('Direct cert'))
    case 16:
      // #. In table, the key had "Default certification" by the signer (verification level)
      return (gt('Default Certification'))
    case 40:
      // #. In table, the key was revoked with this signature
      return (gt('Revocation'))
    case 24:
      // #. In table, the key is a bound subkey
      return (gt('Subkey Binding'))
    case 25:
      // #. In table, primary key binding
      return (gt('Primary Binding'))
    default:
      return (result)
  }
}

export default {
  open: openModalDialog
}
