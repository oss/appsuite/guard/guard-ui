/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ext from '$/io.ox/core/extensions'
import ox from '$/ox'
import yell from '$/io.ox/core/yell'
import ModalDialog from '$/io.ox/backbone/views/modal'
import { PasswordView } from '@/io.ox/guard/core/passwordView'
import guardModel from '@/io.ox/guard/core/guardModel'
import core from '@/io.ox/guard/oxguard_core'
import { refresh } from '@/io.ox/guard/pgp/userKeys'
import gt from 'gettext'

let uploading = false

// Uploads to personal key chain
// upload(files) for uploading public key only
// upload (file, private, pgp Password, Guard password) for uploading private key
function upload (files, priv, pgp, guard) {
  const deferred = $.Deferred()
  if (uploading) return $.Deferred().reject()
  const formData = new FormData()
  for (let l = 0; l < files.length; l++) {
    if (!validFileType(files[l])) {
      deferred.reject()
      return deferred
    }
    if (files[l] !== undefined) { formData.append('key' + l, files[l]) }
  }
  if (priv) {
    formData.append('keyPassword', pgp)
    formData.append('newPassword', guard)
  }
  uploading = true
  const url = ox.apiRoot + '/oxguard/keys?action=upload&respondWithJSON=true&session=' + ox.session
  $.ajax({
    url,
    type: 'POST',
    data: formData,
    processData: false, // tell jQuery not to process the data
    contentType: false, // tell jQuery not to set contentType
    success (data) {
      if (data === null) data = ''
      if (core.checkJsonOK(data)) {
        try {
          if (data.data && data.data.keyRings) {
            const keys = data.data.keyRings
            let added = gt('Added keys: \r\n')
            for (let i = 0; i < keys.length; i++) {
              const key = keys[i]
              if (key.privateRing && key.privateRing.keys) {
                const privateKeys = key.privateRing.keys
                added = added + gt('Private keys: ')
                for (let p = 0; p < privateKeys.length; p++) {
                  const pKey = privateKeys[p]
                  added = added + pKey.fingerPrint + '\r\n'
                  for (let d = 0; d < pKey.userIds.length; d++) {
                    added = added + pKey.userIds[d] + ' '
                  }
                  added = added + '\r\n'
                }
              } else {
                const publicKeys = key.publicRing.keys
                added = added + gt('Public keys: ')
                for (let k = 0; k < publicKeys.length; k++) {
                  const pubKey = publicKeys[k]
                  added = added + pubKey.fingerPrint + '\r\n'
                  for (let j = 0; j < pubKey.userIds.length; j++) {
                    added = added + pubKey.userIds[j] + ' '
                  }
                  added = added + '\r\n'
                }
              }
            }
            yell('success', added)
          }
        } catch (e) {
          console.log(e)
        }
        $('input[name="publicKey"]').val('')
        deferred.resolve('OK')
      } else {
        deferred.reject(data.error)
      }
      uploading = false
    },
    error (XMLHttpRequest) {
      $('input[name="publicKey"]').val('')
      uploading = false
      const resp = XMLHttpRequest.responseText.trim()
      switch (resp.trim()) {
        case 'Bad UID':
          yell('error', gt('Failed to upload') + '\r\n' + gt('Invalid Email Address'))
          break
        case 'Bad ID':
          yell('error', gt('The public key must have your primary email as a user ID'))
          break
        default:
          yell('error', gt('Failed to upload') + ' ' + XMLHttpRequest.responseText.trim())
          break
      }
      deferred.reject(XMLHttpRequest.responseText.trim())
    }
  })
  return (deferred)
}

// Verify ascii data contains key before upload
function validateKeyData (key) {
  if (!/(-----BEGIN PGP PRIVATE KEY BLOCK-----|-----BEGIN PGP PUBLIC KEY BLOCK-----)[\d\w\r\n:. +/=]+(-----END PGP PRIVATE KEY BLOCK-----|-----END PGP PUBLIC KEY BLOCK-----)/i.test(key)) {
    yell('error', gt('Does not appear to be a valid key file type'))
    return false
  }
  return true
}

// Upload ascii key data
function uploadPublicKey (key) {
  const deferred = $.Deferred()
  if (!validateKeyData(key)) {
    deferred.reject()
    return deferred
  }
  const formData = new FormData()
  formData.append('key', key)
  const url = ox.apiRoot + '/oxguard/keys?action=upload&session=' + ox.session
  $.ajax({
    url,
    type: 'POST',
    data: formData,
    processData: false, // tell jQuery not to process the data
    contentType: false, // tell jQuery not to set contentType
    success (data) {
      if (core.checkJsonOK(data)) {
        yell('success', gt('Keys uploaded'))
        refresh()
        deferred.resolve()
      }
    },
    error (XMLHttpRequest) {
      $('input[name="publicKey"]').val('')
      uploading = false
      const resp = XMLHttpRequest.responseText.trim()
      switch (resp.trim()) {
        case 'Bad UID':
          yell('error', gt('Failed to upload') + '\r\n' + gt('Invalid Email Address'))
          break
        case 'Bad ID':
          yell('error', gt('The public key must have your primary email as a user ID'))
          break
        default:
          yell('error', gt('Failed to upload') + ' ' + XMLHttpRequest.responseText.trim())
          break
      }
      deferred.reject(XMLHttpRequest.responseText.trim())
    }
  })
  return deferred
}

function uploadExternalKey (files) {
  const deferred = $.Deferred()
  if (uploading) return $.Deferred().reject()
  const formData = new FormData()
  for (let l = 0; l < files.length; l++) {
    if (files[l] !== undefined) {
      if (!validFileType(files[l])) {
        deferred.reject()
        return deferred
      }
      formData.append('key' + l, files[l])
    }
  }
  uploading = true
  const url = ox.apiRoot + '/oxguard/keys?action=uploadExternalPublicKey&respondWithJSON=true&session=' + ox.session
  $.ajax({
    url,
    type: 'POST',
    data: formData,
    processData: false, // tell jQuery not to process the data
    contentType: false, // tell jQuery not to set contentType
    success (data) {
      if (data === null) data = ''
      if (core.checkJsonOK(data)) {
        deferred.resolve('ok')
      } else {
        deferred.reject()
      }
      uploading = false
    },
    error (XMLHttpRequest) {
      $('input[name="publicKey"]').val('')
      deferred.reject(XMLHttpRequest.responseText.trim())
      uploading = false
    }
  })
  return (deferred)
}

function validFileType (file) {
  if (!/^.*\.(txt|pgp|gpg|asc)$/i.test(file.name)) {
    console.error('bad file type ' + file.name)
    yell('error', gt('Does not appear to be a valid key file type'))
    return false
  }
  return true
}

function createPasswordPrompt (files, def) {
  return new ModalDialog({
    async: true,
    point: 'oxguard/pgp/uploadKeysPasswordPrompt',
    title: gt('Upload Private Keys'),
    width: 450,
    enter: 'ok',
    focus: '#pgppassword'
  })
    .inject({
      doUpload () {
        return doUploadPrivate(files)
      }
    })
    .build(function () {
    })
    .addButton({ label: gt('OK'), action: 'ok' })
    .addCancelButton()
    .on('ok', function () {
      const dialog = this
      this.doUpload().done(function () {
        def.resolve()
        dialog.close()
      })
        .fail(function (e) {
          yell(e)
          $('#uploaderror').text(e)
          dialog.idle()
        })
    })
    .on('cancel', function () {
      def.reject()
    })
    .open()
}

ext.point('oxguard/pgp/uploadKeysPasswordPrompt').extend(
  {
    index: 100,
    id: 'passwordPrompts',
    render () {
      this.$body.append(
        createPrompts()
      )
    }
  })

function createPrompts () {
  const explain = $('<div><p>' + gt('Please enter passwords for the upload') + '</p>')
  const passdiv = $('<div>').addClass('row-fluid')
  const noSaveWorkAround = $('<input style="display:none" type="text" name="dontremember"/><input style="display:none" type="password" name="dontrememberpass"/>')
  const newogpassword = new PasswordView({ id: 'guardpassword', class: 'password_prompt', validate: true }).render().$el
  const newogpassword2 = new PasswordView({ id: 'guardpassword2', class: 'password_prompt', validate: true }).render().$el
  const pgpPassword = new PasswordView({ id: 'pgppassword', class: 'password_prompt' }).render().$el
  const hint = $('<div style="min-height:30px;">')
  passdiv.append(noSaveWorkAround)
  passdiv.append($('<label for="pgppassword">' + gt('Password used to protect the file you are uploading:') + '</label>')).append(pgpPassword)
  passdiv.append('<label for="guardpassword">' + gt('Enter a new password to use.  You will be prompted for this password when this key is used to decrypt an item:') + '</label>').append(newogpassword)
  passdiv.append('<label for="guardpassword2">' + gt('Confirm the new password:') + '</label>').append(newogpassword2)
  passdiv.append(hint)
  const errormessage = $('<span id="uploaderror" style="color:red;"></span>')
  return explain.append(passdiv).append(errormessage)
}

function doUploadPrivate (files) {
  const deferred = $.Deferred()
  const pgp = $('#pgppassword').val()
  const guard = $('#guardpassword').val()
  const guard2 = $('#guardpassword2').val()
  if (guard !== guard2) {
    deferred.reject(gt('Passwords not equal'))
    return deferred
  }
  if (guardModel().getSettings().min_password_length !== undefined) { // Check min password length
    if (guard.trim().length < guardModel().getSettings().min_password_length) {
      deferred.reject(gt('New password must be at least %s characters long', guardModel().getSettings().min_password_length))
      return deferred
    }
  } else if (guard.trim().length < 2) { // If no min defined, make sure at least 2 characters there
    uploadPrivate(files)
    return deferred
  }
  upload(files, true, pgp, guard)
    .done(function () {
      deferred.resolve('OK')
    })
    .fail(function (e) {
      console.log(e)
      deferred.reject(e ? e.trim() : '')
      handleFail(e, yell)
    })
  return deferred
}

function uploadPrivate (files) {
  const deferred = $.Deferred()
  for (let i = 0; i < files.length; i++) {
    if (!validFileType(files[i])) {
      deferred.reject()
      return deferred
    }
  }
  createPasswordPrompt(files, deferred)
  return deferred
}

function handleFail (e, yell) {
  if (e === 'duplicate') {
    yell('error', gt('This key already exists'))
    return
  }
  if (e === 'Bad pgp') {
    yell('error', gt('Bad PGP Password'))
    return
  }
  if (e === 'Bad password') {
    yell('error', gt('Bad Password'))
    return
  }
  if (e === 'Bad ID') {
    yell('error', gt('The private key must have your primary email as a user ID'))
    return
  }
  if (e === 'Bad Merge Password') {
    yell('error', gt('Must use same password as existing key to merge'))
    return
  }
  yell('error', gt('Problem uploading Private Key ') + '\r\n' + e)
}

export default {
  upload,
  uploadPrivate,
  uploadPublicKey,
  uploadExternalKey,
  createPrompts
}
