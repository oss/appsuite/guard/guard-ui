/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import _ from '$/underscore'
import gt from 'gettext'
import core from '@/io.ox/guard/oxguard_core'
import keysAPI from '@/io.ox/guard/api/keys'
import '@/io.ox/guard/style.scss'
import '@/io.ox/guard/pgp/style.scss'
import autocrypt from '@/io.ox/guard/pgp/autocrypt/autoCrypt'
import uploader from '@/io.ox/guard/pgp/uploadkeys'
import confirmView from '@/io.ox/guard/core/confirmView'
import detailView from '@/io.ox/guard/pgp/detailView'
import { createIcon } from '$/io.ox/core/components'

function hideWait () {
  $('#og_wait').hide()
}

const keyRingsCache = {}

// Table generation

// Generate list of public keys
// - contact detail -> PGP Keys
// - settings -> guard -> Public keys of recipients
// - settings -> guard -> Autocrypt keys

function listPublic (opt) {
  const o = _.extend({
    minimal: false,
    header: true,
    class: 'publicKeyDiv',
    title: gt('PGP Public Key List'),
    id: 'keyList'
  }, opt)

  const tableId = 'keylisttable' + o.id
  const container = $('<div class="key-container">').addClass(o.class).append(
    $('<div class="keytable">').append(
      // drawn/updated via 'updateKeyTable'
      $('<table>').attr('id', tableId)
    )
  )

  const fileinput = $('<input type="file" name="publicKey" style="display:none;" multiple>')
  if (o.header) {
    const tableHeader = $('<legend style="border: none">').text(o.title)
    if (!o.minimal) tableHeader.append(fileinput)
    container.prepend(tableHeader)
  }

  update(o.id, o.folder_id, o.contact_id)

  // workaround: until we have a proper models and views
  keysAPI.on('create', triggerRefresh)
  keysAPI.on('delete revoke', function (e, keyId) {
    // check if delete key is part of the table and refresh
    const node = $('#' + tableId + ' [data-ids="' + keyId + '"]')
    if (node.length) triggerRefresh()
  })

  function uploadPublic () {
    fileinput.unbind('change')
    fileinput.on('change', function () {
      const files = this.files
      if (files.length > 0) {
        uploader.uploadExternalKey(files)
          .done(function () {
            updateKeyTable(o.id, o.folder_id, o.contact_id)
          })
          .always(() => fileinput.val(''))
      }
    })
    fileinput.click()
  }

  function triggerRefresh () {
    updateKeyTable(o.id, o.folder_id, o.contact_id)
  }

  return {
    container,
    uploadPublic,
    triggerRefresh
  }
}

function getTable (keyRings, tableId, autoCrypt) {
  let showDelete = false
  const newtable = $('<table class="keytable" id="keylisttable' + tableId + '">')
  // #. Table headers for list of keys, option for Details and to delete
  const headers = $('<th>' + gt('Email') + '</th><th>' + gt('Key Ids') + '</th><th class="keyDelete">' + gt('Delete') + '</th>' +
    (autoCrypt ? ('<th>' + gt('Verified') + '</th>') : ''))
  if (keyRings && keyRings.length > 0) {
    newtable.append(headers)
    for (const i in keyRings) {
      const tr = $('<tr>').attr({
        'data-ids': keyRings[i].ids,
        'data-autocrypt': autoCrypt
      })

      if (keyRings[i].guardKey === true || keyRings[i].owned === false) tr.addClass('oxguard_key_share')
      if (keyRings[i].expired === true) tr.css('color', 'red')
      let email = getUserIds(keyRings[i].publicRing)
      email = core.htmlSafe(email)
      const td1 = $('<td class="oxguard_pubkeylist emailKey" title="' + email + '">')
      td1.append(email)
      tr.append(td1)
      const td2 = $('<td class="oxguard_pubkeylist keyId" title="' + email + ' ' + keyRings[i].ids + '">')
      td2.append($('<a href="#">')
        .attr('ids', keyRings[i].ids)
        .attr('notowned', ((keyRings[i].guardKey === true) || (keyRings[i].owned === false)) ? 'true' : 'false')
        .on('click', detail)
        .append(keyRings[i].ids))
      tr.append(td2)
      const td5 = $('<td class="keyDelete">')
      if ((keyRings[i].guardKey !== true) && (keyRings[i].owned === true)) {
        const del = $('<a href="#">')
          .append(createIcon('bi/trash.svg'))
          .attr({
            value: keyRings[i].ids,
            'data-autocrypt': autoCrypt,
            tableid: tableId,
            title: gt('Delete')
          })
          .on('click', onDelete)
        td5.append(del)
        showDelete = true
      }
      let td6 = ''
      if (autoCrypt) {
        td6 = $('<td class="keyDownload">')
        const verify = keyRings[i].verified
          ? ($('<a href="#">')
              .attr('ids', keyRings[i].ids)
              .attr('verified', true)
              .attr('title', gt('Verified'))
              .append(createIcon('bi/check-square.svg')))
          : ($('<a href="#">')
              .attr('ids', keyRings[i].ids)
              .attr('title', gt('Verify'))
              .append(createIcon('bi/check-square.svg')))
        verify.click(verifyAutocrypt)
        td6.append(verify)
      }
      tr.append(td5).append(td6)
      newtable.append(tr)
      keyRings[i].autoCrypt = autoCrypt
      keyRingsCache[keyRings[i].ids] = keyRings[i]
    }
    if (!showDelete) { // If only system keys, then hide the delete column
      newtable.find('.keyDelete').empty()
    }
    if (tableId) {
      $('.contactKeys' + tableId).show()
    }
    return newtable
  }
  return ('<table id="keylisttable' + tableId + '"><tr><td>' + gt('No Keys Found') + '</td></tr></table>')
}

//  Shows details of keys in div
//  Can be called for local keys, or for pgp public key list
//  Minimal triggers display of details such as primary, signing, etc or not
function keyDetail (keys, minimal) {
  let keyids = ''
  const fingerprints = []
  let keylong = '' // This will be the key that has the ids (usually primary)
  let images = null
  const resultdiv = $('<div>')
  for (const i in keys) {
    const key = keys[i]
    // keyids = keyids + data[i].Key + ' ';  // List of key ids for changing share, inline (linked)
    if (i > 0) resultdiv.append('<hr>')
    const detaildiv = $('<div id="keyDetail"' + (minimal ? ' >' : ' style="padding-left:20px;">'))
    // #. if key is "expired"
    if (key.expired) {
      detaildiv.append('<b>' + gt('Expired') + '</b><br>')
      detaildiv.addClass('expired')
    }
    if (key.revoked) detaildiv.append('<b class="revoked">' + gt('Revoked') + '</b><br>')
    detaildiv.append((key.privateKey ? ('<b>' + gt('Private Key Available') + '</b>') : gt('Public Key')) + '<br>')
    // #. Fingeprint of the key
    detaildiv.append(gt('Fingerprint:') + ' ' + formatFingerprint(key.fingerPrint) + '<br>')
    fingerprints.push(key.fingerPrint)
    detaildiv.append(gt('Created:') + ' ' + new Date(key.creationTime) + '<br>')
    if (!minimal) {
      // #. Key is a Primary key
      detaildiv.append(gt('Primary:') + ' ' + (key.masterKey ? gt('True') : gt('False')) + ',  ')
      // #. Key is an encryption key
      detaildiv.append(gt('Encryption:') + ' ' + (key.usedForEncryption ? gt('True') : gt('False')))
      if (key.usedForSigning) {
        detaildiv.append(', ' + gt('Sign:') + ' ' + gt('True') + '<br>')
      } else {
        detaildiv.append('<br>')
      }
      let ids = ''
      for (const j in key.userIds) {
        if (j > 0) ids = ids + ', '
        ids = ids + core.htmlSafe(key.userIds[j])
      }
      if (ids !== '') {
        detaildiv.append(gt('IDs: ') + ids + '<br>')
        keylong = key.id // The ID of the key with IDs for signature verification
        keyids += ids // Accumulate IDs
      }
      if (key.images !== undefined) {
        if (key.images.length > 0) {
          images = key.images
          const image = $('<img alt="Embedded Image" id="imageID" src="' + key.images[0].imageData + '" style="float:right;max-height:50px;max-width:50px;"/>')
          detaildiv.append(image)
        }
      }
      if (key.validSeconds) {
        detaildiv.append(gt('Expires: %1$s', new Date(key.creationTime + (key.validSeconds * 1000))) + '<br>')
      }
    }
    resultdiv.append(detaildiv)
  }
  return {
    div: resultdiv,
    keyids,
    keylong,
    images,
    fingerprints
  }
}

// Confirm deletion of item
function onDelete (e) {
  e.preventDefault()
  const row = $(e.target).closest('tr')
  const data = {
    id: row.attr('data-ids'),
    keyType: row.attr('data-autocrypt') === 'true' ? 'autocrypt' : undefined
  }
  confirmView.open(gt('Please confirm you want to delete this key.'), data)
}

// Verify autocrypt key action
function verifyAutocrypt (e) {
  e.preventDefault()
  autocrypt.verify(e)
}

// Basic list of public key information
// Shows keyDetail function output, plus adds buttons for sharing, etc in dialog
// Shown with the inspect icon in pgp public key list
function detail (e) {
  const id = $(this).attr('ids')
  const keyData = keyRingsCache[id]
  if (!keyData) return
  const keydetail = keyDetail(keyData.publicRing.keys)
  detailView.open(keydetail, keyData)
  e.preventDefault()
}

function update (id, folder_id, contact_id) {
  updateKeyTable(id, folder_id, contact_id)
}

// Switch depending on if specified contact
function updateKeyTable (id, folder_id, contact_id) {
  const isContact = folder_id && contact_id
  const autoCrypt = id === 'autoCrypt'
  const def = isContact
    ? keysAPI.getContactKeys(folder_id, contact_id)
    : keysAPI.getExternalPublicKeys(autoCrypt)

  return def.then(function (data) {
    $('#keylisttable' + id).replaceWith(getTable(data, id, autoCrypt))
    hideWait()
    return data
  })
}

// Utilities

function getUserIds (publicRing) {
  let ids = ''
  if (publicRing.keys && publicRing.keys.length > 0) {
    for (const i in publicRing.keys) {
      const key = publicRing.keys[i]
      if (key.userIds && key.userIds.length > 0) {
        for (const k in key.userIds) {
          if (key.userIds[k] && key.userIds[k].length > 1) {
            ids += key.userIds[k] + ' '
          }
        }
      }
    }
  }
  return ids
}

function formatFingerprint (fp) {
  if (fp.length < 28) return (fp)
  if (fp.indexOf(' ') < 0) { // if not spaced
    let newFingerprint = ''
    for (let i = 0; i < fp.length; i = i + 4) {
      newFingerprint += fp.substring(i, i + 4) + ' '
    }
    fp = newFingerprint
  }
  const line1 = fp.substring(0, 24)
  const line2 = fp.substring(25, fp.length - 10) + '<b>' + fp.substring(fp.length - 10) + '</b>'
  return ('<span class="fingerPrint">' + line1 + '</span><br/><span class="fingerPrint">' + line2 + '</span>')
}

export default {
  listPublic,
  keyDetail
}
