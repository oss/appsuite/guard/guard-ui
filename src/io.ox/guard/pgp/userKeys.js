/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import _ from '$/underscore'
import '@/io.ox/guard/oxguard_core'
import keysAPI from '@/io.ox/guard/api/keys'
import guardModel from '@/io.ox/guard/core/guardModel'
import gt from 'gettext'
import '@/io.ox/guard/pgp/style.scss'
import uploadPrivateView from '@/io.ox/guard/settings/views/uploadPrivateView'
import Dropdown from '$/io.ox/backbone/mini-views/dropdown'
import Backbone from 'backbone'
import signatureView from '@/io.ox/guard/pgp/signatureView'
import { createButton, createIcon } from '$/io.ox/core/components'
import { downloadPrivateDialog } from '@/io.ox/guard/settings/views/downloadPrivateDialog'
import { deletePublicDialog } from '@/io.ox/guard/settings/views/deletePublicDialog'
import { deletePrivateDialog } from '@/io.ox/guard/settings/views/deletePrivateDialog'
import { revokePrivateDialog } from '@/io.ox/guard/settings/views/revokePrivateDialog'
import { editUserIdDialog } from '@/io.ox/guard/settings/views/editUserIdDialog'
import { privateDetailDialog } from '@/io.ox/guard/settings/views/privateDetailDialog'

// Generate list of public keys
// - settings -> guard -> Your Keys

let userKeyData = null

// register refresh listeners
keysAPI.on('create', refresh)
keysAPI.on('delete revoke', function (e, keyId) {
  // check if delete key is part of the table and refresh
  const node = $('#userkeytable' + ' [data-id="' + keyId + '"]')
  if (node.length) refresh()
})

// List of users private keys
function userKeys () {
  const pubdiv = $('<div class="userKeyDiv"/>')
  const tableHeader = $('<b>').text(gt('Your key list'))
  const tablediv = $('<div class="keytable">')
  const table = $('<table id="userkeytable">')
  pubdiv.append(tableHeader.append(tablediv.append(table)))
  refresh()
  return (pubdiv)
}

export function refresh () {
  const def = $.Deferred()
  updateUserKeyTable().done(function () {
    def.resolve()
  })
  return def
}

function checkExpiring (data) {
  if (data.expired || data.validSeconds === 0) return false
  const checkDates = guardModel().get('expiring') && guardModel().get('expiring').daysWarning ? guardModel().get('expiring').daysWarning : 30
  return (data.validSeconds < checkDates * 86400)
}

function updateUserKeyTable () {
  const deferred = $.Deferred()
  userKeyData = {}
  // Send the request to the oxguard server for the encrypted content key to decrypt
  keysAPI.getKeys().done(function (keyrings) {
    if (keyrings.length === 0) {
      // Keys deleted, no keys, open new key dialog
      guardModel().setAuth('No Key')
      uploadPrivateView.open()
    }
    if (keyrings.length !== 0) {
      // create table and headers
      const table = $('<table id="userkeytable">')

      _.each(keyrings, function (keyring, index) {
        _.each(keyring.publicRing.keys, function (data) {
          // Save in cache
          userKeyData[data.id] = data

          // add row for each key
          table.append(
            $('<tr>')
              .attr({ 'data-id': data.id })
              .append(
                // fingerprint
                $('<td data-name="fingerprint">').append(
                  $('<button type="button" class="btn btn-link fingerprint">').append(
                    data.masterKey ? $() : createIcon('bi/arrow-return-right.svg').addClass('ms-8'),
                    $('<span>').text(data._short)
                  )
                    .addClass(data.revoked ? 'revoked' : '')
                    .addClass(data.expired ? 'expired' : '')
                ),
                $('<td data-name="status">').append(
                  data.expired
                    ? $('<span class="label label-subtle subtle-red me-8">').text(gt('Expired'))
                    : '',
                  data.revoked
                    ? $('<span class="label label-subtle subtle-yellow me-8">').text(gt('Revoked'))
                    : '',
                  checkExpiring(data)
                    ? $('<span class="label label-subtle subtle-yellow me-8">').text(gt('Expiring'))
                    : ''
                ),

                // private key: check mark
                $('<td data-name="privatekey">').append(
                  data.hasPrivateKey
                    ? $('<span class="label label-subtle subtle-green me-8">').text(gt('Has private key'))
                    : $('<span class="label label-subtle subtle-grey me-8">').text(gt('Public key only'))
                ),

                // current: check mark OR checkbox
                $('<td data-name="current">').append(
                  keyring.current
                    ? $('<span class="label label-subtle subtle-green me-8">').text(gt('Current'))
                    : ''
                ),

                // action: more
                $('<td data-name="dropdown">').append(
                  renderDropdown(data)
                )
              )
          )
        })
        // add spacer
        if (index + 1 < keyrings.length) {
          table.append(
            $('<tr class="table-spacer">').append($('<td colspan="7">').append($('<hr>')))
          )
        }
      })
      $('#userkeytable').replaceWith(table)

      table.on('click', '.fingerprint', function (e) {
        const id = $(this).closest('tr').attr('data-id')
        showLocalDetail(id)
        e.preventDefault()
      })

      table.on('click', '.keyedit', function (e) {
        const id = $(this).closest('tr').attr('data-id')
        editUserIdDialog(id)
        e.preventDefault()
      })
    } else {
      $('#userkeytable').replaceWith('<table id="userkeytable"><tr><td>' + gt('No Keys Found') + '</td></tr></table>')
    }

    deferred.resolve('OK')
    $('#og_wait').hide()
  })
    .fail(function () {
      $('#og_wait').hide()
    })
  return deferred
}

function renderDropdown (line) {
  const model = new Backbone.Model(line)
  const dropdown = new Dropdown({
    label: $('<i class="fa fa-bars" aria-hidden="true">'),
    $toggle: createButton({ href: '#', variant: 'none', icon: { name: 'bi/list.svg', title: gt('Manage keys') } }),
    model,
    title: gt('Actions')
  })
  dropdown
    .link('details', gt('Details'), () => onDetails(model))
    .link('signatures', gt('Signatures'), () => onSignatures(model))
    .divider()

  // master key actions
  if (model.get('masterKey')) {
    dropdown
      .link('delete', gt('Delete'), () => onDelete(model))
      .link('revoke', gt('Revoke'), () => onRevoke(model))
    dropdown.divider()
      .link('download', gt('Download'), () => onDownload(model))
      .link('current', gt('Mark current'), () => onCurrent(model))
      .link('edit', gt('Edit IDs'), () => onEdit(model))
  }

  dropdown.render().$el.addClass('dropdown').attr('data-dropdown', 'view')

  // disable actions
  dropdown.$('[data-name="delete"]').toggleClass('disabled',
    // window.oxguarddata.settings.noDeletePrivate || !model.get('masterKey')
    !model.get('masterKey')
  )
  dropdown.$('[data-name="current"]').toggleClass('disabled',
    model.get('_current') || !model.get('masterKey')
  )
  dropdown.$('[data-name="download"]').toggleClass('disabled',
    !model.get('masterKey')
  )
  dropdown.$('[data-name="edit"]').toggleClass('disabled',
    !model.get('masterKey') || !model.get('hasPrivateKey')
  )
  dropdown.$('[data-name="revoke"]').toggleClass('disabled',
    model.get('revoked') || !model.get('hasPrivateKey')
  )

  return dropdown.$el
}

function onCurrent (model) {
  keysAPI.setCurrentKey(model.get('id')).done(function () {
    refresh()
  })
}

function onDelete (model) {
  const id = model.get('id')
  const isPrimary = model.get('masterKey')
  const privateAvail = model.get('hasPrivateKey')
  if (privateAvail) {
    deletePrivateDialog(id, isPrimary)
  } else {
    // If no private avail, we are just deleting a public key
    deletePublicDialog(id, privateAvail)
  }
}

function onDetails (model) {
  showLocalDetail(model.get('id'))
}

function onDownload (model) {
  const privateAvail = model.get('hasPrivateKey')
  if (privateAvail) {
    downloadPrivateDialog(model.get('id'))
  } else {
    const params = {
      keyid: model.get('id'),
      keyType: 'public'
    }
    keysAPI.downloadAsFile(params)
  }
}

function onEdit (model) {
  editUserIdDialog(model.get('id'), refresh)
}

function onRevoke (model) {
  revokePrivateDialog(model.get('id'))
}

function onSignatures (model) {
  signatureView.open(model.get('id'))
}

function showLocalDetail (id) {
  const data = getCacheData(id)
  privateDetailDialog(data)
}

function getCacheData (id) {
  const data = userKeyData[id]
  return (data)
}

export default {
  userKeys
}
