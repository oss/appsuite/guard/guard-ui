/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ox from '$/ox'
import yell from '$/io.ox/core/yell'
import gt from 'gettext'
import certs from '@/io.ox/guard/api/certs'
import ModalDialog from '$/io.ox/backbone/views/modal'
import Backbone from '$/backbone'

// Uploads SMIME key
function upload (files, oldPass, guard, force) {
  const deferred = $.Deferred()
  const formData = new FormData()
  for (let l = 0; l < files.length; l++) {
    if (!validFileType(files[l])) {
      deferred.reject()
      return deferred
    }
    if (files[l] !== undefined) { formData.append('key' + l, files[l]) }
  }
  formData.append('password', oldPass)
  formData.append('newPassword', guard)
  if (force) {
    formData.append('ignoreDate', 'true')
  }

  const url = ox.apiRoot + '/oxguard/certificates?action=upload&respondWithJSON=true&session=' + ox.session
  $.ajax({
    url,
    type: 'POST',
    data: formData,
    processData: false, // tell jQuery not to process the data
    contentType: false, // tell jQuery not to set contentType
    success (data) {
      if (data && data.error) {
        if (data.code === 'SMIME-0011') {
          // Expired certificate.  Allow to upload
          return promptForceupload(files, oldPass, guard).then(deferred.resolve, deferred.reject)
        }
        yell('error', data.error)
        deferred.reject(data.error)
        return
      }
      certs.getCertificates().then(() => {
        deferred.resolve(data.data)
      })
    },
    error (e) {
      yell('error', e.statusText)
      deferred.reject(e.statusText)
    }
  })
  return deferred
}

function validFileType (file) {
  if (!/^.*\.(p12|pfx)$/i.test(file.name)) {
    console.error('bad file type ' + file.name)
    // #.  User attempted to upload an unsupported file type.  Expecting a type of file with standard name PKCS#12.  Only files with .p12 and .pfx suffixes allowed
    yell('error', gt('Only S/MIME file types of .p12 and .pfx supported.  Expects PKCS#12 file type'))
    return false
  }
  return true
}

function promptForceupload (files, oldPass, guard) {
  const deferred = $.Deferred()
  new ModalDialog({
    async: false,
    point: 'oxguard/smime/uploader',
    title: gt('Certificate Expired'),
    id: 'certExpired',
    width: 500,
    model: new Backbone.Model({ files, oldPass, guard })
  })
    .build(function () {
      // #. User tried to upload a certificate that has expired. They may continue to upload, but the certificate will only work for decrypting existing items. They can't use it for new emails.
      const div = $('<div class="keyExpiredDiv">').append(gt('This certificate has expired. If you upload this certificate, you may use it for decryption of prior emails only. It will not be used for new signatures or emails.'))
      this.$body.append(div)
    })
    .addButton({ label: gt('Upload'), action: 'upload' })
    .addButton({ label: gt('Cancel'), action: 'quit' })
    .on('quit', function () {
      this.close()
      deferred.reject('cancel')
    })
    .on('upload', function () {
      return upload(this.model.get('files'), this.model.get('oldPass'), this.model.get('guard'), true).then(deferred.resolve, deferred.reject)
    })
    .open()
  return deferred
}

export default {
  upload
}
