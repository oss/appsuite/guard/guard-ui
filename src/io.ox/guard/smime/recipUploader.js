/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ox from '$/ox'
import yell from '$/io.ox/core/yell'
import gt from 'gettext'

// Uploads SMIME key
export function recipUpload (files) {
  const deferred = $.Deferred()
  const formData = new FormData()
  for (let l = 0; l < files.length; l++) {
    if (!validFileType(files[l])) {
      deferred.reject()
      return deferred
    }
    if (files[l] !== undefined) { formData.append('cert' + l, files[l]) }
  }

  const url = ox.apiRoot + '/oxguard/certificates?action=uploadRecipCertificate&respondWithJSON=true&session=' + ox.session
  $.ajax({
    url,
    type: 'POST',
    data: formData,
    processData: false, // tell jQuery not to process the data
    contentType: false, // tell jQuery not to set contentType
    success (data) {
      if (data && data.error) {
        if (data.code === 'SMIME-0002') {
          // #. User uploaded a certificate but Guard could not verify it as trusted.  They didn't include all of the certificates required to establish trust.
          yell('error', gt('The certificate could not be verified as trusted.  Please include all certificates provided with the certificate to establish the chain of trust.'))
        } else {
          yell('error', data.error)
        }
        deferred.reject(data.error)
        return
      }
      deferred.resolve(data.data)
    },
    error (e) {
      yell('error', e.statusText)
      deferred.reject(e.statusText)
    }
  })
  return deferred
}

function validFileType (file) {
  if (!/^.*\.(p7b|p7c|cer|crt)$/i.test(file.name)) {
    console.error('bad file type ' + file.name)
    // #.  User attempted to upload an unsupported file type. Only files with .crt and .der suffixes allowed
    yell('error', gt('Only S/MIME file types of PKCS (p7b) supported.'))
    return false
  }
  return true
}
