/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ModalDialog from '$/io.ox/backbone/views/modal'
import Backbone from '$/backbone'
import gt from 'gettext'
import '@/io.ox/guard/smime/style.scss'

export function viewCertDialog (email, certData) {
  return new ModalDialog({
    async: true,
    title: gt('Certificate data for %s', email),
    model: new Backbone.Model({ email, certData })
  })
    .addCloseButton()
    .build(function () {
      const certData = this.model.get('certData')
      const div = $('<div class="certificateDataClass">')
      div.append(
        $('<label for="certifierId">').append(gt('Certifier:')),
        $('<span id="certifierId">').append(certData.certifier))
      div.append('<br>')
      div.append(
        $('<label for="certifierExp">').append(gt('Expires:')),
        $('<span id="certifierExp">').append(new Date(certData.expires).toLocaleString()))
      div.append('<br>')
      div.append(
        $('<label for="certifierSerial">').append(gt('Serial Number:')),
        $('<span id="certifierSerial">').append(certData.serial))
      this.$body.append(div)
    })
    .open()
}
