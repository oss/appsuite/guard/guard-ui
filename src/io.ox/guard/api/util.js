/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as encr from '@/io.ox/guard/crypto/encr'
import _ from '$/underscore'
import util from '@/io.ox/guard/util'
import ox from '$/ox'
import gt from 'gettext'
import guardModel from '@/io.ox/guard/core/guardModel'
import yell from '$/io.ox/core/yell'

let badCount = 0

function handleJsonError (error, errorResponse, baton) {
  errorResponse.errorMessage = gt('Error')
  errorResponse.retry = false
  if (error.code !== undefined) {
    switch (error.code) {
      case 'GRD-PGP-0005':
        if (guardModel().hasAuth()) { // If bad password based on auth, then clear auth and redo
          guardModel().clearAuth()
          if (baton) baton.view.redraw()
          return
        }
        errorResponse.errorMessage = gt('Unable to decrypt Email, incorrect password.')
        yell('error', gt('Bad password'))
        badCount++
        if (badCount > 2 && guardModel().get('recoveryAvail')) {
          errorResponse.errorMessage += ('<br/>' + gt('To reset or recover your password, click %s', '<a id="guardsettings" href="#">' + gt('Settings') + '</a>'))
        }
        errorResponse.retry = true
        break
      case 'NOSERVER':
        errorResponse.errorMessage = gt('Unable to contact encryption server. You will not be able to encrypt or decrypt items until this is resolved.  Please contact your administrator or try again later')
        errorResponse.retry = false
        break
      default:
        if (error.error !== undefined) {
          errorResponse.errorMessage = error.error
        }
    }
  } else if (error.error !== undefined) {
    errorResponse.errorMessage = guardModel.getName() + ': ' + error.error
  }
}

const apiUtil = {
  encryptPasswords (data) {
    data = data || {}
    if (!guardModel().get('pubKey')) return data

    _.each(['password', 'encr_password', 'extrapass'], function (key) {
      // missing prop or empty value
      if (!(key in data) || !data[key]) return
      const encrypted = encr.cryptPass(data[key])
      if (!encrypted) return
      const target = 'e_' + key
      data[target] = encrypted
      delete data[key]
    })
    return data
  },

  format: util.format,

  showError (errorJson) {
    const errorResp = {}
    try {
      handleJsonError(errorJson, errorResp)
    } catch (e) {
      errorResp.errorMessage = gt('Unspecified error')
    }

    switch (errorResp.errorMessage.toLowerCase()) {
      case 'bad password':
        ox.trigger('guard:auth:fail:password', gt('Bad password'))
        return
      case 'lockout':
        ox.trigger('guard:auth:fail:password', gt('Account Locked out'))
        return
      case 'invalid authentication':
        ox.trigger('guard:auth:fail:password', gt('Bad password'))
        return
      case 'grd-mw-0003':
        ox.trigger('guard:auth:fail:password', gt('Bad password'))
        return
      case 'grd-mw-0005':
        yell('error', gt('Account locked out due to bad attempts.  Please try again later.'))
        break
      default:
        yell('error', errorResp.errorMessage)
    }

    ox.trigger('guard:auth:fail:password', errorResp.errorMessage)
  }
}

export default apiUtil
