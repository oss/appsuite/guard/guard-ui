/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import Events from '$/io.ox/core/event'
import util from '@/io.ox/guard/api/util'
import http from '$/io.ox/core/http'
import $ from '$/jquery'

// https://documentation.open-xchange.com/components/guard/2.10.3/#tag/login

const encryptPasswords = util.encryptPasswords

const api = {

  // Performs a login to obtain an authentication token and various user specific settings.
  login (data, keyid, type) {
    return http.POST({
      module: 'oxguard/login',
      params: {
        action: 'login',
        keyid,
        time: new Date().getTime(),
        type
      },
      contentType: 'application/json; charset=utf-8;',
      processResponse: false,
      data: JSON.stringify(encryptPasswords(data))
    }).then(function (data) {
      if (data.error) return $.Deferred().reject(data.error)
      return data
    }).fail(util.showError)
  },

  // Performs a login to obtain an authentication token and various user specific settings.
  changePassword (data, type) {
    return http.POST({
      module: 'oxguard/login',
      params: {
        action: 'changepass',
        type
      },
      contentType: 'application/json; charset=utf-8;',
      processResponse: false,
      data: JSON.stringify(data)
    }).fail(util.showError)
  }
}

Events.extend(api)

export default api
