/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from '$/underscore'
import $ from '$/jquery'
import ox from '$/ox'
import http from '$/io.ox/core/http'
import Events from '$/io.ox/core/event'
import Pool from '$/io.ox/core/api/collection-pool'
import util from '@/io.ox/guard/api/util'
import guardModel from '@/io.ox/guard/core/guardModel'

// https://documentation.open-xchange.com/components/guard/2.10.3/#tag/keys

const encryptPasswords = util.encryptPasswords

const pool = _.extend(Pool.create('guard-keys'), {

  getKey (obj) {
    // via id
    if (_.isString(obj)) return this.get('key').get(obj)
    // via attributes object
    return this.get('key').findWhere(obj)
  },

  getKeys (attributes) {
    return this.get('key').where(attributes)
  },

  getCurrentRing () {
    return this.get('ring').findWhere({ current: true })
  }
})

// TODO: in case guards api endpoint would also support input name "body" we could
// switch to io.ox/core/download -> multiple
function asFile (options) {
  options = options || {}
  const name = _.uniqueId('iframe')
  const iframe = $('<iframe>', { src: 'blank.html', name, class: 'hidden download-frame' })
  const form = $('<form>', { iframeName: name, action: options.url, method: 'post', target: name }).append(
    $('<input type="hidden" name="password" value="">').val(options.password)
  )

  // except for iOS we use a hidden iframe
  // iOS will open the form in a new window/tab
  if (!_.device('ios')) $('#tmp').append(iframe)
  $('#tmp').append(form)
  form.submit()
}

const api = {

  // pool.get('key'), pool.get('ring')
  pool,

  // Creates a new PGP key ring for the user and marks the new key ring as "current".
  create (data) {
    return http.POST({
      module: 'oxguard/keys',
      params: {
        action: 'create'
      },
      data: encryptPasswords(data)
    }).done(function (data) {
      api.trigger('create', data)
    }).fail(util.showError)
  },

  // Deletes a specific PGP key ring
  delete (keyid, data) {
    return http.POST({
      module: 'oxguard/keys',
      params: {
        keyid,
        action: 'delete'
      },
      data: encryptPasswords(data || {})
    }).done(function () {
      api.trigger('delete', keyid)
    }).fail(util.showError)
  },

  // Deletes an uploaded external recipient key.
  deleteExternalPublicKey (keyid, keyType) {
    return http.DELETE({
      module: 'oxguard/keys',
      dataType: 'json',
      params: {
        action: 'deleteExternalPublicKey',
        keyids: keyid,
        keyType
      }
    }).then(undefined, function (data) {
      if ('error' in data) return $.Deferred().reject(data)
    }).then(function () {
      api.trigger('delete', keyid)
    })
      .fail(util.showError)
  },

  // Downloads the ASCII armored representation of a specific PGP key ring owned by the user containing public
  // and/or private PGP keys. This request fetches the raw ASCII armored PGP key ring data.
  download (options) {
    const def = $.Deferred()
    const params = _.extend({
      keyType: 'public',
      keyid: undefined,
      session: ox.session
    }, options)

    $.ajax({
      url: ox.apiRoot + '/oxguard/keys?action=downloadKey&' + $.param(_.omit(params, _.isUndefined)),
      type: 'POST',
      dataType: 'text',
      contentType: 'application/x-www-form-urlencoded',
      success (text) {
        if (!text) return def.reject()
        // duckcheck: only errors are returned as JSON
        if (text.indexOf('{') === 0) return def.reject(JSON.parse(text))
        if (text.indexOf('BEGIN PGP') === -1) return def.reject('Invalid PGP structure')
        def.resolve({ key: text })
      },
      error (obj) {
        def.reject(obj.responseText)
      }
    })
    return def
  },

  // Downloads the ASCII armored representation of a specific PGP key ring owned by the user containing public
  // and/or private PGP keys. This request fetches the raw ASCII armored PGP key ring data.
  downloadAsFile (options, data) {
    const params = _.extend({
      keyType: 'public',
      keyid: undefined,
      session: ox.session
    }, options)

    return asFile({
      url: ox.apiRoot + '/oxguard/keys?action=downloadKey&' + $.param(_.omit(params, _.isUndefined)),
      password: data && data.password
    })
  },

  // Gets the collection of public PGP key rings associated with a contact.
  getContactKeys (folder_id, contact_id) {
    return http.GET({
      module: 'oxguard/keys',
      params: {
        action: 'getContactKeys',
        contactFolder: folder_id,
        contactId: contact_id
      },
      appendColumns: false
    }).then(function (data) {
      return data.externalPublicKeyRings
    }).fail(util.showError)
  },

  // Gets a list of upload public keys for external recipients. A user can upload public key rings for other external communication partners.
  // This requests gets the uploaded public key rings for externals including useful meta information.
  getExternalPublicKeys (autocrypt) {
    return http.GET({
      module: 'oxguard/keys',
      params: {
        action: 'getExternalPublicKeys',
        keyType: autocrypt ? 'autocrypt' : undefined
      },
      appendColumns: false
    }).then(function (data) {
      return data.externalPublicKeyRings
    }).fail(util.showError)
  },

  // Gets the the user's collection of public PGP key rings.
  getKeys () {
    return http.GET({
      module: 'oxguard/keys',
      params: {
        action: 'getKeys',
        userid: ox.user_id,
        cid: ox.context_id
      },
      appendColumns: false
    }).then(function (data) {
      const keyRings = (data || {}).keyRings || []
      // mapping and pool
      _.each(keyRings, function (ring, index) {
        // ring pool
        // HACK: use index as model id
        pool.add('ring', _.extend({ id: index + 1 }, ring));
        // key pool
        ['publicRing', 'privateRing'].forEach(function (type) {
          // add keys
          if (!ring[type] || !ring[type].keys || !ring[type].keys.length) return
          ring[type].keys = _.map(ring[type].keys, function (key) {
            const data = _.extend(key, {
              // HACK: adds some helpfull props/references to the model
              _ring: ring[type].hash,
              _current: ring.current,
              _type: type.replace('Ring', ''),
              _short: util.format(key.fingerPrint.substring(key.fingerPrint.length - 8)),
              _expires: key.validSeconds !== 0 ? key.creationTime + (key.validSeconds * 1000) : undefined,
              fingerPrint: util.format(key.fingerPrint)
            })
            if (ring.current) {
              guardModel().updatePGPKeyData(ring)
            }
            pool.add('key', data)
            return data
          })
        })
      })
      return keyRings
    }).fail(util.showError)
  },

  // Queries the public key of a recipient.
  getRecipientsPublicKey (email) {
    return http.GET({
      module: 'oxguard/keys',
      params: {
        action: 'getRecipKey',
        email
      }
    }).fail(util.showError)
  },

  // Revokes a PGP key ring
  revoke (keyid, data) {
    return http.POST({
      module: 'oxguard/keys',
      params: {
        keyid,
        action: 'revoke'
      },
      data: encryptPasswords(data || {})
    }).done(function () {
      // TODO: MW should return JSON in all cases (GUARD-266)
      api.trigger('revoke', keyid)
    }).fail(util.showError)
  },

  // Queries the public key of a recipient.
  getSignatures (keyId) {
    return http.GET({
      module: 'oxguard/keys',
      params: {
        action: 'getSignatures',
        userid: ox.user_id,
        keyid: keyId,
        subkeys: true
      }
    }).then(function (data) {
      return data.signatures
    }).fail(util.showError)
  },

  // Marks a Guard PGP key ring as "current".
  setCurrentKey (keyid) {
    return http.PUT({
      module: 'oxguard/keys',
      params: {
        action: 'setCurrentKey',
        keyid
      }
    }).done(function () {
      api.trigger('setCurrentKey')
    }).fail(util.showError)
  },

  // Marks an uploaded external recipient key as "inline".
  // An external public key which is marked as "inline" produces PGP/INLNE email encryption by default.
  setInline (ids, value) {
    return http.PUT({
      module: 'oxguard/keys',
      params: {
        action: 'inlineExternalPublicKey',
        ids,
        inline: !!value
      },
      appendColumns: false
    }).fail(util.showError)
  },

  // Marks an uploaded external recipient key as shared or as not shared. An external public key which is
  // marked as shared can be accessed from other OX Guard users in the same context.
  setShare (ids, value) {
    return http.PUT({
      module: 'oxguard/keys',
      params: {
        action: 'shareExternalPublicKey',
        ids,
        share: !!value
      },
      appendColumns: false
    }).fail(util.showError)
  }

}

Events.extend(api)

export default api
