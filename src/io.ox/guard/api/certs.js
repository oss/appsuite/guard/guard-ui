/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import _ from '$/underscore'
import ox from '$/ox'
import core from '@/io.ox/guard/oxguard_core'
import og_http from '@/io.ox/guard/core/og_http'
import util from '@/io.ox/guard/api/util'
import guardModel from '@/io.ox/guard/core/guardModel'
import http from '$/io.ox/core/http'

function asFile (options) {
  options = options || {}
  const name = _.uniqueId('iframe')
  const iframe = $('<iframe>', { src: 'blank.html', name, class: 'hidden download-frame' })
  const form = $('<form>', { iframeName: name, action: options.url, method: 'post', target: name }).append(
    $('<input type="hidden" name="password" value="">').val(options.password)
  )

  // except for iOS we use a hidden iframe
  // iOS will open the form in a new window/tab
  if (!_.device('ios')) $('#tmp').append(iframe)
  $('#tmp').append(form)
  form.submit()
}

const api = {
  delete (id, pass) {
    const def = $.Deferred()
    const json = {
      password: pass
    }
    const params = '&serial=' + id
    og_http.simplePost(ox.apiRoot + '/oxguard/certificates?action=delete', params, json)
      .done(function (data) {
        if (core.checkJsonOK(data)) {
          def.resolve()
        } else {
          util.showError(data)
          def.reject()
        }
      })
      .fail(function (e) {
        util.showError(e)
        def.reject()
        console.log(e)
      })
    return def
  },
  getCertificates () {
    return http.GET({
      module: 'oxguard/certificates',
      params: {
        action: 'getCertificates',
        userid: ox.user_id,
        cid: ox.context_id
      }
    }).done(function (data) {
      if (data && _.isArray(data.certificates) && data.certificates.length > 0) {
        guardModel().setCerts(data.certificates)
      } else guardModel().setCerts()
      return data
    })
  },
  createCertificate (password) {
    const def = $.Deferred()
    og_http.simplePost(ox.apiRoot + '/oxguard/certificates?action=create', '', { password })
      .done(function (data) {
        if (core.checkJsonOK(data)) {
          def.resolve(data)
        } else {
          util.showError(data)
          def.reject(data)
        }
      })
      .fail(function (e) {
        util.showError(e)
        def.reject(e.statusText)
        console.log(e)
      })
    return def
  },

  makeCurrent (id) {
    return http.PUT({
      module: 'oxguard/certificates',
      params: {
        action: 'setCurrentCertificate',
        userid: ox.user_id,
        cid: ox.context_id,
        serial: id
      }
    }).fail(function (e) {
      console.log(e)
      util.showError(e)
    })
  },
  getRecipientsPublicKey (email) {
    return http.GET({
      module: 'oxguard/certificates',
      params: {
        action: 'getRecipCertificate',
        email
      }
    }).fail(util.showError)
  },
  downloadAsFile (options, data) {
    const params = _.extend({
      serial: undefined,
      session: ox.session
    }, options)

    return asFile({
      url: ox.apiRoot + '/oxguard/certificates?action=download&' + $.param(_.omit(params, _.isUndefined)),
      password: data && data.password
    })
  }
}

export default api
