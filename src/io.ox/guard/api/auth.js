/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import Events from '$/io.ox/core/event'
import util from '@/io.ox/guard/api/util'
import http from '$/io.ox/core/http'
import guardModel from '@/io.ox/guard/core/guardModel'
import _ from '$/underscore'
import $ from '$/jquery'

// https://documentation.open-xchange.com/components/guard/2.10.3/#tag/auth

const encryptPasswords = util.encryptPasswords

// minutesValid
// -1: or single use
//  0: indefinite

function getType (type) {
  return type || ''
}

const api = {

  // Performs a login to obtain an authentication token
  // optionally: saves the token in the user session for specified period of time (minutesValid))
  authAndSave (data) {
    return http.POST({
      module: 'guard-json',
      params: {
        action: 'auth-token',
        type: getType(data.type),
        time: new Date().getTime()
      },
      contentType: 'application/json; charset=utf-8;',
      data: JSON.stringify(encryptPasswords(data))
    }).fail(util.showError)
  },

  // Checks with the middleware if a valid authentication token exists within the session
  check (type) {
    api.trigger('before:check')
    return http.GET({
      module: 'guard-json',
      params: { action: 'auth-token' }
    }).then(function (retdata) {
      // TODO: most of this "what situation do we have here?" should be handled on the server side
      if (!retdata || !retdata[0]) return $.Deferred().reject('none')
      // error cases
      retdata.forEach(function (data) {
        const invalid = data.minutesValid === undefined || !_.isNumber(data.minutesValid) || !data.auth
        const singleUse = data.minutesValid === -1
        if (singleUse) guardModel().clearAuth(data.type)
        if ((invalid || singleUse) && (data.type === type)) return $.Deferred().reject('exp')
      })
      // 99999: session
      api.trigger('success:check', retdata)
      return retdata
    }).fail(function (e) {
      api.trigger('fail:check', e)
      if (_.isObject(e)) util.showError(e)
    })
  },

  // reset token in the user's session
  resetToken (type) {
    // TODO: should passcode be nulled in general?
    guardModel().clearAuth(type)
    // destroy auth token
    return api.setToken('', 0, type)
  },

  // Saves an existing authentication token (obtained from a login
  // response) in the user's session for specified period of time.
  setToken (auth, time, type) {
    return http.PUT({
      module: 'guard-json',
      params: { action: 'auth-token', type: type || 'pgp' },
      data: {
        auth,
        type: getType(type),
        minutesValid: time || -1
      }
    }).done(function (data) {
      return (data && data[0] ? data.auth : {})
    }).fail(util.showError)
  }
}

Events.extend(api)

export default api
