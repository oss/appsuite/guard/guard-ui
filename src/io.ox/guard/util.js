/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import userAPI from '$/io.ox/core/api/user'
import capabilities from '$/io.ox/core/capabilities'
import { settings } from '$/io.ox/mail/settings'
import guardModel from '@/io.ox/guard/core/guardModel'
import certs from '@/io.ox/guard/api/certs'

/**
 * Test if guard is configured.
 *
 * @return true if configured; false otherwise
 */

const api = {
  isGuardConfigured () {
    if (!guardModel().isLoaded()) return false // Make sure Guard response
    return !guardModel().needsKey()
  },

  isGuardConfiguredAndPGPReady (smime) {
    return this.isGuardConfigured() && !guardModel().allExpired()
  },

  hasSetupDone (refresh) { // refresh is only used in UI e2e testing
    const def = $.Deferred()
    if (guardModel().needsKey()) {
      def.reject()
    } else if (!capabilities.has('guard') && capabilities.has('smime')) {
      const hasSmime = guardModel().hasSmimeKeys()
      // not yet loaded, pull certs
      if (hasSmime === undefined || refresh) {
        certs.getCertificates().then(() => {
          if (guardModel().hasSmimeKeys()) def.resolve()
          else def.reject()
        })
        return def
      }
      if (hasSmime) {
        def.resolve()
        return def
      }
      def.reject()
    } else { def.resolve() }
    return def
  },

  hasCryptoCapability () {
    return (capabilities.has('guard') || capabilities.has('smime'))
  },

  hasGuardMailCapability () {
    return this.hasCryptoCapability() && (capabilities.has('guard-mail'))
  },

  isGuest () {
    return capabilities.has('guest')
  },

  isGuardLoaded () {
    return guardModel().isLoaded()
  },

  hasSmime () {
    return capabilities.has('smime')
  },

  addOnLoaded (loadFunction) {
    if (!guardModel().get('onload')) {
      guardModel().set('onload', [])
    }
    guardModel().get('onload').push(loadFunction)
  },

  hasStoredPassword () {
    return (guardModel().hasAuth('PGP'))
  },

  format (fingerprint) {
    return (fingerprint || '').match(/.{1,4}/g).join(' ')
  },

  autoCryptEnabled () {
    if (!capabilities.has('guard-mail')) return false
    return (guardModel().getSettings().autoCryptEnabled)
  },

  getUsersPrimaryEmail () {
    return $.when(settings.ensureData().then(() => {
      const defaultSendAddress = $.trim(settings.get('defaultSendAddress', ''))
      if (!defaultSendAddress) return $.when(defaultSendAddress)
      return userAPI.get().then(userData => userData.email1)
    }))
  },

  sanitize (data) {
  // eslint-disable-next-line prefer-regex-literals
    const regex = new RegExp('<(?:!--(?:(?:-*[^->])*--+|-?)|script\\b(?:[^"\'>]|"[^"]*"|\'[^\']*\')*>[\\s\\S]*?</script\\s*|style\\b(?:[^"\'>]|"[^"]*"|\'[^\']*\')*>[\\s\\S]*?</style\\s*|/?[a-z](?:[^"\'>]|"[^"]*"|\'[^\']*\')*)>', 'gi')
    return (data.replace(regex, ''))
  },

  validateEmail (email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(email)
  }
}

export default api
