/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import _ from '$/underscore'
import Backbone from '$/backbone'
import auth from '@/io.ox/guard/auth'
import gt from 'gettext'
import '@/io.ox/guard/mail_lock/style.scss'
import guardModel from '@/io.ox/guard/core/guardModel'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import yell from '$/io.ox/core/yell'
import ModalDialog from '$/io.ox/backbone/views/modal'
import util from '@/io.ox/guard/util'
import { settings as mailSettings } from '$/io.ox/mail/settings'
import { createIcon } from '$/io.ox/core/components'

let warningOpen = false

const ToggleEncryptionView = Backbone.View.extend({
  tagName: 'a',
  className: 'toggle-encryption',
  initialize (options) {
    this.mailModel = options.mailModel
    this.listenTo(this.model, 'change:encrypt', function (model, val) {
      this.$('i.fa').toggleClass('encrypted', val)
    })
  },
  events: {
    click: 'toggle',
    'change:encrypt': 'changed'
  },
  toggle (e) {
    e.preventDefault()
    if (this.encryption_forced) {
      yell('error', gt('Reply must be encrypted'))
    } else {
      const model = this.model
      const mailModel = this.mailModel
      if (!model.get('encrypt')) {
        auth.ensureSetup(true, this.model.get('smime')).then(function () {
          model.set('encrypt', !model.get('encrypt'))
          if (!model.get('encrypt')) { // If removing encryption, check for pgp attachments
            checkPGPAttachment(mailModel)
          }
        })
      } else {
        model.set('encrypt', !model.get('encrypt'))
      }
    }
  },
  isEnabled () {
    return guardModel().hasGuardMail() || guardModel().hasSmime() || this.encryption_forced
  },
  forceEncryption () {
    this.encryption_forced = true
    this.model.set('encrypt', true)
    settings.set('encryptDraft', true)
    this.render()
  },
  render () {
    this.$el.attr('tabindex', '-1')
    if (this.isEnabled()) {
      if (!_.device('small')) {
        this.$el.empty().append(
          getLockIcon(this.model.get('encrypt'))
        )
        this.$el.attr('title', gt('Enable Encryption')).attr('href', '#').attr('role', 'button')
      }
    }
    this.listenTo(this.model, 'change:encrypt', function () {
      this.changed()
    })
    if (this.model.get('encrypt')) {
      const view = this.view
      window.setTimeout(function () {
        showAttLink(view, false)
      }, 100)
    }
    return this
  },
  noLinkMail (view) {
    this.view = view
    checkAttLinkView(this.mailModel, view)
  },
  changed () {
    this.$el.empty()
      .append(getLockIcon(this.model.get('encrypt')))
      .attr('title', this.model.get('encrypt') ? gt('Disable Encryption') : gt('Enable Encryption'))
    if (!guardModel().hasGuardMail() && guardModel().hasSmime()) { // If we don't have guard-mail, but has smime, then default to smime
      this.model.set('smime', true)
    }
    if (this.model && this.model.get('encrypt')) { // Check for first use without choosing encrypt drafts
      if (!util.isGuest() && !guardModel().forceEncryptedDrafts() && (settings.get('encryptDraft') === undefined)) {
        const securityModel = this.model
        promptEncryptDraft().then(() => {
          securityModel.trigger('change', securityModel)
        })
      }
    }
    if (this.model && this.model.get('encrypt') && !this.model.get('smime')) {
      showAttLink(this.view, false)
      checkAttLink(this.mailModel)
      try {
        const fromArray = this.mailModel.get('from')
        const from = _.isArray(fromArray[0]) ? fromArray[0][1] : fromArray[1]
        if (fromArray && guardModel().get('primaryEmail') !== undefined) {
          if (guardModel().get('primaryEmail') !== from) {
            const warn = settings.get('warnEmailDifferent')
            if (warn === undefined || warn === false) {
              displayWarning()
            }
          }
        }
      } catch (e) {
        console.log(e)
      }
    } else {
      showAttLink(this.view, true)
    }
  }
})

function getLockIcon (encrypted) {
  return encrypted ? createIcon('./io.ox/guard/core/icons/lock.svg') : createIcon('./io.ox/guard/core/icons/unlock.svg')
}

// Hide or show mail attachment link
function showAttLink (view, show) {
  if (view) {
    view.$('.attachments').toggleClass('guard-encrypted', !show)
  }
}

// Check if Mail Attachment link is shown and encryption enabled
function checkAttLinkView (model, view) {
  if (model.get('encrypt')) {
    checkAttLink(model)
    showAttLink(view, false)
  }
}

// Show a warning if Mail Attachment Link is shown and encryption clicked
function checkAttLink (model) {
  if (model.get('sharedAttachments')) {
    if (model.get('sharedAttachments').enabled) {
      if (warningOpen) return
      warningOpen = true
      const dialog = new ModalDialog({
        async: true,
        point: 'io.ox/guard/mail_lock/shareAttError',
        title: gt('Not Supported'),
        id: 'attError',
        width: 400
      })
        .extend({
          explanation () {
            const text = $('<p>').text(gt('%s is not supported with secured email and will be disabled.', mailSettings.get('compose/shareAttachments/name')))
            this.$body.append(text)
          }
        })
        .addButton({ label: gt('OK'), action: 'ok' })
        .on('ok', function () {
          $('.share-attachments').find(':checkbox').prop('checked', false)
          const share = model.get('sharedAttachments')
          share.enabled = false
          model.set('sharedAttachments', share)
          dialog.close()
          warningOpen = false
        })
        .open()
    }
  }
}

function promptEncryptDraft () {
  const def = $.Deferred()
  new ModalDialog({
    async: true,
    title: gt('Encrypt Drafts?'),
    point: 'oxguard/mail_lock/encryptDraft',
    id: 'encryptDraft',
    width: 450
  })
    .extend({
      explanation () {
        const text = $('<p>').text(gt('Would you like to encrypt the draft email when composing encrypted emails?  If so, you will need to provide your %s password when composing the email so that it may be decrypted when sending. This option can be changed in the settings at any time.', guardModel().getName()))
        const draft = $('<input type="checkbox" id="encrDraftBox">')
        draft.on('click', function (e) {
          settings.set('encryptDraft', $(e.target).is(':checked'))
          settings.save()
        })
        const label = $('<label for="encrDraftBox">').append(draft).append(gt('Encrypt draft emails when composing encrypted emails'))
        this.$body.append(text).append($('<div class="checkbox custom small">').append(label))
      }
    })
    .addButton({ label: gt('OK'), action: 'ok' })
    .on('ok', function () {
      settings.set('encryptDraft', $('#encrDraftBox').is(':checked'))
      settings.save()
      def.resolve()
      this.close()
    })
    .on('cancel', () => {
      def.reject()
    })
    .open()
  return def
}

// Warning that the from address is different from primary
function displayWarning () {
  new ModalDialog({
    async: true,
    point: 'io.ox/guard/mail_lock/otherSendFrom',
    title: gt('Warning'),
    id: 'otherSendFrom',
    width: 450
  })
    .extend({
      explanation () {
        const text = $('<p>').text(gt('Sending an encrypted email from an account other than %s may cause problems with the recipient being able to reply.  Consider using your primary account.', guardModel().get('primaryEmail')))
        const warn = $('<input type="checkbox" id="warnEmail">')
        const warntext = $('<span class="selectable" style="padding-left:10px;">').text(gt('Do not warn me again'))
        warntext.on('click', function () {
          warn.click()
        })
        warn.on('click', function (e) {
          settings.set('warnEmailDifferent', $(e.target).is(':checked'))
          settings.save()
        })
        this.$body.append(text).append('<hr>').append(warn).append(warntext)
      }
    })
    .addButton({ label: gt('OK'), action: 'ok' })
    .on('ok', function () {
      this.close()
    })
    .open()
}

// Check to see if there are pgp attachments.  Checked if encryption removed
// Likely the recipient won't be able to open this attachment if not sent through Guard
function checkPGPAttachment (model) {
  const attachments = model.get('attachments')
  if (attachments && _.isArray(attachments.models)) {
    attachments.models.forEach(function (attModel) {
      if (attModel.get('file-mimetype') === 'application/pgp-encrypted' ||
        (attModel.get('filename') && attModel.get('filename').indexOf('.pgp') > 0)) {
        // #.  Warning to the user after they removed encryption from an email.  One of the attachments is a pgp item, and it will likely be unreadable to the recipient if they remove encryption
        yell('warning', gt('Sending encrypted attachments without encrypting the email will usually result in the attachment being unreadable by the recipient.'))
      }
    })
  }
}

export default {
  View: ToggleEncryptionView
}
