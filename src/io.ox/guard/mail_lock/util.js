/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from '$/underscore'
import $ from '$/jquery'
import guardModel from '@/io.ox/guard/core/guardModel'

function isEncryptEnabled () {
  if (guardModel().hasGuardMail()) return true
  return false
}

function isPGPMail (mail) {
  return isEncryptedMail(mail) || isSignedMail(mail)
}

function isEncryptedMail (mail) {
  if (mail.security_info && mail.security_info.encrypted) return true
  if (mail.content_type) {
    if (mail.content_type.indexOf('multipart/encrypted') > -1) return true
    if (mail.content_type.indexOf('application/pkcs7-mime') > -1) return true
  }
  try {
    if (mail.attachments && mail.attachments[0]) {
      if (mail.attachments[0].content.indexOf('---BEGIN PGP MESSAGE') > -1) {
        mail.PGPInline = true
        return (true)
      }
    }
  } catch (e) {
    console.log('error checking body' + e)
  }
  return (false)
}

function getCryptoType (mail) {
  if (mail.security && mail.security.type) return mail.security.type // decrypted email
  if (mail.security_info && mail.security_info.type) return mail.security_info.type // still encrypted email
  if (mail.content_type.indexOf('application/pkcs7-mime') > -1) return 'SMIME'
}

function isDecrypted (data) {
  if (_.isArray(data)) {
    let found = false
    data.forEach(function (obj) {
      if (obj.security && obj.security.decrypted) found = true
    })
    return found
  }
  if (data.security && data.security.decrypted) return true

  return false
}

function isSignedMail (mail) {
  return mail.content_type === 'multipart/signed'
}

function getPGPInfo () {
  return $.when()
}

function isSmime (data) {
  if (_.isArray(data)) {
    let found = false
    data.forEach(function (obj) {
      if (obj.security && obj.security.type === 'SMIME') found = true // Decrypted
      if (obj.security_info && obj.security_info.type === 'SMIME') found = true // Still encrypted
    })
    return found
  }
  if (data.security && data.security.type === 'SMIME') return true
  if (data.security_info && data.security_info.type === 'SMIME') return true
  return false
}

export { isPGPMail, isDecrypted, isEncryptedMail, isSignedMail, getPGPInfo, isEncryptEnabled, isSmime, getCryptoType }
