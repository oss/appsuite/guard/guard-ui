/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ox from '$/ox'
import http from '@/io.ox/guard/core/og_http'
import gt from 'gettext'
import ModalDialog from '$/io.ox/backbone/views/modal'
import publicKeys from '@/io.ox/guard/pgp/keyDetails'

const checking = []

function checkKey (email) {
  if (checking.indexOf(email) > -1) return ($.Deferred().reject())
  checking.push(email)
  return (getKey(email, false))
}

function downloadKey (email) {
  return (getKey(email, true))
}

// Query Guard server for PGP keys.  Get key if download, else get info on keys
function getKey (email, download) {
  const def = $.Deferred()
  const params = '&email=' + email +
        '&cid=' + ox.context_id +
        '&userid=' + ox.user_id +
        '&createIfMissing=true' +
        (download === true ? '&download=true' : '')

  http.get(ox.apiRoot + '/oxguard/pgpmail?action=getkey', params)
    .done(function (respData) {
      if (respData !== '') {
        let data
        if (typeof respData === 'string') {
          data = JSON.parse(respData).data
        } else {
          data = respData.data
        }
        if (data === 'guest') def.reject()
        if (data === '') def.reject()
        def.resolve(data)
        return
      }
      def.reject()
    })
    .fail(() => def.reject())
    .always(function () {
      // checking.pop(email);
    })
  return (def)
}

function promptKeyImport (data, email) {
  const def = $.Deferred()
  new ModalDialog({
    async: true,
    point: 'io.ox/guard/mail_lock/importKey',
    title: gt('Key found for %1$s.  Import?', email),
    id: 'importKey',
    width: 450,
    def
  })
    .extend({
      body () {
        const keyDetail = publicKeys.keyDetail(data)
        this.$body.append(keyDetail)
      }
    })
    .addButton({ label: gt('Import'), action: 'import' })
    .addCancelButton()
    .on('cancel', function () {
      this.options.def.reject()
    })
    .on('import', function () {
      downloadKey(email)
        .done(data => { this.options.def.resolve(data) })
        .fail(() => { this.options.def.reject() })
      this.close()
    })
    .open()
  return (def)
}

export default {
  checkKey,
  downloadKey,
  promptKeyImport
}
