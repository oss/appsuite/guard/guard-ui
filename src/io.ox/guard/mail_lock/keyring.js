/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import Backbone from '$/backbone'
import $ from '$/jquery'
import ext from '$/io.ox/core/extensions'
import '@/io.ox/guard/mail_lock/style.scss'
import { createIcon } from '$/io.ox/core/components'

const RecipientModel = Backbone.Model.extend({
  initialize () {
    this.listenTo(this, 'change:email', this.lookup)
    // do initial lookup
    this.lookup(this, this.get('email'))
  },
  defaults: {
    email: '',
    keys: []
  },
  addKey (obj) {
    this.set('keys', this.get('keys').concat(obj))
  },
  lookup (model, email) {
    if (!email) return
    const baton = new ext.Baton({ email })
    ext.point('mail_lock/keyring/lookup').invoke('action', model, baton)
  }
})

const RecipientView = Backbone.View.extend({
  initialize (opt) {
    this.renderAddress = opt.renderAddress
    this.listenTo(this.model, 'change:keys', function () {
      this.render()
    })
  },
  tagName: 'span',
  className: 'recipient-state',
  render () {
    const state = createIcon('bi/key.svg')
    let address = ''
    if (this.renderAddress) {
      address = $('<span class="email">').text(this.model.get('email'))
    }
    state.toggleClass('key-found', this.model.get('keys').length > 0)
    const trusted = this.model.get('keys').reduce(function (acc, key) {
      return acc || key.trusted === true
    }, false)
    state.toggleClass('trusted', trusted)
    this.$el.empty().append(address, state)
    return this
  }
})

const recipients = {
  View: RecipientView,
  Model: RecipientModel
}

export default {
  recipients
}
