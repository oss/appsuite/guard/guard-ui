/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import Backbone from '$/backbone'
import ext from '$/io.ox/core/extensions'
import files from '@/io.ox/guard/files/register'
import filesAPI from '$/io.ox/files/api'
import Viewer from '$/io.ox/core/viewer/main'

ext.point('io.ox/portal/widget/myfiles').extend({
  id: 'OxGuardRecentFiles',
  index: 1000000,
  preview () {
    const content = $(this).find('.list-unstyled')
    setupGuard(content)
  }
})

ext.point('io.ox/portal/widget/recentfiles').extend({
  id: 'OxGuardRecentFiles',
  index: 1000000,
  preview () {
    const content = $(this).find('.list-unstyled')
    setupGuard(content)
  }
})

function setupGuard (content) {
  content.unbind('click')
  content.on('click', 'li.item', function (e) {
    e.stopPropagation()
    const item = $(e.currentTarget).data('item')
    if (isEncrypted(item)) {
      const items = $(e.delegateTarget).data('items')
      filesAPI.get(item).done(function (data) {
        const models = filesAPI.resolve(items, false)
        const collection = new Backbone.Collection(models)
        const baton = new ext.Baton({ data, collection })
        files.viewFile(baton)
      })
    } else {
      const items = $(e.delegateTarget).data('items')
      filesAPI.get(item).done(function (data) {
        const models = filesAPI.resolve(items, false)
        const collection = new Backbone.Collection(models)
        const viewer = new Viewer()
        const baton = new ext.Baton({ data, collection })
        viewer.launch({ selection: baton.data, files: baton.collection.models })
      })
    }
  })
}

function isEncrypted (item) {
  if (item.meta) {
    if (item.meta.Encrypted === true) return (true)
  }
  if (item.filename) {
    if (item.filename.indexOf('.pgp') > 0) return (true)
  }
  return (false)
}
