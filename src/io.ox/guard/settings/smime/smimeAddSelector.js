/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ModalView from '$/io.ox/backbone/views/modal'
import ext from '$/io.ox/core/extensions'
import Backbone from '$/backbone'
import { smimeUpload } from '@/io.ox/guard/settings/smime/smimeUpload'
import creator from '@/io.ox/guard/settings/smime/smimeCreator'
import guardModel from '@/io.ox/guard/core/guardModel'
import '@/io.ox/guard/pgp/style.scss'

import gt from 'gettext'

const POINT = 'oxguard/settings/smimeAddSelector'
let INDEX = 0

function open () {
  const def = $.Deferred()
  if (guardModel().canCreateCertificates()) {
    openModalDialog(def)
  } else {
    smimeUpload().then(def.resolve, def.reject)
  }
  return def
}

function openModalDialog (def) {
  const view = new ModalView({
    async: true,
    point: POINT,
    title: gt('Add New Certificate'),
    id: 'addCert',
    width: 640,
    model: new Backbone.Model({ def })
  })
    .addAlternativeButton({ label: gt('Create New'), action: 'create' })
    .addAlternativeButton({ label: gt('Upload'), action: 'upload' })
    .on('upload', () => {
      smimeUpload().then(() => {
        def.resolve()
        view.close()
      }, def.reject)
    })
    .on('create', () => {
      creator.open().then(() => {
        def.resolve()
        view.close()
      }, def.reject)
    })
    .addCancelButton()
    .open()
  return def
}

ext.point(POINT).extend(
  {
    index: INDEX += 100,
    id: 'instructions',
    render: function (baton) {
      const label = $('<label for="uploadPrivateButton">').append(gt('If you already have a S/Mime certificate, click upload to add the certificate to %s.', guardModel().getName()))
        .append('<br>')
      const label2 = $('<label for="createNewButton">').append(gt('If you don\'t have a certificate yet, you may have one generated for you by clicking the "Create new" button.'))
        .append('<br>')
      this.$body.append(
        label, label2
      )
    }
  }
)

export default {
  open
}
