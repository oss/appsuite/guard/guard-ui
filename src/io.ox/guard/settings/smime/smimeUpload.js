/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ModalDialog from '$/io.ox/backbone/views/modal'
import uploader from '@/io.ox/guard/pgp/uploadkeys'
import smimeUploader from '@/io.ox/guard/smime/uploader'
import yell from '$/io.ox/core/yell'

import gt from 'gettext'

export function smimeUpload () {
  const def = $.Deferred()
  new ModalDialog({
    async: true,
    point: 'oxguard/smime/uploadKeys',
    title: gt('Upload S/MIME Keys'),
    enter: 'ok',
    focus: '#pgppassword'
  })
    .build(function () {
      this.$body.append(
        uploader.createPrompts(),
        $('<input type="file" id="smimeUserCertFileInput" accept=".p12, .pfx" style="display:none;">').unbind('change')
      )
    })
    .addButton({ label: gt('OK'), action: 'ok' })
    .addCancelButton()
    .on('cancel', function () {
      def.resolve()
    })
    .on('ok', function () {
      const dialog = this
      const input = $('#smimeUserCertFileInput')
      input.on('change', function () {
        const files = this.files
        if (files.length > 0) {
          dialog.busy()
          smimeUploader.upload(files, $('#pgppassword').val(), $('#guardpassword').val()).done(function (data) {
            if (data && data.id) {
              yell('success', gt('Imported key for ID %s, Serial Number %s, with expiration date of %s', data.id, data.serial, new Date(data.validUntil).toDateString()))
            }
            def.resolve()
            dialog.close()
          })
            .fail(function (e) {
              input.unbind('change')
              input.val(undefined)
              if (e === 'cancel') {
                dialog.close()
                return
              }
              yell('error', e)
              $('#uploaderror').text(e)
              dialog.idle()
            })
        }
      })
      dialog.idle() // unhide the input
      input.click()
    })
    .open()
  return def
}
