/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ModalView from '$/io.ox/backbone/views/modal'
import ext from '$/io.ox/core/extensions'
import certs from '@/io.ox/guard/api/certs'
import { PasswordView } from '@/io.ox/guard/core/passwordView'
import { smimeUpload } from '@/io.ox/guard/settings/smime/smimeUpload'
import keyCreator from '@/io.ox/guard/core/createKeys'
import guardModel from '@/io.ox/guard/core/guardModel'
import yell from '$/io.ox/core/yell'
import '@/io.ox/guard/pgp/style.scss'

import gt from 'gettext'

const POINT = 'oxguard/settings/smimeSetupView'

function open () {
  const def = $.Deferred()
  if (guardModel().canCreateCertificates()) {
    openModalDialog(def)
  } else {
    smimeUpload().then(def.resolve, def.reject)
  }
  return def
}

function openModalDialog (def) {
  const dialog = new ModalView({
    async: true,
    point: POINT,
    title: gt('Create New Certificate'),
    id: 'createCert',
    width: 450
  })
    .addCancelButton()
    .addButton({ label: gt('Create'), action: 'create' })
    .on('create', function () {
      if ($('#guardpassword').val() !== $('#guardpassword2').val()) {
        dialog.idle()
        $('#guardpassword2').focus()
        return
      }
      dialog.busy()
      const waitdiv = keyCreator.waitDiv()
      $('#createCert .modal-footer').append(waitdiv)
      waitdiv.show()
      const password = $('#guardpassword').val()
      certs.createCertificate(password).then(function (result) {
        dialog.close()
        if (result.data) {
          const data = result.data
          if (data.status === 'Done' || data.serial) {
            yell('success', gt('Certificate created'))
            def.resolve()
            return
          }
          if (data.status === 'Rejected') {
            // Error message
            yell('error', gt('There was an issue creating the certificate.  Please contact support. ') + data.message)
            def.resolve()
            return
          }
          if (data.status === 'Pending') {
            yell('success', gt('Certificate request is pending.  Please look for further notifications'))
            def.resolve()
            return
          }
          if (data.message) {
            yell('error', data.message)
          } else {
            yell('error', gt('There was an issue creating the certificate.  Please try again later.'))
          }
        }
        def.resolve()
      }, function (failure) {
        yell('error', gt('There was an issue creating the certificate.') + (failure.error ? failure.error : failure))
        dialog.close()
        def.reject()
      })
    })
    .open()
  return dialog
}

ext.point(POINT).extend(
  {
    index: 100,
    id: 'passwordPrompts',
    render: function () {
      this.$body.append(
        createPrompts(guardModel().get('pki').message)
      )
    }
  }
)

function createPrompts (custMessage) {
  const div = $('<div>')
  const message = $('<div style="margin-bottom:10px">').append(custMessage || gt('This will create a new certificate which will allow you to encrypt and sign emails.'))
  const passdiv = $('<div>').addClass('row-fluid')
  const noSaveWorkAround = $('<input style="display:none" type="text" name="dontremember"/><input style="display:none" type="password" name="dontrememberpass"/>')
  passdiv.append(noSaveWorkAround)
  const newogpassword = new PasswordView({ id: 'guardpassword', class: 'password_prompt', validate: true }).render().$el
  const newogpassword2 = new PasswordView({ id: 'guardpassword2', class: 'password_prompt', validate: true }).render().$el
  const hint = $('<div style="min-height:30px;">')
  passdiv.append('<label for="guardpassword">' + gt('Enter a new password to use.  You will be prompted for this password when this key is used to decrypt an item:') + '</label>').append(newogpassword)
  passdiv.append('<label for="guardpassword2">' + gt('Confirm the new password:') + '</label>').append(newogpassword2)
  passdiv.append(hint)
  const errormessage = $('<span id="uploaderror" style="color:red;"></span>')
  return div.append(message, passdiv, errormessage)
}

export default {
  open
}
