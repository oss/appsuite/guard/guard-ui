/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ox from '$/ox'
import ext from '$/io.ox/core/extensions'
import certs from '@/io.ox/guard/api/certs'
import core from '@/io.ox/guard/oxguard_core'
import '@/io.ox/guard/pgp/style.scss'
import Dropdown from '$/io.ox/backbone/mini-views/dropdown'
import Backbone from 'backbone'
import { createButton } from '$/io.ox/core/components'
import deleteSmimeView from '@/io.ox/guard/settings/smime/deleteSmimeView'
import simeAddSelector from '@/io.ox/guard/settings/smime/smimeAddSelector'
import guardModel from '@/io.ox/guard/core/guardModel'
import gt from 'gettext'

ext.point('oxguard/settings/certificateView').extend(
  // Draw Div with list keys
  {
    index: 100,
    id: 'header',
    render () {
      const tableHeader = $('<b>').text(gt('Your key list'))
      this.append(tableHeader)
    }
  },
  {
    index: 200,
    id: 'switch',
    render () {
      const div = $('<div class="keytable">')
      const node = this
      updateTable().then(function (info) {
        node.append(
          div.append(info)
        )
      })
    }
  }
)

function checkExpiring (data) {
  if (data.expired || !data.expires) return false
  const checkDates = guardModel().get('expiring') && guardModel().get('expiring').daysWarning ? guardModel().get('expiring').daysWarning : 30
  return (data.expires < (new Date().getTime() + (checkDates * 86400000)))
}

function updateTable () {
  const newtable = $('<table id="smimekeytable" class="keyTableList certificates">')
  const headers = $('<tr>').append(
    $('<th>').text(gt('Email')),
    $('<th>').text(gt('Expires')),
    $('<th>').text(gt('Certifier')),
    $('<th>').text(gt('Status')),
    $('<th>').text(gt('Actions'))
  )
  newtable.append(headers)
  const def = $.Deferred()
  certs.getCertificates().done(function (keyResponse) {
    if (keyResponse.certificates && keyResponse.certificates.length > 0) {
      const certs = keyResponse.certificates
      certs.forEach(function (data, index) {
        const tr = data.expired ? $('<tr style="color:red;">') : $('<tr>')
        const td1 = $('<td style="text-align:left;">').append(data.email).attr('title', data.email)
        const td2 = $('<td>').append(new Date(data.expires).toLocaleDateString())
        const td3 = $('<td NOWRAP class="certifier">').append(data.certifier).attr('title', data.certifier)
        const td4 = $('<td>')
        td4.append(
          data.expired
            ? $('<span class="label label-subtle subtle-red me-8">').text(gt('Expired'))
            : '',
          data.current
            ? $('<span class="label label-subtle subtle-green me-8">').text(gt('Current'))
            : '')
        if (checkExpiring(data)) {
          td4.append($('<span class="label label-subtle subtle-yellow me-8">').text(gt('Expiring')))
        }
        const td5 = $('<td>')
        td5.append(renderDropdown(data))

        tr.append(td1, td2, td3, td4, td5)
        newtable.append(tr)
        // add spacer
        if (index + 1 < certs.length) {
          newtable.append(
            $('<tr class="table-spacer">').append($('<td colspan="7">').append($('<hr>')))
          )
        }
      })
    } else if (keyResponse.certificates) {
      headers.remove()
      const link = $('<a class="noKeys">').append(gt('No keys found.  Please add keys by clicking here or using the button above.'))
      link.on('click', function (e) {
        e.preventDefault()
        launchAddKeys()
      })
      newtable.append(link)
    }
    def.resolve(newtable)
  })

  return def
}

function refreshKeys () {
  updateTable().done(updated => {
    $('#smimekeytable').replaceWith(updated)
  })
}

function launchAddKeys () {
  simeAddSelector.open().done(() => refreshKeys())
}

function renderDropdown (line) {
  const model = new Backbone.Model(line)
  const dropdown = new Dropdown({
    label: $('<i class="fa fa-bars" aria-hidden="true">'),
    $toggle: createButton({ href: '#', variant: 'none', icon: { name: 'bi/list.svg', title: gt('Manage keys') } }),
    model,
    title: gt('Actions')
  })
  dropdown
    .link('download', gt('Download'), () => onDownload(model))
    .link('current', gt('Mark current'), () => onCurrent(model))
    .divider()
    .link('delete', gt('Delete'), () => onDelete(model))

  dropdown.render().$el.addClass('dropdown pull-right').attr('data-dropdown', 'view')

  dropdown.$('[data-name="current"]').toggleClass('disabled',
    model.get('current')
  )

  return dropdown.$el.removeClass('pull-right')
}

function onDownload (model) {
  const id = model.get('serial')
  return core.getPassword(undefined, false).then(function (data) {
    core.verifyPassword(data.password, id, 'smime').then(function () {
      ox.trigger('guard:auth:success')
      certs.downloadAsFile({ password: data.password, serial: id, type: 'smime' }, data)
    }, () => {
      ox.trigger('guard:auth:yellandclose')
    })
  })
}

function onCurrent (model) {
  certs.makeCurrent(model.get('serial')).done(function () {
    refreshKeys()
  })
}

function onDelete (model) {
  const id = model.get('serial')
  deleteSmimeView.open(id).then(refreshKeys)
}
