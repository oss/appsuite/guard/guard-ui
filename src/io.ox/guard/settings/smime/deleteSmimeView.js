/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ModalDialog from '$/io.ox/backbone/views/modal'
import certs from '@/io.ox/guard/api/certs'
import guardModel from '@/io.ox/guard/core/guardModel'
import { PasswordView } from '@/io.ox/guard/core/passwordView'
import yell from '$/io.ox/core/yell'
import gt from 'gettext'

function openModalDialog (keyid) {
  const def = $.Deferred()
  new ModalDialog({
    async: true,
    title: gt('Delete Private Key'),
    focus: '#deleteSmimePass'
  })
    .build(function () {
      this.$body.append(
        $('<p style="color:red">').text(
          gt('You will no longer be able to decode any items that were encrypted with this key!')
        ),
        $('<label for="deleteSmimePass">').text(gt('Please enter your %s password', guardModel().getName())),
        new PasswordView({ id: 'deleteSmimePass' }).render().$el
      )
    })
    .addAlternativeButton({ label: gt('Delete'), action: 'delete' })
    .on('delete', function () {
      const dialog = this
      doDelete(keyid).done(() => {
        dialog.close()
        def.resolve()
      })
        .fail(e => {
          dialog.close()
          if (e && e.length > 1) yell('error', e)
          def.reject()
        })
    })
    .on('cancel', () => def.reject())
    .addCloseButton()
    .open()
  return def
}

function doDelete (keyid) {
  const def = $.Deferred()
  const pass = $('#deleteSmimePass').val()
  if (pass.length > 1) {
    certs.delete(keyid, pass).then(def.resolve, def.reject)
    return def
  }
  def.reject(gt('Enter password'))
  return def
}

export default {
  open: openModalDialog
}
