/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import _ from '$/underscore'
import ox from '$/ox'
import ext from '$/io.ox/core/extensions'
import ExtensibleView from '$/io.ox/backbone/views/extensible'
import { checkbox, compactSelect, fieldset } from '$/io.ox/core/settings/util'
import guardUtil from '@/io.ox/guard/util'
import mini from '$/io.ox/backbone/mini-views'
import core from '@/io.ox/guard/oxguard_core'
import guardModel from '@/io.ox/guard/core/guardModel'
import capabilities from '$/io.ox/core/capabilities'
import { selectTypeDialog } from '@/io.ox/guard/settings/views/selectTypeDialog'
import gt from 'gettext'
import keysAPI from '@/io.ox/guard/api/keys'
import yell from '$/io.ox/core/yell'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import openSettings, { closeSettings } from '$/io.ox/settings/util'
import * as util from '$/io.ox/core/settings/util'
import { resetPasswordDialog } from '@/io.ox/guard/settings/views/resetPasswordDialog'
import { secondaryEmailDialog } from '@/io.ox/guard/settings/views/secondaryEmailDialog'
import { createKeys, createKeysWizard } from '@/io.ox/guard/core/createKeys'
import { deleteRecoveryDialog } from '@/io.ox/guard/settings/views/deleteRecoveryDialog'
import publicKeys from '@/io.ox/guard/pgp/keyDetails'
import { renderChangePassword } from '@/io.ox/guard/settings/views/changePassword'
import '@/io.ox/guard/settings/smime/privateView'
import { autocryptTransferDialog } from '@/io.ox/guard/pgp/autocrypt/autocryptTransferDialog'
import userKeys from '@/io.ox/guard/pgp/userKeys'
import { openUploadPrivateDialog, createNewPGP } from '@/io.ox/guard/settings/views/uploadPrivateView'
import { smimeUpload } from '@/io.ox/guard/settings/smime/smimeUpload'
import { recipUpload } from '@/io.ox/guard/smime/recipUploader'
import smimeCreator from '@/io.ox/guard/settings/smime/smimeAddSelector'
import creator from '@/io.ox/guard/settings/smime/smimeCreator'
import certs from '@/io.ox/guard/api/certs'

const st = {
  GUARD: guardModel().getName(),
  // #. Settings View: Guard Default Settings Header
  GUARD_VIEW: gt('%s Default Settings', guardModel().getName()),
  // #. Settings View: Advanced Settings header
  GUARD_ADVANCED: gt('Advanced'),
  // #. Settings View: Explanation of Advanced settings header
  GUARD_ADVANCED_EXPLANATION: gt('Advanced settings for %s', guardModel().getName()),
  // #. Settings View: Password management section
  GAURD_PASSWORD: gt('Password Management'),
  // #. Settings View: Password management header explanation
  GUARD_PASSWORD_EXPLANATION: gt('Manage your password here'),
  // #. Settings View: Manage PGP Passwords
  GAURD_PASSWORD_PGP: gt('Password Management (PGP)'),
  // #. Settings View: Manage PGP Passwords explanation
  GUARD_PASSWORD_EXPLANATION_PGP: gt('Manage your PGP passwords here'),
  // #. Settings View: Manage S/Mime passwords
  GAURD_PASSWORD_SMIME: gt('Password Management (S/MIME)'),
  // #. Settings View: Manage S/Mime password explanation
  GUARD_PASSWORD_EXPLANATION_SMIME: gt('Manage your S/MIME certificate passwords here'),
  // #. Settings View: Explanation of default settings header
  GUARD_VIEW_EXPLANATION: gt('Configure %s default behavior.', guardModel().getName()),
  // #. Settings View: Setup prompt
  GUARD_SETUP: gt('Setup'),
  GUARD_SETUP_SMIME: gt('Setup S/MIME'),
  // #. Settings View: Explanation of need to set up password
  GUARD_SETUP_EXPLANATION: gt('In order to start with %s security, you must set up a password for your secured files.', guardModel().getName()),
  // #. Settings View: Explanation that the user must add an S/Mime certificate to get started
  GUARD_SETUP_SMIME_EXPLANATION: gt('In order to start with %s security, you must add an S/MIME certificate.', guardModel().getName()),
  GUARD_SMIME: gt('S/MIME'),
  // #. Settings View: Explanation for S/Mime header
  GUARD_SMIME_EXPLANATION: gt('Manage S/MIME type encryption'),
  // #. Settings View: Location to manage users keys
  GUARD_YOUR_KEYS: gt('Your Keys'),
  // #. Settings View: Location to manage PGP Keys
  GUARD_YOUR_KEYS_PGP: gt('Your PGP keys'),
  // #. Settings View: Explanation of Your PGP Keys header
  GUARD_YOUR_KEYS_EXPLANATION: gt('Manage your PGP keys'),
  // #. Settings View: Location to manage S/Mime certificates
  GUARD_YOUR_CERTIFICATES: gt('S/MIME Certificates'),
  // #. Settings View: Explanation of S/Mime certificates header
  GUARD_YOUR_CERTIFICATES_EXPLANATION: gt('Manage your S/MIME certificates'),
  // #. Settings View: Manage S/Mime recipients
  GUARD_SMIME_RECIPIENTS: gt('S/MIME Recipients'),
  // #. Settings View: Explanation for manage S/Mime recipients
  GUARD_SMIME_RECIPIENTS_EXPLANATION: gt('View and add recipient certificates'),
  // #. Settings View: View Recipient keys header
  GUARD_RECIPIENT_KEYS: gt('Recipient Keys'),
  // #. Settings View: View PGP Recipient keys
  GUARD_RECIPIENT_KEYS_PGP: gt('Recipient PGP Keys'),
  // #. Settings View: Manage PGP Recipients explanation
  GUARD_RECIPEINT_KEYS_EXPLANATION: gt('Manage recipient PGP keys'),
  // *. Default to using encrypting when starting a new mail
  GUARD_DEFAULT_ENCRYPT: gt('Default to send encrypted when composing email'),
  // *. Encrypt drafts when creating encrypted emails
  ENCRYPT_DRAFT: gt('Encrypt draft emails when composing encrypted emails'),
  // #. Settings View:Default to signing new emails with Guard signature
  GUARD_DEFAULT_SIGN: gt('Default to sign outgoing emails'),
  // #. Settings View: Enables a default to use S/Mime when creating a new email rather than pgp
  GUARD_DEFAULT_SMIME: gt('Default to using S/MIME for signing and encryption'),
  // #. Setting for how long to remember password
  GUARD_REMEMBER_PASSWORD: gt('Remember password default'),

  RESET_PASSWORD: gt('Reset password'),
  // #. Set the email address used when reseting passwords
  GUARD_RESET_EMAIL: gt('Set email address for reset'),
  // *. DOwnload the users public PGP key
  DOWNLOAD_PUBLIC: gt('Download my public key'),
  // #. Settings: Button to upload a new private pgp key
  UPLOAD_PRIVATE: gt('Upload Private Key'),
  // #. Settings: Button to upload a public key to use
  UPLOAD_PUBLIC: gt('Upload Public Key'),
  // #. Upload a new recipient key
  UPLOAD_RECIP: gt('Upload Recipient Key'),
  // #. Header for list of keys collected using autocrypt
  AUTOCRYPT_COLLECTED: gt('Autocrypt Collected keys'),
  // #. Checkbox to enable smime functionality
  ENABLE_SMIME: gt('Enable S/MIME'),
  // #. Upload a new private smime certificate
  UPLOAD_CERTIFICATE: gt('Upload Private Certificate'),
  // #. Upload a certificate for a recipient
  UPLOAD_RECIP_CERTIFICATE: gt('Upload Recipient Certificate'),
  // #. Check if a certificate exists.  Enter an email at the input prompt to check.
  CERTIFICATE_CHECK: gt('Check for certificate. Enter email to look up'),
  // #. Section header for autocrypt settings
  AUTOCRYPT_SETTING: gt('Autocrypt Setting'),
  // #. Settings: Checkbox.  If enabled, user is displayed advanced Guard settings when composing emails
  SHOW_ADVANCED: gt('Show advanced settings when composing mail'),
  // #. Default to using PGP Inline setting when creating new mail
  DEFAULT_INLINE: gt('Default to using PGP inline for new mails'),
  // #. Section header for mail cache behavior
  MAIL_CACHE: gt('Mail Cache'),
  // #. Delete the users password recovery.
  DELETE_RECOVERY: gt('Delete password recovery'),
  // #. Autocrypt setting.  Add public keys to outgoing emails
  AUTOCRYPT_ADD_OUTGOING: gt('Add your public keys in all outgoing emails'),
  // #. Checkbox, import autocrypt keys automatically without asking
  AUTOCRYPT_IMPORT_AUTOMATIC: gt('Import autocrypt keys without asking'),
  // #. Button to start the autocrypt transfer of keys
  AUTOCRYPT_TRANSFER: gt('Autocrypt transfer keys')
}

const guestOnly = guardUtil.isGuest() || (!guardUtil.hasGuardMailCapability() && !guardUtil.hasSmime())

function hasBoth () {
  return (core.hasSmime() && core.hasGuardMail() && !core.isGuest())
}

function hasBothAndEnabled () {
  return (core.hasSmime() && core.hasGuardMail() && !core.isGuest() && settings.get('smime'))
}

function smimeOnly () {
  return (core.hasSmime() && !core.hasGuard())
}

ext.point('io.ox/guard/settings/detail').extend({
  index: 100,
  id: 'view',
  draw () {
    this.append(
      util.header(
        st.GUARD,
        'ox.appsuite.user.sect.guard.settings.html'
      ),
      new ExtensibleView({ point: 'io.ox/guard/settings/detail/viewsection', model: settings })
        .build(function () {
          this.$el.addClass('settings-body io-ox-guard-settings')
          this.listenTo(settings, 'change', () => settings.saveAndYell())
        })
        .render().$el
    )
  }
})

ext.point('io.ox/guard/settings/detail/viewsection').extend({
  id: 'setup',
  index: 100,
  render: util.renderExpandableSection(st.GUARD_SETUP, st.GUARD_SETUP_EXPLANATION, 'io.ox/guard/settings/setup', true)
},
{
  id: 'setupSmime',
  index: 200,
  render: util.renderExpandableSection(st.GUARD_SETUP_SMIME, st.GUARD_SETUP_SMIME_EXPLANATION, 'io.ox/guard/settings/setupsmime', true)
},
{
  id: 'view',
  index: 300,
  render: function (baton) {
    this.$el.append(renderSetupSections.call(this, baton))
  }
},
{
  id: 'reset',
  index: 400,
  render () {
    if (_.url.hash('guardReset') === 'true') {
      _.url.hash({ guardReset: null })
      createKeys(gt('This will create new keys to use for future encryption.'))
        .fail(e => {
          if (e === 'cancel') return
          yell('error', e.responseText)
        })
    }
  }
})

// Render all of the sections for Guard settings
function renderSetupSections (baton) {
  if (guestOnly) {
    ext.point('io.ox/guard/settings/view').disable('compose')
  }
  this.$el = $('<div id="GuardSettingsGroup" class="settings-body io-ox-guard-settings">')
  util.renderExpandableSection(st.GUARD_VIEW, st.GUARD_VIEW_EXPLANATION, 'io.ox/guard/settings/view', false).call(this, baton)
  // Render your keys section.  Sections dependent on user having S/MIME, guard, or both
  if (hasBoth()) {
    util.renderExpandableSection(st.GAURD_PASSWORD_PGP, st.GUARD_PASSWORD_EXPLANATION_PGP, 'io.ox/guard/settings/pgpPassword', false).call(this, baton)
    util.renderExpandableSection(st.GAURD_PASSWORD_SMIME, st.GUARD_PASSWORD_EXPLANATION_SMIME, 'io.ox/guard/settings/smimePassword', false).call(this, baton)
    util.renderExpandableSection(hasBothAndEnabled() ? st.GUARD_YOUR_KEYS_PGP : st.GUARD_YOUR_KEYS, st.GUARD_YOUR_KEYS_EXPLANATION, 'io.ox/guard/settings/pgpkeys', false).call(this, baton)
    util.renderExpandableSection(hasBothAndEnabled() ? st.GUARD_RECIPIENT_KEYS_PGP : st.GUARD_RECIPIENT_KEYS, st.GUARD_RECIPEINT_KEYS_EXPLANATION, 'io.ox/guard/settings/recipients', false).call(this, baton)
    util.renderExpandableSection(st.GUARD_SMIME, st.GUARD_SMIME_EXPLANATION, 'io.ox/guard/settings/certificates', false).call(this, baton)
    util.renderExpandableSection(st.GUARD_SMIME_RECIPIENTS, st.GUARD_SMIME_RECIPIENTS_EXPLANATION, 'io.ox/guard/settings/recipCertificates', false).call(this, baton)
    if (!hasBothAndEnabled()) {
      this.$el.find('details[data-section="io.ox/guard/settings/smimePassword"]').hide()
      this.$el.find('details[data-section="io.ox/guard/settings/recipCertificates"]').hide()
    }
  } else {
    if (smimeOnly()) {
      util.renderExpandableSection(st.GAURD_PASSWORD, st.GUARD_PASSWORD_EXPLANATION, 'io.ox/guard/settings/smimePassword', false).call(this, baton)
      util.renderExpandableSection(st.GUARD_YOUR_CERTIFICATES, st.GUARD_YOUR_CERTIFICATES_EXPLANATION, 'io.ox/guard/settings/certificates', false).call(this, baton)
      util.renderExpandableSection(st.GUARD_SMIME_RECIPIENTS, st.GUARD_SMIME_RECIPIENTS_EXPLANATION, 'io.ox/guard/settings/recipCertificates', false).call(this, baton)
    } else {
      util.renderExpandableSection(st.GAURD_PASSWORD, st.GUARD_PASSWORD_EXPLANATION, 'io.ox/guard/settings/pgpPassword', false).call(this, baton)
      util.renderExpandableSection(st.GUARD_YOUR_KEYS, st.GUARD_YOUR_KEYS_EXPLANATION, 'io.ox/guard/settings/pgpkeys', false).call(this, baton)
      if (!guestOnly) util.renderExpandableSection(st.GUARD_RECIPIENT_KEYS, st.GUARD_RECIPEINT_KEYS_EXPLANATION, 'io.ox/guard/settings/recipients', false).call(this, baton)
    }
  }

  if (!guestOnly) {
    util.renderExpandableSection(st.GUARD_ADVANCED, st.GUARD_ADVANCED_EXPLANATION, 'io.ox/guard/settings/advanced', false).call(this, baton)
  }

  updateWording()
  return this.$el
}

// Update the wording on sections depending on having both smime and pgp enabled
function updateWording () {
  if (hasBoth()) {
    if (hasBothAndEnabled()) {
      const smimeSection = $('details[data-section="io.ox/guard/settings/smimePassword"]')
      smimeSection.show()
      smimeSection.find('.title').text(st.GAURD_PASSWORD_SMIME)
      smimeSection.find('.explanation').text(st.GUARD_PASSWORD_EXPLANATION_SMIME)
      const pgpSection = $('details[data-section="io.ox/guard/settings/pgpPassword"]')
      pgpSection.find('.title').text(st.GAURD_PASSWORD_PGP)
      pgpSection.find('.explanation').text(st.GUARD_PASSWORD_EXPLANATION_PGP)
      $('details[data-section="io.ox/guard/settings/recipCertificates"]').show()
    } else {
      $('details[data-section="io.ox/guard/settings/smimePassword"]').hide()
      $('details[data-section="io.ox/guard/settings/recipCertificates"]').hide()
      const pgpSection = $('details[data-section="io.ox/guard/settings/pgpPassword"]')
      pgpSection.find('.title').text(st.GAURD_PASSWORD)
      pgpSection.find('.explanation').text(st.GUARD_PASSWORD_EXPLANATION)
    }
  }
}

function updateSetupDone () {
  ext.point('io.ox/guard/settings/detail/viewsection').disable('setup')
  ext.point('io.ox/guard/settings/detail/viewsection').disable('setupSmime')
  ext.point('io.ox/guard/settings/detail/viewsection').disable('view')
  guardUtil.hasSetupDone()
    .done(function () {
      ext.point('io.ox/guard/settings/detail/viewsection').enable('view')
    })
    .fail(function () {
      if (capabilities.has('guard') && (capabilities.has('guard-mail') || capabilities.has('guard-drive'))) {
        ext.point('io.ox/guard/settings/detail/viewsection').enable('setup')
      } else if (capabilities.has('smime')) {
        ext.point('io.ox/guard/settings/detail/viewsection').enable('setupSmime')
      }
    })
}

guardModel().get('certsModel').on('change', updateSetupDone)
guardModel().on('change:authcode', updateSetupDone)

if (guardModel().isLoaded()) {
  updateSetupDone()
} else {
  guardUtil.addOnLoaded(updateSetupDone)
}

ext.point('io.ox/guard/settings/setup').extend({
  id: 'starts',
  index: 100,
  render (baton) {
    const startButton = $('<button type="button" class="btn btn-default guard-start-button">').text(gt('Start'))
    startButton.on('click', function () {
      closeSettings()
      createKeysWizard().done(function (result) {
        updateSetupDone()
        openSettings('virtual/settings/io.ox/guard')
      })
    })
    this.append(startButton)
  }
})

ext.point('io.ox/guard/settings/setupsmime').extend({
  id: 'starts',
  index: 100,
  render (baton) {
    const startButton = $('<button type="button" class="btn btn-default guard-start-button">').text(gt('Start'))
    startButton.on('click', function () {
      smimeCreator.open()
        .always(function () {
          guardUtil.hasSetupDone() // Pull certs and verify there
            .done(() => {
              openSettings('virtual/settings/io.ox/guard')
            })
        })
    })
    this.append(startButton)
  }
})

ext.point('io.ox/guard/settings/view').extend(
  {
    id: 'tempPassword', // In settings page, if user needs to setup password then show dialog now
    index: 50,
    render () {
      if (guardModel().needsPassword()) {
        import('@/io.ox/guard/core/tempPassword').then(({ default: tempPass }) => {
          tempPass.createOxGuardPasswordPrompt()
        })
      }
    }
  },
  {
    id: 'compose',
    index: 100,
    render () {
      let smime
      if (core.hasSmime() && guardUtil.hasGuardMailCapability()) {
        smime = checkbox('defaultSmime', st.GUARD_DEFAULT_SMIME, settings).css('display', settings.get('smime') ? 'block' : 'none').addClass('smimeDefaultOption')
        smime.on('change', function () {
          if (settings.get('defaultSmime')) {
            settings.set('defaultInline', false)
          }
        })
      }
      this.append(
        $('<div class="guardDefaults">').append(
          fieldset(
            gt('Compose Options'),
            // Encrypt draft emails when composing encrypted mails
            // #. Settings View: Tells Guard to encrypt the draft email when they are composing an encrypted email
            guardModel().forceEncryptedDrafts() ? '' : checkbox('encryptDraft', st.ENCRYPT_DRAFT, settings),
            // default to encrypt outgoing
            checkbox('defaultEncrypted', st.GUARD_DEFAULT_ENCRYPT, settings),
            // default to signing outgoing emails
            // #. Settings View:Default to signing new emails with Guard signature
            checkbox('defaultSign', st.GUARD_DEFAULT_SIGN, settings),
            smime)
        )
      )
    }
  },
  {
    id: 'defaults',
    index: 120,
    render () {
      this.append(
        $('<div class="guardDefaults">').append(
          fieldset(
            gt('Options'),
            compactSelect('defRemember', st.GUARD_REMEMBER_PASSWORD, settings, [
              { label: gt('Ask each time'), value: 0 },
              { label: gt('10 minutes'), value: 10 },
              { label: gt('20 minutes'), value: 20 },
              { label: gt('30 minutes'), value: 30 },
              { label: gt('1 hour'), value: 60 },
              { label: gt('2 hours'), value: 120 },
              { label: gt('Session'), value: 99999 }
            ])
          )
        )
      )
    }
  }
)

function drawPasswordDiv (type) {
  const div = $('<div class="settingsPasswordDiv">')
  div.append(renderChangePassword(type))
  return div
}

//
// Password
//
ext.point('io.ox/guard/settings/pgpPassword').extend(
  {
    id: 'changePassword',
    index: 100,
    render (baton) {
      this.append(
        drawPasswordDiv('pgp')
      )
    }
  },
  {
    id: 'reset',
    index: 200,
    render () {
      // check enabled
      if (!guardModel().get('recoveryAvail') || guardModel().get('new')) { // If no recovery avail, then don't display reset button
        return
      }

      const node = $('<div class="settingsPasswordButtons">')

      node.append(
        $('<button type="button" class="btn btn-default" id="resetPassword">')
          .text(st.RESET_PASSWORD)
          .on('click', openReset)
          .attr('name', 'pgpResetButton')
      )

      node.append(
        $('<button type="button" class="btn btn-default" id="changeEmail">')
          .text(st.GUARD_RESET_EMAIL)
          .on('click', openSecondary)
          .attr('name', 'pgpSecondaryButton')
      )

      this.append(node)

      function openReset () {
        resetPasswordDialog('pgp')
      }

      function openSecondary () {
        secondaryEmailDialog('pgp')
      }
    }
  }
)

//
// Password
//
ext.point('io.ox/guard/settings/smimePassword').extend(
  {
    id: 'changePassword',
    index: 100,
    render (baton) {
      this.append(
        drawPasswordDiv('smime')
      )
    }
  },
  {
    id: 'reset',
    index: 200,
    render () {
      // check enabled
      if (!guardModel().get('recoveryAvail') || guardModel().get('new')) { // If no recovery avail, then don't display reset button
        return
      }

      const node = $('<div class="settingsPasswordButtons">')

      node.append(
        $('<button type="button" class="btn btn-default" id="resetPassword">')
          .text(st.RESET_PASSWORD)
          .on('click', openReset)
          .attr('name', 'smimeResetButton')
      )

      node.append(
        $('<button type="button" class="btn btn-default" id="changeEmail">')
          .text(gt('Set email address for reset'))
          .on('click', openSecondary)
          .attr('name', 'smimeSecondaryButton')
      )

      this.append(node)

      function openReset () {
        resetPasswordDialog('smime')
      }

      function openSecondary () {
        secondaryEmailDialog('smime')
      }
    }
  }
)

ext.point('io.ox/guard/settings/pgpkeys').extend(
  {
    id: 'pgpKeys',
    index: 100,
    render (baton) {
      const buttonDiv = $('<div class="guardSettingsSub guardButtonsDiv">')
      const userKeyList = userKeys.userKeys()
      const downloadPublicButton = $('<button type="button" class="btn btn-default" name="downloadPublic">')
        .text(st.DOWNLOAD_PUBLIC)
        .on('click', () => keysAPI.downloadAsFile({ keyType: 'public' }))
      const createNew = $('<button type="button" class="btn btn-default" name="createNew">')
      // #. Settings: Button for creating new key
        .text(gt('Create New'))
        .on('click', () => createNewPGP())
      const uploadPrivate = $('<button type="button" class="btn btn-default" name="uploadPrivate">')
      // #. Settings: Button to upload a new private pgp key
        .text(st.UPLOAD_PRIVATE)
        .on('click', () => openUploadPrivateDialog(true))
      const uploadPublic = $('<button type="button" class="btn btn-default" name="uploadPublic">')
        .text(st.UPLOAD_PUBLIC)
        .on('click', () => openUploadPrivateDialog(false))
      buttonDiv.append($('<div>').append(downloadPublicButton), createNew, uploadPrivate, uploadPublic)
      this.append(buttonDiv, userKeyList)
    }
  }
)

ext.point('io.ox/guard/settings/certificates').extend(
  {
    id: 'certificates',
    index: 100,
    render (baton) {
      if (hasBoth()) {
        // #. Settings: Check box, Enables S/Mime
        const smime = checkbox('smime', st.ENABLE_SMIME, settings)
        smime.on('change', function () {
          updateWording()
          if (settings.get('smime')) {
            $('.smimeDefaultOption').show()
            $('.defaultinlineDiv').css('margin-top', '-5px')
            $('#smimeAddDiv .btn').attr('disabled', false)
            $('#smimeRecipAdd .btn').attr('disabled', false)
          } else {
            $('.smimeDefaultOption').hide()
            $('.defaultinlineDiv').css('margin-top', '7px')
            $('#smimeAddDiv .btn').attr('disabled', true)
            $('#smimeRecipAdd .btn').attr('disabled', true)
          }
        })
        this.append($('<div class="guardSettingsSub">').append(smime))
      }
      const node = $('<div id="SmimeKeyList">')
      const uploadCertButton = $('<button type="button" class="btn btn-default" id="uploadPrivateButton" ' + (core.hasSmimeEnabled() ? '' : 'disabled="disabled"') + '">')
      // #. Settings: Button to upload new private certificate
        .text(st.UPLOAD_CERTIFICATE)
        .on('click', function () {
          smimeUpload().then(() => {
            node.empty()
            ext.point('oxguard/settings/certificateView').invoke('render', node)
          })
        })
      const addCertDiv = $('<div id="smimeAddDiv" class = "guardSettingsSub">')
        .append(uploadCertButton)
      if (guardModel().canCreateCertificates()) {
        const newCertButton = $('<button type="button" class="btn btn-default" id="createNewButton" ' + (core.hasSmimeEnabled() ? '' : 'disabled="disabled"') + '">')
        // #. Settings: Button to create a new private certificate
          .text(gt('Create new certificate'))
          .on('click', function () {
            creator.open().then(() => {
              node.empty()
              ext.point('oxguard/settings/certificateView').invoke('render', node)
            })
          })
        addCertDiv.append(newCertButton)
      }
      this.append(addCertDiv, node)
      ext.point('oxguard/settings/certificateView').invoke('render', node)
    }
  }
)

ext.point('io.ox/guard/settings/recipCertificates').extend(
  {
    id: 'recipCertificates',
    index: 100,
    render (baton) {
      const uploadRecipCert = $('<button type="button" class="btn btn-default" id="uploadRecipButton">')
      // #. Settings: Upload a certificate for recipients
        .text(st.UPLOAD_RECIP_CERTIFICATE)
        .on('click', () => {
          fileinput.click()
        })
      const fileinput = $('<input type="file" id="smimeKeyFileInput" accept=".p7b, .p7c, .cer, .crt" style="display:none;">').unbind('change')
        .on('change', function () {
          const files = this.files
          if (files.length > 0) {
            recipUpload(files)
              .done(function (data) {
                if (data && data.certificates && data.certificates.length > 0) {
                  const key = data.certificates[0]
                  // #. Settings: Successfully imported a recipient certificate
                  yell('success', gt('Imported key for ID %s\nSerial Number %s\nExpiration date %s', key.email, key.serial, new Date(key.expires).toDateString()))
                }
              })
              .always(() => {
                fileinput.val(undefined)
              })
          }
        })
      this.append($('<div id="smimeRecipAdd" class="guardSettingsSub">').append(uploadRecipCert, fileinput))
      const lookupDiv = $('<div class="guardSettingsSub lookupDiv">')
      const inputDiv = $('<div class="emailSearch">')
      const input = $('<input class="form-control mb-16" id="certLookup" class="certLookup">')
      const lookupButton = $('<button type="button" class="btn btn-default" id="checkRecipButton">')
      // #. Button:  User has entered an email address.  Triggers search to see if any certificates associated with the email
        .text(gt('Lookup'))
        .on('click', () => {
          lookup(input.val())
        })
      const resultDiv = $('<div id="certLookupResult" class="certResult">')
      lookupDiv.append(
        // #. Settings: Label for input where user can enter email address to check if certificate exists
        $('<label for="certLookup">').append(st.CERTIFICATE_CHECK),
        inputDiv.append(input, lookupButton),
        resultDiv
      )
      this.append(lookupDiv)
    }
  }
)

// Certificate lookup
function lookup (email) {
  // check email
  certs.getRecipientsPublicKey(email).then((certData) => {
    const div = $('<div class="certificateDataClass">')
    if (!certData) {
      $('#certLookupResult').html(div.append(gt('Not found')))
    } else {
      div.append(
        $('<label for="certifierId">').append(gt('Certifier:')),
        $('<span id="certifierId">').append(certData.certifier))
      div.append('<br>')
      div.append(
        $('<label for="certifierExp">').append(gt('Expires:')),
        $('<span id="certifierExp">').append(new Date(certData.expires).toLocaleString()))
      div.append('<br>')
      div.append(
        $('<label for="certifierSerial">').append(gt('Serial Number:')),
        $('<span id="certifierSerial">').append(certData.serial))
      $('#certLookupResult').html(div)
    }
  })
}

ext.point('io.ox/guard/settings/recipients').extend(
  {
    id: 'pgpRecipients',
    index: 100,
    render (baton) {
      const uploadPublic = $('<div class = "guardSettingsSub">').append($('<button type="button" class="btn btn-default" name="uploadRecip">')
        .text(st.UPLOAD_RECIP)
        .on('click', () => publicKeys.listPublic({ id: 'settings' }).uploadPublic()))
      const pgpKeysDiv = $('<fieldset class="keyTableList pgpPublicKeysList" style="margin-top:-20px;">').append(uploadPublic,
        publicKeys.listPublic({ id: 'settings' }).container
      )
      // Don't draw autocrypt div if not enabled
      if (!guardUtil.autoCryptEnabled()) {
        this.append(pgpKeysDiv)
        return
      }
      const autoCryptDiv = $('<fieldset class="keyTableList pgpPublicKeysList">').append(
        (publicKeys.listPublic({
          id: 'autoCrypt',
          title: st.AUTOCRYPT_COLLECTED,
          minimal: true
        }).container.attr('name', 'autocryptCollected')))
      this.append(pgpKeysDiv, autoCryptDiv)
    }
  }
)

ext.point('io.ox/guard/settings/advanced').extend(
  {
    id: 'advanced',
    index: 100,
    render (baton) {
      let advField = ''
      let autoCryptFieled = ''
      // For users with Guard-mail, offer advanced compose settings, and pgp-inline options
      if (core.hasGuardMail()) {
        // #. Settings: Checkbox.  If enabled, user is displayed advanced Guard settings when composing emails
        const advancedBox = checkbox('advanced', st.SHOW_ADVANCED, settings)
        const pgpInline = checkbox('defaultInline', st.DEFAULT_INLINE, settings).addClass('guardAdvanced').addClass('defaultinlineDiv').css('display', settings.get('advanced') ? 'block' : 'none')
        if (!settings.get('smime')) { // If the smime checkbox wasn't drawn, the css:first will not add the needed padding here
          pgpInline.css('margin-top', '7px')
        }
        pgpInline.on('change', function () {
          if (settings.get('defaultInline')) {
            settings.set('defaultSmime', false)
          }
        })
        advField = $('<div>').append(advancedBox, pgpInline).addClass('guardSettingsSub')
        // Add autocrypt options if enabled
        if (guardUtil.autoCryptEnabled()) {
          autoCryptFieled = $('<fieldset>').append(drawAutocrypt()).css('margin-top', '-30px')
        }
      }
      const wipeOptions = drawWipeOptions()
      this.append(advField, autoCryptFieled, wipeOptions, drawDeleteRecovery(), drawVersion())
    }
  }
)

function drawDeleteRecovery (baton) {
  if (!guardModel().get('recoveryAvail') || guardModel().get('new') || guardModel().getSettings().noDeleteRecovery || guardUtil.isGuest()) { // If no recovery avail, then don't display reset button
    return
  }
  const button = $('<button type="button" class="btn btn-default guardAdvanced" id="deleteRecovery">')
  // #. Option to remove the possibility of recovering your password
    .text(st.DELETE_RECOVERY)
    .on('click', openDialog)

  return fieldset(
    gt('Reset Options'),
    button
  )
  function openDialog () {
    selectTypeDialog({
      title: gt('Select type of keys to delete recovery'),
      pgpAction () {
        deleteRecoveryDialog(baton, 'pgp')
      },
      smimeAction () {
        deleteRecoveryDialog(baton, 'smime')
      }
    })
  }
}

function drawWipeOptions () {
  function getOptions () {
    return [{ label: gt('Keep emails decrypted until browser closed'), value: false },
      { label: gt('Do not keep emails decrypted.'), value: true }]
  }

  return fieldset(
    st.MAIL_CACHE,
    new mini.CustomRadioView({ list: getOptions(), name: 'wipe', model: settings }).render().$el
  ).attr('id', 'mailcache')
}

function drawAutocrypt () {
  const div = $('<div class="autocryptSettings">')
  if (!guardUtil.autoCryptEnabled() || !core.hasGuardMail()) return div
  const transfer = $('<button type="button" class="btn btn-default" name="transferKeys">')
    .text(st.AUTOCRYPT_TRANSFER)
    .on('click', autocryptTransferDialog)
  const headerAdd = checkbox('autocryptHeader', st.AUTOCRYPT_ADD_OUTGOING, settings)
  const autoAdd = checkbox('autoAddAutocrypt', st.AUTOCRYPT_IMPORT_AUTOMATIC, settings)
  div.append(
    headerAdd,
    checkbox('enableAutocrypt', gt('Search incoming emails for keys in header'), settings),
    autoAdd, transfer)
  settings.on('change:enableAutocrypt', function () {
    checkEnabled(autoAdd)
  })
  checkEnabled(autoAdd)
  return fieldset(
    st.AUTOCRYPT_SETTING,
    div
  )
}

function checkEnabled (block) {
  const enabled = settings.get('enableAutocrypt')
  block.find('[name="autoAddAutocrypt"]').attr('disabled', !enabled)
  block.find('label').toggleClass('disabled', !enabled)
}

function drawVersion () {
  if (_.device('small')) return ('')
  const div = $('<fieldset class="guardVersion">')
  fetch(ox.abs + ox.root + '/meta').then(async function (response) {
    if (response.ok) {
      try {
        const data = await response.json()
        data.forEach((s) => {
          if (s.id === 'guard-ui') {
            div.append('UI: ' + s.version + ' ' + s.buildDate)
          }
        })
        div.append('<br>build: ' + guardModel().get('server'))
      } catch (e) {
        console.log('Unable to load Guard UI meta data')
        div.append('No version data')
      }
    }
  })
  return (div)
}

// Convert old Guard server settings to local.  Should be able to remove version after 7.10.  Remove call from oxguard/register
function initSettings (data) {
  settings.set('defaultEncrypted', data.pgpdefault === true)
  settings.set('defaultSign', data.pgpsign === true)
  settings.set('defaultInline', data.inline === true)
  settings.save()
}

export const gs = st
export default initSettings
