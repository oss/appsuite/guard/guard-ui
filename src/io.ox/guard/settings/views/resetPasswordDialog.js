/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ox from '$/ox'
import _ from '$/underscore'

import ModalDialog from '$/io.ox/backbone/views/modal'
import authAPI from '@/io.ox/guard/api/auth'
import og_http from '@/io.ox/guard/core/og_http'
import Backbone from '$/backbone'
import gt from 'gettext'
import guardModel from '@/io.ox/guard/core/guardModel'
import yell from '$/io.ox/core/yell'
import keyCreator from '@/io.ox/guard/core/createKeys'
import accountAPI from '$/io.ox/core/api/account'

export function resetPasswordDialog (type) {
  return new ModalDialog({
    async: true,
    focus: 'input[name="active"]',
    title: gt('Reset Password'),
    model: new Backbone.Model({ type })
  })
    .inject({
      doReset () {
        return doReset(this.model.get('type'))
      }
    })
    .build(function () {
      this.$el.addClass('resetDialog')
      this.$body.append(
        $('<p>').text(gt('This will reset your %s password, and send you a new password to the configured email address.', guardModel().getName())),
        $('<p>').text(gt('If you have more than one key, this will reset the password for the one marked as current'))
      )
    })
    .addCancelButton()
    .addButton({ label: gt('Reset'), action: 'reset' })
    .on('reset', function () {
      this.doReset(this.model.get('type')).then(this.close, this.idle)
    })
    .open()
}

async function doReset (type) {
  const def = $.Deferred()
  const performReset = async function (primaryEmail) {
    const waitdiv = keyCreator.waitDiv()
    $('.modal-footer').append(waitdiv)
    waitdiv.show()
    const param = '&userid=' + ox.user_id + '&cid=' + ox.context_id +
      '&default=' + encodeURIComponent(primaryEmail) + '&lang=' + ox.language + '&type=' + type
    og_http.get(ox.apiRoot + '/oxguard/login?action=reset', param)
      .done(function (resp) {
        resp = resp.data ? resp.data : resp
        $('.og_wait').remove()
        authAPI.resetToken()
        // This is for backwards compat with 7.10.6.  Can be removed when 3.0 UI no longer used with old backend
        let data = {}
        if (_.isString(resp)) {
          data.result = resp
        } else {
          data = resp
        }
        //  End of temp fix.
        const isPGP = type && (type.toLowerCase() === 'pgp')
        if (data.error) {
          yell('error', gt('Unable to reset your password') + ': ' + data.error)
          def.reject()
          return
        }
        if (data.result === 'primary') {
          yell('success', gt('A new password has been sent to your email address.'))
          if (isPGP) guardModel().setAuth('Password Needed')
          def.resolve()
          return
        }
        if (data.result === 'ok') {
          yell('success', gt('A new password has been sent to your secondary email address.'))
          if (isPGP) guardModel().setAuth('Password Needed')
          def.resolve()
          return
        }
        if (data.result === 'NoRecovery') {
          yell('error', gt('No password recovery is available.'))
          def.reject()
          return
        }
        if (data.result === 'FailNotify') {
          yell('error', gt('Password reset, but unable to send to your email address.'))
          def.reject()
          return
        }
        if (data.result === 'NoSecondary') {
          yell('error', gt('Unable to find email address to send password reset.'))
          def.reject()
          return
        }
        yell('error', gt('Unable to reset your password'))
        def.reject()
      })
      .fail(function () {
        $('.og_wait').remove()
        yell('error', gt('Unable to reset your password'))
        def.reject()
      })
  }

  accountAPI.getPrimaryAddress().done(function (data) { // Get primary address as backup
    const primaryEmail = data[1]
    performReset(primaryEmail)
  })
    // Possible guest, no primary email
    .fail(() => performReset(ox.user))
  return def
}
