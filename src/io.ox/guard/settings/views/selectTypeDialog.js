/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ModalDialog from '$/io.ox/backbone/views/modal'
import ext from '$/io.ox/core/extensions'
import guardModel from '@/io.ox/guard/core/guardModel'
import core from '@/io.ox/guard/oxguard_core'
import Backbone from '$/backbone'
import gt from 'gettext'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import '@/io.ox/guard/settings/style.scss'

export function selectTypeDialog (o) {
  if (!core.hasSmime() || (core.hasGuard() && !settings.get('smime'))) {
    return o.pgpAction()
  }
  if (!core.hasGuard()) {
    return o.smimeAction()
  }
  const def = $.Deferred()
  openModalDialog(o.title, o.pgpAction, o.smimeAction, def)
  return def
}

function openModalDialog (title, pgpAction, smimeAction, def) {
  return new ModalDialog({
    async: true,
    point: 'oxguard/settings/selectType',
    title: title || gt('Select type to use'),
    width: 500,
    class: 'cryptoSelector',
    model: new Backbone.Model({ title, pgpAction, smimeAction, def })
  })
    .inject({
    })
    .build(function () {
    })
    .addCancelButton()
    .open()
}

ext.point('oxguard/settings/selectType').extend(
  {
    index: 100,
    id: 'PGP',
    render (baton) {
      const def = baton.model.get('def')
      const doAction = function () {
        const pgpAction = baton.model.get('pgpAction')
        baton.view.close()
        getDef(pgpAction).then(def.resolve, def.reject)
      }
      const div = $('<div class="cryptoTypeDiv">').on('click', doAction)
      const innerDiv = $('<div style="width: 100%">')
      const label = $('<label for="PGPButton">').append(gt('PGP (The default type for %s).', guardModel().getName()))
      const button = $('<button type="button" class="btn btn-primary" id="PGPButton">')
        .text(gt('PGP'))
        .on('click', doAction)
      this.$body.append(
        div.append(innerDiv.append(label).append(button))
      )
    }
  },
  {
    index: 200,
    id: 'SMIME',
    render (baton) {
      const def = baton.model.get('def')
      const doAction = function () {
        const smimeAction = baton.model.get('smimeAction')
        baton.view.close()
        getDef(smimeAction).then(def.resolve, def.reject)
      }
      const div = $('<div class="cryptoTypeDiv">').on('click', doAction)
      const innerDiv = $('<div style="width: 100%">')
      const label = $('<label for="smimeButton">').append(
        gt('S/MIME'))
      const button = $('<button type="button" class="btn btn-primary" id="smimeButton">')
        .text(gt('S/MIME'))
        .on('click', doAction)
      this.$body.append(
        div.append(innerDiv.append(label).append(button))
      )
    }
  }
)

function getDef (action) {
  const def = action()
  if (def && def.then) {
    return def
  }
  return $.when()
}
