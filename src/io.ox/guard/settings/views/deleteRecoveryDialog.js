/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ox from '$/ox'
import ModalDialog from '$/io.ox/backbone/views/modal'
import ext from '$/io.ox/core/extensions'
import yell from '$/io.ox/core/yell'
import guardModel from '@/io.ox/guard/core/guardModel'
import core from '@/io.ox/guard/oxguard_core'
import Backbone from '$/backbone'
import og_http from '@/io.ox/guard/core/og_http'
import gt from 'gettext'

export function deleteRecoveryDialog (baton, type) {
  return new ModalDialog({
    async: true,
    focus: 'input[name="active"]',
    point: 'io.ox/guard/settings/reset',
    title: gt('Delete Recovery'),
    model: new Backbone.Model({ type })
  })
    .inject({
    })
    .build(function () {
      this.$body.append(
        $('<p>').text(gt('The password recovery is used to reset your password if you forget it.  If you delete the recovery, you will not be able to reset your password.')),
        $('<p>').text(gt('Note: Changing your password will restore the password recovery.'))
      )
    })
    .addCancelButton()
    .addButton({ label: gt('Delete'), action: 'delete' })
    .on('delete', function () {
      performDelete(this.model.get('type')).done(function () {
        ext.point('io.ox/guard/settings/detail').invoke('draw', $('.guard-settings'), baton) // redraw settings
      }).done(this.close).fail(this.idle).fail(yell)
    })
    .open()
}

function performDelete (type) {
  const def = $.Deferred()
  core.getPassword(gt('Please verify your password before deleting the recovery.'))
    .done(function (e) {
      ox.trigger('guard:auth:success')
      const data = {
        userid: ox.user_id,
        cid: ox.context_id,
        password: e.password
      }
      og_http.post(ox.apiRoot + '/oxguard/login?action=deleterecovery&type=' + type, '', data)
        .done(function (r) {
          if (r && r.error) {
            if (r.error === 'none') {
              yell('error', gt('Unable to delete recovery.  Possibly wrong password for one or more keys, or no keys found'))
            } else {
              yell('error', r.error_desc.trim() === 'Bad password' ? gt('Bad Password') : r.error)
            }
            def.reject()
            return
          }
          yell('success', gt('Recovery deleted'))
          guardModel().set('recoveryAvail', false)
          def.resolve()
        })
        .fail(function (r) {
          yell('error', r.responseText.trim() === 'Bad password' ? gt('Bad Password') : r.responseText)
          def.reject()
        })
    })
    .fail(function () {
      def.reject()
    })
  return def
}
