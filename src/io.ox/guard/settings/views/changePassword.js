/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ox from '$/ox'
import guardModel from '@/io.ox/guard/core/guardModel'
import { PasswordView } from '@/io.ox/guard/core/passwordView'
import authAPI from '@/io.ox/guard/api/auth'
import loginAPI from '@/io.ox/guard/api/login'
import yell from '$/io.ox/core/yell'
import keyCreator from '@/io.ox/guard/core/createKeys'
import auth_core from '@/io.ox/guard/auth'

import gt from 'gettext'

export function renderChangePassword (type) {
  const node = $('<div class="settingsPasswordChange">')
  const currentPassword = new PasswordView({ id: 'oldpass' + type, class: 'password_prompt' })
  const newPassword = new PasswordView({ id: 'newpass1' + type, class: 'password_prompt', validate: true })
  const verifyPassword = new PasswordView({ id: 'newpass2' + type, class: 'password_prompt' })
  const hint = $('<div>')

  if (type === 'pgp') {
    if (guardModel().get('pin')) {
      node.append(
        $('<p>').text(gt('The sender assigned an additional PIN to the account.  This PIN must be entered before you can use this account.  Please enter it now.  Contact the sender directly if you don\'t yet have a pin.')),
        $('<label for="pinInput">').text(gt('PIN')),
        $('<input id="pinInput" name="pinInput" class="form-control password_prompt col-md-6">')
      )
    }
  }

  if (type !== 'pgp' || !guardModel().get('new')) {
    node.append(
      $('<label for="oldpass' + type + '">').text(gt('Old password')),
      currentPassword.render().$el
    )
  }

  node.append(
    $('<label for="newpass1' + type + '">').text(gt('New password')),
    newPassword.render().$el,
    hint
  )

  node.append(
    $('<label for="newpass2' + type + '">').text(gt('Confirm new password')),
    verifyPassword.render().$el
  )

  node.append(
    $('<p>').append(gt('If you have more than one key, this will change the password for the one marked as current')))

  const button = $('<button class="btn btn-default">').append(gt('Change Password'))
  button.on('click', function () {
    changePassword(newPassword, verifyPassword, currentPassword, type)
  })
  node.append($('<div>').append(
    button
  ))

  ox.on('guard:change:password', function (message) {
    newPassword.$el.trigger('invalid', message)
  })
  ox.on('guard:auth:fail:password', function (message) {
    currentPassword.$el.trigger('invalid', message)
  })
  return node
}

export async function changePassword (newPassword, verifyPassword, currentPassword, type) {
  const newpass = newPassword.getValue()
  const newpass2 = verifyPassword.getValue()
  const oldpass = currentPassword.getValue()
  const def = $.Deferred()

  newPassword.model.validate()
  if (newPassword.model.errors.hasErrors()) {
    def.reject()
    return def
  }
  if (newpass !== newpass2) {
    ox.trigger('guard:change:password', gt('Passwords not equal'))
    def.reject()
    return def
  }

  const userdata = {
    newpass,
    oldpass,
    sessionID: ox.session,
    cid: ox.context_id,
    userEmail: ox.user,
    pin: guardModel().get('pin') ? $('#pinInput') : ''
  }
  $('.modal-footer').append(keyCreator.waitDiv().show())
  loginAPI.changePassword(userdata, type, newPassword)
    .done(function (resp) {
      let data = resp.data ? resp.data : resp
      $('.og_wait').remove()
      if (typeof data === 'string') data = $.parseJSON(data)
      if (data.auth.length > 20) {
        $('.settingsPasswordChange input').val('')
        newPassword.$el.trigger('valid')
        guardModel().clearAuth()
        guardModel().set('pin', false)
        guardModel().set('recoveryAvail', data.recovery)
        guardModel().set('new', false)
        yell('success', gt('Password changed successfully'))
        // We want to remove remembered auth codes, as the password has changed
        authAPI.resetToken().always(function () {
          def.resolve()
        })
        return
      }
      auth_core.handleFail(data.auth)
      def.reject()
    })
    .fail(function (e) {
      $('.og_wait').remove()
      let error = gt('Failed to change password')
      if (e.code === 'GRD-AUTH-0002') error = gt('Bad password')
      if (e.error) error = e.error
      yell('error', error)
      def.reject()
    })
  return def
}
