/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ModalDialog from '$/io.ox/backbone/views/modal'
import guardModel from '@/io.ox/guard/core/guardModel'
import yell from '$/io.ox/core/yell'
import ext from '$/io.ox/core/extensions'
import '@/io.ox/guard/oxguard_core'
import gt from 'gettext'
import Backbone from '$/backbone'
import { smimeUpload } from '@/io.ox/guard/settings/smime/smimeUpload'
import keyCreator from '@/io.ox/guard/core/createKeys'
import uploader from '@/io.ox/guard/pgp/uploadkeys'
import { refresh } from '@/io.ox/guard/pgp/userKeys'

let dialog

function openModalDialog (smime) {
  dialog = new ModalDialog({
    async: true,
    point: 'io.ox/guard/settings/uploadPrivate',
    title: gt('Adding Keys'),
    model: new Backbone.Model({ smime })
  })
    .inject({
    })
    .build(function () {
    })
    .addCancelButton()
    .open()
  return dialog
}

ext.point('io.ox/guard/settings/uploadPrivate').extend(
  {
    index: 100,
    id: 'uploadPrivate',
    render (baton) {
      this.$body.append(
        $('<p>').text(gt('If you already have a private key, you can upload it here.')),
        $('<button type="button" class="btn btn-primary upload-view">')
          .text(gt('Upload private key'))
          .on('click', () => {
            if (baton.model.get('smime')) return smimeUpload()
            openUploadPrivateDialog(true)
          }))
    }
  },
  {
    index: 200,
    id: 'uploadPublic',
    render (baton) {
      if (baton.model.get('smime')) return
      this.$body.append(
        $('<p>').text(gt('If you would like to upload a public key only, then do so here.  This will allow you to encrypt files and receive encrypted emails using %s, but you will need to use the private key with another program to decode the items.', guardModel().getName())),
        $('<button type="button" class="btn btn-primary upload-view">')
          .text(gt('Upload public key only'))
          .on('click', function () {
            openUploadPrivateDialog(false)
          }))
    }
  },
  {
    index: 300,
    id: 'createNewKeys',
    render (baton) {
      if (baton.model.get('smime')) return
      const label = $('<p>').text(gt('Finally, you can have a new PGP Key Pair created for your account.'))
      const dialog = this
      const button = $('<button type="button" class="btn btn-primary">').text(gt('Create new keys'))
        .on('click', () => {
          createNewPGP()
          dialog.close()
        })
      this.$body.append(label, button)
    }
  },
  {
    index: 400,
    id: 'fileInputArea',
    render () { // file input area, hidden until button pressed
      this.$body.append($('<input type="file" id="privateKeyFileInput" style="display:none;">'))
    }
  }

)

// Opens the file input and prepares for uploading key
// priv is true if includes private key (will require password prompt, etc)
export function openUploadPrivateDialog (priv) {
  const fileinput = $('<input type="file" id="privateKeyFileInput" style="display:none;">')
  fileinput.unbind('change')
  fileinput.on('change', function () {
    const files = this.files
    if (files.length > 0) {
      if (priv) {
        uploader.uploadPrivate(files)
          .done(function () {
            guardModel().clearAuth()
            refresh()
          })
          .fail(() => { if (dialog) dialog.idle() })
          .always(() => fileinput.remove())
      } else {
        uploader.upload(files)
          .done(function () {
            refresh()
          })
          .fail(() => { if (dialog) dialog.idle() })
          .always(() => fileinput.remove())
      }
    }
  })
  $('body').append(fileinput)
  fileinput.click()
}

export function createNewPGP () {
  keyCreator.createKeys()
    .fail(function (e) {
      if (e === 'cancel') return
      yell('error', e.responseText)
    })
}

export default {
  open: openModalDialog
}
