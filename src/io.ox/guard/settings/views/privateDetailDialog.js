/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import Moment from '$/moment'
import _ from '$/underscore'
import Backbone from '$/backbone'
import keysAPI from '@/io.ox/guard/api/keys'
import ModalDialog from '$/io.ox/backbone/views/modal'
import '@/io.ox/guard/pgp/style.scss'
import gt from 'gettext'
import signatureView from '@/io.ox/guard/pgp/signatureView'
import { downloadPrivateDialog } from '@/io.ox/guard/settings/views/downloadPrivateDialog'
import { createIcon } from '$/io.ox/core/components'

export function privateDetailDialog (data) {
  const dialog = new ModalDialog({
    async: true,
    point: 'io.ox/guard/settings/privateDetailView',
    title: gt('Key Details'),
    id: 'privKeyDetail',
    width: 640,
    model: keysAPI.pool.get('key').get(data.id) || new Backbone.Model(data)
  })
    .inject({
      showSignatures () {
        // Pull list of signatures
        const keyId = this.model.get('id')
        signatureView.open(keyId)
      },
      getInfo (opt) {
        return $('<div class="info-line stripes-red">').append(
          opt.icon ? createIcon(`bi/${opt.icon}.svg`) : $(),
          $('<span class="text">').append(
            $.txt(opt.text)
          )
        )
      }
    })
    .extend({
      classnames () {
        this.$body
          // allow mouse selection
          .addClass('selectable-text')
          // add flags (classnames)
          .toggleClass('revoked', this.model.get('revoked'))
          .toggleClass('expired', this.model.get('expired'))
      },
      'info-line' () {
        this.$body.append(
          $('<section class="info">').append(
            this.model.get('revoked') ? this.getInfo({ text: gt('Revoked') }) : $(),
            this.model.get('expired') ? this.getInfo({ text: gt('Expired') }) : $()
          )
        )
      },
      detail () {
        const model = this.model
        const expires = model.get('_expires') ? new Moment(model.get('_expires')) : undefined
        this.$body.append(
          $('<section class="details">').append(
            $('<dl>').append(
              // short id
              $('<dt>').text(gt('ID')),
              $('<dd>').text(this.model.get('_short')),
              // full fingerprint
              $('<dt>').text(gt('Fingerprint')),
              $('<dd>').text(this.model.get('fingerPrint')),
              // expires
              $('<dt data-dt="expires">').text(data.expired ? gt('Expired') : gt('Expires')),
              $('<dd data-dd="expires">').text(expires ? expires.format('ll') + ' (' + expires.fromNow() + ')' : '-----'),
              // created
              $('<dt>').text(gt('Created')),
              $('<dd>').text(new Moment(this.model.get('creationTime')).format('ll')),
              // has private key
              $('<dt>').text(gt('Private Key')),
              $('<dd>').text(this.model.get('hasPrivateKey') ? gt('yes') : gt('no')),
              // is primary key
              $('<dt>').text(gt('Primary Key')),
              $('<dd>').text(this.model.get('masterKey') ? gt('yes') : gt('no')),
              // used for encryption
              $('<dt>').text(gt('Encryption')),
              $('<dd>').text(this.model.get('encryptionKey') ? gt('yes') : gt('no')),
              // ids
              $('<dt>').text(gt('IDs')),
              _.map(this.model.get('userIds'), function (user) {
                return $('<dd>').text(user)
              })
            )
          )
        )
      }
    })
    .addCloseButton()

  if (data.masterKey) {
    dialog.addAlternativeButton({ label: gt('Download'), action: 'download' })
      .on('download', function () {
        downloadPrivateDialog(this.model.get('id'))
        this.idle()
      })
  }
  dialog.addAlternativeButton({ label: gt('Signatures'), action: 'signatures' })
    .on('signatures', function () {
      this.showSignatures()
      this.idle()
    })

  return dialog.open()
}
