/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ModalDialog from '$/io.ox/backbone/views/modal'
import gt from 'gettext'
import publicKeys from '@/io.ox/guard/pgp/keyDetails'

export function autoCryptKeysDialog () {
  return new ModalDialog({
    async: true,
    title: gt('Autocrypt Keys'),
    id: 'autoCryptList',
    width: 560
  })
    .build(function () {
      this.$header.append(
        $('<p>').text(gt('These keys have been collected from email headers.  Keys are used for encryption, but not signatures verification until the key is verified by you.'))
      )
      this.$el.addClass('maxheight')
      this.$body.append(
        publicKeys.listPublic({
          id: 'autoCrypt',
          title: gt('Collected keys'),
          minimal: true
        }).container
      ).addClass('keyTableList')
    })
    .addCloseButton()
    .open()
}
