/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import Backbone from '$/backbone'
import ModalDialog from '$/io.ox/backbone/views/modal'
import { compactSelect } from '$/io.ox/core/settings/util'
import keysAPI from '@/io.ox/guard/api/keys'
import { PasswordView } from '@/io.ox/guard/core/passwordView'
import yell from '$/io.ox/core/yell'
import guardModel from '@/io.ox/guard/core/guardModel'
import gt from 'gettext'

export function revokePrivateDialog (keyId) {
  return new ModalDialog({
    async: true,
    point: 'io.ox/guard/settings/revokePrivate',
    title: gt('Revoke Private Key'),
    model: new Backbone.Model({ revokereason: 'NO_REASON' }),
    enter: 'cancel',
    focus: '#revokepass'
  })
    .build(function () {
      this.$body.append(
        $('<p>').text(gt('This will advise people not to use this key. You can still use the key to decode existing items.')),
        $('<label for="revokepass">').text(
          gt('Your %s password', guardModel().getName())
        ),
        new PasswordView({ id: 'revokepass' }).render().$el,
        compactSelect('revokereason', gt('Revocation reason'), this.model, [
          { label: gt('No Reason'), value: 'NO_REASON' },
          { label: gt('Key superseded'), value: 'KEY_SUPERSEDED' },
          { label: gt('Key compromised'), value: 'KEY_COMPROMISED' },
          { label: gt('Key retired'), value: 'KEY_RETIRED' },
          { label: gt('User no longer valid'), value: 'USER_NO_LONGER_VALID' }
        ])
      )
    })
    .addButton({ label: gt('Revoke'), action: 'revoke', className: 'btn-default' })
    .on('revoke', function () {
      const password = $('#revokepass').val()

      if (password.length <= 1) {
        this.idle()
        return yell('info', gt('Please enter your password'))
      }

      return keysAPI.revoke(keyId, { password, reason: this.model.get('revokereason') })
        .then(() => { this.close() }, () => { this.idle() })
    })
    .addCloseButton()
    .open()
}
