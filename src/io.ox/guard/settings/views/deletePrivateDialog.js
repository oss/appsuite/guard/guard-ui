/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import guardModel from '@/io.ox/guard/core/guardModel'
import ModalDialog from '$/io.ox/backbone/views/modal'
import gt from 'gettext'
import { revokePrivateDialog } from '@/io.ox/guard/settings/views/revokePrivateDialog'
import yell from '$/io.ox/core/yell'
import '@/io.ox/guard/oxguard_core'
import { PasswordView } from '@/io.ox/guard/core/passwordView'
import keysAPI from '@/io.ox/guard/api/keys'

export function deletePrivateDialog (keyid, isPrimary) {
  const allowDelete = !guardModel().getSettings().noDeletePrivate && isPrimary

  return new ModalDialog({
    async: true,
    point: 'io.ox/guard/settings/deletePrivate',
    id: 'delete-private-key',
    title: allowDelete ? gt('Delete Private Key') : gt('Revoke Private Key'),
    keyid
  })
    .extend({
      options () {
        if (!allowDelete) return
        this.$body.append(
          $('<p>').text(gt('There are two options if you no longer want to use this key.'))
        )
      },
      revoke () {
        this.$body.append(
          allowDelete ? $('<h2>').text(gt('Revoke the key')) : $(),
          $('<p>').text(gt('This advises others that you no longer want this key used. You can continue to decode any encrypted data using the key.')),
          $('<em>').text(gt('Recommended'))
        )
        const dialog = this
        this.addButton({ label: gt('Revoke'), action: 'revoke', className: 'btn-default' })
          .on('revoke', function () {
            revokePrivateDialog(this.options.keyid)
            dialog.close()
          })
      },
      delete () {
        if (!allowDelete) return

        this.$body.append(
          $('<h2>').text(gt('Deleting the key')),
          $('<p>').text(gt('Deleting the private key will render all encrypted items unreadable. You will not be able to decode emails or files that were encrypted with this key. This will delete this key and any sub-keys. This cannot be undone.'))
        )
        const dialog = this
        this.addAlternativeButton({ label: gt('Delete'), action: 'delete' })
          .on('delete', function () {
            deletePrivateConfirmationDialog(this.options.keyid)
            dialog.close()
          })
      },
      close () {
        this.addCloseButton()
      }
    })
    .open()
}

function deletePrivateConfirmationDialog (keyId) {
  return new ModalDialog({
    async: true,
    point: 'io.ox/guard/settings/deletePrivateStep2',
    title: gt('Delete Private Key'),
    focus: '#deletepass'
  })
    .extend({
      warning () {
        this.$body.append(
          $('<p class="bg-danger bg-padding">').append(
            gt('You will no longer be able to decode any items that were encrypted with this key!')
          )
        )
      },
      password () {
        this.$body.append(
          $('<label for="deletepass">').text(
            gt('Your %s password', guardModel().getName())
          ),
          new PasswordView({ id: 'deletepass' }).render().$el
        )
      }
    })
    .addButton({ label: gt('Delete'), action: 'delete', className: 'btn-default' })
    .on('delete', function () {
      const dialog = this
      const password = $('#deletepass').val()
      const invalid = password.length <= 1

      if (invalid) {
        this.idle()
        return yell('info', gt('Please enter your password'))
      }

      return keysAPI.delete(keyId, { password }).then(
        () => { dialog.close() },
        () => { dialog.idle() }
      )
    })
    .addCloseButton()
    .open()
}
