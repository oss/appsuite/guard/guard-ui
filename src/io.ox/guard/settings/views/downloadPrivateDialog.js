/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ox from '$/ox'
import Backbone from '$/backbone'
import ModalDialog from '$/io.ox/backbone/views/modal'
import yell from '$/io.ox/core/yell'
import core from '@/io.ox/guard/oxguard_core'
import keysAPI from '@/io.ox/guard/api/keys'
import gt from 'gettext'

export function downloadPrivateDialog (keyid) {
  return new ModalDialog({
    async: true,
    point: 'io.ox/guard/settings/downloadPrivate',
    title: gt('Download Key'),
    width: 450,
    model: keysAPI.pool.get('key').get(keyid) || new Backbone.Model({ id: keyid })
  })
    .inject({
      download (e) {
        const params = {
          keyid: this.model.get('id'),
          keyType: $(e.target).attr('data-keytype')
        }

        if (params.keyType === 'public') return keysAPI.downloadAsFile(params)

        // private, public_private
        return core.getPassword(undefined, false).then(data => {
          core.verifyPassword(data.password, params.keyid).then(() => {
            ox.trigger('guard:auth:success')
            keysAPI.downloadAsFile(params, { password: data.password })
          }, () => {
            ox.trigger('guard:auth:yellandclose')
          })
        }).done(() => {
          if (!this.disposed) this.close()
        }).fail(function (error) {
          if (error !== 'cancel') yell('error', error)
        })
      }
    })
    .build(function () {
      this.$body.on('click', 'button[data-keytype]', this.download.bind(this))
      const short = this.model.get('_short')
      if (!short) return
      this.$('.modal-title').append(
        $('<span>').text(': ' + short)
      )
      this.$body.append(
        $('<p>').text(gt('Download your private key for use with other email programs. Do not distribute this key. It is for your use only.')),
        $('<button type="button" class="btn btn-primary upload-view" data-keytype="private">').text(gt('Download PGP Private Key')),
        $('<button type="button" class="btn btn-primary upload-view" data-keytype="public_private">').text(gt('Download Public and Private Key')),
        $('<p>').text(gt('Download your public key to share with others.')),
        $('<button type="button" class="btn btn-primary upload-view" data-keytype="public">').text(gt('Download PGP Public Key'))
      )
    })
    .addCloseButton()
    .open()
}
