/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import _ from '$/underscore'
import ox from '$/ox'
import ModalDialog from '$/io.ox/backbone/views/modal'
import ext from '$/io.ox/core/extensions'
import guardModel from '@/io.ox/guard/core/guardModel'
import { PasswordView } from '@/io.ox/guard/core/passwordView'
import authAPI from '@/io.ox/guard/api/auth'
import loginAPI from '@/io.ox/guard/api/login'
import Backbone from '$/backbone'
import yell from '$/io.ox/core/yell'
import keyCreator from '@/io.ox/guard/core/createKeys'
import auth_core from '@/io.ox/guard/auth'

import gt from 'gettext'

export function changePasswordDialog (baton, type) {
  return new ModalDialog({
    async: true,
    focus: 'input[name="active"]',
    point: 'io.ox/guard/settings/passwords',
    title: gt('Change %s Security Password', guardModel().getName()),
    model: new Backbone.Model({ type })
  })
    .inject({
      doChange () {
        return changePass($('#newpass1'), $('#newpass2'), $('#oldpass'), this.model.get('type'))
      }
    })
    .addCancelButton()
    .addButton({ label: gt('Change'), action: 'change' })
    .on('change', function () {
      this.doChange().then(() => {
        this.close()
        ext.point('io.ox/guard/settings/detail').invoke('draw', $('.guard-settings'), baton) // redraw settings
      }, () => {
        this.idle()
      })
    })
    .open()
}

ext.point('io.ox/guard/settings/passwords').extend(
  {
    index: 100,
    id: 'expl',
    render () {
      const node = $('<div>')
      const currentPassword = new PasswordView({ id: 'oldpass', class: 'password_prompt', label: gt('Old password') })
      const newPassword = new PasswordView({ id: 'newpass1', class: 'password_prompt', label: gt('New password'), validate: true })
      const verifyPassword = new PasswordView({ id: 'newpass2', class: 'password_prompt', label: gt('Confirm new password') })
      const hint = $('<div>')

      if (guardModel().get('pin')) {
        node.append(
          $('<p>').text(gt('The sender assigned an additional PIN to the account.  This PIN must be entered before you can use this account.  Please enter it now.  Contact the sender directly if you don\'t yet have a pin.')),
          $('<label for="pinInput">').text(gt('PIN')),
          $('<input id="pinInput" name="pinInput" class="form-control password_prompt">')
        )
      }

      if (!guardModel().get('new')) {
        node.append(
          currentPassword.render().$el
        )
      }

      node.append(
        newPassword.render().$el,
        hint,
        verifyPassword.render().$el
      )

      this.$body.append(
        $('<p>').append(gt('If you have more than one key, this will change the password for the one marked as current')),
        node
      )
    }
  }
)

async function changePass (newpass, newpass2, oldpass, type) {
  const def = $.Deferred()
  const min = guardModel().getSettings().min_password_length
  if (min !== undefined) {
    if (newpass.val().length < min) {
      yell('error', gt('New password must be at least %s characters long', min))
      if (!_.device('ios')) newpass.focus()
      def.reject()
      return def
    }
  }
  if (newpass.val() !== newpass2.val()) {
    newpass2.css('background-color', 'salmon')
    def.reject()
    return def
  }
  newpass2.css('background-color', 'white')
  const userdata = {
    newpass: newpass.val(),
    oldpass: oldpass.val(),
    sessionID: ox.session,
    cid: ox.context_id,
    userEmail: ox.user,
    pin: guardModel().get('pin') ? $('#pinInput').val() : ''
  }
  $('.modal-footer').append(keyCreator.waitDiv().show())
  loginAPI.changePassword(userdata, type)
    .done(function (resp) {
      let data = resp.data ? resp.data : resp
      $('.og_wait').remove()
      if (typeof data === 'string') data = $.parseJSON(data)
      if (data.auth.length > 20) {
        $('input[name="newpass1"]').val('')
        $('input[name="newpass2"]').val('')
        $('input[name="oldpass"]').val('')
        guardModel().clearAuth()
        guardModel().set('pin', false)
        guardModel().set('recoveryAvail', data.recovery)
        guardModel().set('new', false)
        yell('success', gt('Password changed successfully'))
        // We want to remove remembered auth codes, as the password has changed
        authAPI.resetToken().always(function () {
          def.resolve()
        })
      }
      auth_core.handleFail(data.auth)
      def.reject()
    })
    .fail(function (e) {
      $('.og_wait').remove()
      let error = gt('Failed to change password')
      if (e.code === 'GRD-AUTH-0002') error = gt('Bad password')
      if (e.error) error = e.error
      yell('error', error)
      def.reject()
    })
  return def
}
