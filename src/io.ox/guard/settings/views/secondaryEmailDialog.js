/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import _ from '$/underscore'
import ox from '$/ox'
import ModalDialog from '$/io.ox/backbone/views/modal'
import og_http from '@/io.ox/guard/core/og_http'
import { PasswordView } from '@/io.ox/guard/core/passwordView'
import verify from '@/io.ox/guard/core/emailverify'
import guardModel from '@/io.ox/guard/core/guardModel'
import yell from '$/io.ox/core/yell'
import ext from '$/io.ox/core/extensions'
import Backbone from '$/backbone'
import gt from 'gettext'
import accountAPI from '$/io.ox/core/api/account'

function getSecondary (type) {
  const def = $.Deferred()
  og_http.get(
    ox.apiRoot + '/oxguard/login?action=secondary',
    '&userid=' + ox.user_id + '&cid=' + ox.context_id + '&type=' + type
  )
    .done(function (d) {
      d = d.trim()
      if (d.length < 2) {
        accountAPI.getPrimaryAddress().then(addr => {
          if (addr.length < 2) def.reject()
          else def.resolve(addr[1])
        })
      } else {
        def.resolve(d)
      }
    })
    .fail(() => def.reject())
  return def
}

function openModalDialog (secondary, type) {
  return new ModalDialog({
    async: true,
    focus: 'input[name="active"]',
    point: 'io.ox/guard/settings/secondary',
    title: gt('Secondary Email Address'),
    model: new Backbone.Model({ type })
  })
    .inject({
      doChange () {
        const email = $('input[name="newemail"]')
        const verify = $('input[name="newemailverify"]')
        const password = $('#ogpass')
        return changeSecondary(email, verify, password, this.model.get('type'))
      },
      updateEmail () {
        this.$body.find('#secondaryEmail').text(secondary)
      }
    })
    .build(function () {

    })
    .addCancelButton()
    .addButton({ label: gt('Change email address'), action: 'change' })
    .on('change', function () {
      this.doChange().done(this.close).fail(this.idle).fail(yell)
    })
    .on('open', function () {
      this.updateEmail(this.model.get('type'))
    })
    .open()
}

ext.point('io.ox/guard/settings/secondary').extend(
  {
    index: 100,
    id: 'switch',
    render () {
      this.$body.append(
        createSecondaryChange()
      )
    }
  }
)

function createSecondaryChange () {
  // Don't display secondary email if no recovery is set
  if (guardModel().getSettings().noRecovery === true || guardModel().get('recoveryAvail') === false) return
  const emailHeader = $('<div class="oxguard_settings"/>')
    .append(
      $('<p>').text(gt('Change email address used for password reset.')),
      $('<p>').append(gt('Current email address:'), $('<span id="secondaryEmail" style="padding-left:20px;">'))
    )
  const currentInput = new PasswordView({ id: 'ogpass', class: 'password_prompt' }).render().$el
  const emailAddr = $('<input name="newemail" id="newemail" class="form-control mb-16"/>')
  const verifyPass = $('<input name="newemailverify" id="newemailverify" class="form-control mb-16">')
  const hint = $('<td>')
  const emailTable = $('<div>')
    .append(
      $('<label for="ogpass">').append(gt('%s security password', guardModel().getName())),
      currentInput,
      $('<label for="newemail">').append(gt('New secondary email address')),
      emailAddr,
      $('<label for="newemailverify">').append(gt('Confirm secondary email address')),
      verifyPass)

  const errorDiv = $('<div id="newemailerror" class="alert alert-info" style="display:none"/>')
  verify.setValidate(emailAddr, hint)
  verify.autoCompare(emailAddr, verifyPass, hint)
  return (emailHeader.append(emailTable).append(errorDiv))
}

function changeSecondary (emailInput, verifyEmailInput, passwordInput, type) {
  const def = $.Deferred()
  const email = emailInput.val()
  const verifyEmail = verifyEmailInput.val()
  if (email !== verifyEmail) {
    yell('error', gt('Emails not equal'))
    def.reject()
    return def
  }
  if (!verify.validate(email)) {
    yell('error', gt('Invalid email address'))
    def.reject()
    return def
  }
  const data = {
    userid: ox.user_id,
    cid: ox.context_id,
    email,
    password: passwordInput.val()
  }
  og_http.post(ox.apiRoot + '/oxguard/login?action=changesecondary', '&type=' + type, data)
    .done(function () {
      if (data.error) {
        yell('error', data.error)
        def.reject()
        return
      }
      yell('success', gt('Successfully changed email address'))
      $('input[name="ogpass"]').val('')
      $('input[name="newemail"]').val('')
      $('input[name="newemailverify"]').val('')
      def.resolve()
    })
    .fail(function (d) {
      if (d.responseText.trim() === 'Bad password') {
        yell('error', gt('Bad Password'))
        if (!_.device('ios')) {
          passwordInput.focus()
        }
      }
      def.reject()
    })

  return def
}

export function secondaryEmailDialog (type) {
  return getSecondary(type).then(function (secondary) {
    openModalDialog(secondary, type)
  }, function fail (e) {
    yell('error', e.code === 'GUARD_FAILURE'
      ? gt('Unable to retrieve email address for password reset')
      : gt('Please retry later.')
    )
    throw e
  })
}
