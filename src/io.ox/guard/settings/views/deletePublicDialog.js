/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ModalDialog from '$/io.ox/backbone/views/modal'
import yell from '$/io.ox/core/yell'
import '@/io.ox/guard/oxguard_core'
import keysAPI from '@/io.ox/guard/api/keys'
import gt from 'gettext'

export function deletePublicDialog (keyId) {
  return new ModalDialog({
    async: true,
    point: 'io.ox/guard/settings/deletePublic',
    title: gt('Delete Public Key'),
    enter: 'cancel'
  })
    .build(function () {
      this.$body.append(
        $('<p>').text(gt('Please verify you want to delete this key'))
      )
    })
    .addButton({ label: gt('Delete'), action: 'delete', className: 'btn-default' })
    .addCloseButton()
    .on('delete', function () {
      keysAPI.delete(keyId).done(function () {
        yell('success', gt('Key Deleted'))
        this.close()
      }.bind(this))
    })
    .open()
}
