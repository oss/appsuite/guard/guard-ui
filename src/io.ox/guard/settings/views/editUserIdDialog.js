/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ox from '$/ox'
import Backbone from '$/backbone'
import { input as utilInput } from '$/io.ox/core/settings/util'
import http from '@/io.ox/guard/core/og_http'
import ModalDialog from '$/io.ox/backbone/views/modal'
import yell from '$/io.ox/core/yell'
import core from '@/io.ox/guard/oxguard_core'
import util from '@/io.ox/guard/util'
import { PasswordView } from '@/io.ox/guard/core/passwordView'
import gt from 'gettext'

export function editUserIdDialog (id, callback) {
  return new ModalDialog({
    async: true,
    title: gt('Adding UserID')
  })
    .build(function () {
      const nameInput = utilInput('nameInput', gt('Name'), new Backbone.Model())
      nameInput[1].addClass('mb-16')
      const emailInput = utilInput('emailInput', gt('Email'), new Backbone.Model())
      emailInput[1].addClass('mb-16')
      this.$body.addClass('addUserId').append(
        $('<p>').text(gt('Add a name and email address to this public key.')),
        nameInput,
        emailInput,
        $('<label for="passwordInput" id="passwordLabel">').text(gt('Please enter your key password')),
        new PasswordView({ id: 'passwordInput' }).render().$el
      )
    })
    .addCancelButton()
    .addButton({ label: gt('Add'), action: 'add' })
    .on('add', function () {
      const dialog = this
      editUserId(id)
        .done(function () {
          yell('success', gt('User ID added'))
          dialog.close()
          if (callback) {
            callback()
          }
        })
        .fail(function (e) {
          if (e && e.length > 1) yell('error', e)
          dialog.idle()
        })
    })
    .open()
}

function editUserId (id) {
  const def = $.Deferred()
  const data = {
    password: $('#passwordInput').val(),
    email: $('[name="emailInput"]').val(),
    name: $('[name="nameInput"]').val(),
    keyid: id
  }
  if (data.name.length < 2) {
    def.reject(gt('Invalid Name'))
    return def
  }
  if (!util.validateEmail(data.email)) {
    def.reject(gt('Invalid Email Address'))
    return def
  }
  if (data.password.length < 2) {
    def.reject(gt('Enter password'))
    return def
  }
  http.simplePost(ox.apiRoot + '/oxguard/keys?action=addUserId', '', data)
    .done(function (data) {
      if (core.checkJsonOK(data)) {
        def.resolve(gt('User ID added'))
      } else {
        def.reject()
      }
    })
    .fail(e => { def.reject(gt('Failed to add UserID') + e.responseText) })
  return def
}
