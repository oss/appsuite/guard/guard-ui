/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// Open file in viewer

import ox from '$/ox'
import _ from '$/underscore'
import Viewer from '$/io.ox/core/viewer/main'

function viewFile (baton, action, auth) {
  const file_options = {
    params: {
      cryptoAction: 'Decrypt',
      cryptoAuth: auth,
      session: encodeURIComponent(ox.session)
    }
  }

  baton.array().forEach(file => {
    file.file_options = file_options
    file.source = 'guardDrive'
  })
  const selection = [].concat(baton.data)
  // Check if multiple files for scrolling
  if (baton.all) {
    baton.all.models.forEach(file => {
      if (file.get('filename') && (/(\.pgp)$/i.test(file.get('filename')) || (file.get('source') === 'guard') || (file.get('meta') && (file.get('meta').Encrypted === true)))) { // Make sure OxGuard file
        file.set('file_options', file_options)
        file.set('source', 'guardDrive')
      }
      file.on('change:source', () => { // Make sure source isn't changed in file updates
        file.set({ source: 'guardDrive' }, { silent: true })
      })
    })
  }

  if (baton.isViewer) { // If already in viewer, display the version selected
    if (!baton.viewerEvents) { return }
    baton.viewerEvents.trigger('viewer:display:version', baton.data)
    return
  }
  const viewer = new Viewer()
  if (selection.length > 1) {
    // only show selected files - the first one is automatically selected
    viewer.launch({ files: selection })
  } else {
    viewer.launch({ selection: _(selection).first(), files: (baton.all ? baton.all.models : selection) })
  }
}

export default {
  viewFile
}
