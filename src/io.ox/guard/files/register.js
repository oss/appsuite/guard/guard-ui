/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import ox from '$/ox'
import _ from '$/underscore'
import Backbone from '$/backbone'
import ext from '$/io.ox/core/extensions'
import capabilities from '$/io.ox/core/capabilities'
import folderAPI from '$/io.ox/core/folder/api'
import filesAPI from '$/io.ox/files/api'
import util from '@/io.ox/guard/files/util'
import dropzone from '$/io.ox/core/dropzone'
import auth_core from '@/io.ox/guard/auth'
import guardModel from '@/io.ox/guard/core/guardModel'
import registry from '$/io.ox/core/main/registry'
import gt from 'gettext'
import '@/io.ox/guard/files/style.scss'
import { Action, invoke } from '$/io.ox/backbone/views/actions/util'
import yell from '$/io.ox/core/yell'
import downloader from '@/io.ox/guard/files/downloader'
import ModalDialog from '$/io.ox/backbone/views/modal'
import fileUpload from '$/io.ox/files/upload/main'

// For mobile, add a decrypt button
ext.point('io.ox/files/details').extend({
  index: 500,
  id: 'downloadGuardsmall',
  draw (baton) {
    if (_.device('small')) {
      if (_.device('ios')) return // ios doesn't allow downloading.
      if (baton.data.ogfile === true) {
        const download = $('<a href="#" class="btn btn-default">').text(gt('Download decrypted'))
        download.on('click', function (e) {
          e.preventDefault()
          auth_core.authorize(baton).then(function (data) {
            downloader.viewFile(baton, 'download', data)
          })
        })
        $(this).append(download)
      }
    }
  }
})

// Main toolbar in drive

// Links

ext.point('io.ox/files/toolbar/links').extend({
  id: 'downloadEncrypted',
  index: 550,
  title: gt('Download encrypted'),
  ref: 'oxguard/downloadEncrypted',
  mobile: 'lo',
  section: 'guard'
})

ext.point('io.ox/files/toolbar/links').extend({
  id: 'remencrypt',
  index: 550,
  title: gt('Remove encryption'),
  ref: 'oxguard/remencrypt',
  mobile: 'lo',
  section: 'guard'
})

ext.point('io.ox/files/toolbar/links').extend({
  id: 'encrypt',
  index: 550,
  title: gt('Encrypt'),
  ref: 'oxguard/encrypt',
  mobile: 'lo',
  section: 'guard'
})

ext.point('io.ox/files/toolbar/links').extend({
  id: 'sendcopy',
  index: 1200,
  prio: 'lo',
  mobile: 'lo',
  title: gt('Send by mail'),
  ref: 'oxguard/sendcopy',
  section: 'share'
})

// Enhance the download button to check for Guard files
ext.point('io.ox/files/actions/download').replace('default', function (original) {
  return {
    action: function (baton) {
      if (baton.doNotDecrypt) {
        delete baton.doNotDecrypt // wipe
        original.action(baton)
        return
      }
      const toCheck = baton.models === null ? baton.model : baton.models
      if (toCheck != null && util.encryptedOnly(toCheck) && util.isNotAttachment(toCheck)) {
        const list = baton.array()
        auth_core.authorize(list).then(function (auth) {
          list.map(function (file) {
            file.originalFilename = file.filename
            file.filename = (file.filename ? file.filename.replace('.pgp', '') : '')
            file.params = {
              cryptoAction: 'Decrypt',
              cryptoAuth: auth,
              session: ox.session
            }
            file.cache = false
            return file
          })
        }).done(function () {
          original.action(baton)
        })
        return
      }
      original.action(baton)
    }
  }
}
)

// Mobile links
const MobileFileLink = 'io.ox/files/mobile/toolbar/main/multiselect/links'
ext.point(MobileFileLink).extend({
  id: 'encrypt',
  index: 1000,
  prio: 'hi',
  mobile: 'lo',
  title: gt('Encrypt'),
  ref: 'oxguard/encrypt'
})

ext.point(MobileFileLink).extend({
  id: 'remencrypt',
  index: 1000,
  prio: 'hi',
  mobile: 'lo',
  title: gt('Remove encryption'),
  ref: 'oxguard/remencrypt'
})

ext.point(MobileFileLink).extend({
  id: 'sendcopy',
  index: 1200,
  prio: 'hi',
  mobile: 'lo',
  title: gt('Send by mail'),
  ref: 'oxguard/sendcopy',
  section: 'share'
})

// Context menu

ext.point('io.ox/files/listview/contextmenu').extend({
  id: 'encrypt',
  index: 1450,
  ref: 'oxguard/encrypt',
  section: '25',
  title: gt('Encrypt')
})

ext.point('io.ox/files/listview/contextmenu').extend({
  id: 'downloadEncrypt',
  index: 1449,
  ref: 'oxguard/downloadEncrypted',
  section: '25',
  title: gt('Download encrypted')
})

ext.point('io.ox/files/listview/contextmenu').extend({
  id: 'decrypt',
  index: 1450,
  ref: 'oxguard/remencrypt',
  section: '25',
  title: gt('Remove encryption')
})

// Change send copy to handle Guard emails
ext.point('io.ox/files/actions/send').extend({
  id: 'og_stop_send',
  index: 1,
  matches (e) {
    if (e.proceed) return
    if (e.collection.has('some', 'items') && !_.isEmpty(e.data)) {
      const toCheck = e.models === null ? e.model : e.models
      if (util.hasEncrypted(toCheck)) {
        e.stopPropagation()
        return false
      }
    }
  }
})

// Viewer
// Extension to handle direct link files. Will not be used for opening list of files with selection
ext.point('io.ox/core/viewer/main').extend({
  id: 'guardAuthCheck',
  index: 10,
  perform (baton) {
    const def = $.Deferred()
    if (baton.data.selection) {
      return def.resolve() // Not handling here
    }
    const files = baton.data.fileList
    if (!files || files.length === 0) {
      return def.resolve()
    }
    const file = (files[0] instanceof filesAPI.Model) ? files[0].toJSON() : files[0]
    if (isOGFile(file)) {
      auth_core.authorize(file, undefined, true)
        .then(function (auth) {
          const file_options = {
            params: {
              cryptoAction: 'Decrypt',
              cryptoAuth: auth,
              session: ox.session
            }
          }
          // fix for #58378
          for (let i = 0; i < baton.data.fileList.length; i++) {
            // fix for #58617
            if (baton.data.fileList[i] instanceof Backbone.Model) {
              baton.data.fileList[i].set('file_options', file_options)
              baton.data.fileList[i].set('source', 'guardDrive')
            } else {
              baton.data.fileList[i].file_options = file_options
              baton.data.fileList[i].source = 'guardDrive'
            }
          }

          def.resolve(baton)
        }, def.reject)
      return def
    }
    return def.resolve()
  }
})

// ext.point('io.ox/core/viewer/actions/toolbar/popoutstandalone').extend({
//     id: 'popoutstandalone_guardcheck',
//     index: 1,
//     device: '!smartphone',
//     matches (baton) {
//         var model = baton.model;
//         return model.get('group') !== 'localFile' && !baton.context.standalone && model.isEncrypted();
//     },
//     action (baton) {
//         if (baton.model.isEncrypted()) {
//             invoke('oxguard/popoutstandalone', baton);
//             baton.preventDefault();
//         }
//     }
// });

ext.point('io.ox/core/viewer/toolbar/links/drive').extend({
  id: 'GuardViewerEncrypt',
  index: 200,
  title: gt('Encrypt'),
  ref: 'oxguard/encrypt',
  mobile: 'lo'
})

ext.point('io.ox/core/viewer/toolbar/links/guardDrive').extend({
  id: 'GuardViewerRemEncrypt',
  index: 200,
  title: gt('Remove encryption'),
  ref: 'oxguard/remencrypt',
  prio: 'lo',
  mobile: 'lo'
})

ext.point('io.ox/core/viewer/toolbar/links/guardDrive').extend({
  id: 'GuardViewerDownload',
  index: 201,
  title: gt('Download decrypted'),
  ref: 'oxguard/download',
  section: 'export',
  mobile: 'lo'
})

ext.point('io.ox/core/viewer/toolbar/links/guardDrive').extend({
  id: 'GuardViewerDownloadEncr',
  index: 202,
  title: gt('Download encrypted'),
  ref: 'oxguard/downloadEncrypted',
  section: 'export',
  mobile: 'lo'
})

// download current version
ext.point('io.ox/files/versions/links/inline/current').extend({
  id: 'GuardDownload',
  index: 200,
  title: gt('Download decrypted'),
  ref: 'oxguard/download'
})

// download older versions
ext.point('io.ox/files/versions/links/inline/older').extend({
  id: 'GuardDownload',
  index: 200,
  title: gt('Download decrypted'),
  ref: 'oxguard/download'
})

// Actions

// Upload new encrypted file

function isTrash (baton) {
  let folderId
  if (baton.app) {
    folderId = baton.app.folder.get()
  } else if (baton.folder_id !== undefined) {
    folderId = baton.folder_id
  } else if (baton.data) {
    folderId = baton.data.folder_id
  }
  const model = folderAPI.pool.getModel(folderId)
  return model ? folderAPI.is('trash', model.toJSON()) : false
}

function isCurrentVersion (baton) {
  // folder tree folder, always current version
  if (!baton.collection.has('some')) return true
  // drive folder, always current version
  if (baton.collection.has('folders')) return true
  // single selection
  if (baton.collection.has('one') && baton.first().current_version !== false) return true
  // multi selection
  if (baton.collection.has('multiple') && baton.array().every(function (file) { return file.current_version !== false })) return true
  // default
  return false
}

ext.point('io.ox/secondary').extend({
  id: 'guard-files-upload-file',
  index: 250,
  render (baton) {
    if (!capabilities.has('guard-drive') || capabilities.has('guest')) return
    if (baton.appId !== 'io.ox/files') return
    this.action('io.ox/files/actions/uploadEncrypt', _.device('smartphone') ? gt('File (encrypted)') : gt('Upload file (encrypted)'), baton)
  }
})

// Upload new encrypted file

new Action('io.ox/files/actions/uploadEncrypt', {
  capabilities: 'guard-drive && !guest',
  folder: 'create',
  matches (baton) {
    if (_(['14', '15']).contains(baton.folder_id)) return false
    if (isTrash(baton)) return false
    return true
  },
  action (baton) {
    auth_core.ensureSetup(true).then(function () {
      baton.file_options = { params: { cryptoAction: 'Encrypt' } }
      baton.guardFileOnce = true
      invoke('io.ox/files/actions/upload', baton)
    })
  }
})

// Make sure that the cryptoAction flag is only passed once to the upload, unless user intentionally selects upload encrypted twice
// Bug Guard-269
ext.point('io.ox/files/actions/upload').replace('default', function (original) {
  return {
    action (baton) {
      // If via the uploadEncrypt action, perform with the cryptoActions
      if (baton.guardFileOnce) {
        baton.guardFileOnce = false
        original.action(baton)
        return
      }
      // Wipe the cryptoActions
      baton.file_options = { params: { cryptoAction: '' } }
      original.action(baton)
    }
  }
})

new Action('oxguard/downloadEncrypted', {
  matches (baton) {
    if (baton.first()) {
      if (isOGFile(baton.first())) {
        return (true)
      }
    }
    return false
  },
  action (baton) {
    baton.array().forEach(file => {
      // For encrypted, wipe the cryptoAuth and cryptoAction
      if (file && file.params) {
        file.params = {}
      }
    })
    baton.doNotDecrypt = true // Mark this as do not decrypt so that the download action doesn't prompt
    if (isCurrentVersion(baton)) {
      invoke('io.ox/files/actions/download', baton)
    } else {
      invoke('io.ox/files/actions/downloadversion', baton)
    }
  }
})

// Disable publication of Guard files
ext.point('io.ox/files/actions/getalink').extend({
  index: 10,
  id: 'guardCheck',
  matches (baton) {
    if (isOGFile(baton.first())) {
      baton.stopPropagation()
      return false
    }
  }
})

ext.point('io.ox/files/actions/share').replace('default', function (original) {
  return {
    action (baton) {
      if (isOGFile(baton.first())) {
        const options = {
          optPrompt: gt('Please re-enter your %s password.', guardModel().getName()),
          minSingleUse: true
        }
        auth_core.authorize(baton, options)
          .done(function () {
            original.action(baton)
          })
          .fail(function (e) {
            if (e === 'cancel') return
            yell('error', e)
          })
      } else {
        original.action(baton)
      }
    }
  }
})

// Replacement popout for Guard
// new Action('oxguard/popoutstandalone', {
//     collection: 'one',
//     id: 'guardPopout',
//     matches (baton) {
//         var currentApp = ox.ui.App.getCurrentApp().getName();
//         // detail is the target of popoutstandalone, no support for mail attachments
//         return (currentApp !== 'io.ox/files/detail' && baton.model.isEncrypted());
//     },
//     action (e) {
//         ox.launch('io.ox/files/detail/main').done(function () {
//             var node = this.getWindowNode();
//             this.setTitle(e.data.filename);
//             var app = this;
//             ox.load(['io.ox/mail/actions/viewer']).done(function (action) {
//                 action({ files: [e.data], app: app, container: node, standalone: true });
//             });
//         });
//     }
// });

// View link within versions

new Action('oxguard/versionView', {
  matches (baton) {
    if (!baton.isViewer) { return false }
    return (isOGFile(baton.first()))
  },
  action (baton) {
    viewFile(baton)
  }
})
new Action('oxguard/sendcopy', {
  capabilities: 'guard-drive && webmail',
  collection: 'some && items',
  matches (baton) {
    if (!capabilities.has('webmail')) return false
    if (_.isEmpty(baton.data)) return false
    if (baton.openedBy === 'io.ox/mail/compose') return false
    if (isTrash(baton)) return false
    if (baton.isViewer && !isCurrentVersion(baton)) return false
    if (baton.array().reduce(function (memo, obj) {
      return memo || obj.file_size > 0
    }, false)) {
      const toCheck = baton.models === null ? baton.model : baton.models
      return util.hasEncrypted(toCheck) && util.isNotAttachment(toCheck)
    }
    return false
  },
  action (baton) {
    const list = baton.array().filter(function (obj) { return obj.file_size !== 0 })
    if (list.length === 0) return
    registry.call('io.ox/mail/compose', 'open', {
      attachments: list.map(function (file) {
        return { origin: 'drive', id: file.id, folder_id: file.folder_id, encrypted: true }
      })
    })
  }
})

function okToDecrypt (baton, data) {
  const virtual = _.contains(['14', '15'], data.id)
  return baton.collection.has('some', 'modify', 'items') && util.encryptedOnly(baton.models) &&
    folderAPI.can('create', data) && !virtual && !folderAPI.is('trash', data) &&
    !folderAPI.isExternalFileStorage(data)
}

new Action('oxguard/remencrypt', {
  capabilities: 'guard-drive && !guest',
  collection: '!folders',
  matches (baton) {
    // all files must be encrypted
    if (baton.isViewer && !isCurrentVersion(baton)) return false
    if (!anyOGFiles(baton.data)) return false
    if (baton.app) {
      return baton.app.folder.getData().then(function (data) {
        return okToDecrypt(baton, data)
      })
    }
    const folderId = baton.first().folder_id
    if (folderId) {
      return folderAPI.get(folderId).then(function (data) {
        return okToDecrypt(baton, data)
      })
    }
    return false
  },
  action (baton) {
    auth_core.authorize(baton).then(function (auth) {
      let warnShares = false
      const list = baton.array()
      for (const file in list) {
        if (list[file].object_permissions && list[file].object_permissions.length > 0) {
          warnShares = true
        }
        list[file].file_options = {
          params: {
            cryptoAction: 'Decrypt',
            cryptoAuth: auth
          }
        }
      }
      if (warnShares) {
        new ModalDialog({
          async: true,
          point: 'io.ox/guard/files/shareWarning',
          title: gt('Shares found'),
          id: 'shareWarning',
          width: 400
        })
          .extend({
            explanation () {
              this.$body
                .append($('<p>').text(gt('You are removing encryption from file(s) that have shares. These shares will no longer work once the file is decrypted.  You can add the shares back once the file is decrypted.')))
                .append($('<p>').text(gt('Do you wish to proceed?')))
            }
          })
          .addButton({ label: gt('Remove encryption'), action: 'decrypt' })
          .addCancelButton()
          .on('decrypt', function () {
            this.close()
            doRemEncryptFile(list)
          })
          .open()
      } else {
        doRemEncryptFile(list)
      }
      baton.openedBy = 'io.ox/files'
    })
  }
})

function doRemEncryptFile (list) {
  list.forEach((f) => {
    filesAPI.copy([f], f.folder_id, false)
      .then(function (e) {
        if (e.length === 1) {
          filesAPI.remove([f], true)
        } else if (_.isString(e)) {
          yell('error', e)
        }
      })
  })
}

new Action('oxguard/open', {
  matches (baton) {
    if (baton.data) {
      if (isOGFile(baton.first())) {
        return (true)
      }
    }
    return (false)
  },
  action (baton) {
    auth_core.authorize(baton.data).then(function (auth) {
      const params = {
        cryptoAction: 'Decrypt',
        cryptoAuth: auth,
        session: ox.session
      }
      _(baton.array()).each(function (file) {
        window.open(filesAPI.getUrl(file, 'open', { params }))
      })
    })
  }
})

new Action('oxguard/download', {
  device: '!ios',
  collection: 'some',
  matches (baton) {
    const toCheck = baton.models === null ? baton.model : baton.models
    if (toCheck === undefined) { // Not a model in versions
      if (baton.data) {
        return isOGFile(baton.first())
      }
      return false
    }
    return util.encryptedOnly(toCheck) && util.isNotAttachment(toCheck)
  },
  action (baton) {
    const list = baton.array()
    auth_core.authorize(list).then(function (auth) {
      list.forEach(file => {
        file.originalFilename = file.filename
        file.filename = (file.filename ? file.filename.replace('.pgp', '') : '')
        file.params = {
          cryptoAction: 'Decrypt',
          cryptoAuth: auth,
          session: ox.session
        }
        file.cache = false
      })
      invoke('io.ox/files/actions/downloadversion', baton)
        .then(function () { // Guard-221, subsequent calls to download returns decrypted file
          list.forEach(file => {
            file.filename = file.originalFilename
            file.params.cryptoAction = undefined
            file.params.cryptoAuth = undefined
          })
        })
    })
  }
})

// Main listview handling

// Call for viewer.  Check if Guard file
ext.point('io.ox/files/actions/viewer').extend({
  id: 'guardview',
  index: 1,
  collection: 'some && items',

  matches (baton) {
    // don't open a new viewer instance within the viewer
    if (baton.isViewer) { return false }

    return hasAnyEncryptedFileInSelection(baton)
  },

  action (baton) {
    viewFile(baton)
  }
})

ext.point('io.ox/files/actions/viewer/display-version').replace(('default'), function (original) {
  return {
    action (baton) {
      if (hasAnyEncryptedFileInSelection(baton)) {
        viewFile(baton)
      } else {
        original.action(baton)
      }
    }
  }
})

// fix for #58841 - Viewer: In a mixture of encrypted and unencrypted files, preview fails if started with an unencrypted file.
// approach to fulfill user expectation to not being prompted for a password when viewing a single, unencrypted file.
function hasAnyEncryptedFileInSelection (baton) {
  const selection = baton.array()
  let hasAny = false

  if (selection.length === 1) {
    // portal provides selection only and no baton.all
    hasAny = isOGFile(selection[0])
  } else {
    // check selected files for multi selection
    hasAny = selection.some(isOGFile)
  }

  return hasAny
}

// Handle viewing Guard file
function viewFile (baton) {
  // if (hasAnyEncryptedFile(baton)) {
  if (hasAnyEncryptedFileInSelection(baton)) {
    auth_core.authorize(baton).then(function (auth) {
      downloader.viewFile(baton, 'view', auth)
    })
  }
}

ext.point('io.ox/files/dropzone').extend({
  id: 'dragDrop',
  getDropZones (baton) {
    if (!capabilities.has('guard-drive') || capabilities.has('guest')) return
    const app = baton.app
    const zone2 = new dropzone.Inplace({
      caption: gt('Drop files here to encrypt')
    })
    zone2.on({
      show () {
        app.listView.$el.stop().hide()
      },
      hide () {
        app.listView.$el.fadeIn('fast')
      },
      drop (files) {
        auth_core.ensureSetup(true).then(function () {
          fileUpload.setWindowNode(app.getWindowNode())
          fileUpload.offer(files, { folder: app.folder.get(), params: { cryptoAction: 'Encrypt' } })
        })
      }
    })
    return baton.dropZones.push(zone2)
  }
})

// Encrypting files

function okToEncrypt (baton, data) {
  const virtual = _.contains(['14', '15'], data.id)
  return baton.collection.has('some', 'modify', 'items') && !util.encryptedOnly(baton.models) &&
    folderAPI.can('create', data) && !virtual && !folderAPI.is('trash', data) &&
    !folderAPI.isExternalFileStorage(data)
}

new Action('oxguard/encrypt', {
  capabilities: 'guard-drive && !guest',
  collection: 'some',
  matches (baton) {
    if (baton.isViewer && !isCurrentVersion(baton)) return false
    // all files must be unencrypted
    if (baton.app) {
      return baton.app.folder.getData().then(function (data) {
        if (allOGFiles(baton.data)) return false
        return okToEncrypt(baton, data)
      })
    }
    const folderId = _.isArray(baton.data) ? baton.data[0].folder_id : baton.data.folder_id
    if (folderId) {
      return folderAPI.get(folderId).then(function (data) {
        if (allOGFiles(baton.data)) return false
        return okToEncrypt(baton, data)
      })
    }
    return false
  },
  action (baton) {
    const list = _.isArray(baton.data) ? baton.data : [baton.data]
    auth_core.ensureSetup(true).then(function () {
      let warnShares = false
      for (const file in list) {
        if (list[file].object_permissions && list[file].object_permissions.length > 0) {
          warnShares = true
        }
        list[file].file_options = {
          params: {
            cryptoAction: 'Encrypt'
          }
        }
      }
      if (warnShares) {
        new ModalDialog({
          async: true,
          point: 'io.ox/guard/files/shareWarning',
          title: gt('Shares found'),
          id: 'shareWarning',
          width: 400
        })
          .extend({
            explanation () {
              this.$body
                .append($('<p>').text(gt('There are shares associated with the file(s) you are going to encrypt. These shares will no longer work once the file is encrypted.  You can add the shares back once the file is encrypted.')))
                .append($('<p>').text(gt('Do you wish to proceed?')))
            }
          })
          .addButton({ label: gt('Encrypt'), action: 'encrypt' })
          .addCancelButton()
          .on('encrypt', function () {
            this.close()
            doEncryptFile(list)
          })
          .open()
      } else {
        doEncryptFile(list)
      }
    })
  }
})

function doEncryptFile (list) {
  list.forEach(f => {
    filesAPI.copy([f], f.folder_id, false)
      .then(function (e) {
        if (e.length === 1) {
          filesAPI.remove([f], true)
        } else if (_.isString(e)) {
          yell('error', e)
        }
      })
  })
}

// Util, prompts, etc

// Checks if any of the files are encrypted
function anyOGFiles (_files) {
  const files = _.isArray(_files) ? _files : [_files]
  let any = false
  files.forEach(file => {
    any = any || isOGFile(file)
  })
  return any
}

// Checks if all files are Encrypted
function allOGFiles (_files) {
  const files = _.isArray(_files) ? _files : [_files]
  let all = true
  files.forEach(file => {
    all = all && isOGFile(file)
  })
  return all
}

function isOGFile (file) {
  try {
    if (!file.filename) return (false)
    if (file.meta !== undefined) {
      if (file.meta.Encrypted) return (true)
    }
    if (/(\.pgp)$/i.test(file.filename)) return true
  } catch (e) {
    return (false)
  }
  return (false)
}

export default {
  viewFile
}
