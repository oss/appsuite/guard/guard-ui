/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery'
import _ from '$/underscore'
import ox from '$/ox'
import Tour from '$/io.ox/core/tk/wizard'
import hotspot from '$/io.ox/core/tk/hotspot'
import util from '@/io.ox/guard/util'
import capabilities from '$/io.ox/core/capabilities'
import HelpView from '$/io.ox/backbone/mini-views/helplink'
import keys from '@/io.ox/guard/core/createKeys'
import gt from 'gettext'
import guardModel from '@/io.ox/guard/core/guardModel'
import apps from '$/io.ox/core/api/apps'
import registry from '$/io.ox/core/main/registry'
import core from '@/io.ox/guard/oxguard_core'
import '@/io.ox/guard/tour/style.scss'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import openSettings, { closeSettings } from '$/io.ox/settings/util'
import { openModals } from '$/io.ox/backbone/views/modal'

function writeMail () {
  registry.call('io.ox/mail/compose', 'open')
    .then(function (e) {
      e.app.view.securityModel.set('encrypt', true)
    })
}

function openFiles () {
  ox.launch(() => import('$/io.ox/files/main'))
}

function setEncryptDefault () {
  navTo('view')
  if (!settings.get('defaultEncrypted')) {
    settings.set('defaultEncrypted', true)
  }
}

const helpView = new HelpView({
  base: 'help',
  modal: true,
  content: gt.pgettext('tour', 'Learn more about %s', guardModel().getName())
})

const helpViewLink = helpView.render().$el

Tour.registry.add({
  id: 'default/oxguard',
  priority: 1
}, function () {
  // PGP setup check
  if (!util.isGuardConfigured()) {
    keys.createKeysWizard()
    return
  }
  const def = $.Deferred()
  const tour = new Tour()
  let composeApp
  let driveText = ''
  let resetText = ''
  // Select text depending on if recovery is available
  if (guardModel().get('recoveryAvail')) {
    resetText = gt.pgettext('tour', 'You already set up your %s account. But you can always change your encryption password, or request a temporary password if you forget it.', guardModel().getName())
  } else {
    resetText = gt.pgettext('tour', 'You already set up your %s account.  You can change your encryption password here.', guardModel().getName())
  }
  // minimize all open windows

  apps.models.forEach(function (app) {
    if (app.getWindow() && app.getWindow().floating) {
      app.getWindow().floating.minimize()
    }
  })
  tour.toggleBackdrop(true)
  if (capabilities.has('guard-mail') || capabilities.has('smime')) {
    tour.step({ labelNext: gt('Next') })
      .title(gt.pgettext('tour', 'The %s module', guardModel().getName()))
      .content(gt.pgettext('tour', 'Security and privacy matters to everyone. That\'s why you can now send and receive encrypted emails by simply clicking on the lock icon.'))
      .waitFor('.og-ready', 10)
      .on('wait', function () {
        if (!composeApp) {
          // compose not started, yet
          ox.once('mail:NEW:ready, mail:new:ready', function (mode, app) {
            // if guard password prompt opens close it again
            const openmodels = openModals.queue.find(modal => modal?.point?.id === 'oxguard_core/auth')
            if (openmodels) openmodels.close()
            composeApp = app
            // HACK: can not detect that mail compose window is rendered
            composeApp.view.$el.addClass('og-ready')
            window.setTimeout(function () {
              composeApp.config.set('autoDismiss', true)
              composeApp.view.securityModel.set('tour', true)
              hotspot.add('.toggle-encryption', { top: 14, left: 14 })
              composeApp.view.$el.closest('.io-ox-mail-compose-window').find('a.toggle-encryption .bi-lock').click()
            }, 500)
          })
          registry.call('io.ox/mail/compose', 'open').then(e => {
            composeApp = e.app
            // Assign a CID for the app
            composeApp.cid = 'compose.tour'
          })
        } else {
          ox.ui.App.reuse(composeApp.cid)
        }
      })
      .end()
    tour.once('stop', function () {
      if (composeApp && composeApp.model) {
      // prevent app from asking about changed content
        composeApp.view.dirty(false)
        composeApp.quit()
      }
      def.resolve()
    })
  }

  tour
    .step({ back: false })
    // #. %s product Name
    .title(gt.pgettext('tour', '%s Security Settings', guardModel().getName()))
    .content(resetText)
    .on('wait', function () {
      if (composeApp) {
        composeApp.getWindow().floating.minimize() // Minimize the floating window
      }
      if (core.hasSmimeEnabled()) {
        navTo('smimePassword')
      } else {
        navTo('pgpPassword')
      }
    })
    .waitFor(core.hasSmimeEnabled() ? getWait('smimePassword') : getWait('pgpPassword'))
    .referTo('.password_prompt')
    .spotlight(core.hasSmimeEnabled() ? '[data-section="io.ox/guard/settings/smimePassword"]' : '[data-section="io.ox/guard/settings/pgpPassword"]', { padding: 10 })
    .on('show', function () {
      $(window).trigger('resize.wizard.spotlight')
    })
    .end()
  if (capabilities.has('guard-mail') || capabilities.has('smime')) {
    tour.step()
      .title((settings.get('advanced') ? gt.pgettext('tour', 'PGP Encryption Settings') : gt.pgettext('tour', 'Security Settings')))
      .content((settings.get('advanced')
        ? gt.pgettext('tour', 'From here you can update your encryption settings. For example you can choose to send encrypted emails by default.')
        : gt.pgettext('tour', 'From here, you can update your security settings.  For example, you can choose to send secure emails by default.')))
      .on('wait', function () {
        navTo('view')
      })
      .waitFor(getWait('view'))
      .spotlight('.guardDefaults', { padding: 10 })
      .hotspot('#oxguard-defaultencrypted', { top: 7, left: 7 })
      .end()
      .step()
      .title(gt.pgettext('tour', 'Signature'))
      .content(gt.pgettext('tour', 'In addition to encrypting your emails you can also sign them. This means that email recipients can be sure that the email really came from you. By ticking this checkbox your signature will be added to outgoing emails by default.'))
      .on('wait', function () { // Should already be open, but needed for moving back
        navTo('view')
      })
      .waitFor('.guardDefaults')
      .spotlight('.guardDefaults', { padding: 10 })
      .hotspot('.guardDefaults [name="defaultSign"]', { top: 7, left: 7 })
      .end()
    if (core.hasGuardMail()) {
      tour.step()
        .title(gt.pgettext('tour', 'Advanced Settings'))
      // #. Part of the tour, showing where to manage recipient public keys
        .on('wait', function () {
          navTo('recipients')
        })
        .content(gt.pgettext('tour', 'Here you can manage PGP Public keys for your recipients.'))
        .waitFor(getWait('recipients')) // hack for waiting for scoll to finish
        .spotlight('.pgpPublicKeysList', { padding: 20 })
        .on('show', function () {
          $(window).trigger('resize.wizard.spotlight')
        })
        .end()
        .step()
        .title(gt.pgettext('tour', 'Advanced Settings'))
      // #. Part of the tour, showing where you can manage your private keys
        .content(gt.pgettext('tour', 'Users can manage their private keys here.'))
        .on('wait', function () {
          navTo('pgpkeys')
        })
        .waitFor(getWait('pgpkeys'))
        .spotlight('.userKeyDiv', { padding: 20 })
        .on('show', function () {
          $(window).trigger('resize.wizard.spotlight')
        })
        .end()
    }
  }
  if (capabilities.has('guard-mail') || capabilities.has('smime')) {
    tour
      .step({ back: false })
      .title(gt.pgettext('tour', 'Enabling and disabling encryption'))
      .content($('<div>').append(
        $('<p>').text(gt.pgettext('tour', 'You are now able to send secure emails.')),
        $('<p>').text(gt.pgettext('tour', 'Sending without encryption is easy, too: Simply click on the lock icon again.'))
      ))
      .on('wait', function () {
        closeSettings()
        ox.ui.App.reuse(composeApp.cid)
        composeApp.getWindow().floating.toggle()

        window.setTimeout(function () {
          hotspot.add('.toggle-encryption', { top: 14, left: 7 })
          composeApp.view.securityModel.set('encrypt', false)
        }, 500)
      })
      .hotspot(['#statuslock'])
      .waitFor('a.toggle-encryption')
      .on('next', function () {
        if (composeApp && composeApp.model) {
          // prevent app from asking about changed content
          composeApp.view.dirty(false)
          composeApp.quit()
        }
      })
      .end()
  }
  if (capabilities.has('guard-drive') && capabilities.has('infostore')) {
    tour.step({ back: false })
      .title(gt.pgettext('tour', 'Encrypt files'))
      .content(gt.pgettext('tour', 'You can also protect your files. Just click on Encrypt and the selected file will be stored securely.'))
      .navigateTo(() => import('$/io.ox/files/main'))
      .waitFor('.file-list-view')
      .on('wait', function () {
        closeSettings()
        window.setTimeout(function () {
          const firstFile = $('.file-list-view > .list-item.selectable:not(.file-type-folder):not(.file-type-guard):first')
          if (firstFile.length > 0) {
            firstFile.click()
            window.setTimeout(function () {
              $('.classic-toolbar [data-action=more] ~ ul').css('display', 'block')
              hotspot.add('a[data-action="oxguard/encrypt"]', { top: 13, left: 10 })
            }, 1000)
          } else {
            hotspot.add('.dropdown.more-dropdown', { top: 13, left: 10 })
          }
        }, 500)
      })
      .on('next', function () {
        $('.classic-toolbar [data-action=more] ~ ul').css('display', '')
      })
      .end()
    driveText = $('<p>').append($('<a href="#">').click(openFiles).text(gt.pgettext('tour', 'Encrypt your sensitive files in Drive')))
  }
  if (location.href.indexOf('office?app') === -1) { // Restart Guided tour not available in Office tabs
    tour
      .step()
      .title(gt.pgettext('tour', 'Restart Guided Tour'))
      .content(gt.pgettext('tour', '<em>Hint:</em> you can start guided tours, any time you need them, from the system menu.'))
      .hotspot('[data-name="GuardTour"]', { top: 13, left: 10 })
      .waitFor('a[data-name="GuardTour"]')
      .on('wait', function () {
        $('#io-ox-topbar-help-dropdown-icon').find('.dropdown-toggle').click()
      })
      .on('next', function () {
        if ($('a[data-name="GuardTour"]').css('display') === 'block') {
          $('#io-ox-topbar-help-dropdown-icon').find('.dropdown-toggle').click()
        }
      })
      .end()
  }
  const finished = $('<div>')
  if (capabilities.has('guard-mail') || capabilities.has('smime')) {
    finished.append(
      $('<p>').text(gt.pgettext('tour', 'You successfully learned how to protect your emails and files.')),
      $('<p>').text(gt.pgettext('tour', 'What you can do now:')),
      $('<p>').append($('<a href="#">').on('click', function () {
        writeMail()
        tour.close()
      }).text(gt.pgettext('tour', 'Write an encrypted email'))),
      driveText,
      $('<hr>'),
      $('<p>').append($('<a href="#">').click(setEncryptDefault).text(gt.pgettext('tour', 'Activate encryption for all new emails'))),
      $('<p>').append($('<a href="#">').click(() => navTo('view')).text(gt.pgettext('tour', 'Review your encryption settings'))),
      $('<p class="help">').append(helpViewLink))
  }
  tour
    .step()
    .title(gt.pgettext('tour', 'Guided Tour completed'))
    .content(finished)
    .on('ready', function () {
      helpView.$el.on('click', function () {
        def.resolve()
        tour.close()
      })
    })
    .end()

  // Check setup is done before launching wizard
  util.hasSetupDone().then(() => {
    tour.start()
  }, () => {
    tour.toggleBackdrop(false)
    if (util.hasSmime()) {
      navTo('setupsmime')
    } else {
      navTo('setup') // shouldn't happen if createKeysWizard captured above
    }
  })
  return def
})

Tour.registry.add({
  id: 'default/oxguard/createKeys',
  priority: 1
}, function () {
  const def = $.Deferred()

  const configured = util.isGuardConfiguredAndPGPReady()

  if (_.device('small') || _.device('smartphone')) {
    if (configured) return // no tour for mobile
    return keys.createKeys()
  }

  const tour = new Tour()
  let model = new keys.CreateKeysModel({
    initialSetup: true
  })
  let view = new keys.CreateKeysView({
    model
  })

  tour.once('stop', function () {
    if (!model.get('pending')) { // awaiting some other promise
      if (model.get('sent') === false) {
        def.reject('cancel')
      } else {
        def.resolve('OK')
      }
    }
    view.remove()
    model.off()
    view = model = null
  })
  // Guard is not configured, yet.
  if (!configured) {
    tour.step({ labelNext: gt('Start Setup') })
      .title(gt.pgettext('tour', 'Setup %s', guardModel().getName()))
      .content($('<div>').append(
        guardModel().allExpired()
        // #. Keys are expired.  Wizard opened to create new keys for the user.
          ? $('<p>').text(gt.pgettext('tour', 'Your keys are expired.  The next steps will set up new keys for your account.'))
        // #. %s product name
          : $('<div>').append($('<p>').text(gt.pgettext('tour', 'Welcome to %1$s. You will now be taken on a tour, showing you how to use %1$s.', guardModel().getName())))
            .append($('<p>').text(gt.pgettext('tour', 'Firstly however, you’ll need to setup %1$s by choosing a password which will be used to encrypt or decrypt items such as email or files.', guardModel().getName())))))
      .end()
      .step()
    // #. %s product name
      .title(gt('Choose %s Password', guardModel().getName()))
      .content(view.render().$el)
      .beforeShow(function () {
        const self = this
        this.toggleNext(false)
        model.on('change', function (model) {
          self.toggleNext(model.isValid())
        })
        model.once('send:ok', function () {
          // HACK: no way to pause the wizard until some event
          $('body').addClass('og-ready')
        })
        model.once('send:error', function () {
          tour.close()
        })
        window.setTimeout(function () {
          if (_.device('desktop')) {
            $('#newogpassword').focus()
          } else {
            $('[type="password"]').removeAttr('readonly') // Remove the read only but do not focus.  User needs to click for keyboard
          }
        }, 1000)
      })
      .on('next', function () {
        model.send()
        $('.wizard-container').append(view.$wait)
      })
      .end()
  }

  // If we are already configured, can start with the offer for more tour
  if (configured) {
    $('body').addClass('og-ready')
  }

  tour
    .step({ labelDone: gt('Close') })
  // #. %s product Name
    .title(gt.pgettext('tour', '%s set up completed', guardModel().getName()))
    .content($('<div>').append(
      $('<p>').text(gt.pgettext('tour', 'Congratulations, you have successfully setup %s', guardModel().getName())),
      $('<p>').text(gt.pgettext('tour', 'Now you can learn more about how to use %s by following the links below:', guardModel().getName())),
      $('<p>').append($('<a id="startTourLink" href="#">').on('click', function () {
        model.set('pending', true)
        $('.wizard-step .btn-primary').click()
        Tour.registry.get('default/oxguard').get('run')().then(def.resolve, def.resolve)
        // #. %s product name
      }).text(gt('Guided tour for %s', guardModel().getName()))),
      $('<p>').append(helpViewLink)
    ))
    .mandatory()
    .beforeShow(function () {
      // no going back, from here
      this.toggleBack(false)
    })
    .on('ready', function () {
      // HACK: no way to pause the wizard until some event
      $('body').removeClass('og-ready')
      helpView.$el.on('click', function () {
        def.resolve()
        tour.close()
      })
    })
    .waitFor('.og-ready', 30)
    .end()
    .start()

  return def
})

function navTo (section) {
  openSettings('virtual/settings/io.ox/guard', 'io.ox/guard/settings/' + section)
}

function getWait (section) {
  return '[data-section="io.ox/guard/settings/' + section + '"][open="open"]'
}

export default Tour.registry
